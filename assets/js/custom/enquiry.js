$(document).ready(function () {
    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            navListItems.css('border-color', '#314559').css('background-color', '#34495e');
            $item.css('background-color', '#5cb85c').css('border-color', ' #4cae4c');
            allWells.hide();
            $target.show();
            // $target.find('input:eq(0)').focus();
        }
    });

    // allNextBtn.click(function () {
    //     var curStep = $(this).closest(".setup-content"),
    //         curStepBtn = curStep.attr("id"),
    //         nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
    //         curInputs = curStep.find("input, select"),
    //         isValid = true;
    //
    //     // Validation Part
    //     $("div input").removeClass("has-error");
    //     $("div select").removeClass("has-error");
    //     for (var i = 0; i < curInputs.length; i++) {
    //         if (curInputs[i].tagName == 'INPUT' && !curInputs[i].validity.valid) {
    //             isValid = false;
    //             $(curInputs[i]).closest("div").addClass("has-error");
    //         } else if (curInputs[i].tagName == 'SELECT' && curInputs[i].value == '0') {
    //             isValid = false;
    //             $(curInputs[i]).closest("div").addClass("has-error");
    //         }
    //     }
    //
    //     if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    // });

    $('div.setup-panel div a.btn-success').trigger('click');

    $('#end_date').datetimepicker();
    $("#start_date").datetimepicker();
/*    $("#start_date").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: 0,
        onChange: function (date) {
            var date2 = $('#start_date').datepicker('getDate');
            date2.setDate(date2.getDate() + 1);
            $('#end_date').datepicker('setDate', date2);
            //sets minDate to dt1 date + 1
            $('#end_date').datepicker('option', 'minDate', date2);
        }
    });

    $("#end_date").datepicker({
        dateFormat: "dd-mm-yy",
        onChange: function () {
            var dt1 = $('#start_date').datepicker('getDate');
            console.log(dt1);
            var dt2 = $('#end_date').datepicker('getDate');
            if (dt2 <= dt1) {
                var minDate = $('#end_date').datepicker('option', 'minDate');
                $('#end_date').datepicker('setDate', minDate);
            }
        }
    });*/

    $('#enquiry-form').formValidation({
        // I am validating Bootstrap form
        framework: 'bootstrap',
        // icon: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        fields: {
            start_date: {
                validators: {
                    notEmpty: {
                        message: 'Start Date is required and cannot be empty'
                    },
                }
            },
            end_date: {
                validators: {
                    notEmpty: {
                        message: 'End Date is required and cannot be empty'
                    },
                }
            },
            seaters: {
                validators: {
                    notEmpty: {
                        message: 'Seaters is required and cannot be empty'
                    },
                }
            },
            trip_from: {
                validators: {
                    notEmpty: {
                        message: 'Trip from is required and cannot be empty'
                    },
                }
            },
            trip_to: {
                validators: {
                    notEmpty: {
                        message: 'Trip to is required and cannot be empty'
                    },
                }
            },
            vehicle: {
                validators: {
                    notEmpty: {
                        message: 'Vehicle type is required and cannot be empty'
                    },
                }
            },
            // vehicle_model: {
            //     validators: {
            //         notEmpty: {
            //             message: 'vehicle model is required and cannot be empty'
            //         },
            //     }
            // },
            min_km: {
                validators: {
                    notEmpty: {
                        message: 'Min km is required and cannot be empty'
                    },
                }
            },
            approx_km: {
                validators: {
                    notEmpty: {
                        message: 'Approx km is required and cannot be empty'
                    },
                }
            },
            customer_name: {
                validators: {
                    notEmpty: {
                        message: 'Customer name is required and cannot be empty'
                    },
                }
            },
            customer_mobile: {
                validators: {
                    notEmpty: {
                        message: 'Customer mobile is required and cannot be empty'
                    },
                }
            },
            per_km: {
                validators: {
                    notEmpty: {
                        message: 'Price Per Km. is required and cannot be empty'
                    },
                }
            },
            per_km_ac: {
                validators: {
                    notEmpty: {
                        message: 'Price Per Km. for A/C is required and cannot be empty'
                    },
                }
            },
            driver_allowance: {
                validators: {
                    notEmpty: {
                        message: 'Driver Allowance is required and cannot be empty'
                    },
                }
            },
            extra_per_km: {
                validators: {
                    notEmpty: {
                        message: 'Price per extra Km. is required and cannot be empty'
                    },
                }
            },
            extra_per_km_ac: {
                validators: {
                    notEmpty: {
                        message: 'Price per extra Km. for A/C is required and cannot be empty'
                    },
                }
            },
            driver_allowance_night: {
                validators: {
                    notEmpty: {
                        message: 'Driver Allowance per Night is required and cannot be empty'
                    },
                }
            },
            advance_payment_type: {
                validators: {
                    notEmpty: {
                        message: 'Advance Payment is required and cannot be empty'
                    },
                }
            },
        }
    });

    $('#enquiry-form').formValidation('enableFieldValidators', "start_date", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "end_date", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "seaters", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "trip_from", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "trip_to", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "vehicle", false);
    // $('#enquiry-form').formValidation('enableFieldValidators', "vehicle_model", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "min_km", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "approx_km", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "customer_name", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "customer_mobile", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "per_km", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "per_km_ac", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "driver_allowance", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "extra_per_km", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "extra_per_km_ac", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "driver_allowance_night", false);
    $('#enquiry-form').formValidation('enableFieldValidators', "advance_payment_type", false);

    $('#enquiry-form').keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    var vehiclePrice;
    $('#vehicle').change(function () {
        var vehicleId = $(this).val();
        var vehicle_model = $('option:selected',this).attr('id');
        var company_id = $('#company_id').val();
        $("#vehicle_model_id").val(vehicle_model);
        $.ajax({
            type: "POST",
            url: base_url + 'enquiries/getCarPriceDetails',
            data: 'vehicle_model=' + vehicle_model +'&company_id=' + company_id,
            cache: false,
            dataType: 'json',
            success: function (result) {
                console.log(result);
                if(result.status){
                    vehiclePrice = result.priceDetails;
                }else{
                    // alert for price configuration
                    vehiclePrice = result.priceDetails;
                }
                $("#enquiry-form input[name=min_km]").val(vehiclePrice.min_km);
                $("#enquiry-form input[name=per_km]").val(vehiclePrice.per_km);
                $("#enquiry-form input[name=extra_per_km]").val(vehiclePrice.extra_per_km);
                $("#enquiry-form input[name=per_km_ac]").val(vehiclePrice.per_km_ac);
                $("#enquiry-form input[name=extra_per_km_ac]").val(vehiclePrice.extra_per_km_ac);
                $("#enquiry-form input[name=driver_allowance]").val(vehiclePrice.driver_allowance);
                $("#enquiry-form input[name=driver_allowance_night]").val(vehiclePrice.driver_allowance_night);

            }
        });

    });
    
    $('.step1').click(function () {
        $('#enquiry-form').formValidation('enableFieldValidators', "start_date", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'start_date');
        $('#enquiry-form').formValidation('enableFieldValidators', "end_date", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'end_date');
        $('#enquiry-form').formValidation('enableFieldValidators', "seaters", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'seaters');
        $('#enquiry-form').formValidation('enableFieldValidators', "trip_from", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'trip_from');
        $('#enquiry-form').formValidation('enableFieldValidators', "trip_to", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'trip_to');
        $('#enquiry-form').formValidation('enableFieldValidators', "vehicle", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'vehicle');
        // $('#enquiry-form').formValidation('enableFieldValidators', "vehicle_model", true);
        // $("#enquiry-form").bootstrapValidator('revalidateField', 'vehicle_model');
        $('#enquiry-form').formValidation('enableFieldValidators', "min_km", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'min_km');
        $('#enquiry-form').formValidation('enableFieldValidators', "approx_km", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'approx_km');
        $('#enquiry-form').formValidation('enableFieldValidators', "customer_name", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'customer_name');
        $('#enquiry-form').formValidation('enableFieldValidators', "customer_mobile", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'customer_mobile');

        var submitdisabled = $("#enquiry-form button[type=submit]").prop('disabled');
        if(!submitdisabled){
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input, select"),
                isValid = true;

            // Validation Part
            $("div input").removeClass("has-error");
            $("div select").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (curInputs[i].tagName == 'INPUT' && !curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest("div").addClass("has-error");
                } else if (curInputs[i].tagName == 'SELECT' && curInputs[i].value == '0') {
                    isValid = false;
                    $(curInputs[i]).closest("div").addClass("has-error");
                }
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
            return true;
        }else{
            return false;
        }

    });

    $('.step2').click(function () {
        $('#enquiry-form').formValidation('enableFieldValidators', "per_km", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'per_km');
        $('#enquiry-form').formValidation('enableFieldValidators', "per_km_ac", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'per_km_ac');
        $('#enquiry-form').formValidation('enableFieldValidators', "driver_allowance", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'driver_allowance');
        $('#enquiry-form').formValidation('enableFieldValidators', "extra_per_km", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'extra_per_km');
        $('#enquiry-form').formValidation('enableFieldValidators', "extra_per_km_ac", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'extra_per_km_ac');
        $('#enquiry-form').formValidation('enableFieldValidators', "driver_allowance_night", true);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'driver_allowance_night');

        var submitdisabled = $("#enquiry-form button[type=submit]").prop('disabled');
        if(!submitdisabled){
            var start_date = new Date($('#start_date').val());
            var end_date = new Date($('#end_date').val());
            var diffDays = end_date.getDate() - start_date.getDate();
            console.log(diffDays)

            $('#approx_km1').html($('input[name=approx_km]').val());
            $('#price_per_km1').html($('input[name=per_km]').val());
            $('#driver_allowance1').html($('input[name=driver_allowance]').val());
            $('#number_of_days1').html(diffDays);
            var total1 = (parseInt($('input[name=approx_km]').val()) * parseInt($('input[name=per_km]').val())) + (parseInt($('input[name=driver_allowance]').val()) * parseInt(diffDays));
            console.log(total1)
            $('#total1').html(total1);

            $('#approx_km2').html($('input[name=extra_per_km]').val());
            $('#price_per_km2').html($('input[name=extra_per_km_ac]').val());
            $('#driver_allowance2').html($('input[name=driver_allowance_night]').val());
            $('#number_of_days2').html(diffDays);
            var total2 = (parseInt($('input[name=extra_per_km]').val()) * parseInt($('input[name=extra_per_km_ac]').val())) + (parseInt($('input[name=driver_allowance_night]').val()) * parseInt(diffDays));
            console.log(total2)
            $('#total2').html(total2);

            $('input[name=fixed_amount]').val(total1 + total2);
            $('input[name=non_ac_percentage]').val(total1 * 25/100);
            $('input[name=ac_percentage]').val(total2 * 25/100);

            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input, select"),
                isValid = true;

            // Validation Part
            $("div input").removeClass("has-error");
            $("div select").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (curInputs[i].tagName == 'INPUT' && !curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest("div").addClass("has-error");
                } else if (curInputs[i].tagName == 'SELECT' && curInputs[i].value == '0') {
                    isValid = false;
                    $(curInputs[i]).closest("div").addClass("has-error");
                }
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
            return true;
        }else{
            return false;
        }

    });

    $("#enquiry-form button[type=submit]").click(function () {
        $('#enquiry-form').formValidation('enableFieldValidators', "advance_payment_type", false);
        $("#enquiry-form").bootstrapValidator('revalidateField', 'advance_payment_type');

    });
});