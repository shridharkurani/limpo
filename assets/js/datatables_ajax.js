$(document).ready(function() {
    $('.MyDatatable').DataTable( {
       // dom: 'Blfrtip',
        bFilter: true, //Removes search box.
       
        "oLanguage": {
      "sSearch": "Search here: "
    },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
});