$(document).ready(function() {
    $("#sendOTP").click(function (e) {
        e.preventDefault();
        var userMobile = $("input[name='user_mobile']").val();
        if(userMobile.length == 10){
            $.ajax({
                method: "POST",
                url: base_url+"ajax/otpDetail",
                data: {mobileNumber : userMobile},
                success: function(data){
                    var data = $.parseJSON(data);
                    console.log(data);
                    if(data.status == 1) {
                        alert("Please insert OTP sent to entered mobile number and proceed for registration.");
                    }
                    else if(data.status == 2) {
                        alert("Mobile Number already registered with us, Please try another.");
                    }
                    else if(data.status == 0) {
                        alert("Invalid Mobile Number.");
                    }
                    else {
                        alert("Something wrong, Please try later or contact us.");
                    }
                }
            });
        }
        else {
            alert("Please enter valid mobile number.");
        }
    });

    /*
    * currently we are not using this*/
    $("#couponCodeBtn").click(function () {
        var couponShortCode = $("input[name='coupon_code']").val();
        console.log(couponShortCode)
        if(couponShortCode) {
            $.ajax({
                method: "GET",
                url: base_url+"ajax/get_coupon_code_validation",
                data: {coupon_code : couponShortCode},
                success: function(data){
                    var data = $.parseJSON(data);
                    console.log(data);
                    if(data) {

                    }
                    /*
                    // $("#vehicle_model").children('option:not(:first)').remove();
                    $("#vehicle_model").children('option').remove();
                    $("#vehicle_model").append("<option value='0'> Select </option>");

                    $.each(data, function(key, value) {
                        $('#vehicle_model')
                            .append($("<option></option>")
                                .attr("value",value.model_id)
                                .text(value.model_name));
                    });*/
                }
            });
        }
    });

    $("#vehicle_type").change(function() {
        $.ajax({
            method: "POST",
            url: base_url+"ajax/get_vehicle_models",
            data: {vehicle_type:$(this).val()},
            success: function(data){
                var data = $.parseJSON(data);
                // $("#vehicle_model").children('option:not(:first)').remove();
                $("#vehicle_model").children('option').remove();
                $("#vehicle_model").append("<option value='0'> Select </option>");

                $.each(data, function(key, value) {
					$('#vehicle_model')
					 .append($("<option></option>")
					 .attr("value",value.model_id)
					 .text(value.model_name));
				});
            }
        });
	});
});