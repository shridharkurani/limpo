<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'company/add';
$filter_url = 'companies/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <!-- <a href="<?php echo base_url().'enquiries/add'; ?>" class="btn btn-info"><i class="fa fa-plus-circle"></i> <?= lang('add new enquiry','ucword'); ?></a> -->
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet portlet-grey">
                                    <div class="portlet-heading">
                                        
                                        <?php  echo form_open($filter_url,'id=account_signin');  ?>
                                        <div class="row ">
                                          <div class="col-lg-3">
                                             <label><?= lang('Compnay Name','ucword'); ?>
                                              <input type="text" name="cmpny_name" class="form-control col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-3">
                                             <label><?= lang('Compnay Email','ucword'); ?>
                                              <input type="text" name="cmpny_email" class="form-control col-lg-12">
                                              </label>
                                           </div>

                                          <div class="col-lg-3">
                                             <label><?= lang('Company Mobile','ucword'); ?>
                                              <input type="text" name="cmpny_mobile" class="form-control col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-3">
                                             <label><?= lang('Owner Name','ucword'); ?>
                                              <input type="text" name="owner_name" class="form-control col-lg-12">
                                              </label>
                                           </div>

                                          <div class="row text-center">
                                           <div class="col-lg-12">
                                             <label>&nbsp; <input class="btn btn-info form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                           </div>                                            
                                          </div>
                                        </div>
                                        <?php echo form_close(); ?>
                    
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="transactionsPortlet" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive dashboard-demo-table">
                                               <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Company Name</th>
                                                        <th>Company Email</th>
                                                        <th>Company Mobile</th>
                                                        <th>Company Owner</th>
                                                        <th>Company Owner Mobile</th>
                                                        <th>Company Contact Person</th>
                                                        <th>Company Contact Person Mobile</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if(is_array($allCmpnyDetails) && !empty($allCmpnyDetails)) {
                                                        // meDebug($allCmpnyDetails,true);

                                                      foreach ($allCmpnyDetails as $key => $cmpnyDetails) {
                                                         // meDebug($cmpnyDetails);
                                                        $viewUrl = base_url().'company/view/'.$cmpnyDetails['id'];
                                                        $activateUrl = base_url().'company/actions/'.$cmpnyDetails['id'].'/1';
                                                        $deactivateUrl = base_url().'company/actions/'.$cmpnyDetails['id'].'/2';
                                                        $deleteUrl =  base_url().'company/actions/'.$cmpnyDetails['id'].'/3';
                                                        switch ($cmpnyDetails['flag']) {
                                                          case 1: $Status = 'Active';
                                                            break;
                                                          case 2: $Status = 'Blocked';
                                                            break;
                                                          case 3: $Status = 'Deleted';
                                                            break;
                                                          default:$Status = 'Pending for approval';
                                                            break;
                                                        }
                                                        $button = '
                                                          <div class="btn-group">
                                                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                                          </button>
                                                          <ul class="dropdown-menu pull-right" role="menu">
                                                              <li>
                                                                <a href="'.$viewUrl.'">View</a>
                                                              </li>
                                                              <li>
                                                                <a href="'.$activateUrl.'">Activate</a>
                                                              </li>
                                                              <li>
                                                                <a href="'.$deactivateUrl.'">Block</a>
                                                              </li>
                                                              <li>
                                                                <a href="'.$deleteUrl.'">Delete</a>
                                                              </li>
                                                          </ul>
                                                        </div>';
                                                        echo "<tr>";
                                                        echo "<td>".$cmpnyDetails['cmpny_name']."</td>";
                                                        echo "<td>".$cmpnyDetails['cmpny_email']."</td>";
                                                        echo "<td>".$cmpnyDetails['cmpny_mobile']."</td>";
                                                        echo "<td>".$cmpnyDetails['owner_name']."</td>";
                                                        echo "<td>".$cmpnyDetails['owner_mobile']."</td>";
                                                        echo "<td>".$cmpnyDetails['contact_person_name']."</td>";
                                                        echo "<td>".$cmpnyDetails['contact_person_mobile']."</td>";
                                                        echo "<td>".$Status."</td>";
                                                        echo "<td>".$button."</td>";
                                                        echo "<tr>";
                                                      } 
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>                                                    
                                                    <tr>
                                                        <th>Company Name</th>
                                                        <th>Company Email</th>
                                                        <th>Company Mobile</th>
                                                        <th>Company Owner</th>
                                                        <th>Company Owner Mobile</th>
                                                        <th>Company Contact Person</th>
                                                        <th>Company Contact Person Mobile</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

  </div>
</div>