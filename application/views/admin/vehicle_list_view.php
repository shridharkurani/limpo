<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'vehicles/add';
$filter_url = 'vehicles/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <a href="<?php echo base_url().'vehicles/add_edit'; ?>" class="btn btn-info"><i class="fa fa-plus-circle"></i> <?= lang('add new vehicle','ucword'); ?></a>
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet portlet-grey">
                                <div class="portlet-heading">
                                    <?php  echo form_open($filter_url,'id=account_signin');  ?>
                                    <div class="row ">
                                      <div class="col-lg-3">
                                         <label><?= lang('Vehicle Type','ucword'); ?>
                                          <select class="form-control col-lg-12" name="vehicle_type" id="vehicle_type">
                                            <option value="0"> Select </option>
                                            <?php
                                            if(is_array($modelTypeDetails) && !empty($modelTypeDetails)) {
                                              foreach ($modelTypeDetails as $key => $model) {
                                                echo "<option value=".$model['model_type'].">".strtoupper($model['model_type'])."</option>";
                                              }
                                            }
                                            ?>

                                          </select>
                                          </label>
                                       </div>
                                      <div class="col-lg-3">
                                         <label><?= lang('Vehicle Model','ucword'); ?>
                                          <select class="form-control col-lg-12" name="vehicle_model" id="vehicle_model">
                                            <option value="0"> Select Vehicle Type </option>
                                            <?php
                                            /*if(is_array($modelNameDetails) && !empty($modelNameDetails)) {
                                              foreach ($modelNameDetails as $key => $modelName) {
                                                echo "<option value=".$modelName['model_id'].">".strtoupper($modelName['model_name'])."</option>";
                                              }
                                            }*/
                                            ?>

                                          </select>
                                          </label>
                                       </div>
                                       <div class="col-lg-3">
                                         <label><?= lang('vehicle number','ucword'); ?>
                                          <input type="text" name="vehicle_number" class=" col-lg-12 form-control">
                                            
                                          </select>
                                          </label>
                                       </div>
                                       <div class="col-lg-3">
                                         <label><?= lang('status','ucword'); ?>
                                          <select class="form-control col-lg-12" name="status" id="status">
                                            <option value="1"> Active </option>
                                            <option value="2"> Blocked </option>
                                            <!-- <option value="0"> Deleted </option> -->
                                            
                                          </select>
                                          </label>
                                       </div>

                                     <!--  <div class="col-lg-3">
                                         <label><?= lang('Price Per Km','ucword'); ?>
                                          <input type="text" name="price_per_km" class="form-control">
                                          </label>
                                       </div>
                                      <div class="col-lg-3">
                                         <label><?= lang('Price Per Km A/C','ucword'); ?>
                                          <input type="text" name="price_per_km_ac" class="form-control">
                                          </label>
                                       </div> -->

                                      <div class="row text-center">
                                       <div class="col-lg-12">
                                         <label>&nbsp; <input class="btn btn-default form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                       </div>                                            
                                      </div>
                                    </div>
                                    <?php echo form_close(); ?>
                
                                    <div class="clearfix"></div>
                                </div>
                                <div id="transactionsPortlet" class="panel-collapse collapse in">
                                    <div class="portlet-body">
                                        <div class="table-responsive dashboard-demo-table">
                                           <table id="example" class="table table-striped table-bordered" style="width:100%">
                                  <thead>
                                      <tr>
                                        <th>S.No.</th>
                                        <th>Vehicle Model</th>
                                        <th>Vehicle Type</th>
                                        <th>Vehicle Number</th>
                                        <th>Facilities</th>
                                        <th>Images Exist?</th>
                                        <th>Status</th>
                                        <th>Actions</th><!-- activate, Block, delete, edit. -->
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      if(is_array($vehicleDetails) && !empty($vehicleDetails)) {
                                          $cnt = 1;
                                          foreach ($vehicleDetails as $key => $vehicle) {
                                            if(!empty($vehicle['facilities']) && is_array($vehicle['facilities'])) {
                                              $Facilities = "";
                                              foreach ($vehicle['facilities'] as $key => $value) {
                                                $Facilities .=  $value['facility_name'].", ";
                                              }
                                              $Facilities = rtrim($Facilities,', ');//rtrim(",",$Facilities);

                                            }
                                            else {
                                              $Facilities = ' - ';
                                            }
                                            switch ($vehicle['flag']) {
                                              case 1: $Status = 'Active';
                                                break;
                                              case 2: $Status = 'Blocked';
                                                break;
                                              case 3: $Status = 'Deleted';
                                                break;
                                              default:$Status = 'Pending for approval';
                                                break;
                                            }
                                            $actionBtn = '<div class="btn-group">
                                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                              </button>
                                              <ul class="dropdown-menu pull-right" role="menu">
                                                  
                                                <li>
                                                  <a href="'.base_url().'vehicles/add_edit/'.$vehicle['id'].'">Edit</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/view/'.$vehicle['id'].'">View</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/actions/'.$vehicle['id'].'/1">Activate</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/actions/'.$vehicle['id'].'/2">Block</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/actions/'.$vehicle['id'].'/3">Delete</a>
                                                </li>
                                              </ul>
                                            </div>';
                                            echo "<tr>";
                                            echo "<td>".$cnt++."</td>";
                                            echo "<td>".$vehicle['model_type']."</td>";
                                            echo "<td>".$vehicle['model_name']."</td>";
                                            echo "<td>".$vehicle['vehicle_number']."</td>";

                                            echo "<td>".$Facilities."</td>";
                                            echo "<td>".$vehicle['image_exist']."</td>";
                                            echo "<td>".$Status."</td>";
                                            echo "<td>".$actionBtn."</td>";
                                            echo "</tr>";
                                          }
                                      }
                                    ?>
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                        <th>S.No.</th>
                                        <th>Vehicle Model</th>
                                        <th>Vehicle Type</th>
                                        <th>Vehicle Number</th>
                                        <th>Facilities</th>
                                        <th>Images Exist?</th>
                                        <th>Status</th>
                                        <th>Actions</th><!-- activate, Block, delete, edit. -->
                                      </tr>
                                  </tfoot>
                              </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->

                    </div>
                </div>
            </div>
  </div>
</div>