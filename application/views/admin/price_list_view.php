<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'prices/add';
$filter_url = 'prices/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <a href="<?php echo base_url().'prices/add'; ?>" class="btn btn-info"><i class="fa fa-plus-circle"></i> <?= lang('add new enquiry','ucword'); ?></a>
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet portlet-grey">
                                    <div class="portlet-heading">
                                        <?php  echo form_open($search_url,'id=account_signin');  ?>
                                        <div class="row ">
                                          <div class="col-lg-3">
                                             <label><?= lang('from','ucword'); ?>
                                              <input type="text" name="vehicle_no" class="form-control">
                                              </label>
                                           </div>
                                          <div class="col-lg-3">
                                             <label><?= lang('to','ucword'); ?>
                                              <input type="text" name="vehicle_no" class="form-control">
                                              </label>
                                           </div>

                                          <div class="col-lg-3">
                                             <label><?= lang('start date','ucword'); ?>
                                              <input type="text" name="vehicle_no" class="form-control">
                                              </label>
                                           </div>
                                          <div class="col-lg-3">
                                             <label><?= lang('end date','ucword'); ?>
                                              <input type="text" name="vehicle_no" class="form-control">
                                              </label>
                                           </div>

                                          <div class="row text-center">
                                           <div class="col-lg-12">
                                             <label>&nbsp; <input class="btn btn-info form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                           </div>                                            
                                          </div>
                                        </div>
                                        <?php echo form_close(); ?>
                    
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="transactionsPortlet" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive dashboard-demo-table">
                                               <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
              <th>S.No.</th>
              <th>Model</th>
              <th>Per Km.</th>
              <th>Per Km. A/C.</th>
              <th>Driver Allowance</th>
              <th>Minimum Km.</th>
              <th>Extra Driver Allowance</th>
              <th>Extra per Km.</th>
              <th>Extra per Km. for A/C</th>
              <th>Status</th>
              <th>Actions</th><!-- activate, deactivate, delete, edit. -->
            </tr>
        </thead>
        <tbody>
          <?php
            if(is_array($allCmpnyDetails) && !empty($allCmpnyDetails)) {
                foreach ($allCmpnyDetails as $key => $cmpnyDetails) {
                  switch ($cmpnyDetails['flag']) {
                    case '1':
                      $status = 'Active';
                      break;
                    case '2':
                      $status = 'Deactive';
                      break;
                    case '3':
                      $status = 'Deleted';
                      break;
                    default: // 0 For pending.
                      $status = 'Peding';
                      break;
                  }
                  $actionBtn = '<div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                          <a href="'.base_url().'company/view/'.$cmpnyDetails['id'].'">View</a>
                        </li>
                        <li>
                          <a href="'.base_url().'company/action/'.$cmpnyDetails['id'].'/1">Activate</a>
                        </li>
                        <li>
                          <a href="'.base_url().'company/action/'.$cmpnyDetails['id'].'/2">Deactivate</a>
                        </li>
                        <li>
                          <a href="'.base_url().'company/action/'.$cmpnyDetails['id'].'/3">Delete</a>
                        </li>
                    </ul>
                  </div>';
                  echo "<tr>";
                  echo "<td>".$cmpnyDetails['id']."</td>";
                  echo "<td>".$cmpnyDetails['cmpny_name']."</td>";
                  echo "<td>".$cmpnyDetails['cmpny_mobile']."</td>";
                  echo "<td>".$cmpnyDetails['cmpny_email']."</td>";
                  echo "<td>".$cmpnyDetails['contact_person_name']."</td>";
                  echo "<td>".$cmpnyDetails['contact_person_mobile']."</td>";
                  echo "<td>".$cmpnyDetails['contact_person_mobile']."</td>";
                  echo "<td>".$status."</td>";
                  echo "<td>".$actionBtn."</td>";
                  echo "</tr>";
                }
            }
          ?>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>
                  <div class="btn-group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                      <li>
                        <a href="#"><?= lang('show','ucword'); ?></a>
                      </li>
                      <li>
                        <a href="#"><?= lang('edit','ucword'); ?></a>
                      </li>
                      <li><a class="nav-link" data-toggle="modal" href="#hospitalDeleteModal" </a></li>
                      <li>
                      </li>
                  </ul>
                </div>
                </td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>63</td>
                <td>2011/07/25</td>
                <td>
                  <div class="btn-group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                      <li>
                        <a href="#"><?= lang('show','ucword'); ?></a>
                      </li>
                      <li>
                        <a href="#"><?= lang('edit','ucword'); ?></a>
                      </li>
                      <li><a class="nav-link" data-toggle="modal" href="#hospitalDeleteModal" </a></li>
                      <li>
                      </li>
                  </ul>
                </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>S.No.</th>
                <th>Model</th>
                <th>Per Km.</th>
                <th>Per Km. A/C.</th>
                <th>Driver Allowance</th>
                <th>Minimum Km.</th>
                <th>Extra Driver Allowance</th>
                <th>Extra per Km.</th>
                <th>Extra per Km. for A/C</th>
            </tr>
        </tfoot>
    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

  </div>
</div>