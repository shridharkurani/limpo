<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'discounts/add_edit';
$filter_url = 'discounts/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
        <!-- begin PAGE TITLE AREA -->
        <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
        <?php echo $this->layouts->print_msg_div($this->session); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <?php echo $this->layouts->print_breadcrumb(); ?><br>
                    <h1> <?php echo $page_title;?>
                        <div class="pull-right right-margin-2px">
                            <a href="<?php echo base_url().'discounts/add_edit'; ?>" class="btn btn-primary"><?= lang('add new discount','ucword'); ?></a>
                        </div></h1>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE AREA -->

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="portlet portlet-grey">
                            <div id="transactionsPortlet" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div class="table-responsive dashboard-demo-table">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Discount Id</th>
                                                <th>Discount Short Name</th>
                                                <th>Discount Percentage</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(is_array($discounts) && !empty($discounts)) {
                                                    $cnt = 1;
                                                    foreach ($discounts as $key => $discount) {
                                                    $actionBtn = '<div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropup pull-right" role="menu">
                                                            
                                                          <li>
                                                            <a href="'.base_url().'discounts/add_edit/'.$discount['id'].'">Edit</a>
                                                          </li>
                                                         
                                                          <li>
                                                            <a href="'.base_url().'discounts/actions/'.$discount['id'].'/3">Delete</a>
                                                          </li>
                                                        </ul>
                                                      </div>';
                                                     echo "<tr><td>".$cnt++."</td>";
                                                    echo "<td>".$discount['id']."</td>";
                                                    echo "<td>".$discount['discount_short_code']."</td>";
                                                    echo "<td>".$discount['discount_percentage']."</td>";
                                                    echo "<td>".$actionBtn."</td></tr>";
                                                }
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Discount Id</th>
                                                <th>Discount Short Name</th>
                                                <th>Discount Percentage</th>
                                                <th>Actions</th>
                                            </tr>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->

                </div>
            </div>
        </div>

    </div>
</div>