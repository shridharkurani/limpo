<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 06-02-2021
 * Time: 01:28
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  if($this->session->userdata('user_type')!="")
         {
             $user_type = $this->session->userdata('user_type');
         }
         if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
?>
<div class="body container">
    <?php
    if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
        echo '<ul class="breadcrumb row custom-breadcrumb">';
        $count = count($breadcrumb_array);
        foreach ($breadcrumb_array as $key => $value) {
            if (--$count <= 0) {
                echo "<li class='active'>".$key."</li>";
                break;
            }
            echo '<li><a href='.$value.'>'.$key.'</a></li>';
        }
        echo "</ul>";
    }
    ?>
<legend class="text-right"><label><a href="<?php echo base_url().'services/add'; ?>" class='btn btn-success'>Add New Service</a></label></legend>
	<div class="nav nav-tabs nav-justified">
	    <div class="panel-body threepanel">
	        <div class="tab-content">
	            <section  class="tab-pane fade in active" id="newPanel">
	            	<?php
	            	if(!empty($services)) {
                           foreach ($services as $key => $value) {
	            			echo "<div class='row thumbnail list_view'>";
	            			echo "<legend>

	            			<label><a href=".base_url()."services/show/".$services[$key]->ser_id.">".$services[$key]->ser_name."</a></label>
	            			
	            				<span class='pull-right'><a  class='btn btn-danger' href='".base_url()."services/delete_service/".$services[$key]->ser_id."'>Delete</a></span>

	            				<span class='pull-right'><a  class='btn btn-info' href=".base_url()."services/edit/".$services[$key]->ser_id.">Edit</a></span>";


                        echo"</legend>";

	            		echo "<p>".$services[$key]->ser_description."</p>";
	            		echo "</div>";



	            	}
	            }
	            	else {
	            		echo "No New servicses found here.";
	            	}
	            	?>
	            </section>




	        </div>
	    </div>
	</div>
</div>