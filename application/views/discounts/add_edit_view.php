<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isset($discountId) && $discountId != 0)
    $url = base_url()."discounts/add_edit/".$discountId;
else
    $url = base_url()."discounts/add_edit";
echo form_open_multipart($url);
if(!empty($discountDetails)) {
    $discountDetails['discount_short_code'] = $discountDetails['discount_short_code'];
    $discountDetails['discount_percentage'] = $discountDetails['discount_percentage'];

}
//meDebug($discountDetails, 1);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="col-lg-12">
                <div class="page-title">
                    <?php echo $this->layouts->print_breadcrumb(); ?><br>
                    <h1> <?php echo $page_title;?> </h1>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <br class="portlet-body">
                        <div class="row">
                            <center><legend>Discount Details</legend></center>
                            <div class="col-lg-12">
                                <div class="col-md-6 required">
                                    <label>Discount Short Code</label>
                                    <input type="text" class="form-control numeric_field" name="discount_short_code" value="<?php
                                    echo set_value('discount_short_code', isset($discountDetails['discount_short_code']) ? $discountDetails['discount_short_code'] : '');
                                    ?>" />
                                    <?php echo form_error('discount_short_code', '<div class="inline_error">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 required">
                                    <label>Discount Percentage</label><span>(Ex: Insert only number 10 if 10% discount)</span>
                                    <input type="text" class="form-control numeric_field" name="discount_percentage" value="<?php
                                    echo set_value('discount_percentage', isset($discountDetails['discount_percentage']) ? $discountDetails['discount_percentage'] : '');
                                    ?>" />
                                    <?php echo form_error('discount_percentage', '<div class="inline_error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <br></br>

                        <div class=" text-center col-lg-12">
                            <legend>&nbsp</legend>
                            <!-- <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>"> -->
                            <input type="submit" name="" value="Save Discount Details" class="btn btn-default">
                        </div>
                    </div>
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.page-content -->
</div>
</form>