<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 06-02-2021
 * Time: 02:04
 */
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart (base_url().'services/add');

if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';

}

if(isset($err_msg)){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';

}
if (isset($msg)) {
        echo ' <div class="alert alert-success row"><b>
        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}
?>
<div class="container">

    <div class="row well">
         <legend><label class="default_font_color">Create New service</label>
            <input type="submit" name="" value="Submit" class="btn hoverable_btn pull-right" /></legend>
            <div class="col-md-4">
                <label>service Name <i class="required"> * </i></label>
                <input name="service_name" class="form-control" type="text" value="<?php echo set_value('service_name'); ?>">
            </div>

            <div class="col-md-4">
                <label>Service Image <i class="required"> * </i>
</label><small > (Image max size:2MB & Image type: gif, jpg, png.)</small>
                <input type="file" name="userfile" size="20" class="form-control" />
            </div>

            <div class="col-md-12">
                <label>Service Description <i class="required"> * </i></label>
                <textarea name="service_desc" class="form-control" rows="4" cols="50" > <?php echo set_value('service_desc'); ?>    </textarea>
            </div>

        <div class="col-md-12 text-center">
            <label class=""><input type="submit" name="" value="Submit" class="btn hoverable_btn"></label>
        </div>

    </div>


</div>
