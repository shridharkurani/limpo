<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 06-02-2021
 * Time: 03:24
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';

}

if(isset($err_msg)){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';

}
if (isset($msg)) {
        echo ' <div class="alert alert-success row"><b>
        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}
?>

<div class="inner_body">
<div class="body container">




<h3 class="show_text row"> Service details</h3>
	<div class="row ">
		<?php
		$is_autorised = 0;
	 	$user_type = $this->session->userdata('user_type');
		$is_logged_in = $this->session->userdata('logged_in');
	 	$enquiry_target = '';
		if(!empty($result))
		{
			foreach ($result as $key => $value )
			{?>
                <div class="inner_body">
        <div class="body container">
            <div class="section light directory-single">

                    <h5> <span><?php echo $result[$key]->ser_name; ?></span>  </h5>
                    <div class="row col-md-6 col-lg-6 col-sm-6">
                        <div class="mainfunctionimg1">
                            <?php $ser_img_name = (!empty($result)) ? asset_url()."uploads/service/".$result[$key]->ser_image : 'NO image found';?>
                        <?php if( !empty($ser_img_name)){?>
                            <img src="<?php echo $ser_img_name;?>" alt = "<?php $result[$key]->ser_name ?>"/>
                            <?php }else{?>
                            <p class="row" > No image </p>
                        <?php }?>
                            <div class="col-md-12 col-sm-12">
                                <section style="margin-top:2em;">
                                    <h4 class="show_text row">Description</h4>
                                    <p class="row" >   <?php echo $result[$key]->ser_description; ?></p>
                                </section>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
                </div>

            <?php }

	    	}



	    	else {
	    		echo "<div class='col-sm-12 col-md-12 col-lg-12'> <h5>No Options found... </h5></div>";
	    	}
	?>
    </div>
</div>


