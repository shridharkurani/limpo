<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'packages/add_edit';
$filter_url = 'packages/filter';
$userId = $this->session->userdata('user_id');
?>
<div id="page-wrapper">
    <div class="page-content">
        <!-- begin PAGE TITLE AREA -->
        <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
        <?php echo $this->layouts->print_msg_div($this->session); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <?php echo $this->layouts->print_breadcrumb(); ?><br>
                    <h1> <?php echo $page_title;?>
                        <div class="pull-right right-margin-2px">
<!--                            <label class="text-green">Available Service Count : </label><label class="text-red"> --><?php //echo $remainingServiceCnt;?><!--</label>-->
                        </div>
                    </h1>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE AREA -->

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="portlet portlet-grey">
                            <div class="portlet-heading hidden">

                                <?php  echo form_open($filter_url,'class=mar_pad_1');  ?>
                                <div class="row ">
                                    <div class="col-lg-4 col-sm-12 col-md-3">
                                        <label class="width100"><?= lang('Customer Name','ucword'); ?>
                                            <input type="text" name="customerName" class="form-control  col-lg-12">
                                        </label>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-3">
                                        <label class="width100"><?= lang('Customer Mobile','ucword'); ?>
                                            <input type="text" name="customerMobile" class="form-control  col-lg-12">
                                        </label>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-3">
                                        <label class="width100"><?= lang('from city','ucword'); ?>
                                            <input type="text" name="fromCity" class="form-control  col-lg-12">
                                        </label>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-3">
                                        <label class="width100"><?= lang('to city','ucword'); ?>
                                            <input type="text" name="toCity" class="form-control col-lg-12">
                                        </label>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-3">
                                        <label class="width100"><?= lang('start date','ucword'); ?>
                                            <input type="text" name="startDate" class="form-control col-lg-12">
                                        </label>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-3">
                                        <label class="width100"><?= lang('end date','ucword'); ?>
                                            <input type="text" name="endDate" class="form-control col-lg-12">
                                        </label>
                                    </div>

                                    <div class="row text-center">
                                        <div class="col-lg-12">
                                            <label>&nbsp <input class="btn btn-primary form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                            <label>&nbsp <a href="<?php echo base_url().'packages';?>" class="btn btn-default">Clear Search</a></label>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>

                                <div class="clearfix"></div>
                            </div>
                            <div id="transactionsPortlet" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div class="table-responsive dashboard-demo-table">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Service Id</th>
                                                <th>package Name</th>
                                                <th>Service Name</th>
                                                <th>Purchased Date</th>
                                                <th>Package Total Count</th>
                                                <th>Package Remaining Count</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(is_array($packages) && !empty($packages)) {
                                                $cnt = 1;
                                                foreach ($packages as $key => $package) {
                                                    $requestBtn = '<div class="">
                                                            <a href="'.base_url().'packages/request/'.$userId.'/'.$package['package_id'].'/'.$package['ser_id'].'">Request For service</a>
                                                            </div>';
                                                    $actionBtn = '<div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropup pull-right" role="menu">
                                                            
                                                          <li>
                                                            <a href="'.base_url().'packages/add_edit/'.$package['id'].'">Edit</a>
                                                          </li>
                                                            
                                                          <li>
                                                            <a href="'.base_url().'packages/view/'.$package['id'].'">View</a>
                                                          </li>
                                                         
                                                          <li>
                                                            <a href="'.base_url().'packages/actions/'.$package['id'].'/3">Delete</a>
                                                          </li>
                                                        </ul>
                                                      </div>';
//                                                    meDebug($package,1 );
                                                    echo "<tr><td>".$cnt++."</td>";
                                                    echo "<td>".$package['ser_id']."</td>";
                                                    echo "<td>".$package['pkg_name']."</td>";
                                                    echo "<td>".$package['ser_name']."</td>";
                                                    echo "<td>".date('m/d/y h:i:s A', strtotime($package['created_date']))."</td>";
                                                    echo "<td>".$package['service_count']."</td>";
                                                    echo "<td>".$package['remaining_count']."</td>";
                                                    echo "<td>".$requestBtn."</td></tr>";
                                                }
                                            }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>package Id</th>
                                                <th>package Name</th>
                                                <th>Service Name</th>
                                                <th>Purchased Date</th>
                                                <th>Package Total Count</th>
                                                <th>Package Remaining Count</th>
                                                <th>Actions</th>
                                            </tr>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->

                </div>
            </div>
        </div>

    </div>
</div>