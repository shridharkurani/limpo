<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."company/add_edit";
echo form_open_multipart($url);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
<div id="page-wrapper" style="margin: 0px;">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <!--<div class="col-lg-12">
                <div class="page-title">
                    <?php /*echo $this->layouts->print_breadcrumb(); */?><br>
                    <h1> <?php /*echo $page_title;*/?> </h1>
                </div>
            </div>-->
            <div class="row">
                <div class="portlet1"  style="margin: 50px;">
                    <!-- <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Company / Organization Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div> -->
                    <div class="row">
                        <center><h1>Existing Packages</h1></center>
                    </div>
                    <div id="products" class="row view-group">
                        <?php
                        $cnt = 1;
                        foreach ($packages as $package) {
                            if(4 % $cnt == 1) {
                        ?>
                        <div class="row">
                            <?php
                            }
                            else {
                            ?>
                            <div class="section">
                                <?php
                            }
                                ?>
                                <div class="item col-lg-4 col-sm-12 col-md-6">
                                    <div class="thumbnail card" style="min-height: 350px;">
                                        <div class="img-event" style="margin: 1%;">
                                            <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: LIMPO "><title>LIMPO</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="40%" y="50%" fill="#dee2e6" dy=".3em">LIMPO</text></svg>
                                        </div>
                                        <div class="caption card-body">
                                            <h4 class="group card-title inner list-group-item-heading">
                                                <?php echo ucwords($package['pkg_name']);?></h4>
                                            <p class="group inner list-group-item-text">
                                                <?php echo $package['packageServices'];?></p><br>

                                            <!--<p class="group inner list-group-item-text">
                                                <?php /*echo $package['pkg_desc'];*/?> </p>-->
                                            <div class="row">
                                                <div class="col-md-6 pull-left">
                                                    <a class="btn btn-warning" href="<?php echo base_url().'packages/view/'.$package['id']; ?>">View More</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a class="btn btn-success  pull-right" href="<?php echo base_url().'packages/book/'.$package['id']; ?>">Book This Package</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- this is close for section or row -->
                                <?php

                        }
                        ?>
                    </div>
                    <!-- /.portlet-body -->
                </div>
                <!-- /.portlet -->

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.page-content -->
</div>
</div>
</form>