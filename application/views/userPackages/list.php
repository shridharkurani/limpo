<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 13-02-2021
 * Time: 00:59
 */
if ( !defined('BASEPATH')) exit('No direct script access allowed');

  if($this->session->userdata('user_type')!="")
         {
             $user_type = $this->session->userdata('user_type');
         }
         if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
?>

<div class="body container">
    <?php
    if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
        echo '<ul class="breadcrumb row custom-breadcrumb">';
        $count = count($breadcrumb_array);
        foreach ($breadcrumb_array as $key => $value) {
            if (--$count <= 0) {
                echo "<li class='active'>".$key."</li>";
                break;
            }
            echo '<li><a href='.$value.'>'.$key.'</a></li>';
        }
        echo "</ul>";
    }
    ?>
    <legend class="text-right"><label><a href="<?php echo base_url().'packages/add'; ?>" class='btn btn-success'>Add New Package</a></label></legend>
    <div class="nav nav-tabs nav-justified">
        <div class="panel-body threepanel">
            <div class="tab-content">
                <section  class="tab-pane fade in active" id="newPanel">
                    <?php

                    if(!empty($data)) {
                        foreach ($data as $key => $value) {
                            echo "<div class='row thumbnail list_view'>";

                            echo "<legend>

	            			<label><a href=".base_url()."packages/show/".$data[$key]->pkg_id.">".$data[$key]->pkg_name."</a></label>
	            			
	            				<span class='pull-right'><a  class='btn btn-danger' href='".base_url()."packages/delete_package/".$data[$key]->pkg_id."'>Delete</a></span>

	            				<span class='pull-right'><a  class='btn btn-info' href=".base_url()."packages/edit/".$data[$key]->pkg_id.">Edit</a></span>";


                            echo"</legend>";

                            echo "<p>".$data[$key]->pkg_desc."</p>";
                            echo "</div>";

                        }
                    }
                    else {
                        echo "No New Packages found here.";
                    }
                    ?>
                </section>




            </div>
        </div>
    </div>
</div>