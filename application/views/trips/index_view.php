<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'trips';
$filter_url = 'trips/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
	                        <div class="pull-right right-margin-2px">
	                          <a href="<?php echo base_url().'enquiries'; ?>" class="btn btn-primary"><?= lang('enquiries list','ucword'); ?></a>
	                        </div>
                    	</h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet portlet-grey">
                                    <div class="portlet-heading hidden">
                                        
                                        <?php  echo form_open($filter_url,'class=mar_pad_1');  ?>
                                        <div class="row ">
                                         <div class="col-lg-4 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('Customer Name','ucword'); ?>
                                              <input type="text" name="customerName" class="form-control  col-lg-12">
                                              </label>
                                          </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('Customer Mobile','ucword'); ?>
                                              <input type="text" name="customerMobile" class="form-control  col-lg-12">
                                              </label>
                                          </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('from city','ucword'); ?>
                                              <input type="text" name="fromCity" class="form-control  col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('to city','ucword'); ?>
                                              <input type="text" name="toCity" class="form-control col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('start date','ucword'); ?>
                                              <input type="text" name="startDate" class="form-control col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('end date','ucword'); ?>
                                              <input type="text" name="endDate" class="form-control col-lg-12">
                                              </label>
                                           </div>

                                          <div class="row text-center">
                                           <div class="col-lg-12">
                                             <label>&nbsp; <input class="btn btn-primary form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                             <label>&nbsp; <a href="<?php echo base_url().'enquiries';?>" class="btn btn-default">Clear Search</a></label>
                                           </div>                                            
                                          </div>  
                                        </div>
                                        <?php echo form_close(); ?>
                    
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="transactionsPortlet" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive dashboard-demo-table">
                                               <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Enquiry Id</th>
                                                        <th>Trip Id</th>
                                                        <th>Customer Name</th>
                                                        <th>Customer Mobile</th>
                                                        <th>From</th>
                                                        <th>To</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Driver Name</th>
                                                        <th>Driver Mobile</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  <?php
                                                    if(is_array($tripArray) && !empty($tripArray)) {
                                                      $cnt = 1;
                                                      foreach ($tripArray as $key => $trip) {

                                                      switch ($trip['flag']) {
                                                        case 1: $Status = 'Payment Done';
                                                          break;
                                                        case 2: $Status = 'Confirmed By Owner';
                                                          break;
                                                        case 3: $Status = 'Deleted';
                                                          break;
                                                        case 4: $Status = 'Completed';
                                                          break;
                                                        default:$Status = 'Pending for approval';
                                                          break;
                                                      }
                                                      $actionBtn = '<div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropup pull-right" role="menu">
                                                            
                                                          <li>
                                                            <a href="'.base_url().'enquiries/add_edit/'.$trip['tripId'].'">Edit</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.base_url().'enquiries/view/'.$trip['tripId'].'">View</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.base_url().'enquiries/actions/'.$trip['tripId'].'/1">Activate</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.base_url().'enquiries/actions/'.$trip['tripId'].'/2">Block</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.base_url().'enquiries/actions/'.$trip['tripId'].'/3">Delete</a>
                                                          </li>
                                                        </ul>
                                                      </div>';
                                                        /*$customerLink = "<a href='#'>".$trip['fullname']."</a>";*/
                                                        $fromDate = date('m-d-Y', strtotime($trip['start_date']));
                                                        $endDate = date('m-d-Y', strtotime($trip['end_date']));
                                                        echo "<tr><td>".$cnt++."</td>";
                                                        echo "<td>".$trip['tripId']."</td>";
                                                        echo "<td>".$trip['enquiryId']."</td>";
                                                        echo "<td>".$trip['fullname']."</td>";
                                                        echo "<td>".$trip['mobile']."</td>";
                                                        echo "<td>".$trip['trip_from']."</td>";
                                                        echo "<td>".$trip['trip_to']."</td>";
                                                        echo "<td>".$fromDate."</td>";
                                                        echo "<td>".$endDate."</td>";
                                                        echo "<td>".$trip['driverName']."</td>";
                                                        echo "<td>".$trip['driverMobile']."</td>";
                                                        echo "<td>".$Status."</td>";
                                                        echo "<td>".$actionBtn."</td></tr>";
                                                      }
                                                    }
                                                  ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Enquiry Id</th>
                                                        <th>Trip Id</th>
                                                        <th>Customer Name</th>
                                                        <th>Customer Mobile</th>
                                                        <th>From</th>
                                                        <th>To</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Driver Name</th>
                                                        <th>Driver Mobile</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

  </div>
</div>