<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 06-02-2021
 * Time: 13:05
 */
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $full_url = base_url().'packages/edit/'.$pkg_id;
    echo form_open_multipart($full_url);

if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';

}

if(isset($err_msg)){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';

}
if (isset($msg)) {
        echo ' <div class="alert alert-success row"><b>
        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}

if(empty($posted_data) && !empty($pkg_details)) {
    foreach ($pkg_details as $key => $value){
        $pkg_name = $pkg_details[$key]->pkg_name;
        $pkg_desc = $pkg_details[$key]->pkg_desc;
        $pkg_services = $pkg_details[$key]->pkg_services;
        $pkg_price = $pkg_details[$key]->pkg_price;
    }
}
else if ($posted_data) {
    //set input values from post request.
    $pkg_name = $posted_data['pkg_name'];
    $pkg_desc = $posted_data['pkg_desc'];
    $pkg_services = $posted_data['pkg_services'];
    $pkg_price = $posted_data['pkg_price'];
print_r($pkg_services);
}

foreach ($pkg_details as $key => $detail){
   $selectedservices = explode(",",$pkg_details[$key]->pkg_services);
}

foreach($services as $key=>$value) {
    $allservicesArr[] = $services[$key]->ser_name;
}
?>

<div class="container">
    <div class="row well">
         <legend><label class="default_font_color">Update Package</label>
            <input type="submit" name="" value="Submit" class="btn hoverable_btn pull-right" /></legend>

            <div class="col-md-6">
                <label>Package Name <i class="required"> * </i></label>
                <input name="pkg_name" class="form-control" type="text" value="<?php echo set_value('pkg_name', $pkg_name); ?>">
            </div>

        <div class="col-md-6">
            <label>Package price <i class="required"> * </i></label>
            <input name="pkg_price" class="form-control" type="text" value="<?php echo set_value('pkg_price',$pkg_price); ?>">
        </div>

        <div class="col-md-12">
            <label>Select Services
                <i class="required"> * </i></label>
            <div class="controls">
                <?php foreach($allservicesArr as $list) {
                    if(in_array($list,$selectedservices)){
                    ?> <input checked="checked" type="checkbox" name="pkg_services[]" value="<?php echo $list ?>" >
                    <?php } else {?>
                        <input type="checkbox" name="pkg_services[]" value="<?php echo $list ?>" >
                    <?php }
                    echo $list;
                    echo "&nbsp &nbsp";
                }?>

            </div>
        </div>




             <div class="col-md-12">
                <label>Package Description  <i class="required"> * </i></label>
                <textarea name="pkg_desc" class="form-control" rows="8" cols="50" ><?php echo set_value('pkg_desc',$pkg_desc); ?> </textarea>
            </div>

        <div class="col-md-12 text-center">
            <input type="submit" name="" value="Submit" class="btn hoverable_btn">
        </div>


    </div>


</div>