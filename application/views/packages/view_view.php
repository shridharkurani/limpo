<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."packages/view/".$packageId;
echo form_open_multipart($url);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
<div id="page-wrapper" style="margin: 0px;">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <!--<div class="col-lg-12">
                <div class="page-title">
                    <?php /*echo $this->layouts->print_breadcrumb(); */?><br>
                    <h1> <?php /*echo $page_title;*/?> </h1>
                </div>
            </div>-->
            <div class="row">
                <div class="portlet1"  style="margin: 50px;">
                    <!-- <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Company / Organization Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div> -->
                    <div class="row">
                        <?php echo $this->layouts->print_msg_div($this->session); ?>
                        <center><legend><h1><?php echo $package['pkg_name'];?></h1></legend></center>
                    </div>
                    <div id="products" class="row view-group">
                        <div class="row">
                            <div class="section">
                                <div class="item col-lg-4 col-sm-12 col-md-6">
                                    <div class="thumbnail card" style="">
                                        <div class="img-event" style="margin: 1%;">

                                            <?php
                                            if(!empty($package['pkg_image'])) {
                                                if(!empty($package['pkg_image'])) {
                                                    $imageArray = explode(".", $package['pkg_image']);
                                                    $serviceImage = !empty($imageArray) ? $imageArray[0].'_thumb.'.$imageArray[1] :  '';
                                                    $imageFullUrl = base_url().'assets/uploads/packages/thumb/'.$serviceImage;
                                                }
                                                echo "<img style='max-height: 180px;' width='100%' for='files' class='medium_img rounded' src=".$imageFullUrl." >";
                                                ?>
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <svg class="bd-placeholder-img card-img-top" width="100%" height="180"
                                                     xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                                     focusable="false" role="img" aria-label="Placeholder: LIMPO PACKAGE"><title>
                                                        LIMPO PACKAGE</title>
                                                    <rect width="100%" height="100%" fill="#868e96"></rect>
                                                    <text x="40%" y="50%" fill="#dee2e6" dy=".3em">LIMPO PACKAGE</text>
                                                </svg>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="caption card-body">
                                            <h3 class="group card-title inner list-group-item-heading">
                                                <?php echo ucwords($package['pkg_name']);?></h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="item col-lg-4 col-sm-12 col-md-6">
                                    <div class="thumbnails card">
                                        <div class="caption card-body">
                                            <legend><h4 for="">Package Description</h4></legend>
                                            <p class="group inner list-group-item-text">
                                                <?php echo $package['pkg_desc'];?></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="item col-lg-4 col-sm-12 col-md-6">
                                    <div class="thumbnail card">
                                        <div class="caption card-body">
                                            <legend><h4 for="">Payment Details</h4></legend>
                                            <div>
                                                <label>Available Counts : </label>
                                                <span><?php echo $package['availble_counts'];?></span>
                                            </div>
                                            <div>
                                                <label>Payble amount : </label>
                                                <span><?php echo $paybleAmount;?></span>
                                            </div>

                                            <br>
                                            <div class="required1">
                                                <label>Have a discount code ?</label>
                                                <input type="text" class="form-control numeric_field" name="coupon_code" placeholder="Add discount code" value="<?php
                                                echo set_value('coupon_code', isset($packageDetails['coupon_code']) ? $packageDetails['coupon_code'] : '');
                                                ?>" />
                                                <?php echo form_error('coupon_code', '<div class="inline_error">', '</div>'); ?>
                                            </div>
                                            <br><div class="row">
                                                <div class="col-md-6 pull-left">
                                                    <input type="submit" class="btn btn-info" name="apply_coupon_code" value="Apply Coupon Code" />
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="submit" class="btn btn-success" name="pay" value="Pay Rs <?php echo $paybleAmount;?> " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="background: #ffffff">
                            <div id="products" class="row view-group">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="page-header">
                                                <h1>
                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#exampleModal">
                                                        Add Your Comment
                                                    </button>
                                                    Comments </h1>
                                            </div>
                                            <?php
                                            if(!empty($packageComments)) {
                                                ?>
                                                <div class="comments-list">
                                            <?php
                                                foreach ($packageComments as $packageComment) {
                                                    ?>
                                                    <div class="media">
                                                        <p class="pull-right">
                                                            <small>Commented on - <?php echo date('m/d/Y h:i:s A',strtotime($packageComment['created_date'])); ?></small>
                                                        </p>
                                                        <div class="media-body">

                                                            <h3 class="text-blue media-heading user_name"><?php echo ucwords($packageComment['user_fullname']);?></h3>
                                                            <?php echo ucwords($packageComment['comment']);?>
<!--
                                                            <p>
                                                                <small><a href="">Like</a> - <a href="">Share</a>
                                                                </small>
                                                            </p>-->
                                                        </div>
                                                    </div>
                                                    <legend>&nbsp</legend>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet-body -->
                            </div>
                        </div>
                        <!-- /.portlet-body -->
                    </div>
                    <!-- /.portlet -->

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</div>
<div class="hidden">
    <input type="text" class="form-control numeric_field" name="packageId" value="<?php
    echo set_value('packageId', isset($packageId) ? $packageId : '');
    ?>" />
    <input type="text" class="form-control numeric_field" name="couponCodeId" value="<?php
    echo set_value('couponCodeId', isset($couponCodeId) ? $couponCodeId : '');
    ?>" />
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Your Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows='5' name="user_comment" placeholder=""><?php if(isset($user_comment)) echo $user_comment;?></textarea>
            </div>
            <div class="modal-footer">
                <input type="submit" name="addComment" value="Add Comment" class="btn-primary btn"/>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>