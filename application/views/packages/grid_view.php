<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."pacakges/add_edit";
echo form_open_multipart($url);
?>



<section id="pricing">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">Our Pricing</h2>
            <p class="text-center wow fadeInDown">We offer competetive pricing and value added services.</p>
        </div>

        <div class="row">
            <?php
            foreach ($packages as $package) {
//                meDebug($package,1);
                ?>
            <div class="col-md-4">
                <ul class="price" style="width:100%">
                    <li class="header"><?php echo $package['pkg_name'];?></li>
                    <li class="grey"><?php echo $package['packageServices'];?></li>
                    <?php
                        echo $package['pkg_desc'];
                    ?>

                    <li class="grey"><a href="<?php echo base_url().'packages/view/'.$package['id'];?>" class="button w3-hover-green">Book Now</a></li>
                </ul>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</section><!--/#pricing-->
</div>
</form>