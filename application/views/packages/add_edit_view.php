<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isset($packageId) && $packageId != 0)
    $url = base_url()."packages/add_edit/".$packageId;
else
    $url = base_url()."packages/add_edit";
echo form_open_multipart($url);
if(!empty($packageDetails)) {
    $packageDetails['package_name'] = $packageDetails['pkg_name'];
    $packageDetails['package_price'] = $packageDetails['pkg_price'];
    $packageDetails['package_count'] = $packageDetails['availble_counts'];
    $packageDetails['package_desc'] = $packageDetails['pkg_desc'];

    $serImage = $packageDetails['pkg_image'];
    if(!empty($serImage)) {
        $imageArray = explode(".", $serImage);
        $packageDetails['cmpny_logo'] = !empty($imageArray) ? $imageArray[0].'_thumb.'.$imageArray[1] :  '';
    }

}
//meDebug($packageDetails, 1);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="col-lg-12">
                <div class="page-title">
                    <?php echo $this->layouts->print_breadcrumb(); ?><br>
                    <h1> <?php echo $page_title;?> </h1>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <br class="portlet-body">
                    <div class="row">
                        <center><legend>Package Details</legend></center>
                        <div class="col-lg-12">
                            <div class="col-md-4 required">
                                <label>Package Name</label>
                                <input type="text" class="form-control numeric_field" name="package_name" value="<?php
                                echo set_value('package_name', isset($packageDetails['package_name']) ? $packageDetails['package_name'] : '');
                                ?>" />
                                <?php echo form_error('package_name', '<div class="inline_error">', '</div>'); ?>
                            </div>

                            <div class="col-md-4 required">
                                <label>Package Available Counts</label>
                                <input type="text" class="form-control numeric_field" name="package_count" value="<?php
                                echo set_value('package_count', isset($packageDetails['package_count']) ? $packageDetails['package_count'] : '');
                                ?>" />
                                <?php echo form_error('package_count', '<div class="inline_error">', '</div>'); ?>
                            </div>

                            <div class="col-md-4 required">
                                <label>Package Price in Rupees</label>
                                <input type="text" class="form-control numeric_field" name="package_price" value="<?php
                                echo set_value('package_price', isset($packageDetails['package_price']) ? $packageDetails['package_price'] : '');
                                ?>" />
                                <?php echo form_error('package_price', '<div class="inline_error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <br></br>
                            <div class="col-md-12 ">
                                <label>Select Services
                                    <i class="required"> * </i></label>
                                <div class="controls">
                                    <?php

                                    if(!empty($servicesIdsArray)) {
                                        $servicesIdsArray = explode(",", $servicesIdsArray);
                                    }
                                    if(is_array($services) && !empty($services)) {
                                        foreach ($services as $key => $service) {
                                            $isChecked = (in_array($service['ser_id'], $servicesIdsArray)) ? "checked" : "" ;
                                            echo "<div class='col-lg-3'>";
                                            echo "<label><input type='checkbox' ".$isChecked." name='package_services[]' value=".$service['ser_id'].">   ".$service['ser_name']."</label>";
                                            echo "</div>";
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br></br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-6">
                                <label>Description</label>
                                <?php
                                if($this->input->post('package_desc')) {
                                    $package_desc = $this->input->post('package_desc');
                                }
                                else if(isset($packageDetails['package_desc'])) {
                                    $package_desc = $packageDetails['package_desc'];
                                }
                                else $package_desc = '';
                                ?>
                                <textarea class="textarea form-control" rows='5' name="package_desc" placeholder=""><?php if(isset($package_desc)) echo $package_desc;?></textarea>
                            </div>

                            <div class="col-md-6">
                                <?php
                                if (isset($packageDetails['cmpny_logo']) && !empty($packageDetails['cmpny_logo'])) {
                                    echo "<label>Package Image</label>";

                                    echo "<div class='row'>";
                                    echo "<div class='col-md-6'>";
                                    echo "<img for='files' class='medium_img rounded' src=".base_url().'assets/uploads/packages/thumb/'.$packageDetails['cmpny_logo'].' />';
                                    echo "</div>";
                                    echo "</div>";

                                    echo "<div>";
                                    echo '<input type="file" id="files" style="display:none !important;" class="hidden" value="" name="cmpny_logo" />';
                                    echo '<input type="text" class="hidden" value="'.$packageDetails['cmpny_logo'].'" name="saved_cmpny_logo" />';
                                    echo "<label for='files' class='btn btn-default'>Upload New Image</label>";
                                    echo "<span id='updating_logo'></span>";
                                    echo "</div>";

                                }
                                else {
                                    echo "<label>Package Image</label>";
                                    echo '<input type="file" class="form-control" value="" name="cmpny_logo" '.set_value('cmpny_logo','a.png').'/>';
                                }
                                ?>
                                <?php echo form_error('cmpny_logo', '<div class="inline_error">', '</div>'); ?>
                            </div>
                        </div>

                    </div>

                    <div class=" text-center col-lg-12">
                        <legend>&nbsp</legend>
                        <!-- <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>"> -->
                        <input type="submit" name="" value="Save package Details" class="btn btn-default">
                    </div>
                </div>
            </div>
            <!-- /.portlet-body -->
        </div>
        <!-- /.portlet -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /.page-content -->
</div>
</form>