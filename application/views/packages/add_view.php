<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 13-02-2021
 * Time: 01:03
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart (base_url().'packages/add');

if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';

}

if(isset($err_msg)){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';

}
if (isset($msg)) {
        echo ' <div class="alert alert-success row"><b>
        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}

?>

<div class="container">
    <?php
    if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
        echo '<ul class="breadcrumb row custom-breadcrumb">';
        $count = count($breadcrumb_array);
        foreach ($breadcrumb_array as $key => $value) {
            if (--$count <= 0) {
                echo "<li class='active'>".$key."</li>";
                break;
            }
            echo '<li><a href='.$value.'>'.$key.'</a></li>';
        }
        echo "</ul>";
    }
    ?>

    <div class="row well">
         <legend><label class="default_font_color">Create New Package</label>
            <input type="submit" name="" value="Submit" class="btn hoverable_btn pull-right" /></legend>
        <div class="row">
            <div class="col-md-6">
                <label>Package Name <i class="required"> * </i></label>
                <input name="pkg_name" class="form-control" type="text" value="<?php echo set_value('pkg_name'); ?>">
            </div>
            <div class="col-md-6">
                <label>Package price<i class="required"> * </i></label>
                <input name="pkg_price" class="form-control" type="text" value="<?php echo set_value('pkg_price'); ?>">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Select Services
                    <i class="required"> * </i></label>
                <div class="controls">
                    <?php foreach($services as $key=>$value) {?>
                        <input type="checkbox" name="pkg_services[]" value="<?php echo $services[$key]->ser_name ?>" >
                        <?php echo $services[$key]->ser_name ?>
                    <?php }?>
                </div>
            </div>
        </div>
        <!--   <div class="col-md-4">
            <label>Select Services
                <i class="required"> * </i></label>
          <div class="controls">
                <select name="pkg_services[]" multiple id="<?/* $services[$key]->ser_id*/?>">
                    <?php /*foreach($services as $key=>$value):*/?>
                        <?php /*$selected = in_array($services[$key]->ser_name,$services) ? " selected " : null;*/?>
                        <option value="<?/*=$services[$key]->ser_name*/?>"
                            <?/*=$selected*/?> ><?/*=$services[$key]->ser_name*/?>
                        </option>
                    <?php /*endforeach*/?>
                </select>
            </div>-->

        <div class="row">
            <div class="col-md-12">
                <label>Package description<i class="required"> * </i></label>
                <textarea name="pkg_desc" class="form-control" type="text" value="<?php echo set_value('pkg_desc'); ?>">
                </textarea>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <label class=""><input type="submit" name="" value="Submit" class="btn hoverable_btn"></label>
        </div>

    </div>


</div>
