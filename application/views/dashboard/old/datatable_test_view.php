<div id="page-wrapper">
  <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h1> <?php echo $page_title;?> </h1>
                        <?php echo $this->layouts->print_msg_div($this->session); ?>
                            <ol class="breadcrumb">
                                <li class="active" style="padding: 1%;"><i class="fa fa-dashboard"></i> Dashboard</li>
                                <li>Admin</li>
                                <li class="pull-right">
                                &nbsp
                                    <input type="submit" name="search" class="btn  btn-info" value="Search">
                                </li>
                                <li></li>
                                <li class="pull-right">
                                <?php
                                $frommonth = 0;
                                $frommonth = ($this->input->post('month'))? $this->input->post('month'):$frommonth;
                                
                                ?>
                                    <select name="month" class="form-control">
                                        <?= formsearchMonth($frommonth); ?>
                                    </select>
                                </li>
                            </ol>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
    <table id="example1" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th><!-- 
                <th>Position</th>
                <th>Office</th>
                <th>Start date</th>
                <th>Salary</th> -->
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>First name</th>
                <th>Last name</th><!-- 
                <th>Position</th>
                <th>Office</th>
                <th>Start date</th>
                <th>Salary</th> -->
            </tr>
        </tfoot>
    </table>
    </div>
    </div>

<?php echo $this->layouts->print_js(); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="
https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script><!-- 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#example1').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: "<?php echo base_url().'crud/fetch_user';?>",
            type: "POST"
        },
        "columnDefs" : [
        {
            "target": [0,3,4],
            "orderable" : false
        }]
    });
/*    $('#example1').DataTable( {
       // dom: 'Blfrtip',
        bFilter: true, //Removes search box.
       
        "oLanguage": {
      "sSearch": "Search here: "
    },
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "search": {
          "regex": true
        },
    "ajax": {
        "url": "../ajax/get_ajax_reports",
        "type":  'post',
        "data": function ( d ) {
            d.limit = 10;
            d.start = 0;
            // d.custom = $('#myInput').val();
            // etc
        }
    },
        "columns": [
            { "data": "month" },
            { "data": "id" },
            // { "data": "month" },
            // { "data": "Order_Time" },
            // { "data": "Last_Image_Arr_Time" },
            // { "data": "id" }
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });*/
});
</script>