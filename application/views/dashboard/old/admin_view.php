<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'dashboard/admin';
echo form_open($url,'id=account_signup');
?>
<div id="page-wrapper">
  <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                <div>
                    <div class="page-title">
                        <h1> <?php echo $page_title;?> </h1>
                        <?php echo $this->layouts->print_msg_div($this->session); ?>
                            <ol class="breadcrumb">
                                <li class="active" style="padding: 1%;">
                                    <i class="fa fa-dashboard"></i> 
                                    <?= lang('dashboard','ucword'); ?>
                                </li>
                                <li>
                                    <?= lang('admin','ucword'); ?>
                                </li>
                                <li class="pull-right">
                                &nbsp
                                    <input type="submit" name="search" class="btn  btn-info" value="Show Reports">
                                </li>
                                <li></li>
                                <li class="pull-right">
                                <?php
                                $frommonth = 0;
                                $frommonth = ($this->input->post('month'))? $this->input->post('month'):$frommonth;
                                
                                ?>
                                    <select name="month" class="form-control">
                                        <?= formsearchMonth($frommonth); ?>
                                    </select>
                                </li>
                            </ol>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

                            <section class="col-md-12 thumbnail tab-pane active" id="Analytics">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading red">
                                                <i class="fa fa-inr fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content red">
                                            <div class="circle-tile-description text-faded">
                                               <label>Total Enquiries</label>
                                           </div>
                                           <div class="circle-tile-number text-faded">
                                                10
                                            <span id="sparklineD"></span>
                                            </div>
                                            <!-- <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading green">
                                                <i class="fa fa-inr fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content green">
                                            <div class="circle-tile-description text-faded">
                                               <label>Total Vehicles</label>
                                           </div>
                                           <div class="circle-tile-number text-faded">
                                                10
                                            <span id="sparklineD"></span>
                                            </div>
                                            <!-- <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading orange">
                                                <i class="fa fa-inr fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content orange">
                                            <div class="circle-tile-description text-faded">
                                             <label>Drivers</label>
                                            </div>
                                             <div class="circle-tile-number text-faded">
                                                10
                                            </div>
                                            <!-- <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading purple">
                                                <i class="fa fa-credit-card fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content purple">
                                            <div class="circle-tile-description text-faded">
                                                <label>Trips</label>
                                            </div>
                                            <div class="circle-tile-number text-faded">
                                                10
                                            </div>
                                            <!-- <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>           
                            </section>
        </div>
    </div>