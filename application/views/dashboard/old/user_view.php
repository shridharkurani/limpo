<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'dashboard/user';
echo form_open($url,'id=account_signup');

?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h1> <?php echo $page_title;?> </h1>
                        <?php echo $this->layouts->print_breadcrumb(); ?>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->
        <?php
        
        if($this->session->flashdata('msg')){

                echo ' <div class="alert alert-success row error_msgs"><b>

                <a href="#" class="close" data-dismiss="alert">&times;</a>';
            echo $this->session->flashdata('msg');
            echo '</b></div>';
        }
        if($this->session->flashdata('errormsg')){ 
                echo ' <div class="alert alert-danger row error_msgs"><b>

                <a href="#" class="close" data-dismiss="alert">&times;</a>';
            echo $this->session->flashdata('errormsg'); 
            echo '</b></div>';

        }

        if(isset($err_msg)){ 
                echo ' <div class="alert alert-danger row error_msgs"><b>

                <a href="#" class="close" data-dismiss="alert">&times;</a>';
            echo $err_msg; 
            echo '</b></div>';

        }
        if (isset($msg)) {
                echo ' <div class="alert alert-success row"><b>
                <a href="#" class="close" data-dismiss="alert">&times;</a>';
            echo $msg;
            echo '</b></div>';
        }
        ?>
		<?php  echo form_open($url,'id=account_signin');  ?>
		<div class="row well">
		<div class="page-content">
                <!-- begin DASHBOARD CIRCLE TILES -->
                <div class="row">
                    <!-- <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-users fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    Users
                                </div>
                                <div class="circle-tile-number text-faded">
                                    265
                                    <span id="sparklineA"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-money fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    Department
                                </div>
                                <div class="circle-tile-number text-faded">
                                    $32,384
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-bell fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    Complaints
                                </div>
                                <div class="circle-tile-number text-faded">
                                    9 New
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-tasks fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                On going
                                </div>
                                <div class="circle-tile-number text-faded">
                                    10
                                    <span id="sparklineB"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-shopping-cart fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                   Completed
                                </div>
                                <div class="circle-tile-number text-faded">
                                    24
                                    <span id="sparklineC"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-comments fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                      Rejected 
                                </div>
                                <div class="circle-tile-number text-faded">
                                    96
                                    <span id="sparklineD"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end DASHBOARD CIRCLE TILES -->

              
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4><i class="fa fa-map-marker fa-fw"></i> ALL Complaints list</h4>
                                        </div>
                                   
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="portlet-body">

                                        <br>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover table-green" id="map-table-example">
                                                <thead>
                                                    <tr>
													<th>ID</th>
                                                         <th>Name</th>
                                                            <th>Email Id</th>
                                                            <th>Mobile No</th>
															 <th>Ward no</th>
															 	 <th>Arear Name</th>
															 
															   <th>Department</th>
															    <th>Category</th>
															
															<th>Pin code</th>
                                                            <th>Status</th>
														 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
													 <td><strong>02</strong></td>
                                                         <td>Arun</td>
                                                            <td>chalam09@gmail.com</td>
                                                            <td>9886453038</td>
															 <td>Arun</td>
															  <td>Arun</td>
															 
															 
															    <td>BESCOME</td>
																 <td>Street Light</td>
																 
															<td>560099</td>
                                                          <td>   <a href="member_viewmore.html" class="btn btn-xs btn-orange"><i class="fa fa-clock-o"></i> View More</a>
														  
														
                                                            </td>
                                                    </tr>

                                                
                                                
                                                 
                                                 
                                              
                                              
                                              
													
													 

                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
		</div>
		<?php echo form_close(); ?>

	</div>
</div>