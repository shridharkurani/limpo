<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'dashboard/admin';
echo form_open($url,'id=account_signup');
?>
<div id="page-wrapper">
  <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h1> <?php echo $page_title;?> </h1>
                        <?php echo $this->layouts->print_msg_div($this->session); ?>
                            <ol class="breadcrumb">
                                <li class="active" style="padding: 1%;"><i class="fa fa-dashboard"></i> Dashboard</li>
                                <li>Admin</li>
                                <li class="pull-right">
                                &nbsp
                                    <input type="submit" name="search" class="btn  btn-info" value="Search">
                                </li>
                                <li></li>
                                <li class="pull-right">
                                <?php
                                $frommonth = 0;
                                $frommonth = ($this->input->post('month'))? $this->input->post('month'):$frommonth;
                                
                                ?>
                                    <select name="month" class="form-control">
                                        <?= formsearchMonth($frommonth); ?>
                                    </select>
                                </li>
                            </ol>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->
                <!-- begin DASHBOARD CIRCLE TILES -->
               <!--  <table id="ajaxDatatable" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>ZIP / Post code</th>
                            <th>Country</th>
                        </tr>
                    </thead>
                </table> -->
                <div class="section row">
                    <div class="">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a id="how_it_works_btn" class="hoverable_btn" href="#Analytics" data-toggle="tab" aria-expanded="true">Statastics
                            <br>&nbsp</a></li>
                            <li class=""><a href="#Hospitals" data-toggle="tab" class="hoverable_btn" aria-expanded="false">New Hospitals <br>&nbsp</a></li>
                            <li class=""><a href="#Radiologist" data-toggle="tab" class="hoverable_btn" aria-expanded="false">New Radiologist <br>&nbsp</a></li>
                            <li class=""><a href="#Notification" data-toggle="tab" class="hoverable_btn" aria-expanded="false"><?= lang('notification','ucword') ;?><br>&nbsp</a></li>
                            <li class=""><a href="#Invoice" data-toggle="tab" class="hoverable_btn" aria-expanded="false">Demo Invoice <br>&nbsp</a></li>
                            <li class=""><a href="#Status" data-toggle="tab" class="hoverable_btn" aria-expanded="false">Report Status Not In Our List</a></li>
                        </ul>

                        <div class="tab-content">
                            <section class="col-md-12 thumbnail tab-pane active" id="Analytics">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading green">
                                                <i class="fa fa-hospital-o fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content green">
                                            <div class="circle-tile-description text-faded">
                                                Total No of Hospital
                                            </div>
                                            <div class="circle-tile-number text-faded">
                                                <?= $stats['total_hospital'] ?>
                                                <span id="sparklineA"></span>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading green">
                                                <i class="fa fa-hospital-o fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content green">
                                            <div class="circle-tile-description text-faded">
                                                New Hospitals
                                            </div>
                                            <div class="circle-tile-number text-faded">
                                                <?= $stats['new_hospital'] ?>                   
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading red">
                                                <i class="fa fa-file-text-o fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content red">
                                            <div class="circle-tile-description text-faded">
                                                No. of Reports
                                            </div>
                                            <div class="circle-tile-number text-faded">
                                                <?= $stats['total_reports'] ?>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading blue">
                                                <i class="fa fa-pencil fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content blue">
                                            <div class="circle-tile-description text-faded">
                                                No of Studies (With Test)
                                            </div>
                                            <div class="circle-tile-number text-faded">
                                                <?= $stats['total_study_with_test'] ?>                   
                                                
                                                <span id="sparklineB"></span>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading blue">
                                                <i class="fa fa-files-o fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content blue">
                                            <div class="circle-tile-description text-faded">
                                              No of Study (Without Test)
                                          </div>
                                          <div class="circle-tile-number text-faded">
                                                <?= $stats['total_study_without_test'] ?>
                                            <span id="sparklineC"></span>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading orange">
                                                <i class="fa fa-inr fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content orange">
                                            <div class="circle-tile-description text-faded">
                                               Total Income from Reported Hospital Group
                                           </div>
                                           <div class="circle-tile-number text-faded">
                                                <?= $stats['total_report_income'] ?>
                                            <span id="sparklineD"></span>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading orange">
                                                <i class="fa fa-inr fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content orange">
                                            <div class="circle-tile-description text-faded">
                                             Total Income From Study Group
                                            </div>
                                             <div class="circle-tile-number text-faded">
                                                <?= $stats['total_study_income'] ?>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="circle-tile">
                                        <a href="#">
                                            <div class="circle-tile-heading purple">
                                                <i class="fa fa-credit-card fa-fw fa-3x"></i>
                                            </div>
                                        </a>
                                        <div class="circle-tile-content purple">
                                            <div class="circle-tile-description text-faded">
                                                Total Payment to doctors
                                            </div>
                                            <div class="circle-tile-number text-faded">
                                                <?= $stats['total_doctor_payment'] ?>
                                            </div>
                                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                    </div>
                                </div>           
                            </section>
                            <section class="col-md-12 thumbnail tab-pane " id="Hospitals">
                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4><i class=""></i>New Hospitals</h4>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <br>
                                        <div class="table-responsive">
                                            <table class="MyDatatable table-striped table-bordered table-hover table-green" id="map-table-example">
                                                <thead>
                                                    <tr>
                                                       <th>Serial</th>
                                                       <th>Hospital Name</th>
                                                       <th>Total Report</th>
                                                       <th>Total Study</th>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                                   <?php
                                                   if(is_array($new_hospital_list) && !empty($new_hospital_list)) {
                                                    $cnt = 1;
                                                       foreach ($new_hospital_list as $key => $value) {
                                                       ?>
                                                       <tr>
                                                         <td><?= $cnt; $cnt++; ?></td>
                                                         <td><?= $value->name; ?></td>
                                                         <td><?= $value->total_report; ?></td>
                                                         <td><?= $value->total_studies; ?></td>
                                                     </tr>
                                                     <?php
                                                        }
                                                    }
                                             ?>
                                         </tbody>
                                     </table>
                                 </div>
                                 </div>
                             </div>
                            </section>

                            <section class="col-md-12 thumbnail tab-pane " id="Radiologist">
                                    <div class="portlet portlet-blue">
                                        <div class="portlet-heading">
                                            <div class="portlet-title">
                                                <h4><i class=""></i>New Radiologist</h4>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="portlet-body">
                                            <br>
                                            <div class="table-responsive">
                                                <table class="MyDatatable table table-striped table-bordered table-hover table-green" id="map-table-example">
                                                    <thead>
                                                        <tr>
                                                           <th>Serial</th>
                                                           <th>Name</th>
                                                           <th>Total Report</th>
                                                       </tr>
                                                   </thead>
                                                   <tbody>
                                                   <?php
                                                   if(is_array($new_radiologist_list) && !empty($new_radiologist_list)) {
                                                    $cnt = 1;
                                                       foreach ($new_radiologist_list as $key => $value) {
                                                       ?>
                                                       <tr>
                                                         <td><?= $cnt; $cnt++; ?></td>
                                                         <td><?= $value->name; ?></td>
                                                         <td><?= $value->total_report; ?></td>
                                                     </tr>
                                                     <?php
                                                        }
                                                    }
                                             ?>
                                                 </tbody>
                                             </table>
                                         </div>
                                     </div>
                                 </div>
                            </section>

                            <section class="col-md-12 thumbnail tab-pane " id="Notification">
                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4><i class=""></i>Notification - These Hospitals Report - Price is Not Set.</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <br>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover table-green" id="map-table-example">
                                                <tr>    
                                                    <td>                            
                                                        <b>Hospital </b> 
                                                        <select name="hospitalname" id="hospital_id" class="" style="padding: 3px;">
                                                        <option value="0">Select Hospital</option>
                                                        <option value="88">CEMAIN</option>
                                                        <option value="38">HOSPITAL PRIVADO SANTA FE</option>
                                                        <option value="127">IDM OAXACA</option>
                                                    </select>                               
                                                    <button class="btn btn-success" name="Search" type="button" id="Search">Search</button> 
                                                    <input type="hidden" name="searchAction" value="" id="searchAction">                            
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tbody>
                                               <?php
                                               $variable = array('1');
                                               foreach ($variable as $key => $value) {
                                               ?>
                                               <tr>
                                                 <td><?= $value; ?></td>
                                                 <td><?= $value; ?></td>
                                             </tr>
                                             <?php
                                         }
                                         ?>
                                     </tbody>
                                 </table>
                                </div>
                            </section>

                            <section class="col-md-12 thumbnail tab-pane " id="Invoice">

                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4><i class=""></i>Demo Invoice</h4>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <br>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover table-green" id="map-table-example">
                                                <thead>
                                                    <tr>
                                                       <th>Serial</th>
                                                       <th>Hospital Name</th>
                                                       <th>Study & Report Status</th>
                                                       <th>Report</th>
                                                       <td>Study</td>
                                                       <td>Reports Charge</td>
                                                       <td>Study Charge</td>
                                                       <td>Total</td>
                                                       <td>Invoice Date</td>
                                                       <td>Month</td>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                                   <?php
                                                   $variable = array('Demo Invoice');
                                                   foreach ($variable as $key => $value) {
                                                   ?>
                                                   <tr>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                     <td><?= $value; ?></td>
                                                 </tr>
                                                 <?php
                                             }
                                             ?>
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                         </div>
                            </section>

                            <section class="col-md-12 thumbnail tab-pane " id="Status">
                            <div class="portlet portlet-blue">
                                <div class="portlet-heading">
                                    <div class="portlet-title">
                                        <h4><i class=""></i>Report Status Not In Our List</h4>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="portlet-body">
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover table-green" id="map-table-example">
                                            <thead>
                                                <tr>
                                                   <th>Serial</th>
                                                   <th>Report Name</th>
                                                   <th>Modality</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <?php
                                               $variable = array('Report Status Not In Our List');
                                               foreach ($variable as $key => $value) {
                                               ?>
                                               <tr>
                                                 <td><?= $value; ?></td>
                                                 <td><?= $value; ?></td>
                                                 <td><?= $value; ?></td>
                                             </tr>
                                             <?php
                                         }
                                         ?>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>
                            </section>
                        </div>
                    
                    </div>
                </div>
                <!-- end DASHBOARD CIRCLE TILES -->
</div>

<!-- /#page-wrapper -->
<!-- end MAIN PAGE CONTENT -->
</div>

<?php echo $this->layouts->print_js(); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="
https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script><!-- 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
    $('.MyDatatable').DataTable( {
       // dom: 'Blfrtip',
        bFilter: true, //Removes search box.
       
        "oLanguage": {
      "sSearch": "Search here: "
    },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
});
</script>