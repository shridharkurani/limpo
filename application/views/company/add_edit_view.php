<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."company/add_edit";
echo form_open_multipart($url);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <!-- <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <center><legend>Company / Organization Details</legend></center>
                                    <div class="col-lg-12">
                                        <div  class="col-md-4 required">
                                          <label>Company / Organization Name</label>
                                          <input type="text" class="form-control" name="cmpny_name" value="<?php 
                                            echo set_value('cmpny_name', isset($cmpnyDetails['cmpny_name']) ? $cmpnyDetails['cmpny_name'] : '');
                                             ?>" />
                                          <?php echo form_error('cmpny_name', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Company Mobile Number</label>
                                          <input type="text" class="form-control numeric_field" name="cmpny_mobile" value="<?php
                                          echo set_value('cmpny_mobile', isset($cmpnyDetails['cmpny_mobile']) ? $cmpnyDetails['cmpny_mobile'] : '');
                                          ?>" />
                                          <?php echo form_error('cmpny_mobile', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4">
                                          <label>Alternative Mobile Number</label>
                                          <input type="text" class="form-control numeric_field" name="cmpny_alt_mobile" value="<?php
                                          echo set_value('cmpny_alt_mobile', isset($cmpnyDetails['cmpny_alt_mobile']) ? $cmpnyDetails['cmpny_alt_mobile'] : '');
                                          ?>" />
                                          <?php echo form_error('cmpny_alt_mobile', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">


                                        <div class="col-md-4">
                                          <label>Company Email</label>
                                          <input type="text" class="form-control numeric_field" name="cmpny_email" value="<?php
                                          echo set_value('cmpny_email', isset($cmpnyDetails['cmpny_email']) ? $cmpnyDetails['cmpny_email'] : '');
                                          ?>" />
                                          <?php echo form_error('cmpny_email', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php
                                                if (isset($cmpnyDetails['cmpny_logo']) && !empty($cmpnyDetails['cmpny_logo'])) {
                                                  $img_url_arr = explode(".", $cmpnyDetails['cmpny_logo']);
                                                echo "<label>Company Logo</label>";

                                                echo "<div class='row'>";
                                                    echo "<div class='col-md-6'>";
                                                        echo "<img for='files' class='medium_img rounded' src=".base_url().'assets/uploads/company/thumb/'.$img_url_arr[0].'_thumb.'.$img_url_arr[1].' />';
                                                    echo "</div>";
                                                echo "</div>";

                                                    echo "<div>";
                                                        echo '<input type="file" id="files" style="display:none !important;" class="hidden" value="" name="cmpny_logo" />';
                                                        echo '<input type="text" class="hidden" value="'.$cmpnyDetails['cmpny_logo'].'" name="saved_cmpny_logo" />';
                                                        echo "<label for='files' class='btn btn-default'>Upload New Logo</label>";
                                                        echo "<span id='updating_logo'></span>";
                                                    echo "</div>";

                                                }
                                                else {
                                                echo "<label>Company Logo</label>";
                                                echo '<input type="file" class="form-control" value="" name="cmpny_logo" '.set_value('cmpny_logo','a.png').'/>';
                                                }
                                            ?>
                                          <?php echo form_error('cmpny_logo', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4">
                                          <label>Company Address</label>
                                          <?php
                                            if($this->input->post('cmpny_address')) {
                                                $cmpny_address = $this->input->post('cmpny_address');
                                            }
                                            else if(isset($cmpnyDetails['cmpny_address'])) {
                                                $cmpny_address = $cmpnyDetails['cmpny_address'];
                                            }
                                            else $cmpny_address = '';
                                            ?>
                                            <textarea class="form-control" rows='5' name="cmpny_address" placeholder=""><?php if(isset($cmpny_address)) echo $cmpny_address;?></textarea>
                                          </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <center><legend>Other Details</legend></center>
                                    <div  class="col-md-6">
                                      <div class="col-lg-12">
                                          <label><input type="checkbox" name="is_contact_person_owner" value="1" <?php if(isset($cmpnyDetails['is_contact_person_owner']) && !(empty($cmpnyDetails['is_contact_person_owner']))) echo "checked";?> >&nbsp Contact Owner for communications</label>
                                      </div>
                                      <div  class="col-md-12">
                                        <label>Contact Person Name</label>
                                        <input type="text" class="form-control" name="contact_person_name" value="<?php 
                                          echo set_value('contact_person_name', isset($cmpnyDetails['contact_person_name']) ? $cmpnyDetails['contact_person_name'] : '');
                                           ?>" />
                                        <?php echo form_error('contact_person_name', '<div class="inline_error">', '</div>'); ?>
                                      </div>
                                      <div class="col-md-12">
                                        <label>Contact Person  Mobile Number</label>
                                        <input type="text" class="form-control numeric_field" name="contact_person_mobile" value="<?php
                                        echo set_value('contact_person_mobile', isset($cmpnyDetails['contact_person_mobile']) ? $cmpnyDetails['contact_person_mobile'] : '');
                                        ?>" />
                                        <?php echo form_error('contact_person_mobile', '<div class="inline_error">', '</div>'); ?>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                          <label>Company Description</label>
                                          <?php
                                            if($this->input->post('cmpny_desc')) {
                                                $cmpny_desc = $this->input->post('cmpny_desc');
                                            }
                                            else if(isset($cmpnyDetails['cmpny_desc'])) {
                                                $cmpny_desc = $cmpnyDetails['cmpny_desc'];
                                            }
                                            else $cmpny_desc = '';
                                            ?>
                                            <textarea class="form-control" rows='6' name="cmpny_desc" placeholder=""><?php if(isset($cmpny_desc)) echo $cmpny_desc;?></textarea>
                                    </div>
                                <div class=" text-center col-lg-12">
                                    <legend>&nbsp</legend>
                                    <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>">
                                    <input type="submit" name="" value="Save Details" class="btn btn-default">
                                </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>