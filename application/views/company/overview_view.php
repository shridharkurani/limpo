<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."company/overview";
echo form_open_multipart($url);
// meDebug($this->session->all_userdata(),true);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br> <a class="pull-right btn btn-primary" href="<?php echo base_url().'company/add_edit';?>">Edit Company Profile</a>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <!-- <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                     <center><legend>Company / Organization Details</legend></center>
                                        <div class="row">
                                          <label class="col-md-6">Company / Organization Name</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo (isset($cmpnyDetails['cmpny_name']) ? ucwords($cmpnyDetails['cmpny_name']) : ' - ');
                                            ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Mobile Number</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo (isset($cmpnyDetails['cmpny_mobile']) ? $cmpnyDetails['cmpny_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Email</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo (isset($cmpnyDetails['cmpny_email']) ? $cmpnyDetails['cmpny_email'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Alternative Mobile Number</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo (isset($cmpnyDetails['cmpny_alt_mobile']) ? $cmpnyDetails['cmpny_alt_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Address</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo (isset($cmpnyDetails['cmpny_address']) ? $cmpnyDetails['cmpny_address'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                     <center><legend>Other Details</legend></center>
                                        <div class="row">
                                          <label class="col-md-6">Contact Person Name</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo ((isset($cmpnyDetails['contact_person_name']) && !empty($cmpnyDetails['contact_person_name'])) ? ucwords($cmpnyDetails['contact_person_name']) : ' - ');
                                            ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Contact Person Mobile Number</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo ((isset($cmpnyDetails['contact_person_mobile']) && !empty($cmpnyDetails['contact_person_mobile'])) ? $cmpnyDetails['contact_person_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Create Date</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo ((isset($cmpnyDetails['created_date']) && !empty($cmpnyDetails['created_date'])) ? date('d-m-Y',strtotime($cmpnyDetails['created_date'])) : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Created By</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo ((isset($cmpnyDetails['cmpny_alt_mobile']) && !empty($cmpnyDetails['cmpny_alt_mobile'])) ? $cmpnyDetails['cmpny_alt_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                     <center><legend>Company Description</legend></center>
                                     <?php 
                                        echo (isset($cmpnyDetails['cmpny_desc']) ? $cmpnyDetails['cmpny_desc'] : ' ');
                                      ?>                                    
                                   </div>
                                 </div>

                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>