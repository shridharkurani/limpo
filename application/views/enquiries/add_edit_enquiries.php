<?php
/**
 * Created by PhpStorm.
 * User: faisad.ali
 * Date: 23/06/2018
 * Time: 02:55
 */
?>
<!DOCTYPE html>
<html lang="en">

<?php echo $this->load->view('home/header'); ?>
<link href="<?php echo base_url(); ?>assets/css/custom/enquiry.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>assets/css/plugins/bootstrap-datepicker/datepicker3.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>assets/css/plugins/formvalidation.bootstrap/formValidation.css" rel="stylesheet"/>
<body>

<?php echo $this->load->view('home/top_panel'); ?>

<?php echo $this->load->view('home/left_panel'); ?>

<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <ul class="breadcrumb custom-breadcrumb">
                        <li><a href=<?php echo base_url(); ?>dashboard/><i class='fa fa-dashboard'> </i> Dashboard</a>
                        </li>
                        <li class='active'>Trip Detailssssssssss</li>
                    </ul>
                    <br>
                    <h1></h1>
                </div>
            </div>
            <div class="col-lg-12">
                <?php
                if (isset($enquiryId) && $enquiryId != 0){
                    $url = base_url() . "enquiries/add_edit/" . $enquiryId;
                }else{
                    $url = base_url() . "enquiries/add_edit";
                }
                ?>
                <form action="<?php echo $url; ?>" method="post" accept-charset="utf-8"
                      enctype="multipart/form-data" autocomplete="off" id="enquiry-form">
                    <input type="hidden" name="company_id" id="company_id" value="<?php echo $this->session->userdata('cmpnyId'); ?>"/>
                    <div class="portlet portlet-default">
                        <div class="portlet-body">
                            <div class="row">
                                <legend class="text-center">Trip Details</legend>
                                <div class="col-lg-12">
                                    <!-- Step Circle -->
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step col-xs-4">
                                                <a href="#basic" type="button" class="btn btn-success btn-circle">1</a>
                                                <p>
                                                    <small>Basic Details</small>
                                                </p>
                                            </div>
                                            <div class="stepwizard-step col-xs-4">
                                                <a href="#price" type="button" class="btn btn-default btn-circle"
                                                   disabled="disabled">2</a>
                                                <p>
                                                    <small>Price Details</small>
                                                </p>
                                            </div>
                                            <div class="stepwizard-step col-xs-4">
                                                <a href="#payment" type="button" class="btn btn-default btn-circle"
                                                   disabled="disabled">3</a>
                                                <p>
                                                    <small>Payment Details</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Trip Enquiry From -->
                                    <div class="panel panel-primary setup-content" id="basic">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Basic Details</h3>
                                        </div>
                                        <div class="panel-body row">
                                            <div class="col-lg-12">
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Start Date</label>
                                                    <?php
                                                    if(isset($enquiryDetails['start_date'])){
                                                        $date1 = new DateTime($enquiryDetails['start_date']);
                                                        $start_date = $date1->format('m/d/Y');
                                                    }else{
                                                        $start_date = '';
                                                    }
                                                    ?>
                                                    <input type="text" name="start_date" id="start_date"
                                                           class="form-control"
                                                           value="<?php
                                                           echo set_value('start_date', !empty($start_date) ? $start_date : '');
                                                           ?>" required="required" readonly/>
                                                    <?php echo form_error('start_date', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">End Date</label>
                                                    <?php
                                                    if(isset($enquiryDetails['end_date'])){
                                                        $date1 = new DateTime($enquiryDetails['end_date']);
                                                        $end_date = $date1->format('m/d/Y');
                                                    }else{
                                                        $end_date = '';
                                                    }
                                                    ?>
                                                    <input type="text" name="end_date" id="end_date"
                                                           class="form-control" value="<?php
                                                    echo set_value('end_date', !empty($end_date) ? $end_date : '');
                                                    ?>" required="required" readonly/>
                                                    <?php echo form_error('end_date', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Seaters</label>
                                                    <input type="number" name="seaters" class="form-control" value="<?php
                                                    echo set_value('seaters', isset($enquiryDetails['seaters']) ? $enquiryDetails['seaters'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('seaters', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-md-4 required">
                                                    <label class="control-label">From</label>
                                                    <input type="text" name="trip_from" id="trip_from" class="form-control"
                                                           value="<?php
                                                           echo set_value('trip_from', isset($enquiryDetails['trip_from']) ? $enquiryDetails['trip_from'] : '');
                                                           ?>" required="required"/>
                                                    <?php echo form_error('trip_from', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Via</label>
                                                    <input type="text" name="trip_via" id="trip_via" class="form-control" value="<?php
                                                    echo set_value('trip_via', isset($enquiryDetails['trip_via']) ? $enquiryDetails['trip_via'] : '');
                                                    ?>"/>
                                                    <?php echo form_error('trip_via', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">To</label>
                                                    <input type="text" name="trip_to" id="trip_to" class="form-control" value="<?php
                                                    echo set_value('trip_to', isset($enquiryDetails['trip_to']) ? $enquiryDetails['trip_to'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('trip_to', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Vehicle</label>
                                                    <select name="vehicle" id="vehicle" class="form-control"
                                                            value="<?php echo set_select('vehicle'); ?>">
                                                        <option value="0">Select your vehicle</option>
                                                        <?php
                                                        if ($this->input->post('vehicle')) {
                                                            $vehicle = $this->input->post('vehicle');
                                                        } else if (isset($vehicleDetails['vehicle_number'])) {
                                                            $vehicle = $vehicleDetails['vehicle_number'];
                                                        } else {
                                                            $vehicle = '';
                                                        }
                                                        foreach ($vehicleDetails as $key => $value) {
                                                            $valId = strtolower($value['id']);
                                                            $vehicleNumber = strtoupper($value['vehicle_number']);
                                                            echo '<option id = '.$value['model_id'].' value=' . $valId . '' . set_select("vehicle", $vehicleNumber, ($vehicle === $valId ? TRUE : FALSE)) . '>';
                                                            echo strtoupper($vehicleNumber) . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <!-- <div class="col-md-3 required">
                                                    <label class="control-label">Vehicle Type</label>
                                                    <select name="vehicle_type" id="vehicle_type" class="form-control"
                                                            value="<?php echo set_select('vehicle_type'); ?>">
                                                        <option value="0">Select</option>
                                                        <?php
                                                        if ($this->input->post('vehicle_type')) {
                                                            $vehicle_type = $this->input->post('vehicle_type');
                                                        } else if (isset($enquiryDetails['model_type'])) {
                                                            $vehicle_type = $enquiryDetails['model_type'];
                                                        } else {
                                                            $vehicle_type = '';
                                                        }
                                                        foreach ($modelTypeDetails as $key => $value) {
                                                            $valId = strtolower($value['model_type']);
                                                            echo '<option value=' . $valId . '' . set_select("vehicle_type", $valId, ($vehicle_type === $valId ? TRUE : FALSE)) . '>';
                                                            echo strtoupper($valId) . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Vehicle Model</label>
                                                    <select name="vehicle_model" id="vehicle_model" class="form-control"
                                                            value="<?php echo set_select('vehicle_model'); ?>">
                                                        <option value="0">Select Vehicle Type</option>
                                                        <?php
                                                        if ($this->input->post('vehicle_model')) {
                                                            $vehicle_model = $this->input->post('vehicle_model');
                                                        } else if (isset($enquiryDetails['model_id'])) {
                                                            $vehicle_model = $enquiryDetails['model_id'];
                                                        }
                                                        // meDebug($enquiryDetails, true);
                                                        if (isset($modelNameDetails)) {
                                                            foreach ($modelNameDetails as $key => $value) {
                                                                $val = $value['model_name'];
                                                                $valId = $value['model_id'];
                                                                echo '<option value=' . $valId . '' . set_select("vehicle_model", $valId, ($vehicle_model === $valId ? TRUE : FALSE)) . '>';
                                                                echo strtoupper($val) . '</option>';
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php echo form_error('vehicle_model', '<div class="inline_error">', '</div>'); ?>
                                                </div> -->
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Minimum Km. Per Day</label>
                                                    <input type="number" class="form-control" name="min_km" id="min_km" value="<?php
                                                    echo set_value('min_km', isset($enquiryDetails['min_km']) ? $enquiryDetails['min_km'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('min_km', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Approx Km. for this trip</label>
                                                    <input type="number" class="form-control" name="approx_km"
                                                           value="<?php
                                                           echo set_value('approx_km', isset($enquiryDetails['approx_km']) ? $enquiryDetails['approx_km'] : '');
                                                           ?>" required="required"/>
                                                    <?php echo form_error('approx_km', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <legend class="text-center">Customer Details</legend>
                                                <div class="col-md-3 required">
                                                    <label class="control-label">Customer Name</label>
                                                    <input type="hidden" name="customer_id" value="<?php echo isset($enquiryDetails['customer_id']) ? $enquiryDetails['customer_id'] : ''; ?>">
                                                    <input type="text" class="form-control " name="customer_name"
                                                           value="<?php
                                                           echo set_value('customer_name', isset($enquiryDetails['customer_name']) ? $enquiryDetails['customer_name'] : '');
                                                           ?>" required="required"/>
                                                    <?php echo form_error('customer_name', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-3 required">
                                                    <label class="control-label">Customer Mobile</label>
                                                    <input type="text" class="form-control" name="customer_mobile"
                                                           value="<?php
                                                           echo set_value('customer_mobile', isset($enquiryDetails['customer_mobile']) ? $enquiryDetails['customer_mobile'] : '');
                                                           ?>" required="required"/>
                                                    <?php echo form_error('customer_mobile', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Customer Alternative Mobile</label>
                                                    <input type="text" class="form-control " name="customer_alt_mobile"
                                                           value="<?php
                                                           echo set_value('customer_alt_mobile', isset($enquiryDetails['customer_alt_mobile']) ? $enquiryDetails['customer_alt_mobile'] : '');
                                                           ?>"/>
                                                    <?php echo form_error('customer_alt_mobile', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Customer Email</label>
                                                    <input type="text" class="form-control " name="customer_email"
                                                           value="<?php
                                                           echo set_value('customer_email', isset($enquiryDetails['customer_email']) ? $enquiryDetails['customer_email'] : '');
                                                           ?>"/>
                                                    <?php echo form_error('customer_email', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <br/>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="col-lg-12">
                                                    <button class="btn btn-primary nextBtn pull-right step1"
                                                            type="button">
                                                        Next
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-primary setup-content" id="price">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Price Details</h3>
                                        </div>
                                        <div class="panel-body row">
                                            <div class="col-lg-12">
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Price Per Km.</label>
                                                    <input type="number" class="form-control numeric_field" name="per_km"
                                                           value="<?php
                                                           echo set_value('per_km', isset($enquiryDetails['per_km']) ? $enquiryDetails['per_km'] : '');
                                                           ?>" required="required" />
                                                    <?php echo form_error('price_per_km', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Price Per Km. for A/C</label>
                                                    <input type="number" class="form-control numeric_field"
                                                           name="per_km_ac" value="<?php
                                                    echo set_value('per_km_ac', isset($enquiryDetails['per_km_ac']) ? $enquiryDetails['per_km_ac'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('per_km_ac', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Driver Allowance</label>
                                                    <input type="number" class="form-control numeric_field"
                                                           name="driver_allowance" value="<?php
                                                    echo set_value('driver_allowance', isset($enquiryDetails['driver_allowance']) ? $enquiryDetails['driver_allowance'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('driver_allowance', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <legend class="text-center">Extra Charge Details</legend>
                                            <div class="col-lg-12">
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Price per extra Km.</label>
                                                    <input type="number" class="form-control numeric_field" name="extra_per_km" value="<?php
                                                    echo set_value('extra_per_km', isset($enquiryDetails['extra_per_km']) ? $enquiryDetails['extra_per_km'] : '');
                                                    ?>"  required="required"/>
                                                    <?php echo form_error('extra_per_km', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Price per extra Km. for A/C</label>
                                                    <input type="number" class="form-control numeric_field" name="extra_per_km_ac" value="<?php
                                                    echo set_value('extra_per_km_ac', isset($enquiryDetails['extra_per_km_ac']) ? $enquiryDetails['extra_per_km_ac'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('extra_per_km_ac', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                                <div class="col-md-4 required">
                                                    <label class="control-label">Driver Allowance per Night</label>
                                                    <input type="number" class="form-control numeric_field" name="driver_allowance_night" value="<?php
                                                    echo set_value('driver_allowance_night', isset($enquiryDetails['driver_allowance_night']) ? $enquiryDetails['driver_allowance_night'] : '');
                                                    ?>" required="required"/>
                                                    <?php echo form_error('driver_allowance_night', '<div class="inline_error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-12">
                                                    <button class="btn btn-primary prevBtn pull-right hidden"
                                                            type="button">
                                                        Prev
                                                    </button>
                                                    <button class="btn btn-primary nextBtn pull-right step2"
                                                            type="button">
                                                        Next
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-primary setup-content" id="payment">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Payment Details</h3>
                                        </div>
                                        <div class="panel-body row">
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <div class="portlet portlet-blue">
                                                        <div class="portlet-heading">
                                                            <div class="portlet-title">
                                                                <h4>Total Payment for Non A/C.</h4>
                                                            </div>
                                                            <div class="portlet-widgets">
                                                                <a data-toggle="collapse" data-parent="#accordion"
                                                                   href="#inlineFormExample">
                                                                    <i class="fa fa-chevron-down"></i>
                                                                </a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div id="inlineFormExample"
                                                             class="panel-collapse collapse in">
                                                            <div class="portlet-body inline-block">
                                                                <div class="col-lg-6">
                                                                    Approx Km.
                                                                </div>
                                                                <div class="col-lg-6" id="approx_km1">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Price per Km. -
                                                                </div>
                                                                <div class="col-lg-6" id="price_per_km1">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Driver Allowance
                                                                </div>
                                                                <div class="col-lg-6" id="driver_allowance1">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Number of Days
                                                                </div>
                                                                <div class="col-lg-6" id="number_of_days1">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Total
                                                                </div>
                                                                <div class="col-lg-6" id="total1">
                                                                    -
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.portlet -->
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="portlet portlet-blue">
                                                        <div class="portlet-heading">
                                                            <div class="portlet-title">
                                                                <h4>Total Payment for A/C.</h4>
                                                            </div>
                                                            <div class="portlet-widgets">
                                                                <a data-toggle="collapse" data-parent="#accordion"
                                                                   href="#inlineFormExample"><i
                                                                            class="fa fa-chevron-down"></i></a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div id="inlineFormExample"
                                                             class="panel-collapse collapse in">
                                                            <div class="portlet-body inline-block">
                                                                <div class="col-lg-6">
                                                                    Approx Km.
                                                                </div>
                                                                <div class="col-lg-6" id="approx_km2">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Price per Km. -
                                                                </div>
                                                                <div class="col-lg-6" id="price_per_km2">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Driver Allowance
                                                                </div>
                                                                <div class="col-lg-6" id="driver_allowance2">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Number of Days
                                                                </div>
                                                                <div class="col-lg-6" id="number_of_days2">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    Total
                                                                </div>
                                                                <div class="col-lg-6" id="total2">
                                                                    -
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.portlet -->
                                                </div>

                                            </div>
                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <legend class="text-center">Advance Payment Options</legend>
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <label>
                                                        <input type="radio" name="advance_payment_type" value="1" required <?php echo (isset($enquiryDetails['advance_payment_type']) && $enquiryDetails['advance_payment_type'] == 1) ? 'checked' : ''?>/>
                                                        Fixed Amount
                                                    </label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" name="fixed_amount" class="form-control" readonly />
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <label>
                                                        <input type="radio" name="advance_payment_type" value="2" required <?php echo (isset($enquiryDetails['advance_payment_type']) && $enquiryDetails['advance_payment_type'] == 2) ? 'checked' : ''?> />
                                                        25% of the Non-AC Price
                                                    </label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" name="non_ac_percentage" class="form-control" readonly />
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <label>
                                                        <input type="radio" name="advance_payment_type" value="3" required <?php echo (isset($enquiryDetails['advance_payment_type']) && $enquiryDetails['advance_payment_type'] == 3) ? 'checked' : ''?> />
                                                        25% of the AC Price
                                                    </label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" name="ac_percentage" class="form-control" readonly />
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <label>
                                                        <input type="radio" name="advance_payment_type" value="4" required <?php echo (isset($enquiryDetails['advance_payment_type']) && $enquiryDetails['advance_payment_type'] == 4) ? 'checked' : ''?>/>
                                                        No Advance Payment
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <br/>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-12">
                                                    <button class="btn btn-primary pull-right" type="submit">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <br/><br/>
                                <hr/>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $this->load->view('home/footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datetimepicker.min.js"></script>
<!-- /home/shridhar/Public/test/cdp/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css -->
<script src="<?php echo base_url(); ?>assets/js/plugins/formvalidation.bootstrap/formValidation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/formvalidation.bootstrap/formvalidation.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom/enquiry.js"></script>

</body>
</html>
