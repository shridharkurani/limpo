<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'enquiries/add';
$filter_url = 'enquiries/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <!--<a href="<?php /*echo base_url().'enquiries/add_edit'; */?>" class="btn btn-primary"><?/*= lang('add new enquiry','ucword'); */?></a>-->
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet portlet-grey">
                                    <div class="portlet-heading hidden">
                                        
                                        <?php  echo form_open($filter_url,'class=mar_pad_1');  ?>
                                        <div class="row ">
                                         <div class="col-lg-4 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('Customer Name','ucword'); ?>
                                              <input type="text" name="customerName" class="form-control  col-lg-12">
                                              </label>
                                          </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('Customer Mobile','ucword'); ?>
                                              <input type="text" name="customerMobile" class="form-control  col-lg-12">
                                              </label>
                                          </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('from city','ucword'); ?>
                                              <input type="text" name="fromCity" class="form-control  col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('to city','ucword'); ?>
                                              <input type="text" name="toCity" class="form-control col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('start date','ucword'); ?>
                                              <input type="text" name="startDate" class="form-control col-lg-12">
                                              </label>
                                           </div>
                                          <div class="col-lg-4 col-sm-12 col-md-3">
                                             <label class="width100"><?= lang('end date','ucword'); ?>
                                              <input type="text" name="endDate" class="form-control col-lg-12">
                                              </label>
                                           </div>

                                          <div class="row text-center">
                                           <div class="col-lg-12">
                                             <label>&nbsp; <input class="btn btn-primary form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                             <label>&nbsp; <a href="<?php echo base_url().'enquiries';?>" class="btn btn-default">Clear Search</a></label>
                                           </div>                                            
                                          </div>  
                                        </div>
                                        <?php echo form_close(); ?>
                    
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="transactionsPortlet" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive dashboard-demo-table">
                                               <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Customer Details</th>
                                                        <th>Customer Mobile</th>
                                                        <th>Package Name</th>
                                                        <th>Service Name</th>
                                                        <th>Available Count</th>
                                                        <th>Request Date and Time</th>
                                                        <th>Status</th>
                                                        <th>Move Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  <?php
                                                    if(is_array($enquiriesArray) && !empty($enquiriesArray)) {
                                                      $cnt = 1;
                                                      foreach ($enquiriesArray as $key => $enquiry) {

                                                          /*
                                                           *
                                                           * 1 - Booking / request placed, 2 -accepted, 3 - in progress, 4 - completed, 5 -cancelled
                                                           * */
                                                      switch ($enquiry['flag']) {
                                                        case 1: $Status = 'Request Placed';
                                                          break;
                                                        case 2: $Status = 'Accepted';
                                                          break;
                                                        case 3: $Status = 'In Progress';
                                                          break;
                                                          case 4: $Status = 'Completed';
                                                              break;
                                                          case 5: $Status = 'Cancelled';
                                                              break;
                                                        default:$Status = 'Pending';
                                                          break;
                                                      }
                                                      $acceptUrl = ($enquiry['flag'] >= 2) ? '#' : base_url()."enquiries/actions/".$enquiry['service_id']."/2";
                                                      $inProgressUrl = ($enquiry['flag'] >= 3) ? '#' : base_url()."enquiries/actions/".$enquiry['service_id']."/3";
                                                      $completedUrl = ($enquiry['flag'] >= 4) ? '#' : base_url()."enquiries/actions/".$enquiry['service_id']."/4";
                                                      $actionBtn = '<div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropup pull-right" role="menu">
                                                            
                                                          <li>
                                                            <a href="'.$acceptUrl.'">Accept</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.$inProgressUrl.'"> Mark In progress</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.$completedUrl.'">Mark As Completed</a>
                                                          </li>
                                                          <li>
                                                            <a href="'.base_url().'enquiries/actions/'.$enquiry['service_id'].'/5">Cancel</a>
                                                          </li>
                                                        </ul>
                                                      </div>';
                                                        /*$customerLink = "<a href='#'>".$enquiry['fullname']."</a>";*/
                                                        $booked_date_ime = date('Y-m-d h:i A', strtotime($enquiry['booked_date_ime']));
//                                                        $seaterStr = $enquiry['model_name']." (".$enquiry['seaters'].")";
                                                        echo "<tr><td>".$cnt++."</td>";
                                                        echo "<td>".$enquiry['user_fullname']."</td>";
                                                        echo "<td>".$enquiry['user_mobile']."</td>";
                                                        echo "<td>".$enquiry['pkg_name']."</td>";
                                                        echo "<td>".$enquiry['ser_name']."</td>";
                                                        echo "<td>".$enquiry['remaining_count']."</td>";
                                                        echo "<td>".$booked_date_ime."</td>";
                                                         echo "<td>".$Status."</td>";
                                                        echo "<td>".$actionBtn."</td></tr>";
                                                      }
                                                    }
                                                  ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Customer Details</th>
                                                        <th>Customer Mobile</th>
                                                        <th>Package Name</th>
                                                        <th>Service Name</th>
                                                        <th>Available Count</th>
                                                        <th>Request Date and Time</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

  </div>
</div>