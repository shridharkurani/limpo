<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// echo form_open($url,'');
$actionName  = $this->uri->segment(2);
if($actionName == 'confirm') {
    $url = 'enquiries/confirm/'.$enquiryId;
}
else {
    $url = 'payments/request';
}
?>
<div class="content">
    <div class="width90">
        <div id="wrapper">
        <nav class="navbar-top" role="navigation">
            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="<?= base_url();?>">
                        <img src="<?php echo asset_url()."imgs/login.png";?>" class="hisrc img-responsive" alt="" width="172" height="20">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

                <!-- begin LEFT SIDE WIDGETS -->
                <ul class="nav navbar-left">
                   <!--  <li class="tooltip-sidebar-toggle">
                        <a href="#" id="sidebar-toggle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Sidebar Toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li> -->
                    <!-- You may add more widgets here using <li> -->
                </ul>
                <!-- end LEFT SIDE WIDGETS -->

                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">

                    <!-- /.dropdown -->
                    <!-- end TASKS DROPDOWN -->

                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?= strtoupper($this->session->userdata('user_fullname'))." ";?><i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user" style="width100%;">
                            
                            <!-- <li>
                                <a href="profile.html">
                                    <i class="fa fa-user"></i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> My Messages
                                    <span class="badge green pull-right">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-bell"></i> My Alerts
                                    <span class="badge orange pull-right">9</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-tasks"></i> My Tasks
                                    <span class="badge blue pull-right">10</span>
                                </a>
                            </li>
                            <li>
                                <a href="calendar.html">
                                    <i class="fa fa-calendar"></i> My Calendar
                                </a>
                            </li> -->
                            <li class="divider"></li>
                            <!-- <li>
                                <a href="#">
                                    <i class="fa fa-gear"></i> Settings
                                </a>
                            </li> -->
                            <li>
                                <a class="logout_open" href="<?= base_url().'account/logout';?>" data-popup-ordinal="0" id="open_34592005">
                                    <i class="fa fa-sign-out"></i> Logout
                                    <!-- <strong>John Smith</s trong> -->
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->

                </ul>
                <!-- /.nav -->
                <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

            </div>
            <!-- /.nav-top -->
        </nav>
        </div>
        <div class="">
            
            <br></br>
        </div>
        <div class="row">
            <br></br>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <?php
                    if($actionName == 'view'){
                ?>
                <center>
                    <button class="btn btn-default"  data-toggle="modal" data-target="#exampleModalCenter">Confirm trip by paying <?= $completeEnquiryDetails['advance_payment'];?> Rs. <br> (Advance Payment).
                    </button>
                </center>
                <?php
                }
                else if ($actionName == 'confirm') {
                ?>
                <?php echo form_open($url);?>

                    <div class="col-lg-12 portlet portlet-blue confirmEnquiry" style="padding: 2%;">
                        <center><legend>Confirm Enquiry Details</legend></center>
                        <div class="col-md-3 required">
                        <label class="control-label">Vehicle</label>
                        <select name="vehicle" id="vehicle" class="form-control"
                                value="<?php echo set_select('vehicle'); ?>">
                            <option value="0">Select vehicle</option>
                            <?php
                                if($this->input->post('vehicle')) {
                                  $vehicle =  $this->input->post('vehicle');
                                }
                                else if(isset($tripDetails['vehicle_id'])) {
                                  $vehicle =  $tripDetails['vehicle_id'];
                                } else {
                                    $vehicle = 0;
                                }
                                foreach ($vehicleDetails as $key => $value) {
                                    $valId = $value['id'];
                                    $vehicleNumber = strtoupper($value['vehicle_number']);
                                    echo '<option value=' . $valId . '' . set_select("vehicle", $valId, ($vehicle == $valId ? TRUE : FALSE)) . '>';
                                    echo strtoupper($vehicleNumber) . '</option>';
                                }
                            ?>
                        </select>
                            <?php echo form_error('vehicle', '<div class="inline_error">', '</div>'); ?>
                        </div>
                        <div class="col-md-3 required">
                        <label class="control-label">Driver</label>
                        <select name="driverName" id="driverName" class="form-control"
                                value="<?php echo set_select('driverName'); ?>">
                            <option value="0">Select Driver</option>
                            <?php
                            if ($this->input->post('driverName')) {
                                $driverId = $this->input->post('driverName');
                            } else if (isset($tripDetails['driver_id'])) {
                                $driverId = $tripDetails['driver_id'];
                            } else {
                                $driverId = 0;
                            }
                            foreach ($driver_details as $key => $value) {
                                $valId = strtolower($value['id']);
                                $driverName = ucwords($value['fullname']);
                                echo '<option  value=' . $valId . '' . set_select("driverName", $valId, ($driverId === $valId ? TRUE : FALSE)) . '>';
                                echo $driverName . '</option>';
                            }
                            ?>
                        </select>
                            <?php echo form_error('driverName', '<div class="inline_error">', '</div>'); ?>
                        </div>
                        <div class="col-md-3 required">
                          <label>Payment Status</label>
                            <select name="paymentStatus" id="paymentStatus" class="form-control"
                                    value="<?php echo set_select('paymentStatus'); ?>">
                                <?php
                                $paymentStatusArray = array(
                                                        1 => 'No Advance Payment Collected, Driver will collect complete amount',
                                                        2 => 'Advance Payment Collected, Remaining amount will be collected by driver',
                                                        3 => 'Other'
                                                    );
                                ?>
                                <option value="0">Select Payment Status</option>
                                <?php
                                    if ($this->input->post('paymentStatus')) {
                                        $paymentStatusId = $this->input->post('paymentStatus');
                                    } else if (isset($tripDetails['vehicle_type_id'])) {
                                        $paymentStatusId = $tripDetails['vehicle_type_id'];
                                    } else {
                                        $paymentStatusId = 0;
                                    }
                                    foreach ($paymentStatusArray as $key => $value) {
                                        $paymentStatus = ucwords($value);
                                        echo '<option  value=' . $key . '' . set_select("paymentStatus", $key, ($paymentStatusId == $key ? TRUE : FALSE)) . '>';
                                        echo $paymentStatus . '</option>';
                                    }
                                ?>
                            </select>
                            <?php echo form_error('paymentStatus', '<div class="inline_error">', '</div>'); ?>
                        </div>
                        <div class="col-md-3 required">
                          <label>Vehicle Type</label>
                            <select name="vehicleType" id="vehicleType" class="form-control"
                                    value="<?php echo set_select('vehicleType'); ?>">
                                <?php
                                $vehicleType = array(
                                                        1 => 'AC',
                                                        2 => 'Non-AC',
                                                    );
                                ?>
                                <option value="0">Select Vehicle Type</option>
                                <?php
                                    if ($this->input->post('vehicleType')) {
                                        $vehicleTypeId = $this->input->post('vehicleType');
                                    } else if (isset($tripDetails['vehicle_type_id'])) {
                                        $vehicleTypeId = $tripDetails['vehicle_type_id'];
                                    } else {
                                        $vehicleTypeId = 0;
                                    }
                                    foreach ($vehicleType as $key => $value) {
                                        $vehicleType = ucwords($value);
                                        echo '<option  value=' . $key . '' . set_select("vehicleType", $key, ($paymentStatusId == $key ? TRUE : FALSE)) . '>';
                                        echo $vehicleType . '</option>';
                                    }
                                ?>
                            </select>
                            <?php echo form_error('vehicleType', '<div class="inline_error">', '</div>'); ?>
                        </div>
                        <div class="col-lg-12">
                            <br>
                            <center>                                
                                <?php
                                    if(is_array($tripDetails) && !empty($tripDetails)) {
                                        echo '<div class=""></div>';
                                        echo '<div class="alert alert-success row error_msgs"><b>
                                                <a href="#" class="close" data-dismiss="alert">×</a>Enquiry Already Confirmed</b></div>';
                                    }
                                    else {
                                        echo '<button class="btn btn-lg btn-success ">Confirm enquiry</button>';
                                    }
                                ?>
                            </center>
                               <!--  <button class="btn btn-lg btn-danger col-lg-offset-1 col-lg-4 col-sm-3 col-md-3">Cancel enquiry</button> -->
                        </div>
                    </div>
                </form>
                <?php
                    }
                ?>
                <br>
                <h2>
                <?php 
                $title = "Outstation trip from ".ucwords($completeEnquiryDetails['trip_from'])." to ".ucwords($completeEnquiryDetails['trip_to'])." on ".date('d-M-y H:i:s A', strtotime($completeEnquiryDetails['start_date']))." - ".$completeEnquiryDetails['model_name']." ( ".$completeEnquiryDetails['seating_capacity']." Seaters )";
                echo $title; 
                ?>
                </h2>
                <br>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="portlet portlet-red">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Trip Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="redPortlet" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Source City </label>
                                <p class="col-lg-6"><?= ucwords($completeEnquiryDetails['trip_from']);?> </p>
                            </div>

                            <div class="row">
                                <label class="col-lg-6"> Destination City</label>
                                <p class="col-lg-6"><?= ucwords($completeEnquiryDetails['trip_to']);?> </p>
                            </div>
                             <div class="row">
                                <label class="col-lg-6 text-bold"> Start Date </label>
                                <p class="col-lg-6"><?= date('d-M-y H:i:s A', strtotime($completeEnquiryDetails['start_date']));?></p>
                            </div>

                            <div class="row">
                                <label class="col-lg-6"> End Date</label>
                                <p class="col-lg-6"><?= date('d-M-y H:i:s A', strtotime($completeEnquiryDetails['end_date']));?></p>
                            </div>
                             <div class="row">
                                <label class="col-lg-6 text-bold"> Number of days and night </label>
                                <p class="col-lg-6">2 Days, 1 Night</p>
                            </div>

                            <div class="row">
                                <label class="col-lg-6"> Customer Name</label>
                                <p class="col-lg-6"><?= ucwords($completeEnquiryDetails['fullname']);?></p>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="portlet portlet-blue">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Vehicle Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="redPortlet" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Vehicle number </label>
                                <p class="col-lg-6"><?= strtoupper($completeEnquiryDetails['vehicle_number']).' ( '.$completeEnquiryDetails['model_name'].' )';?></p>
                            </div>

                            <div class="row">
                                <label class="col-lg-6"> Seater / Seat Capacity</label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['seating_capacity'];?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-12">
                                    <p>Avilable Facilities</p>
                                </label>
                                <p class="col-lg-12">
                                    <?php
                                    if(is_array($completeEnquiryDetails['facilities']) && !(empty($completeEnquiryDetails['facilities']))) {
                                        foreach ($completeEnquiryDetails['facilities'] as $key => $facility) {
                                            echo "<p class='col-lg-4'>".ucwords($facility['facility_name'])."</p>";
                                        }
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="row">

                                    <div class="col-lg-12">
                                      <center><legend>Vehicle Images</legend></center>
                                      <div class="row">
                                        <?php
                                        $imgsList = $completeEnquiryDetails['vehicleImages'];
                                        // meDebug($imgsList,1);

                                        if(is_array($imgsList) && !empty($imgsList)) {
                                            foreach ($imgsList as $key => $value) {
                                              $imgKey = $key + 1;
                                              $imgExplode = explode(".", $value['image_name']);
                                              $url = asset_url()."uploads/vehicles/thumb/".$imgExplode['0'].'_thumb.'.$imgExplode['1'];
                                              // echo $url;
                                              echo '<div class="column"><img src='.$url.' style="width:100%" onclick="openModal();currentSlide('.$imgKey.')" class="hover-shadow cursor"></div>';
                                            }
                                        }
                                        ?>
                                      </div>

                                      <div id="lightBoxModal" class="modal">
                                        <span class="close cursor" onclick="closeModal()">&times;</span>
                                        <div class="modal-content">
                                          <?php
                                        // meDebug($imgsList,1);
                                          if(is_array($imgsList) && !empty($imgsList)) {
                                            foreach ($imgsList as $key => $value) {
                                              $imgExplode = explode(".", $value['image_name']);
                                              $url = asset_url()."uploads/vehicles/".$imgExplode['0'].".".$imgExplode['1'];
                                              // echo $url;
                                              echo '<div class="mySlides"><div class="numbertext">1 / 4</div><img src="'.$url.'" style="width:100%"></div>';
                                            }
                                          }
                                        ?>
                                          
                                          <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                          <a class="next" onclick="plusSlides(1)">&#10095;</a>

                                          <div class="caption-container">
                                            <p id="caption"></p>
                                          </div>
                                          <?php
                                          // meDebug($imgsList,1);
                                         /*   if(is_array($imgsList) && !empty($imgsList)) {
                                              foreach ($imgsList as $key => $value) {
                                                $imgExplode = explode(".", $value['image_name']);
                                                $url = asset_url()."uploads/vehicles/thumb/".$imgExplode['0'].'_thumb.'.$imgExplode['1'];
                                                // echo $url;
                                                echo '<div class="column"><img class="demo cursor" src='.$url.' style="width:100%" onclick="currentSlide('.$key.')" alt="Nature and sunrise"></div>';
                                              }
                                            }*/
                                          ?>
                                        </div>
                                      </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="portlet portlet-orange">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Price Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="orangePortlet" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Approx Km for this trip </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['approx_km']." Km.";?></p>
                            </div>                              
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Minimum km. per day </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['min_km']." Km.";?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Price Per Km </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['per_km']." Rs Per Km";?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Price Per Km (A/C) </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['per_km_ac']." Rs Per Km";?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Driver Allowance </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['driver_allowance']." Rs.";?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Extra Driver Allowance for night </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['driver_allowance_night']." Rs.";?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="portlet portlet-green">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Payment Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="greenPortlet" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Base price for Non A/C Vehicle </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['priceArray']['nonAcBasePare']." Rs"?></p>
                            </div>                              
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Base price for A/c Vehicle </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['priceArray']['acBasePare']." Rs"?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Driver Allowance </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['priceArray']['dayTotalDriverAllowance']." Rs";?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Extra Driver Allowance for night </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['priceArray']['nightTotalDriverAllowance']." Rs";?></p>
                            </div>

                            <div class="row">
                                <label class="col-lg-6 text-bold"> Approx Trip cost for Non A/C Vehicle </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['priceArray']['totalNonAcCost']." Rs";?></p>
                            </div>
                            <div class="row">
                                <label class="col-lg-6 text-bold"> Approx Trip cost for A/C Vehicle </label>
                                <p class="col-lg-6"><?= $completeEnquiryDetails['priceArray']['totalAcCost']." Rs";?></p>
                            </div>

                            <div class="row">
                                <label class="col-lg-6 text-bold"> Advance Payment </label>
                                <?php 
                                    $type = '';
                                    switch ($completeEnquiryDetails['advance_payment_type']) {
                                        case '1': $type = 'Fixed Amount';
                                            break;
                                        case '2': $type = '25% of the Non-AC Price';
                                            break;
                                        case '3': $type = '25% of the AC Pricet';
                                            break;
                                        case '4': $type = 'No Advance Payment';
                                            break;
                                        
                                        default: $type = 'Fixed Amount';
                                            break;
                                    }
                                ?>
                                <label class="col-lg-6"><?= (empty($completeEnquiryDetails['advance_payment']) ? 0 : $completeEnquiryDetails['advance_payment']) ." Rs ( ".$type." )";?></label>
                            </div>
                        </div>
                        <?php
                            if($actionName == 'view'){
                        ?>
                        <div class="portlet-footer">
                            <center><button class="btn btn-default"  data-toggle="modal" data-target="#exampleModalCenter">Confirm trip by paying <?= $completeEnquiryDetails['advance_payment'];?> Rs. <br> (Advance Payment).</button></center>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if($actionName == 'view'){
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="portlet portlet-basic">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Contact Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <label class="col-lg-6 text-bold"> Contact Person name </label>
                            <p class="col-lg-6"><?= $completeEnquiryDetails['contact_person_name'];?></p>
                        </div>
                        <div class="row">
                            <label class="col-lg-6 text-bold"> Mobile </label>
                            <p class="col-lg-6"><?= $completeEnquiryDetails['cmpny_mobile'];?></p>
                        </div>
                        <div class="row">
                            <label class="col-lg-6 text-bold"> Alternative Mobile </label>
                            <p class="col-lg-6"><?= $completeEnquiryDetails['contact_person_mobile'];?></p>
                        </div>                 
                        <div class="row">
                            <label class="col-lg-6 text-bold"> Alternative Mobile </label>
                            <p class="col-lg-6"><?= $completeEnquiryDetails['cmpny_alt_mobile'];?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="row">
            <div class="sharebtn col-lg-6 col-md-6 col-sm-6">
                <?php $shareText = base_url().'enquiries/view/'.$enquiryId.' '.$title;?>
                <a class="btn btn-lg btn-primary" href="whatsappSahreTextatsapp://send?text=<?= $shareText;?> " data-action="share/whatsapp/share">Share via Whatsapp</a>
            </div>
            <div class="sharebtn col-lg-6 col-md-6 col-sm-6">

                <input type="text" value="<?= $shareText;?>" id="myInput" class="hidden">

                <a class="btn btn-lg btn-primary" onclick="myFunction()" href="#">Copy Url to share</a>
            </div>
        </section>
        <?php
        }
        ?>
    </div>
</div>
<div class="hiddenFields">
    <input type="hidden" name="enquiryId" value="<?= $enquiryId;?>">    
</div>

<?php echo form_close(); ?>
<script type="text/javascript">
    function myFunction() {
      /* Get the text field */
      var copyText = document.getElementById("myInput");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
    }
</script>
<!-- 
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-header" style="border: none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="portlet portlet-green">
            <div class="portlet-heading login-heading text-center">
                <div class="portlet-title">
                    <h2 class="text-center"><label class="text-center">User Details</label></h2>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="portlet-body">
                    <fieldset>
                        <div class="form-group">
                            <label>User FullName</label>
                            <input class="form-control" name="userFullName" type="text" value="<?= ucwords($completeEnquiryDetails['fullname']);?>">
                        </div>
                        <div class="form-group">
                            <label>User Mobile</label>
                            <input class="form-control" name="userMobile" type="text" placeholder="Enter Your mobile to get OTP">
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-primary" value="Send OTP"><br></br>
                            <p class="small">
                                By clicking buttons I agree to all <a class="" href="<?php base_url().'termsAndCondtions';?>">Terms and conditions</a>
                            </p>
                        </div>
                    </fieldset>
                
            </div>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --------------<
    </div>
  </div>
</div> -->