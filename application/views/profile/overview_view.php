<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."company/overview";
echo form_open_multipart($url);
// meDebug($this->session->all_userdata(),true);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <!-- <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                          <label class="col-md-6">Full Name</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo (isset($userDetails['user_fullname']) ? ucwords($userDetails['user_fullname']) : ' - ');
                                            ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Mobile Number</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo (isset($userDetails['user_mobile']) ? $userDetails['user_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Email</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo (isset($userDetails['user_email']) ? $userDetails['user_email'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>