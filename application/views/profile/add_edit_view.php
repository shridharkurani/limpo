<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."profile/add_edit";
echo form_open_multipart($url);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <!-- <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <center><legend>Personal Details</legend></center>
                                    <div class="col-lg-12">
                                        <div  class="col-md-4 required">
                                          <label>Full Name</label>
                                          <input type="text" class="form-control" name="user_fullname" value="<?php
                                            echo set_value('user_fullname', isset($cmpnyDetails['user_fullname']) ? $cmpnyDetails['user_fullname'] : '');
                                             ?>" />
                                          <?php echo form_error('user_fullname', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Mobile Number</label>
                                          <input type="text" class="form-control numeric_field" name="user_mobile" value="<?php
                                          echo set_value('user_mobile', isset($cmpnyDetails['user_mobile']) ? $cmpnyDetails['user_mobile'] : '');
                                          ?>" />
                                          <?php echo form_error('user_mobile', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                        <div class="col-md-4">
                                          <label>Email</label>
                                          <input type="text" class="form-control " name="user_email" value="<?php
                                          echo set_value('user_email', isset($cmpnyDetails['user_email']) ? $cmpnyDetails['user_email'] : '');
                                          ?>" />
                                          <?php echo form_error('user_email', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="row ">
                                    <div class="bd-example container"><br></br>
                                        <?php
                                        $cnt = 1;
                                        foreach ($packages as $package) {
                                            if($cnt % 3 == 1) {
                                                ?>

                                                <div class="col-lg-4">
                                                    <div class="card">
                                                        <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text></svg>
                                                        <div class="card-body">
                                                            <h3 class="card-title"><?php echo $package['pkg_name'];?></h3>
                                                            <p class="card-text"><?php echo $package['pkg_name'];?></p>
                                                            <!--                                <a href="#" class="btn btn-primary">Pay --><?php //echo $package['pkg_price'];?><!-- And Buy this Package</a>-->
                                                            <a href="<?php echo base_url().'packages/view/'.$package['id'];?> " class="btn btn-primary">View More</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            else {
                                                ?><div class="col-lg-4">
                                                <div class="card">
                                                    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text></svg>
                                                    <div class="card-body">
                                                        <h3 class="card-title"><?php echo $package['pkg_name'];?></h3>
                                                        <p class="card-text"><?php echo $package['pkg_name'];?></p>
                                                        <!--                                <a href="#" class="btn btn-primary">Pay --><?php //echo $package['pkg_price'];?><!-- And Buy this Package</a>-->
                                                        <a href="<?php echo base_url().'packages/view/'.$package['id'];?> " class="btn btn-primary">View More</a>
                                                    </div>
                                                </div>
                                                </div>
                                                <?php

                                            }
                                            $cnt ++;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="bd-example container"><br></br>
                                        <?php
                                        $cnt = 1;
                                        foreach ($packages as $package) {
                                            if($cnt % 2 == 1) {
                                                ?>

                                                <div class="col-lg-5">
                                                    <div class="card">
                                                        <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text></svg>
                                                        <div class="card-body">
                                                            <h3 class="card-title"><?php echo $package['pkg_name'];?></h3>
                                                            <p class="card-text"><?php echo $package['pkg_name'];?></p>
                                                            <!--                                <a href="#" class="btn btn-primary">Pay --><?php //echo $package['pkg_price'];?><!-- And Buy this Package</a>-->
                                                            <a href="<?php echo base_url().'packages/view/'.$package['id'];?> " class="btn btn-primary">View More</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            else {
                                                ?><div class="col-lg-5">
                                                <div class="card">
                                                    <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text></svg>
                                                    <div class="card-body">
                                                        <h3 class="card-title"><?php echo $package['pkg_name'];?></h3>
                                                        <p class="card-text"><?php echo $package['pkg_name'];?></p>
                                                        <!--                                <a href="#" class="btn btn-primary">Pay --><?php //echo $package['pkg_price'];?><!-- And Buy this Package</a>-->
                                                        <a href="<?php echo base_url().'packages/view/'.$package['id'];?> " class="btn btn-primary">View More</a>
                                                    </div>
                                                </div>
                                                </div>
                                                <?php

                                            }
                                            $cnt ++;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>