<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url().'company';
?>
<form class="form-horizontal" method="post" role="form" action=<?= $url;?> >
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div  class="col-md-4">
                                          <label>Company / Organization Name</label>
                                          <input type="text" class="form-control" name="cmpny_name" value="<?php 
                                            echo set_value('cmpny_name', isset($cmpnyDetails['cmpny_name']) ? $cmpnyDetails['cmpny_name'] : '');
                                             ?>" />
                                        </div>

                                        <div class="col-md-4">
                                          <label>Company Mobile Number</label>
                                          <input type="text" class="form-control numeric_field" name="cmpny_mobile" value="<?php
                                          echo set_value('cmpny_mobile', isset($cmpnyDetails['cmpny_mobile']) ? $cmpnyDetails['cmpny_mobile'] : '');
                                          ?>" />
                                        </div>

                                        <div class="col-md-4">
                                          <label>Alternative Mobile Number</label>
                                          <input type="text" class="form-control numeric_field" name="cmpny_alt_mobile" value="<?php
                                          echo set_value('cmpny_alt_mobile', isset($cmpnyDetails['cmpny_alt_mobile']) ? $cmpnyDetails['cmpny_alt_mobile'] : '');
                                          ?>" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">


                                        <div class="col-md-4">
                                          <label>Company Email</label>
                                          <input type="text" class="form-control numeric_field" name="cmpny_email" value="<?php
                                          echo set_value('cmpny_email', isset($cmpnyDetails['cmpny_email']) ? $cmpnyDetails['cmpny_email'] : '');
                                          ?>" />
                                        </div>
                                        <div class="col-md-4">
                                            <label>Company Logo</label>
                                            <?php
                                                if (isset($cmpnyDetails['cmpny_logo']) && !empty($cmpnyDetails['cmpny_logo'])) {
                                                echo "<div class='row'>";
                                                    echo "<div class='col-md-6'>";
                                                        echo "<img for='files' class='medium_img rounded' src=".base_url().'assets/uploads/company/'.$cmpnyDetails['cmpny_logo'].">";
                                                    echo "</div>";
                                                echo "</div>";

                                                    echo "<div>";
                                                        echo '<input type="file" id="files" style="display:none !important;" class="hidden" value="" name="cmpny_logo" />';
                                                        echo "<label for='files' class='btn btn-default'>Upload New Logo</label>";
                                                        echo "<span id='updating_logo'></span>";
                                                    echo "</div>";

                                                }
                                                else {
                                                echo '<input type="file" class="form-control" value="" name="cmpny_logo" '.set_value('cmpny_logo','a.png').'/>';
                                                }
                                            ?>
                                        </div>

                                        <div class="col-md-4">
                                          <label>Company Address</label>
                                          <?php
                                            if($this->input->post('cmpny_address')) {
                                                $cmpny_address = $this->input->post('cmpny_address');
                                            }
                                            else if(isset($cmpnyDetails['cmpny_address'])) {
                                                $cmpny_address = $cmpnyDetails['cmpny_address'];
                                            }
                                            else $cmpny_address = '';
                                            ?>
                                            <textarea class="form-control" name="cmpny_address" placeholder="What organizers or visitors may know about your company?"><?php if(isset($cmpny_address)) echo $cmpny_address;?>
                                            </textarea>
                                          </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <center><legend>Contact Person Details</legend></center>
                                    <div class="col-lg-12">
                                        <label><input type="checkbox" name="">&nbsp Contact Owner for communications</label>
                                    </div>
                                    <div  class="col-md-6">
                                          <label>Company / Organization Name</label>
                                          <input type="text" class="form-control" name="ex_cmpny_name" value="<?php 
                                            echo set_value('ex_cmpny_name', isset($cmpnyDetails['exhibitor_cmpny_name']) ? $cmpnyDetails['exhibitor_cmpny_name'] : '');
                                             ?>" />
                                        </div>

                                        <div class="col-md-6">
                                          <label>Company Mobile Number</label>
                                          <input type="text" class="form-control numeric_field" name="ex_mobile" value="<?php
                                          echo set_value('ex_mobile', isset($cmpnyDetails['exhibitor_mobile']) ? $cmpnyDetails['exhibitor_mobile'] : '');
                                          ?>" />
                                          </div>
                                <div class=" text-center col-lg-12">
                                    <legend>&nbsp</legend>
                                    <input type="submit" name="" value="Save" class="btn btn-info">
                                </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>