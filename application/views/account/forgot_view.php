<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'account/forgot';

echo form_open($url,'id=account_signin');
?>
    <div class="body signin_body">
        <div class="">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                     <img src="<?php echo asset_url().'imgs/logo.png'; ?>" alt="">
                </div>
                <div class="portlet portlet-blue">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h3 class="text-center"><label class="text-center">Forgot Password</label></h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <form accept-charset="UTF-8" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <label>Email </label>
                                    <input class="form-control" placeholder="Enter your E-mail to reset your password" name="email_mobile_username" type="text">
                                    <?php echo form_error('email_mobile_username', '<div class="inline_error">', '</div>'); ?>
                                </div>
                                    <br>
                                    <div class="text-center">
                                        <input type="submit" class="btn btn-primary" value="Send link to reset password">
                                    </div>
                                    <br>
                                    <p class="small">
                                    <a class="pull-right" href="<?php echo base_url().'account/signin'; ?>">Have password ? Sign in here </a>
                                </p>
                            </fieldset>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>