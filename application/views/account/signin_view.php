<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'account/signin';

echo form_open($url,'id=account_signin');
?>
    <div class="body signin_body">
        <div class="conta">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                    <a href="<?php echo base_url();?>"><img src="<?php echo asset_url().'imgs/logo.png'; ?>" alt=""></a>

                    <?php echo $this->layouts->print_msg_div($this->session); ?>
                </div>
                <div class="login-banner text-center">
                </div>
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading text-center">
                        <div class="portlet-title">
                            <h3 class="text-center"><label class="text-center">Login to <?php echo SITE_NAME;?></label></h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <form accept-charset="UTF-8" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <label>Email or Username</label>
                                    <input class="form-control" name="email_mobile_username" type="text">
                                    
                                    <?php echo form_error('email_mobile_username', '<div class="inline_error">', '</div>'); ?>

                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" name="password" type="password">
                                    
                                    <?php echo form_error('password', '<div class="inline_error">', '</div>'); ?>
                                </div>
                                <br>
                                <div class="text-center"><input type="submit" class="btn btn-primary" value="Sign In"></div>
                            </fieldset>
                            <br>
                            <p class="small text-center">
<!--                                <a href="--><?php //echo base_url().'account/forgot'; ?><!--">Forgot your password?</a>-->
                                <a class="text-center" href="<?php echo base_url().'account/register'; ?>">Don't Have account? Register Here</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>