<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo form_open('account/register','id=account_signup_frm');
?>
<div class="body register_body">
    <div class="">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-banner text-center">
                <a href="<?php echo base_url();?>"><img src="<?php echo asset_url().'imgs/logo.png'; ?>" alt=""></a>
            </div>
            <div class="login-banner text-center">
            </div>
            <div class="portlet portlet-green">
                <div class="portlet-heading login-heading text-center">
                    <div class="portlet-title">
                        <h3 class="text-center"><label class="text-center">Register to <?php echo SITE_NAME;?></label></h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo base_url().'account/register'; ?>" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                    <label>Full Name</label>
                                    <input name="user_fullname" placeholder="Your Fullname"  class="form-control" type="text" autofocus value="<?php echo set_value('user_fullname'); ?>" />
                                    <?php echo form_error('user_fullname', '<div class="inline_error">', '</div>'); ?>
                                    <br>
                                    <label>Mobile Number</label>
                                    <input name="user_mobile" placeholder="Mobile Number"  class="form-control" type="text" value="<?php echo set_value('user_mobile'); ?>" />
                                    <?php echo form_error('user_mobile', '<div class="inline_error">', '</div>'); ?>
                                    <br>
                                    <label>Email</label>
                                    <input name="user_email" placeholder="Your Email Address"  class="form-control" type="text" value="<?php echo set_value('user_email'); ?>" />
                                    <?php echo form_error('user_email', '<div class="inline_error">', '</div>'); ?>

                                    <br>
                                    <label>Password</label>
                                    <input name="user_pass" placeholder="Your password..."   class="form-control" type="password"  value="<?php //echo set_value('user_pass'); ?>" />
                                    <?php echo form_error('user_pass', '<div class="inline_error">', '</div>'); ?>
                                    <br>
                                    <!--<div class="col-lg-12">
                                        <div class="col-lg-8 col-md-6">
                                            <label>OTP</label>
                                            <input name="user_otp" placeholder="OTP(One time Password)"  class="form-control" type="text" value="<?php /*echo set_value('user_otp'); */?>" />
                                            <?php /*echo form_error('user_otp', '<div class="inline_error">', '</div>'); */?>
                                        </div>
                                        <div class="col-md-4 col-md-6">
                                            <label>&nbsp</label>
                                            <a href="#" id="sendOTP" class="btn btn-red">Send OTP</a>
                                        </div>
                                    </div>-->
                                <br>
                                <legend>&nbsp</legend>

                                <div class="text-center"><input type="submit" class="btn btn-primary" value="Register"></div>

                                    <br>
                                <p class="small">
                                    <a class="pull-right" href="<?php echo base_url().'account/signin'; ?>">Already have a account ? Sign in here </a>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>