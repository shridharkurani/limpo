<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'account/otp';
meDebug($registerArray);
echo form_open($url,'id=account_signin');
?>
    <div class="body signin_body">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                     <img src="<?php echo asset_url().'imgs/logo.png'; ?>" alt="">
                </div>
                <div class="portlet portlet-blue">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h4><strong>Forgot Password</strong>
                            </h4>
                        </div>
                        <div class="portlet-widgets">
                            <a class="btn btn-default" href="<?php echo base_url().'account/signin'; ?>">Sign In</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <form accept-charset="UTF-8" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <label>Email or Mobile No.</label>
                                    <input class="form-control" placeholder="E-mail" name="email_mobile_username" type="text">
                                    <?php echo form_error('email_mobile_username', '<div class="inline_error">', '</div>'); ?>
                                </div>
                                <input type="submit" name="Signin_submit" value="Send link to reset password" class="btn btn-lg btn-green btn-block">
                            </fieldset>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>