<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isset($serviceId) && $serviceId != 0)
    $url = base_url()."services/add_edit/".$serviceId;
else
    $url = base_url()."services/add_edit";
echo form_open_multipart($url);
if(!empty($serviceDetails)) {
    $serviceDetails['service_name'] = $serviceDetails['ser_name'];
    $serviceDetails['service_pin'] = $serviceDetails['service_pin'];
    $serviceDetails['service_desc'] = $serviceDetails['ser_description'];
    $serImage = $serviceDetails['ser_image'];
    if(!empty($serImage)) {
        $imageArray = explode(".", $serImage);
        $serviceDetails['cmpny_logo'] = !empty($imageArray) ? $imageArray[0].'_thumb.'.$imageArray[1] :  '';
    }

}
//meDebug($serviceDetails, 1);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="col-lg-12">
                <div class="page-title">
                    <?php echo $this->layouts->print_breadcrumb(); ?><br>
                    <h1> <?php echo $page_title;?> </h1>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <br class="portlet-body">
                        <div class="row">
                            <center><legend>Service Details</legend></center>
                            <div class="col-lg-12">
                                <div class="col-md-6 required">
                                    <label>Service Name</label>
                                    <input type="text" class="form-control numeric_field" name="service_name" value="<?php
                                    echo set_value('service_name', isset($serviceDetails['service_name']) ? $serviceDetails['service_name'] : '');
                                    ?>" />
                                    <?php echo form_error('service_name', '<div class="inline_error">', '</div>'); ?>
                                </div>
                                <div class="col-md-6 required">
                                    <label>Available Pin Codes</label><span>(Add comma Separated)</span>
                                    <input type="text" class="form-control numeric_field" name="service_pin" value="<?php
                                    echo set_value('service_pin', isset($serviceDetails['service_pin']) ? $serviceDetails['service_pin'] : '');
                                    ?>" />
                                    <?php echo form_error('service_pin', '<div class="inline_error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <br></br>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-md-6">
                                    <label>Description</label>
                                    <?php
                                    if($this->input->post('service_desc')) {
                                        $service_desc = $this->input->post('service_desc');
                                    }
                                    else if(isset($serviceDetails['service_desc'])) {
                                        $service_desc = $serviceDetails['service_desc'];
                                    }
                                    else $service_desc = '';
                                    ?>
                                    <textarea class="textarea form-control" rows='5' name="service_desc" placeholder=""><?php if(isset($service_desc)) echo $service_desc;?></textarea>
                                </div>

                                <div class="col-md-4">
                                    <?php
                                    if (isset($serviceDetails['cmpny_logo']) && !empty($serviceDetails['cmpny_logo'])) {
                                        echo "<label>Service Image</label>";

                                        echo "<div class='row'>";
                                        echo "<div class='col-md-6'>";
                                        echo "<img for='files' class='medium_img rounded' src=".base_url().'assets/uploads/services/thumb/'.$serviceDetails['cmpny_logo'].' />';
                                        echo "</div>";
                                        echo "</div>";

                                        echo "<div>";
                                        echo '<input type="file" id="files" style="display:none !important;" class="hidden" value="" name="cmpny_logo" />';
                                        echo '<input type="text" class="hidden" value="'.$serviceDetails['cmpny_logo'].'" name="saved_cmpny_logo" />';
                                        echo "<label for='files' class='btn btn-default'>Upload New Image</label>";
                                        echo "<span id='updating_logo'></span>";
                                        echo "</div>";

                                    }
                                    else {
                                        echo "<label>Service Image</label>";
                                        echo '<input type="file" class="form-control" value="" name="cmpny_logo" '.set_value('cmpny_logo','a.png').'/>';
                                    }
                                    ?>
                                    <?php echo form_error('cmpny_logo', '<div class="inline_error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>

                        <div class=" text-center col-lg-12">
                            <legend>&nbsp</legend>
                            <!-- <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>"> -->
                            <input type="submit" name="" value="Save service Details" class="btn btn-default">
                        </div>
                    </div>
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.page-content -->
</div>
</form>