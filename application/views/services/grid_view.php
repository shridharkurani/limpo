<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."services/all";
echo form_open_multipart($url);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
<div id="page-wrapper" style="margin: 0px;">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <!--<div class="col-lg-12">
                <div class="page-title">
                    <?php /*echo $this->layouts->print_breadcrumb(); */?><br>
                    <h1> <?php /*echo $page_title;*/?> </h1>
                </div>
            </div>-->
            <div class="row">
                <div class="portlet1"  style="margin: 50px;">
                    <!-- <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Company / Organization Details</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div> -->
                    <div class="row">
                        <center><h1>LIMPO Services</h1></center>
                        <div class="col-lg-12 pull-right" style="text-align: right;">
                            <div class="col-md-12">
                                <input type="text" class="numeric_field" name="pincode" value="<?php
                                echo set_value('pincode', isset($searchData['pincode']) ? $searchData['pincode'] : '');
                                ?>" />
                                <button>Search by Pincode</button>
                                <a href="<?php echo base_url().'services/all/';?>">Show All Services</a>
                            </div>

                        </div>
                    </div>
                    <br>
                    <div id="products" class="row view-group">
                        <?php
                        $cnt = 1;
                        if(!empty($services)) {
                            foreach ($services as $service) {
                            if (4 % $cnt == 1) {
                            ?>
                            <div class="row">
                                <?php
                                }
                                else {
                                ?>
                                <div class="section">
                                    <?php
                                    }
                                    ?>
                                    <div class="item col-lg-4 col-sm-12 col-md-6">
                                        <div class="thumbnail card" style="min-height: 350px;">
                                            <div class="img-event" style="margin: 1%;">

                                                <?php
                                                if(!empty($service['ser_image'])) {
                                                    if(!empty($service['ser_image'])) {
                                                        $imageArray = explode(".", $service['ser_image']);
                                                        $serviceImage = !empty($imageArray) ? $imageArray[0].'_thumb.'.$imageArray[1] :  '';
                                                        $imageFullUrl = base_url().'assets/uploads/services/thumb/'.$serviceImage;
                                                    }
                                                    echo "<img style='max-height: 180px;' width='100%' for='files' class='medium_img rounded' src=".$imageFullUrl." >";
                                                    ?>
                                                <?php
                                                }
                                              else{
                                                ?>
                                                <svg class="bd-placeholder-img card-img-top" width="100%" height="180"
                                                     xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                                     focusable="false" role="img" aria-label="Placeholder: LIMPO SERVICE"><title>
                                                        LIMPO</title>
                                                    <rect width="100%" height="100%" fill="#868e96"></rect>
                                                    <text x="40%" y="50%" fill="#dee2e6" dy=".3em">LIMPO SERVICE</text>
                                                </svg>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="caption card-body">
                                                <h3 class="group card-title inner list-group-item-heading">
                                                    <?php echo ucwords($service['ser_name']); ?></h3>
                                                <h5 class="group inner list-group-item-text">Available In :
                                                    <?php echo $service['pincodes']; ?></h5><br>
                                                <p class="group inner list-group-item-text">
                                                    <?php echo $service['ser_description']; ?></p><br>

                                                <!--<p class="group inner list-group-item-text">
                                                    <?php /*echo $service['pkg_desc'];*/ ?> </p>-->
                                                <!--<div class="row">
                                                    <div class="col-md-6 pull-left">
                                                        <a class="btn btn-warning" href="<?php /*echo base_url().'packages/view/'.$service['id']; */ ?>">View More</a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a class="btn btn-success  pull-right" href="<?php /*echo base_url().'payments/pay/'.$service['id']; */ ?>">Book Now for Rs - <?php /*echo $service['pkg_price'];*/ ?> </a>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- this is close for section or row -->
                                <?php

                                }
                            }
                        else {
                            ?>
                            <h1 class="text-center text-danger">No services Found.</h1>
                                <?php
                        }
                            ?>
                        </div>
                        <!-- /.portlet-body -->
                    </div>
                    <!-- /.portlet -->

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</div>
</form>