<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 06-02-2021
 * Time: 13:05
 */
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    $full_url = base_url().'services/edit/'.$service_id;
    echo form_open_multipart($full_url);

if($this->session->flashdata('msg')){
        echo ' <div class="alert alert-success row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';

}

if(isset($err_msg)){
        echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';

}
if (isset($msg)) {
        echo ' <div class="alert alert-success row"><b>
        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo ' <div class="alert alert-danger row error_msgs"><b>

        <a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}

if(empty($posted_data) && !empty($service_details)) {
    foreach ($service_details as $key => $value){
        $ser_name = $service_details[$key]->ser_name;
        $ser_desc = $service_details[$key]->ser_description;
        $ser_image= $service_details[$key]->ser_image;
    }
}
else if ($posted_data) {
    //set input values from post request.
    $ser_name = $posted_data['ser_name'];
    $ser_desc = $posted_data['ser_desc'];
    $ser_image= $posted_data['userfile'];

}

?>

<div class="container">
    <div class="row well">
         <legend><label class="default_font_color">Update Service</label>
            <input type="submit" name="" value="Submit" class="btn hoverable_btn pull-right" /></legend>

            <div class="col-md-6">
                <label>Service Name <i class="required"> * </i></label>
                <input name="service_name" class="form-control" type="text" value="<?php echo set_value('service_name', $ser_name); ?>">
            </div>
        <div >

        </div>

        <div class="col-md-12">
            <div class="col-md-2">
                  <img src="<?php echo asset_url()."uploads/service/".$ser_image; ?>" class="img-responsive img-thumbnail" width="100px;"/>
            </div>
            <div class="col-md-8">
            <label> Service  Image <i class="required"> * </i>
</label><small> (Image max size:2MB & Image type: gif, jpg, png.)</small>
               <input name="userfile" class="form-control" type="file">
            </div>
        </div>



             <div class="col-md-12">
                <label>Service Description  <i class="required"> * </i></label>
                <textarea name="service_desc" class="form-control" rows="8" cols="50" ><?php echo set_value('ser_desc',$ser_desc); ?> </textarea>
            </div>

        <div class="col-md-12 text-center">
            <input type="submit" name="" value="Submit" class="btn hoverable_btn">
        </div>


    </div>


</div>