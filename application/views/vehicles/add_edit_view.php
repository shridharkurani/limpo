<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isset($vehicleId) && $vehicleId != 0)
$url = base_url()."vehicles/add_edit/".$vehicleId;
else 
$url = base_url()."vehicles/add_edit";

echo form_open_multipart($url);
// meDebug($this->session->all_userdata(),true);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default"><!-- 
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <center><legend>Vehicle Details</legend></center>
                                    <div class="col-lg-12">
                                        <div  class="col-md-4 required">
                                          <label>Vehicle Type</label>
                                          <select name="vehicle_type" id="vehicle_type" class="form-control" value="<?php echo set_select('vehicle_type'); ?>">
                                          <option value="0">Select</option>
                                          <?php
                                            if($this->input->post('vehicle_type')) {
                                              $vehicle_type =  $this->input->post('vehicle_type');
                                            }
                                            else if(isset($vehicleDetails['model_type'])) {
                                              $vehicle_type =  $vehicleDetails['model_type'];
                                            }

                                            foreach ($modelTypeDetails as $key => $value) {
                                              $valId = strtolower(trim($value['model_type']));
                                              $vehicle_type = strtolower(trim($vehicle_type));
                                              echo '<option value='.$valId.''.set_select("vehicle_type",$valId ,($vehicle_type === $valId  ? TRUE : FALSE )).'>';
                                              echo strtoupper($valId).'</option>';    
                                            }
                                            ?>
                                          </select>
                                          <?php echo form_error('vehicle_type', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div  class="col-md-4 required">
                                          <label>Vehicle Model</label>
                                          <select name="vehicle_model" id="vehicle_model" class="form-control" value="<?php echo set_select('vehicle_model'); ?>">
                                            <option value="0">Select Vehicle Type</option>
                                          <?php
                                            if($this->input->post('vehicle_model')) {
                                              $vehicle_model =  $this->input->post('vehicle_model');
                                            }
                                            else if(isset($vehicleDetails['model_id'])) {
                                              $vehicle_model =  $vehicleDetails['model_id'];
                                            }
                                            // meDebug($vehicleDetails,true);
                                            foreach ($modelNameDetails as $key => $value) {
                                              $val = $value['model_name'];
                                              $valId = strtolower($value['model_id']);
                                              echo '<option value='.$valId.''.set_select("vehicle_model",$valId ,($vehicle_model === $valId  ? TRUE : FALSE )).'>';
                                              echo strtoupper($val).'</option>';    
                                            }
                                            ?>
                                          </select>
                                          <?php echo form_error('vehicle_model', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                        <div class="col-md-4 required">
                                          <label>Vehicle Number</label>
                                          <input type="text" class="form-control numeric_field" name="vehicle_number" value="<?php
                                          echo set_value('vehicle_number', isset($vehicleDetails['vehicle_number']) ? $vehicleDetails['vehicle_number'] : '');
                                          ?>" />
                                          <?php echo form_error('vehicle_number', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                    <center><legend>Avilable Facilities</legend></center>
                                    	<?php
                                    	if(is_array($facilityArray) && !empty($facilityArray)) {
                                    		foreach ($facilityArray as $key => $facility) {
                                    				if(is_array($facilityArray) && !empty($faciltiesIdsArray))
                                    					$isChecked = (in_array($facility['id'], $faciltiesIdsArray)) ? "checked" : "" ;
                                    				else 
                                    					$isChecked = '';
                                    			echo "<div class='col-lg-3'>";
                                    			echo "<label><input type='checkbox' ".$isChecked." name='facility[]' value=".$facility['id'].">   ".$facility['facility_name']."</label>";
                                    			echo "</div>";
                                    		}
                                    	}
                                    	?>
                                    </div>
                                    <div class="col-lg-12">
                                     <center><legend>Vehicle Images</legend></center>
                                     <?php //meDebug($imgsList,1);?>                      
                                    </div>
                                    <div class="qcontainer">
                                      <div id="img-container" class="">
                                        <div class="">
                                        </div>
                                      </div>
                                    </div>
                                    <?php if(isset($vehicleId) && !empty($vehicleId) && $vehicleId) {
                                            if(is_array($imgsList) && !empty($imgsList)) {
                                              foreach ($imgsList as $key => $value) {
                                                $imgExplode = explode(".", $value['image_name']);
                                                $url = asset_url()."uploads/vehicles/thumb/".$imgExplode['0'].'_thumb.'.$imgExplode['1'];
                                                // echo $url;

                                            ?>                                          
                                            <div class="col-lg-3 col-sm-4 col-6" style="text-align: -webkit-center;">
                                              <a href="#" title="Image 1">
                                                <img src="<?php echo $url;?>" class="thumbnail img-responsive" />
                                              </a>
                                            </div>
                                            <?php
                                              }
                                            }
                                          }
                                    else {
                                      ?>

                                    <div class="col-lg-12">
                                      <input name="userfile[]" id="userfile" type="file" multiple="" />
                                    </div>
                                      <?php
                                    }
                                    ?>

                                    </div>

                                <div class=" text-center col-lg-12">
                                    <legend>&nbsp</legend>
                                    <!-- <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>"> -->
                                    <input type="submit" name="" value="Save Vehicle Details" class="btn btn-default">
                                </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>