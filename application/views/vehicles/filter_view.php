<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'vehicles/add';
$filter_url = 'vehicles/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <a href="<?php echo base_url().'vehicles/add_edit'; ?>" class="btn btn-primary"><?= lang('add new vehicle','ucword'); ?></a>
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet portlet-grey">
                                <div class="portlet-heading">
                                    <?php  echo form_open($filter_url,'class=mar_pad_1');  ?>
                                    <div class="row ">
                                      <div class="col-lg-3 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('Vehicle Type','ucword'); ?>
                                          <select name="vehicle_type" id="vehicle_type" class="form-control col-lg-12" value="<?php echo set_select('vehicle_type'); ?>">
                                          <option value="0">Select</option>
                                          <?php
                                            if($this->input->post('vehicle_type')) {
                                              $vehicle_type =  $this->input->post('vehicle_type');
                                            }
                                            else if(isset($enquiryDetails['model_type'])) {
                                              $vehicle_type =  $enquiryDetails['model_type'];
                                            }

                                            foreach ($modelTypeDetails as $key => $value) {
                                              $valId = strtolower($value['model_type']);             
                                              echo '<option value='.$valId.''.set_select("vehicle_type",$valId ,($vehicle_type === $valId  ? TRUE : FALSE )).'>';
                                              echo strtoupper($valId).'</option>';    
                                            }
                                            ?>
                                          </select>
                                          </label>
                                       </div>
                                      <div class="col-lg-3 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('Vehicle Model','ucword'); ?>
                                          <select name="vehicle_model" id="vehicle_model" class="form-control col-lg-12" value="<?php echo set_select('vehicle_model'); ?>">
                                            <option value="0">Select Vehicle Type</option>
                                          <?php
                                            if($this->input->post('vehicle_model')) {
                                              $vehicle_model =  $this->input->post('vehicle_model');
                                            }
                                            else if(isset($enquiryDetails['model_id'])) {
                                              $vehicle_model =  $enquiryDetails['model_id'];
                                            }
                                            // meDebug($enquiryDetails,true);
                                            foreach ($modelNameDetails as $key => $value) {
                                              $val = $value['model_name'];
                                              $valId = $value['model_id'];
                                              echo '<option value='.$valId.''.set_select("vehicle_model",$valId ,($vehicle_model === $valId  ? TRUE : FALSE )).'>';
                                              echo strtoupper($val).'</option>';    
                                            }
                                            ?>
                                          </select>
                                          </label>
                                       </div>
                                      <div class="col-lg-3 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('vehicle number','ucword'); ?>
                                          <input type="text" name="vehicle_number" value="<?php echo isset($postArray['vehicle_number']) ? $postArray['vehicle_number'] : ''?>" class=" col-lg-12 form-control">
                                            
                                          </select>
                                          </label>
                                       </div>
                                      <div class="col-lg-3 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('status','ucword'); ?>
                                          <select class="form-control col-lg-12" name="status" id="status">

                                            <?php
                                              if($postArray['filters']['status'] == 1) {
                                                $selected = '1';
                                              }
                                              elseif($postArray['filters']['status'] == 2) {
                                                $selected = '2';
                                              }
                                            ?>

                                            <option value="1"  <?php if($selected == '1') { echo "selected";}?> > Active </option>
                                            <option value="2"  <?php if($selected == '2') { echo "selected";}?> > Blocked </option>
                                            
                                          </select>
                                          </label>
                                       </div>
                                      <div class="row text-center">
                                       <div class="col-lg-12">
                                         <label>&nbsp; <input class="btn btn-primary form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                         <label>&nbsp; <a href="<?php echo base_url().'vehicles';?>" class="btn btn-default">Clear Search</a></label>
                                       </div>                                            
                                      </div>
                                    </div>
                                    <?php echo form_close(); ?>
                
                                    <div class="clearfix"></div>
                                </div>
                                <div id="transactionsPortlet" class="panel-collapse collapse in">
                                    <div class="portlet-body">
                                        <div class="table-responsive dashboard-demo-table">
                                           <table id="example" class="table table-striped table-bordered" style="width:100%">
                                  <thead>
                                      <tr>
                                        <th>S.No.</th>
                                        <th>Vehicle Model</th>
                                        <th>Vehicle Type</th>
                                        <th>Vehicle Number</th>
                                        <th>Facilities</th>
                                        <th>Images Exist?</th>
                                        <th>Status</th>
                                        <th>Actions</th><!-- activate, Block, delete, edit. -->
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      if(is_array($vehicleDetails) && !empty($vehicleDetails)) {
                                          $cnt = 1;
                                          foreach ($vehicleDetails as $key => $vehicle) {
                                            if(!empty($vehicle['facilities']) && is_array($vehicle['facilities'])) {
                                              $Facilities = "";
                                              foreach ($vehicle['facilities'] as $key => $value) {
                                                $Facilities .=  $value['facility_name'].", ";
                                              }
                                              $Facilities = rtrim($Facilities,', ');//rtrim(",",$Facilities);

                                            }
                                            else {
                                              $Facilities = ' - ';
                                            }
                                            switch ($vehicle['flag']) {
                                              case 1: $Status = 'Active';
                                                break;
                                              case 2: $Status = 'Blocked';
                                                break;
                                              case 3: $Status = 'Deleted';
                                                break;
                                              default:$Status = 'Pending for approval';
                                                break;
                                            }
                                            $actionBtn = '<div class="btn-group">
                                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                              </button>
                                              <ul class="dropdown-menu pull-right" role="menu">
                                                  
                                                <li>
                                                  <a href="'.base_url().'vehicles/add_edit/'.$vehicle['id'].'">Edit</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/view/'.$vehicle['id'].'">View</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/actions/'.$vehicle['id'].'/1">Activate</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/actions/'.$vehicle['id'].'/2">Block</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'vehicles/actions/'.$vehicle['id'].'/3">Delete</a>
                                                </li>
                                              </ul>
                                            </div>';
                                            echo "<tr>";
                                            echo "<td>".$cnt++."</td>";
                                            echo "<td>".$vehicle['model_type']."</td>";
                                            echo "<td>".$vehicle['model_name']."</td>";
                                            echo "<td>".$vehicle['vehicle_number']."</td>";

                                            echo "<td>".$Facilities."</td>";
                                            echo "<td>".$vehicle['image_exist']."</td>";
                                            echo "<td>".$Status."</td>";
                                            echo "<td>".$actionBtn."</td>";
                                            echo "</tr>";
                                          }
                                      }
                                    ?>
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                        <th>S.No.</th>
                                        <th>Vehicle Model</th>
                                        <th>Vehicle Type</th>
                                        <th>Vehicle Number</th>
                                        <th>Facilities</th>
                                        <th>Images Exist?</th>
                                        <th>Status</th>
                                        <th>Actions</th><!-- activate, Block, delete, edit. -->
                                      </tr>
                                  </tfoot>
                              </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->

                    </div>
                </div>
            </div>
  </div>
</div>