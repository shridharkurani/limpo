<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'vehicles/add';
$search_url = 'vehicles/search';
?>

<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <a href="<?php echo base_url().'vehicles/add'; ?>" class="btn btn-info"><i class="fa fa-plus-circle"></i> <?= lang('add new vehicle','ucword'); ?></a>
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="portlet portlet-grey">
                                    <div class="portlet-heading">
                                        <?php  echo form_open($search_url,'id=account_signin');  ?>
                                        <div class="row ">
                                          <div class="col-lg-3">
                                             <label><?= lang('vehicle no.','ucword'); ?>
                                              <input type="text" name="vehicle_no" class="form-control">
                                              </label>
                                           </div>
                                           <div class="col-lg-3">
                                             <label><?= lang('vehicle type','ucword'); ?>
                                                <select class="form-control col-lg-3" name="hospital_name">
                                                  <option value=""><?= '--'.lang('select hospital','ucword').'--'; ?></option>
                                                  <?php
                                                    foreach($hospital_data as $hospital_value):
                                                  ?>
                                                      <option value="<?= $hospital_value->hospital_id ?>"><?= $hospital_value->name ?></option>
                                                  <?php
                                                    endforeach;
                                                  ?>
                                                </select></label>
                                           </div>

                                           <div class="col-lg-3"> 
                                            <label><?= lang('vehicle brand','ucword'); ?>
                                            <select class="form-control col-lg-3" name="status">
                                              <option value=""><?= '--'.lang('select action','ucword').'--'; ?></option>
                                              <option value="1"><?= lang('active','ucword'); ?></option>
                                              <option value="2"><?= lang('in-Active','ucword'); ?></option>
                                            </select></label>
                                          </div>

                                          <div class="col-lg-3">
                                             <label><?= lang('vehicle model','ucword'); ?>
                                                <select class="form-control" name="group_name">
                                                <option value="0" selected disabled><?= lang('select group name','ucword'); ?></option>
                                                <?php
                                                  foreach($group_data as $group_value):
                                                ?>
                                                    <option value="<?php echo $group_value->group_id ?>"><?php echo $group_value->group_name ?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                                </select>
                                              </label>
                                           </div>

                                          <div class="row text-center">
                                           <div class="col-lg-12">
                                             <label>&nbsp; <input class="btn btn-info form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                           </div>                                            
                                          </div>
                                        </div>
                                        <?php echo form_close(); ?>
                    
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="transactionsPortlet" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive dashboard-demo-table">
                                               <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>S.No.</th>
                <th>Vehicle No.</th>
                <th>Vehicle Type</th>
                <th>Vehicle Brand</th>
                <th>Vehicle Model</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>
                  <div class="btn-group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                      <li>
                        <a href="#"><?= lang('show','ucword'); ?></a>
                      </li>
                      <li>
                        <a href="#"><?= lang('edit','ucword'); ?></a>
                      </li>
                      <li><a class="nav-link" data-toggle="modal" href="#hospitalDeleteModal" </a></li>
                      <li>
                      </li>
                  </ul>
                </div>
                </td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
                <td>
                  <div class="btn-group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                      <li>
                        <a href="#"><?= lang('show','ucword'); ?></a>
                      </li>
                      <li>
                        <a href="#"><?= lang('edit','ucword'); ?></a>
                      </li>
                      <li><a class="nav-link" data-toggle="modal" href="#hospitalDeleteModal" </a></li>
                      <li>
                      </li>
                  </ul>
                </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            
            <tr>
                <th>S.No.</th>
                <th>Vehicle No.</th>
                <th>Vehicle Type</th>
                <th>Vehicle Brand</th>
                <th>Vehicle Model</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

  </div>
</div>