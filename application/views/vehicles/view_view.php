<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = base_url()."vehicles/add_edit/".$vehicleId;
// echo form_open_multipart($url);
// meDebug($this->session->all_userdata(),true);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br> <a class="pull-right btn btn-default" href="<?php echo $url;?>">Edit Vehicle Details</a>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                     <center><legend>Vehicle Details</legend></center>

                                        <div class="row">
                                          <label class="col-md-6">Vehicle Type</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo (isset($vehicleDetails['model_type']) ? $vehicleDetails['model_type'] : ' - ');
                                            ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Vehicle Model</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo (isset($vehicleDetails['model_name']) ? $vehicleDetails['model_name'] : ' - ');
                                            ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Vehicle Number</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo (isset($vehicleDetails['vehicle_number']) ? $vehicleDetails['vehicle_number'] : ' - ');
                                            ?>
                                          </span>
                                        </div>

                                        <div class="row">
                                          <label class="col-md-6">Avilable Facilities</label>
                                          <span class="col-md-6"> : 
                                            <?php
                                            if(is_array($vehicleDetails['facilities']) && !empty($vehicleDetails['facilities'])) {
                                            	$facilityStr = '';
                                            	foreach ($vehicleDetails['facilities'] as $key => $facility) {
                                            		$facilityStr .= $facility['facility_name'].", ";
                                            	}
                                            	$facilityStr = rtrim($facilityStr,", ");
                                            	echo $facilityStr;
                                            }
                                            else {
                                            	echo " - ";
                                            }
                                            ?>
                                          </span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="col-lg-12">
                                      <center><legend>Vehicle Images</legend></center>
                                      <div class="row">
                                        <?php
                                        // meDebug($imgsList,1);
                                          if(is_array($imgsList) && !empty($imgsList)) {
                                            foreach ($imgsList as $key => $value) {
                                                $imgKey = $key + 1;
                                              
                                              $imgExplode = explode(".", $value['image_name']);
                                              $url = asset_url()."uploads/vehicles/thumb/".$imgExplode['0'].'_thumb.'.$imgExplode['1'];
                                              // echo $url;
                                              echo '<div class="column"><img src='.$url.' style="width:100%" onclick="openModal();currentSlide('.$imgKey.')" class="hover-shadow cursor"></div>';
                                            }
                                          }
                                        ?>
                                      </div>

                                      <div id="lightBoxModal" class="modal">
                                        <span class="close cursor" onclick="closeModal()">&times;</span>
                                        <div class="modal-content">
                                          <?php
                                        // meDebug($imgsList,1);
                                          if(is_array($imgsList) && !empty($imgsList)) {
                                            foreach ($imgsList as $key => $value) {
                                              $imgExplode = explode(".", $value['image_name']);
                                              $url = asset_url()."uploads/vehicles/".$imgExplode['0'].".".$imgExplode['1'];
                                              // echo $url;
                                              echo '<div class="mySlides"><div class="numbertext">1 / 4</div><img src="'.$url.'" style="width:100%"></div>';
                                            }
                                          }
                                        ?>
                                          
                                          <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                          <a class="next" onclick="plusSlides(1)">&#10095;</a>

                                          <div class="caption-container">
                                            <p id="caption"></p>
                                          </div>
                                          <?php
                                          // meDebug($imgsList,1);
                                            if(is_array($imgsList) && !empty($imgsList)) {
                                              foreach ($imgsList as $key => $value) {
                                                $imgKey = $key + 1;
                                                $imgExplode = explode(".", $value['image_name']);
                                                $url = asset_url()."uploads/vehicles/thumb/".$imgExplode['0'].'_thumb.'.$imgExplode['1'];
                                                // echo $url;
                                                echo '<div class="column"><img class="demo cursor" src='.$url.' style="width:100%" onclick="currentSlide('.$imgKey.')" alt="Nature and sunrise"></div>';
                                              }
                                            }
                                          ?>
                                        </div>
                                      </div>
                  	
                                    </div>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                                      <div class="modal-dialog">
                                      <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h3 class="modal-title">Heading</h3>
                                      </div>
                                      <div class="modal-body">
                                        
                                      </div>
                                      <div class="modal-footer">
                                        <button id="prev-btn" class="btn btn-warning">Prev</button>
                                            <button id="next-btn" class="btn btn-primary">Next</button>
                                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                       </div>
                                      </div>
                                    </div>
                                </div>
                                <!-- 
                                <div class="row">
                                    <div class="col-lg-12">
                                     <center><legend>Other Details</legend></center>
                                        <div class="row">
                                          <label class="col-md-6">Contact Person Name</label>
                                          <span class="col-md-6"> : 
                                            <?php 
                                              echo ((isset($vehicleDetails['contact_person_name']) && !empty($vehicleDetails['contact_person_name'])) ? $vehicleDetails['contact_person_name'] : ' - ');
                                            ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Contact Person Mobile Number</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo ((isset($vehicleDetails['contact_person_mobile']) && !empty($vehicleDetails['contact_person_mobile'])) ? $vehicleDetails['contact_person_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Create Date</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo ((isset($vehicleDetails['created_date']) && !empty($vehicleDetails['created_date'])) ? date('d-m-Y',strtotime($vehicleDetails['created_date'])) : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                        <div class="row">
                                          <label class="col-md-6">Created By</label>
                                          <span class="col-md-6"> :  
                                          <?php 
                                            echo ((isset($vehicleDetails['cmpny_alt_mobile']) && !empty($vehicleDetails['cmpny_alt_mobile'])) ? $vehicleDetails['cmpny_alt_mobile'] : ' - ');
                                          ?>
                                          </span>
                                        </div>
                                    </div>
                                </div> -->

                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>