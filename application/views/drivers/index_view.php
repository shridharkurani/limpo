<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'drivers/add';
$filter_url = 'drivers/filter';
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> 
                        <div class="pull-right right-margin-2px">
                          <a href="<?php echo base_url().'drivers/add_edit'; ?>" class="btn btn-primary"><?= lang('add new driver','ucword'); ?></a>
                        </div></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet portlet-grey">
                                <div class="portlet-heading hidden">
                                    <?php  echo form_open($filter_url,'class=mar_pad_1');  ?>
                                    <div class="row ">
                                      <div class="col-lg-4 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('Driver Name','ucword'); ?>
                                            <input type="text" name="driver_name" class=" col-lg-12 form-control">
                                          </label>
                                       </div>
                                      <div class="col-lg-4 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('Driver mobile','ucword'); ?>
                                            <input type="text" name="driver_mobile" class=" col-lg-12 form-control">
                                          </label>
                                       </div>
                                      <div class="col-lg-4 col-sm-12 col-md-3">
                                         <label class="width100"><?= lang('status','ucword'); ?>
                                          <select class="form-control col-lg-12" name="status" id="status">
                                            <option value="1"> Active </option>
                                            <option value="2"> Blocked </option>
                                          </select>
                                          </label>
                                       </div>
                                       
                                      <div class="row text-center">
                                       <div class="col-lg-12">
                                         <label>&nbsp; <input class="btn btn-primary form-control" type="submit" name="" value="<?= lang('filter and search','ucword'); ?>"></label>
                                         <label>&nbsp; <a href="<?php echo base_url().'drivers';?>" class="btn btn-default">Clear Search</a></label>
                                       </div>                                            
                                      </div>                       
                                      </div>
                                    </div>
                                    <?php echo form_close(); ?>
                
                                    <div class="clearfix"></div>
                                </div>
                                <div id="transactionsPortlet" class="panel-collapse collapse in">
                                    <div class="portlet-body">
                                        <div class="table-responsive dashboard-demo-table">
                                           <table id="example" class="table table-striped table-bordered" style="width:100%">
                                  <thead>
                                      <tr>
                                        <th>S.No.</th>
                                        <th>Driver Name</th>
                                        <th>Driver Mobile</th>
                                        <th>Status</th>
                                        <th>Actions</th><!-- activate, Block, delete, edit. -->
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      if(is_array($driverDetails) && !empty($driverDetails)) {
                                          $cnt = 1;
                                          foreach ($driverDetails as $key => $driver) {
                                            switch ($driver['flag']) {
                                              case 1: $Status = 'Active';
                                                break;
                                              case 2: $Status = 'Blocked';
                                                break;
                                              case 3: $Status = 'Deleted';
                                                break;
                                              default:$Status = 'Pending for approval';
                                                break;
                                            }
                                            $actionBtn = '<div class="btn-group">
                                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i>
                                              </button>
                                              <ul class="dropdown-menu pull-right" role="menu">
                                                  
                                                <li>
                                                  <a href="'.base_url().'drivers/add_edit/'.$driver['id'].'">Edit</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'drivers/view/'.$driver['id'].'">View</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'drivers/actions/'.$driver['id'].'/1">Activate</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'drivers/actions/'.$driver['id'].'/2">Block</a>
                                                </li>
                                                <li>
                                                  <a href="'.base_url().'drivers/actions/'.$driver['id'].'/3">Delete</a>
                                                </li>
                                              </ul>
                                            </div>';
                                            echo "<tr>";
                                            echo "<td>".$cnt++."</td>";
                                            echo "<td>".$driver['fullname']."</td>";
                                            echo "<td>".$driver['mobile']."</td>";
                                            echo "<td>".$Status."</td>";
                                            echo "<td>".$actionBtn."</td>";
                                            echo "</tr>";
                                          }
                                      }
                                    ?>
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                        <th>S.No.</th>
                                        <th>Driver Name</th>
                                        <th>Driver Mobile</th>
                                        <th>Status</th>
                                        <th>Actions</th><!-- activate, Block, delete, edit. -->
                                      </tr>
                                  </tfoot>
                              </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->

                    </div>
                </div>
            </div>
  </div>
</div>