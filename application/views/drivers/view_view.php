<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$url = 'user/add';

// echo $this->session->userdata('user_id');
// exit();
?>
<div id="page-wrapper">
    <div class="page-content">
     <!-- begin PAGE TITLE AREA -->
            <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h1> <?php echo $page_title;?> </h1>
                        <?php echo $this->layouts->print_breadcrumb(); ?>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php echo $this->layouts->print_msg_div($this->session); ?>
            <!-- /.row -->
            <!-- end PAGE TITLE AREA -->

		<?php  echo form_open($url,'id=account_signin');  ?>
		<div class="row well">
			<div class="col_row col-lg-12">
				<div class="col-lg-4">
					<label><?= lang('first name','ucword'); ?></label>
					<input class="form-control" type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" id="firstname">
					<?php echo form_error('first_name', '<p class="inline_error">', '</p>'); ?>
				</div>
				<div class="col-lg-4">
					<label><?= lang('last name','ucword'); ?></label>
					<input class="form-control"  type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" id="lastname">
					<?php echo form_error('last_name', '<p class="inline_error">', '</p>'); ?>
				</div>
				<div class="col-lg-4">
					<label><?= lang('user type','ucword'); ?></label>
					<select class="form-control"  name="user_type" id="usertype">
						<option value="0" disabled="disabled" selected="selected"><?= '--'.lang('select user type','ucword').'--'; ?></option>
						<?php
							foreach($user_levels as $level_data):
						?>
						<option value="<?= $level_data->level_id ?>"><?= $level_data->level_name ?></option>
						<?php 
							endforeach;
						?>
					</select>
					<?php echo form_error('user_type', '<p class="inline_error">', '</p>'); ?>
				</div>
			</div>


			<div class="col_row col-lg-12">
				<div class="col-lg-4">
					<label><?= lang('city','ucword'); ?></label>
					<input class="form-control"  type="text" name="city" value="<?php echo set_value('city'); ?>" id="cityname">
					<?php echo form_error('city', '<p class="inline_error">', '</p>'); ?>
				</div>
				<div class="col-lg-4">
					<label><?= lang('zipcode','ucword'); ?></label>
					<input class="form-control"  type="text" name="zipcode" value="<?php echo set_value('zipcode'); ?>" id="zipcode">
					<?php echo form_error('zipcode', '<p class="inline_error">', '</p>'); ?>
				</div>
				<div class="col-lg-4">
					<label><?= lang('street','ucword'); ?></label>
					<input class="form-control"  type="text" name="street_name" value="<?php echo set_value('street_name'); ?>" id="streetname">
					<?php echo form_error('street_name', '<p class="inline_error">', '</p>'); ?>
				</div>
			</div>
			<div class="col_row col-lg-12">
				<div class="col-lg-4">
					<label><?= lang('house','ucword'); ?></label>
					<input class="form-control"  type="text" name="house_name" value="<?php echo set_value('house_name'); ?>" id="housename">
					<?php echo form_error('house_name', '<p class="inline_error">', '</p>'); ?>
				</div>
				<div class="col-lg-4">
					<label><?= lang('address 1','ucword'); ?></label>
					<textarea class="form-control"  name="address_first" id="addressfirst"><?php echo set_value('address_first'); ?></textarea>
					<?php echo form_error('address_first', '<p class="inline_error">', '</p>'); ?>
				</div>
				<div class="col-lg-4">
					<label><?= lang('address 2','ucword'); ?></label>
					<textarea class="form-control"  name="address_second" id="addresssecond"><?php echo set_value('address_second'); ?></textarea>
					<?php echo form_error('address_second', '<p class="inline_error">', '</p>'); ?>
				</div>
			</div>

			<div class="col_row col-lg-12">
				<div class="col-lg-4">
					<label><?= lang('language','ucword'); ?></label>
					<select class="form-control"  name="user_lang">
						<option value="0" disabled="disabled" selected="selected"><?= '--'.lang('select language','ucword').'--'; ?></option>
						<option value="english"><?= lang('english','ucword'); ?></option>
						<option value="spanish"><?= lang('spanish','ucword'); ?></option>
					</select>
					<?php echo form_error('user_lang', '<p class="inline_error">', '</p>'); ?>
				</div>
			</div>

			<div class="col_row col-lg-12">
				<h3><?= lang('basic details','ucword'); ?></h3>
				<div class="col-lg-6">
					<label><?= lang('username','ucword'); ?></label>
					<input class="form-control"  type="text" name="user_name" value="<?php echo set_value('user_name'); ?>" id="username">
					<?php echo form_error('user_name',  '<p class="inline_error">', '</p>'); ?>
				</div>

				<div class="col-lg-6">
					<label><?= lang('email id','ucword'); ?></label>
					<input class="form-control"  type="text" name="email_id" value="<?php echo set_value('email_id'); ?>" id="emailid">
					<?php echo form_error('email_id', '<p class="inline_error">', '</p>'); ?>
				</div>

				<div class="col-lg-6">
					<label><?= lang('phone number','ucword'); ?></label>
					<input class="form-control"  type="text" name="user_phone" value="<?php echo set_value('user_phone'); ?>" id="userphone">
					<?php echo form_error('user_phone',  '<p class="inline_error">', '</p>'); ?>
				</div>

				<div class="col-lg-6">
					<label><?= lang('password','ucword'); ?></label>
					<input class="form-control"  type="password" name="password" value="<?php echo set_value('password'); ?>" id="password">
					<p><?php echo form_error('password',  '<p class="inline_error">', '</p>'); ?>
				</div>
			</div>
			<div class="col_row col-lg-12">
				<center><input class="btn" type="submit" value="<?= lang('submit','ucword'); ?>"></input></center>
			</div>
		</div>
		<?php echo form_close(); ?>

	</div>
</div>