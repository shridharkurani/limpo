<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isset($driverId) && $driverId != 0)
$url = base_url()."drivers/add_edit/".$driverId;
else 
$url = base_url()."drivers/add_edit";

echo form_open_multipart($url);
// meDebug($this->session->all_userdata(),true);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default"><!-- 
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <center><legend>Driver Details</legend></center>
                                    <div class="col-lg-12">
                                        <div class="col-md-4 required">
                                          <label>Driver Name</label>
                                          <input type="text" class="form-control" name="driver_name" value="<?php
                                          echo set_value('driver_name', isset($driverDetails['fullname']) ? $driverDetails['fullname'] : '');
                                          ?>" />
                                          <?php echo form_error('driver_name', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Driver Mobile</label>
                                          <input type="text" class="form-control" name="driver_mobile" value="<?php
                                          echo set_value('driver_mobile', isset($driverDetails['mobile']) ? $driverDetails['mobile'] : '');
                                          ?>" />
                                          <?php echo form_error('driver_mobile', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                       <!--  <div class="col-md-4 required">
                                          <label>Vehicle Number</label>
                                          <input type="text" class="form-control" name="vehicle_number" value="<?php
                                          echo set_value('vehicle_number', isset($driverDetails['vehicle_number']) ? $driverDetails['mobile'] : '');
                                          ?>" />
                                          <?php echo form_error('vehicle_number', '<div class="inline_error">', '</div>'); ?>
                                        </div> -->
                                    </div>
                                    <div class="col-lg-12">
                                    <center><legend>Languages Known by Driver</legend></center>
                                    	<?php
                                    	if(is_array($languageArray) && !empty($languageArray)) {
                                    		foreach ($languageArray as $key => $language) {
                                    				if(isset($langauagesArraywithIds) && is_array($langauagesArraywithIds) && !empty($langauagesArraywithIds))
                                    					$isChecked = (in_array($language['id'], $langauagesArraywithIds)) ? "checked" : "" ;
                                    				else 
                                    					$isChecked = '';
                                    			echo "<div class='col-lg-3'>";
                                    			echo "<label><input type='checkbox' ".$isChecked." name='language[]' value=".$language['id'].">     ".$language['language_name']."</label>";
                                    			echo "</div>";
                                    		}
                                    	}
                                    	?>
                                    </div>
                                   <!--  <div class="col-lg-12">
                                    <center><legend>Places Known by Driver</legend></center>
                                      <?php
                                      if(is_array($placesArray) && !empty($placesArray)) {
                                        foreach ($placesArray as $key => $place) {
                                            if(isset($placesArraywithIds) && is_array($placesArraywithIds) && !empty($placesArraywithIds))
                                              $isChecked = (in_array($place['id'], $placesArraywithIds)) ? "checked" : "" ;
                                            else 
                                              $isChecked = '';
                                          echo "<div class='col-lg-3'>";
                                          echo "<label><input type='checkbox' ".$isChecked." name='place[]' value=".$place['id'].">     ".$place['place_name']."</label>";
                                          echo "</div>";
                                        }
                                      }
                                      ?>
                                    </div> -->
                                   <!--  <div class="col-lg-12">
                                    <center><legend>Vehicle Images</legend></center>
                                    <div class="col-lg-12">
				                        <div class="portlet portlet-default dropzone-portlet">
				                            <div class="portlet-heading">
				                                <div class="portlet-title">
				                                    <h4>Dropzone Uploader Example</h4>
				                                </div>
				                                <div class="clearfix"></div>
				                            </div>
				                            <div class="portlet-body">
				                            	<form action="" method="post" enctype="multipart/form-data">
												  <input type="file" name="file" />
												</form>

												<form action="upload.php" class="dropzone"></form>
				                            </div>
				                        </div>
				                    </div>
                                    </div> -->

                                <div class=" text-center col-lg-12">
                                    <legend>&nbsp</legend>
                                    <!-- <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>"> -->
                                    <input type="submit" name="" value="Save Price Details" class="btn btn-default">
                                </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>