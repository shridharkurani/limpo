<?php
/**
 * Created by PhpStorm.
 * User: faisad.ali
 * Date: 23/06/2018
 * Time: 03:36
 */
?>
<!-- Top Panel -->
<nav class="navbar-top hidden" role="navigation">
    <!-- begin BRAND HEADING -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
            <i class="fa fa-bars"></i> Menu
        </button>
        <div class="navbar-brand">
            <a href="http://mytrips.net/">
                <!-- <img src="http://mytrips.net/assets/imgs/logo.png" class="img-responsive" alt=""> -->
            </a>
        </div>
    </div>
    <!-- end BRAND HEADING -->
    <div class="nav-top">
        <!-- begin LEFT SIDE WIDGETS -->
        <ul class="nav navbar-left">
            <li class="tooltip-sidebar-toggle">
                <a href="#" id="sidebar-toggle" data-toggle="tooltip" data-placement="right" title="Sidebar Toggle">
                    <i class="fa fa-bars"></i>
                </a>
            </li>
            <!-- You may add more widgets here using <li> -->
        </ul>
        <!-- end LEFT SIDE WIDGETS -->
        <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
        <ul class="nav navbar-right">
            <!-- begin USER ACTIONS DROPDOWN -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i> Faisad <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="http://mytrips.net/user/myaccount">
                            <i class="fa fa-gear"></i> My Account
                        </a>
                    </li>
                    <li>
                        <a class="logout_open" href="http://mytrips.net/account/logout">
                            <i class="fa fa-sign-out"></i> Logout
                            <strong> </strong>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-menu -->
            </li>
            <!-- /.dropdown -->
            <!-- end USER ACTIONS DROPDOWN -->

        </ul>
        <!-- /.nav -->
        <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

    </div>
</nav>
<!-- End Top Panel  -->
