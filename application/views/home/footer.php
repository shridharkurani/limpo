<?php
/**
 * Created by PhpStorm.
 * User: faisad.ali
 * Date: 23/06/2018
 * Time: 03:37
 */
?>

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/dropzone/js/dropzone.js"></script>
<script src="<?php echo base_url(); ?>assets/js/my.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/hisrc/hisrc.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables_ajax.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>

