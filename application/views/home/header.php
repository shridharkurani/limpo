<?php
/**
 * Created by PhpStorm.
 * User: faisad.ali
 * Date: 23/06/2018
 * Time: 03:16
 */
?>

<head>
    <meta content="utf-8" http-equiv="encoding">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo SITE_NAME . ' - ' . DEFAULT_SITE_DESC; ?></title>
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/plugins/DataTables/datatables.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/plugins.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/view.css" rel="stylesheet"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/local_image/icos/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo base_url(); ?>assets/local_image/icos/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo base_url(); ?>assets/local_image/icos/apple-touch-icon-114x114.png">
    <!-- Favicons Ends here -->
    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
    </script>
</head>

