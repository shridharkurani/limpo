
<div id="wrapper">
<nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url()."imgs/nammalogo.png";?>" class="hisrc img-responsive" alt="" width="172" height="20">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

                <!-- begin LEFT SIDE WIDGETS -->
                <ul class="nav navbar-left">
                    <li class="tooltip-sidebar-toggle">
                        <a href="#" id="sidebar-toggle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Sidebar Toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <!-- You may add more widgets here using <li> -->
                </ul>
                <!-- end LEFT SIDE WIDGETS -->

                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">

                    <!-- /.dropdown -->
                    <!-- end TASKS DROPDOWN -->

                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?= strtoupper($this->session->userdata('user_fullname'))." ";?><i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user" style="width: 100%;">
                            
                            <!-- <li>
                                <a href="profile.html">
                                    <i class="fa fa-user"></i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> My Messages
                                    <span class="badge green pull-right">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-bell"></i> My Alerts
                                    <span class="badge orange pull-right">9</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-tasks"></i> My Tasks
                                    <span class="badge blue pull-right">10</span>
                                </a>
                            </li>
                            <li>
                                <a href="calendar.html">
                                    <i class="fa fa-calendar"></i> My Calendar
                                </a>
                            </li> -->
                            <li class="divider"></li>
                            <!-- <li>
                                <a href="#">
                                    <i class="fa fa-gear"></i> Settings
                                </a>
                            </li> -->
                            <li>
                                <a class="logout_open" href="<?= base_url().'account/logout';?>" data-popup-ordinal="0" id="open_34592005">
                                    <i class="fa fa-sign-out"></i> Logout
                                    <!-- <strong>John Smith</s trong> -->
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->

                </ul>
                <!-- /.nav -->
                <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

            </div>
            <!-- /.nav-top -->
</nav>
    <!-- begin SIDE NAVIGATION -->
    <nav class="navbar-side" role="navigation">

        <div class="navbar-collapse sidebar-collapse collapse">
            <ul id="side" class="nav navbar-nav side-nav">
                <!-- begin SIDE NAV USER PANEL -->
                <li class="side-user hidden-xs">
                <?php
                $session_profile_pic = $this->session->userdata('session_profile_pic');
                if(empty($session_profile_pic)) {
                    $profile_pic = 'imgs/logo.png';
                }
                else {
                    $profile_pic = $session_profile_pic;
                }
                ?>
                <img class="img-circle" width="160" height="160" src="<?php echo asset_url().'uploads/company/thumb/'.$profile_pic; ?>" alt="">
                <div class="clearfix"></div>
                <div>
                    <legend><label class="CmpnyHeroName"><?= $this->session->userdata('session_cmpny_display_name');?></label></legend>
                </div>
                </li>
                <li>
                    <a class="active"  href="<?php echo base_url().'dashboard/'; ?>">
                       <h5>  <i class="fa fa-dashboard "></i> Dashboard </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'company/overview'; ?>">
                        <h5> <i class="fa fa fa-flag "></i> Company Profile </h5>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url().'prices'; ?>">
                        <h5>  <i class="fa  fa-rupee"></i> Price settings </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'vehicles'; ?>">
                        <h5> <i class="fa fa-car "></i> Vehicles </h5>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url().'drivers'; ?>">
                        <h5>  <i class="fa fa-user "></i> Drivers </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'trips'; ?>">
                        <h5> <i class="fa  fa-credit-card"></i>Payments</h5>
                    </a>
                </li>
                
                <li>
                    <a href="<?php echo base_url().'enquiries'; ?>">
                       <h5>  <i class="fa  fa-calendar"></i> Enquiries </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'trips'; ?>">
                       <h5>  <i class="fa far fa-road"></i> Trips </h5>
                    </a>
                </li>


            </ul>
            <!-- /.side-nav -->
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <!-- /.navbar-side -->
    <!-- end SIDE NAVIGATION -->

</div>