<?php
/**
 * Created by PhpStorm.
 * User: faisad.ali
 * Date: 23/06/2018
 * Time: 02:55
 */
?>
<!DOCTYPE html>
<html lang="en">

<?php echo $this->load->view('home/header'); ?>

<body>

<?php echo $this->load->view('home/top_panel'); ?>

<?php echo $this->load->view('home/left_panel'); ?>

<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <ul class="breadcrumb custom-breadcrumb">
                        <li><a href=http://mytrips.net/dashboard/><i class='fa fa-dashboard'> </i> Dashboard</a>
                        </li>
                        <li class='active'>Company Profile</li>
                    </ul>
                    <br>
                    <h1></h1>
                </div>
            </div>
            <div class="col-lg-12">
                <form action="http://mytrips.net/enquiries/add_edit" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="portlet portlet-default">
                        <div class="portlet-body">
                            <div class="row">
                                <legend class="text-center">Trip Details</legend>
                                <div class="col-lg-12">
                                    <!-- Step Circle -->
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step col-xs-4">
                                                <a href="#basic" type="button" class="btn btn-success btn-circle">1</a>
                                                <p>
                                                    <small>Basic Details</small>
                                                </p>
                                            </div>
                                            <div class="stepwizard-step col-xs-4">
                                                <a href="#price" type="button" class="btn btn-default btn-circle"
                                                   disabled="disabled">2</a>
                                                <p>
                                                    <small>Price Details</small>
                                                </p>
                                            </div>
                                            <div class="stepwizard-step col-xs-4">
                                                <a href="#payment" type="button" class="btn btn-default btn-circle"
                                                   disabled="disabled">3</a>
                                                <p>
                                                    <small>Payment Details</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Trip Enquiry From -->
                                    <form role="form">
                                        <div class="panel panel-primary setup-content" id="basic">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Basic Details</h3>
                                            </div>
                                            <div class="panel-body row">
                                                <div class="col-lg-12">
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Start Date</label>
                                                        <input type="text" name="start_date" id="start_date"
                                                               class="form-control"
                                                               value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">End Date</label>
                                                        <input type="text" name="end_date" class="form-control" value=""
                                                               required="required"/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Seaters</label>
                                                        <input type="text" name="seaters" class="form-control" value=""
                                                               required="required"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">From</label>
                                                        <input type="text" name="trip_from" class="form-control"
                                                               value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">Via</label>
                                                        <input type="text" name="trip_via" class="form-control"
                                                               value=""/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">To</label>
                                                        <input type="text" name="seaters" class="form-control" value=""
                                                               required="required"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-md-3 required">
                                                        <label class="control-label">Vehicle Type</label>
                                                        <select name="vehicle_type" id="vehicle_type"
                                                                class="form-control"
                                                                value="">
                                                            <option value="0">Select</option>
                                                            <option value=hitachi>HITACHI</option>
                                                            <option value=pushback>PUSHBACK</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 required">
                                                        <label class="control-label">Vehicle Model</label>
                                                        <select name="vehicle_model" id="vehicle_model"
                                                                class="form-control"
                                                                value="">
                                                            <option value="0">Select Vehicle Type</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 required">
                                                        <label class="control-label">Minimum Km. Per Day</label>
                                                        <input type="text" class="form-control" name="min_km" value=""
                                                               required="required"/>
                                                    </div>
                                                    <div class="col-md-3 required">
                                                        <label class="control-label">Approx Km. for this trip</label>
                                                        <input type="text" class="form-control" name="approx_km"
                                                               value="" required="required"/>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <legend class="text-center">Customer Details</legend>
                                                    <div class="col-md-3 required">
                                                        <label class="control-label">Customer Name</label>
                                                        <input type="text" class="form-control " name="customer_name"
                                                               value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-3 required">
                                                        <label class="control-label">Customer Mobile</label>
                                                        <input type="text" class="form-control" name="customer_mobile"
                                                               value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label">Customer Alternative Mobile</label>
                                                        <input type="text" class="form-control "
                                                               name="customer_alt_mobile"
                                                               value=""/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="control-label">Customer Email</label>
                                                        <input type="text" class="form-control " name="customer_email"
                                                               value=""/>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <button class="btn btn-primary nextBtn pull-right"
                                                                type="button">
                                                            Next
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-primary setup-content" id="price">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Price Details</h3>
                                            </div>
                                            <div class="panel-body row">
                                                <div class="col-lg-12">
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Price Per Km.</label>
                                                        <input type="text" class="form-control numeric_field"
                                                               name="per_km"
                                                               value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Price Per Km. for A/C</label>
                                                        <input type="text" class="form-control numeric_field"
                                                               name="per_km_ac" value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Driver Allowance</label>
                                                        <input type="text" class="form-control numeric_field"
                                                               name="driver_allowance" value="" required="required"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <legend class="text-center">Extra Charge Details</legend>
                                                <div class="col-lg-12">
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Price per extra Km.</label>
                                                        <input type="text" class="form-control numeric_field"
                                                               name="extra_km" value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Price per extra Km. for A/C</label>
                                                        <input type="text" class="form-control numeric_field"
                                                               name="extra_km_ac" value="" required="required"/>
                                                    </div>
                                                    <div class="col-md-4 required">
                                                        <label class="control-label">Driver Allowance per Night</label>
                                                        <input type="text" class="form-control numeric_field"
                                                               name="extra_driver_allowance" value=""
                                                               required="required"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <button class="btn btn-primary prevBtn pull-right"
                                                                type="button">
                                                            Prev
                                                        </button>
                                                        <button class="btn btn-primary nextBtn pull-right"
                                                                type="button">
                                                            Next
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-primary setup-content" id="payment">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Payment Details</h3>
                                            </div>
                                            <div class="panel-body row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <div class="portlet portlet-blue">
                                                            <div class="portlet-heading">
                                                                <div class="portlet-title">
                                                                    <h4>Total Payment for Non A/C.</h4>
                                                                </div>
                                                                <div class="portlet-widgets">
                                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                                       href="#inlineFormExample">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div id="inlineFormExample"
                                                                 class="panel-collapse collapse in">
                                                                <div class="portlet-body inline-block">
                                                                    <div class="col-lg-6">
                                                                        Approx Km.
                                                                    </div>
                                                                    <div class="col-lg-6" id="approx_km">
                                                                        -
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        Price per Km. -
                                                                    </div>
                                                                    <div class="col-lg-6" id="price_per_km">
                                                                        -
                                                                    </div>
                                                                    <div class="col-lg-6" id="driver_allowance">
                                                                        Driver Allowance
                                                                    </div>
                                                                    <div class="col-lg-6" id="approx_km">
                                                                        -
                                                                    </div>
                                                                    <div class="col-lg-6" id="number_of_days">
                                                                        Number of Days
                                                                    </div>
                                                                    <div class="col-lg-6" id="approx_km">
                                                                        -
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.portlet -->
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="portlet portlet-blue">
                                                            <div class="portlet-heading">
                                                                <div class="portlet-title">
                                                                    <h4>Total Payment for A/C.</h4>
                                                                </div>
                                                                <div class="portlet-widgets">
                                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                                       href="#inlineFormExample"><i
                                                                                class="fa fa-chevron-down"></i></a>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div id="inlineFormExample"
                                                                 class="panel-collapse collapse in">
                                                                <div class="portlet-body inline-block">
                                                                    <div class="col-lg-6">
                                                                        Approx Km.
                                                                    </div>
                                                                    <div class="col-lg-6" id="approx_km_ac">
                                                                        -
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        Price per Km. -
                                                                    </div>
                                                                    <div class="col-lg-6" id="price_per_km_ac">
                                                                        -
                                                                    </div>
                                                                    <div class="col-lg-6" id="driver_allowance_ac">
                                                                        Driver Allowance
                                                                    </div>
                                                                    <div class="col-lg-6" id="approx_km">
                                                                        -
                                                                    </div>
                                                                    <div class="col-lg-6" id="number_of_days">
                                                                        Number of Days
                                                                    </div>
                                                                    <div class="col-lg-6" id="approx_km">
                                                                        -
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.portlet -->
                                                    </div>

                                                </div>
                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <legend class="text-center">Advance Payment Options</legend>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <label><input type="radio" name="advance_payment_type"> Fixed
                                                            Amount</label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="fixed_amount" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <label><input type="radio" name="advance_payment_type"> 25% of
                                                            the Non-AC Price</label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" readonly=""
                                                               name="non_ac_percentage"/>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <label><input type="radio" name="advance_payment_type"> 25% of
                                                            the AC
                                                            Price</label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" readonly=""
                                                               name="ac_percentage" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <label><input type="radio" name="advance_payment_type"> No
                                                            Advance
                                                            Payment</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <br/>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12">
                                                        <button class="btn btn-primary pull-right" type="submit">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <br/><br/>
                                <hr/>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $this->load->view('home/footer'); ?>

</body>
</html>
