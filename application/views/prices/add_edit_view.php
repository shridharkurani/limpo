<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(isset($priceId) && $priceId != 0)
$url = base_url()."prices/add_edit/".$priceId;
else 
$url = base_url()."prices/add_edit";

echo form_open_multipart($url);
// meDebug($this->session->all_userdata(),true);
?>
<!-- <form class="form-horizontal" method="post" role="form" action=<?= $url;?> > -->
    <div id="page-wrapper">
        <div class="page-content page-content-ease-in">
            <div class="row">
            <?php echo $this->layouts->print_msg_div($this->session); ?>
                <div class="col-lg-12">
                    <div class="page-title">
                        <?php echo $this->layouts->print_breadcrumb(); ?><br>
                        <h1> <?php echo $page_title;?> </h1>
                    </div>
                </div>
                    <div class="col-lg-12">
                        <div class="portlet portlet-default"><!-- 
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Company / Organization Details</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="portlet-body">
                                <div class="row">
                                    <center><legend>Vehicle Details</legend></center>
                                    <div class="col-lg-12">
                                        <div  class="col-md-4 required">
                                          <label>Vehicle Type</label>
                                          <select name="vehicle_type" id="vehicle_type" class="form-control" value="<?php echo set_select('vehicle_type'); ?>">
                                          <option value="0">Select</option>
                                          <?php
                                            if($this->input->post('vehicle_type')) {
                                              $vehicle_type =  $this->input->post('vehicle_type');
                                            }
                                            else if(isset($priceDetails['model_type'])) {
                                              $vehicle_type =  $priceDetails['model_type'];
                                            }

                                            foreach ($modelTypeDetails as $key => $value) {
                                              $valId = strtolower($value['model_type']);             
                                              echo '<option value='.$valId.''.set_select("vehicle_type",$valId ,($vehicle_type === $valId  ? TRUE : FALSE )).'>';
                                              echo strtoupper($valId).'</option>';    
                                            }
                                            ?>
                                          </select>
                                          <?php echo form_error('vehicle_type', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div  class="col-md-4 required">
                                          <label>Vehicle Model</label>
                                          <select name="vehicle_model" id="vehicle_model" class="form-control" value="<?php echo set_select('vehicle_model'); ?>">
                                            <option value="0">Select Vehicle Type</option>
                                          <?php
                                            if($this->input->post('vehicle_model')) {
                                              $vehicle_model =  $this->input->post('vehicle_model');
                                            }
                                            else if(isset($priceDetails['model_id'])) {
                                              $vehicle_model =  $priceDetails['model_id'];
                                            }
                                            // meDebug($priceDetails,true);
                                            foreach ($modelNameDetails as $key => $value) {
                                              $val = $value['model_name'];
                                              $valId = $value['model_id'];
                                              echo '<option value='.$valId.''.set_select("vehicle_model",$valId ,($vehicle_model === $valId  ? TRUE : FALSE )).'>';
                                              echo strtoupper($val).'</option>';    
                                            }
                                            ?>
                                          </select>
                                          <?php echo form_error('vehicle_model', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Min Km. Per day</label>
                                          <input type="text" class="form-control numeric_field" name="min_km" value="<?php
                                          echo set_value('min_km', isset($priceDetails['min_km']) ? $priceDetails['min_km'] : '');
                                          ?>" />
                                          <?php echo form_error('min_km', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                      <center><legend>Price Details</legend></center>
                                        <div class="col-md-4 required">
                                          <label>Price Per Km.</label>
                                          <input type="text" class="form-control numeric_field" name="price_per_km" value="<?php
                                          echo set_value('price_per_km', isset($priceDetails['per_km']) ? $priceDetails['per_km'] : '');
                                          ?>" />
                                          <?php echo form_error('price_per_km', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Price Per Km. for A/C</label>
                                          <input type="text" class="form-control numeric_field" name="price_per_km_ac" value="<?php
                                          echo set_value('price_per_km_ac', isset($priceDetails['per_km_ac']) ? $priceDetails['per_km_ac'] : '');
                                          ?>" />
                                          <?php echo form_error('price_per_km_ac', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Driver Allowance.</label>
                                          <input type="text" class="form-control numeric_field" name="driver_allowance" value="<?php
                                          echo set_value('driver_allowance', isset($priceDetails['driver_allowance']) ? $priceDetails['driver_allowance'] : '');
                                          ?>" />
                                          <?php echo form_error('driver_allowance', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                      <center><legend>Extra Charge Details</legend></center>
                                        <div class="col-md-4 required">
                                          <label>Driver allowance per night</label>
                                          <input type="text" class="form-control numeric_field" name="extra_driver_allowance" value="<?php
                                          echo set_value('extra_driver_allowance', isset($priceDetails['driver_allowance_night']) ? $priceDetails['driver_allowance_night'] : '');
                                          ?>" />
                                          <?php echo form_error('extra_driver_allowance', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Price per extra Km.</label>
                                          <input type="text" class="form-control numeric_field" name="extra_km" value="<?php
                                          echo set_value('extra_km', isset($priceDetails['extra_per_km']) ? $priceDetails['extra_per_km'] : '');
                                          ?>" />
                                          <?php echo form_error('extra_km', '<div class="inline_error">', '</div>'); ?>
                                        </div>

                                        <div class="col-md-4 required">
                                          <label>Price per extra Km. A/C</label>
                                          <input type="text" class="form-control numeric_field" name="extra_km_ac" value="<?php
                                          echo set_value('extra_km_ac', isset($priceDetails['extra_per_km_ac']) ? $priceDetails['extra_per_km_ac'] : '');
                                          ?>" />
                                          <?php echo form_error('extra_km_ac', '<div class="inline_error">', '</div>'); ?>
                                        </div>
                                    </div>
                                <div class=" text-center col-lg-12">
                                    <legend>&nbsp</legend>
                                    <!-- <input type="text" id="hidden_owner_name" name="hidden_owner_name" class="hidden" value="<?= $this->session->userdata('user_fullname'); ?>">
                                    <input type="text" id="hidden_owner_mobile" name="hidden_owner_mobile" class="hidden" value="<?= $this->session->userdata('user_mobile'); ?>"> -->
                                    <input type="submit" name="" value="Save Price Details" class="btn btn-default">
                                </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
    </div>
</form>