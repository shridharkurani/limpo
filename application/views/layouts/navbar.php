<div id="wrapper">
    <header id="header">
        <nav id="main-nav" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a style="padding:0px;"class="navbar-brand" href="<?php echo base_url().'index.php'.'#home';?>"><img style="width: 48%;" src="<?php echo asset_url().'imgs/logo.png';?>" alt="logo"></a>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a href="<?php echo base_url().'index.php'.'#home';?>">Home</a></li>
                        <li class="scroll"><a href="<?php echo base_url().'index.php'.'#about';?>">Who We</a></li>
                        <li class="scroll"><a href="<?php echo base_url().'packages/all';?>">Pricing</a></li>
                        <li class="scroll"><a href="<?php echo base_url().'index.php'.'#portfolio';?>">Gallery</a></li>
                        <li class="scroll"><a href="<?php echo base_url().'index.php'.'#contact-us';?>">Contact</a></li>
                        <li class="scroll"><a href="<?php echo base_url().'services/all';?>">Services</a></li>

                        <?php
                        $sessionData = $this->session->all_userdata();
                        if(!empty($sessionData['logged_in']) && $sessionData['logged_in'] == 1) {
                            ?>
                            <!-- begin USER ACTIONS DROPDOWN -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?= strtoupper($this->session->userdata('user_fullname'))." ";?><i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user" style="width: 100%;">

                                    <!-- <li>
                                        <a href="profile.html">
                                            <i class="fa fa-user"></i> My Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailbox.html">
                                            <i class="fa fa-envelope"></i> My Messages
                                            <span class="badge green pull-right">4</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-bell"></i> My Alerts
                                            <span class="badge orange pull-right">9</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-tasks"></i> My Tasks
                                            <span class="badge blue pull-right">10</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="calendar.html">
                                            <i class="fa fa-calendar"></i> My Calendar
                                        </a>
                                    </li> -->

                                    <!--<li>
                                        <a href="calendar.html">
                                            <i class="fa fa-calendar"></i> My Calendar
                                        </a>
                                    </li>-->
                                    <li>
                                        <a class="logout_open" href="<?php echo base_url().'dashboard/';?>" data-popup-ordinal="0" id="open_34592005">
                                            <i class="fa fa-gear"></i> Dashboard
                                        </a>
                                        </a>
                                    </li>
                                    <li class="divider"></li>


                                    <li>
                                        <a class="logout_open" href="<?= base_url().'account/logout';?>" data-popup-ordinal="0" id="open_34592005">
                                            <i class="fa fa-sign-out"></i> Logout
                                            <!-- <strong>John Smith</s trong> -->
                                        </a>
                                    </li>
                                </ul>
                                <!-- /.dropdown-menu -->
                            </li>
                            <!-- /.dropdown -->
                            <!-- end USER ACTIONS DROPDOWN -->
                            <?php
                        }else {
                            ?>

                            <li class="scroll"><a href="<?php echo base_url().'index.php'.'#contact-us';?>http://localhost/gotripz/account/signin">Login /Register</a></li>
                        <?php

                        }
//                        meDebug($sessionData,1);

                        ?>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->