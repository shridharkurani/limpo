<?php

$userType = $this->session->userdata('user_type');

if($userType == 'admin') {
    ?>
    <!-- begin SIDE NAVIGATION -->
    <nav class="navbar-side" role="navigation">

        <div class="navbar-collapse sidebar-collapse collapse">
            <ul id="side" class="nav navbar-nav side-nav">
                <!-- begin SIDE NAV USER PANEL -->
                <li class="side-user hidden-xs">
                    <?php
                    $session_profile_pic = $this->session->userdata('session_profile_pic');
                    if(empty($session_profile_pic)) {
                        $profile_pic = asset_url().'imgs/logo.png';
                    }
                    else {
                        $profile_pic = asset_url().'uploads/company/thumb/'.$session_profile_pic;
                    }
                    $profile_pic = asset_url().'imgs/logo.png';
                    ?>
                    <img class="img-circle" width="160" height="160" src="<?php echo $profile_pic; ?>" alt="">
                    <div class="clearfix"></div>
                    <div>
                        <?php
                        if ($this->session->userdata('session_cmpny_display_name')) {
                            $title = $this->session->userdata('session_cmpny_display_name');
                        } else {
                            $title = "LIMPO";
                        }
                        ?>
                        <legend><label class="CmpnyHeroName"><?= $title;?></label></legend>
                    </div>
                </li>
                <li>
                    <a href="<?php echo base_url().'dashboard/'; ?>">
                        <h5>  <i class="fa fa-dashboard "></i> Dashboard </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'services'; ?>">
                        <h5> <i class="fa fa fa-flag "></i> Services</h5>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url().'packages'; ?>">
                        <h5>  <i class="fa  fa-rupee"></i> Packages </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'discounts'; ?>">
                        <h5> <i class="fa fa-car "></i> Discounts </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'enquiries'; ?>">
                        <h5>  <i class="fa  fa-calendar"></i> Enquiries </h5>
                    </a>
                </li>

            </ul>
            <!-- /.side-nav -->
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <!-- /.navbar-side -->
    <!-- end SIDE NAVIGATION -->
<?php
}
else {
?>
    <!-- begin SIDE NAVIGATION -->
    <nav class="navbar-side" role="navigation">

        <div class="navbar-collapse sidebar-collapse collapse">
            <ul id="side" class="nav navbar-nav side-nav">
                <!-- begin SIDE NAV USER PANEL -->
                <li class="side-user hidden-xs">
                    <?php
                    $session_profile_pic = $this->session->userdata('session_profile_pic');
                    if(empty($session_profile_pic)) {
                        $profile_pic = asset_url().'imgs/logo.png';
                    }
                    else {
                        $profile_pic = asset_url().'uploads/company/thumb/'.$session_profile_pic;
                    }
                    $profile_pic = asset_url().'imgs/logo.png';
                    ?>
                    <img class="img-circle" width="160" height="160" src="<?php echo $profile_pic; ?>" alt="">
                    <div class="clearfix"></div>
                    <div>
                        <?php
                        if ($this->session->userdata('session_cmpny_display_name')) {
                            $title = $this->session->userdata('session_cmpny_display_name');
                        } else {
                            $title = "LIMPO";
                        }
                        ?>
                        <legend><label class="CmpnyHeroName"><?= $title;?></label></legend>
                    </div>
                </li>
                <li>
                    <a class="active"  href="<?php echo base_url().'profile/'; ?>">
                        <h5>  <i class="fa fa-dashboard "></i> Personal Details </h5>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'packages/user'; ?>">
                        <h5> <i class="fa fa fa-flag "></i> Services / Packages </h5>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url().'enquiries/user'; ?>">
                        <h5>  <i class="fa  fa-rupee"></i> Requests </h5>
                    </a>
                </li>

            </ul>
            <!-- /.side-nav -->
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <!-- /.navbar-side -->
    <!-- end SIDE NAVIGATION -->
<?php

}
?>
</div>