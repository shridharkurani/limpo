<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="utf-8" http-equiv="encoding">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $layout_title; ?></title>
	<meta name="description" content="<?= $layout_description;?>">
	<meta name="author" content="Gotripz">
	<?php echo $this->layouts->print_css(); ?>
<!-- Favicons ================================================== -->
<link rel="shortcut icon" href="<?php echo base_url()."assets/local_image/icos/favicon.ico"; ?>" type="image/x-icon">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()."assets/local_image/icos/apple-touch-icon-72x72.png"; ?>" >
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()."assets/local_image/icos/apple-touch-icon-114x114.png"; ?>" >

<!-- Favicons Ends here ======================================== -->
</head>
<body>
	
<?php
// meDebug($this->session->all_userdata());
?>