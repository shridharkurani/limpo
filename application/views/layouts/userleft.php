<!-- begin SIDE NAVIGATION -->
<nav class="navbar-side" role="navigation">

    <div class="navbar-collapse sidebar-collapse collapse">
        <ul id="side" class="nav navbar-nav side-nav">
            <!-- begin SIDE NAV USER PANEL -->
            <li class="side-user hidden-xs">
                <div class="clearfix"></div>
                <div>

                </div>
            </li>
            <li>
                <a class="active"  href="<?php echo base_url().'dashboard/'; ?>">
                    <h5>  <i class="fa fa-dashboard "></i> Personal Details </h5>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url().'services'; ?>">
                    <h5> <i class="fa fa fa-flag "></i> Services </h5>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url().'packages'; ?>">
                    <h5>  <i class="fa  fa-rupee"></i> Orders </h5>
                </a>
            </li>

        </ul>
        <!-- /.side-nav -->
    </div>
    <!-- /.navbar-collapse -->
</nav>
<!-- /.navbar-side -->
<!-- end SIDE NAVIGATION -->

</div>