<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 13-02-2021
 * Time: 02:01
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class package_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    function getAllPackages(){
        $this->db->select('*');
        $this->db->from("packages");
        $query = $this->db->get();
        return $query->result();
    }

    function save_package($input_array) {
        $this->db->insert("packages", $input_array);
        return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
    }

    function get_package_details($pkg_id){
        $this->db->select("*");
        $this->db->from("packages pkg");
        $this->db->where(array( 'pkg.pkg_id' => $pkg_id));
        $query = $this->db->get();
        return $query->result();
    }

    function update_package($input_array) {
        $this->db->where('pkg_id',$input_array['pkg_id']);
        $query = $this->db->update('packages', $input_array);
        return ($query) ? true : false ;
    }

    function delete_package($pkg_id){
        $query =  $this->db->get_where("packages",array('pkg_id' => $pkg_id));
        if( $query->num_rows() > 0 )
        {
            $this->db->delete("packages", array('pkg_id' => $pkg_id));
            return true;
        }
        return false;
    }
}