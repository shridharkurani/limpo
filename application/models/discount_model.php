<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 19-02-2021
 * Time: 15:22
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class discount_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    function save_discount($input_array) {
        $this->db->insert("discounts", $input_array);
        return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
    }

    function getAllDiscounts(){
        $this->db->select('*');
        $this->db->from("discounts");
        $query = $this->db->get();
        return $query->result();
    }

    function get_discount_details($dis_id){
        $this->db->select("*");
        $this->db->from("discounts dis");
        $this->db->where(array( 'dis.dis_id' => $dis_id));
        $query = $this->db->get();
        return $query->result();
    }

    function update_discount($input_array){
        $this->db->where('dis_id',$input_array['dis_id']);
        $query = $this->db->update('discounts', $input_array);
        return ($query) ? true : false ;
    }

    function delete_discount($dis_id){
        $query =  $this->db->get_where("discounts",array('dis_id' => $dis_id));
        if( $query->num_rows() > 0 )
        {
            $this->db->delete("discounts", array('dis_id' => $dis_id));
            return true;
        }
        return false;
    }
}