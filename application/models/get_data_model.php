<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class get_data_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*
     * Service details
     * */

    function getOne($table, $id, $idName = 'id') {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where(array($idName => $id));
        $query = $this->db->get();
//        meDebug($this->db->last_query(), 1);
        return $query->row_array();
    }


    function getAllServicesByPincode($pincode = 0) {
        $this->db->select("*");
        $this->db->from('services s');
        $this->db->join('service_pincodes sp','sp.service_id = s.ser_id','left');
        $this->db->group_by('s.ser_id');
        $this->db->where(array('s.flag' => 1));
        if($pincode != 0) {
            $this->db->where(array('sp.pincode' => $pincode));
        }
        $query = $this->db->get();

        $returnArray = array();
        if(!empty($query->result_array())) {
            $returnArray = $query->result_array();
            foreach ($returnArray as $key => $row)
            {
                $pinCodesArray = $this->getPinCodes($row['ser_id']);
                $returnArray[$key]['pincodes'] = (!empty($pinCodesArray)) ? implode(",", $pinCodesArray) : '' ;
            }
        }
        return $returnArray;
    }

    function getAllServices() {
        $this->db->select('*');
        $this->db->from("services");
        $this->db->where(array('flag' => 1));
        $query = $this->db->get();
        $returnArray = array();
        if(!empty($query->result_array())) {
            $returnArray = $query->result_array();
            foreach ($returnArray as $key => $row)
            {
                $pinCodesArray = $this->getPinCodes($row['ser_id']);
                $returnArray[$key]['pincodes'] = (!empty($pinCodesArray)) ? implode(",", $pinCodesArray) : '' ;
            }
        }
        return $returnArray;
    }

    function getAlldiscounts() {
        $this->db->select('*');
        $this->db->from("discounts");
        $this->db->where(array('flag' => 1));
        $query = $this->db->get();
        $returnArray = $query->result_array();
        return $returnArray;
    }

    function getAllpackages($returnType) {
        $this->db->select('*');
        $this->db->from("packages");
        $this->db->where(array('flag' => 1));
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
//        meDebug($this->db->last_query());
        $returnArray = array();
        if(!empty($query->result_array())) {
            $returnArray = $query->result_array();
            foreach ($returnArray as $key => $row)
            {
                $packageServicesArray = $this->getPackageServices($row['id'], $returnType);
                $returnArray[$key]['packageServices'] = $packageServicesArray;//: '' ;
            }
        }
//        meDebug($returnArray,1 );
        return $returnArray;
    }

    function getEnquiryDetailsByUserId($userId = 0) {
        $this->db->select('sr.id as service_id,u.user_fullname,u.user_mobile,p.pkg_name,s.ser_name, sr.booked_date_ime,up.remaining_count,sr.flag as flag');
        $this->db->group_by('sr.id,sr.user_id');
        $this->db->from("service_requests sr");
        $this->db->join('user_packages up','up.package_id = sr.package_id AND up.user_id = sr.user_id','left');
        $this->db->join('user_account_details u','u.user_id = sr.user_id','left');
        $this->db->join('packages p','p.id = sr.package_id','left');
        $this->db->join('services s','s.ser_id = sr.service_id','left');
        $this->db->order_by("sr.id", "desc");
        if($userId != 0) { // fetch all
            $this->db->where(array('sr.user_id' => $userId));
        }
        $query = $this->db->get();
        $returnArray = $query->result_array();
//        meDebug($returnArray,1);
        return $returnArray;
    }

    function getUserpackages($userId) {
        $this->db->select('up.*,s.ser_id,s.ser_name,p.pkg_name');
        $this->db->group_by('up.id');
        $this->db->from("user_packages up");
        $this->db->join('packages p','p.id = up.package_id','left');
        $this->db->join('package_services ps','p.id = up.package_id','left');
        $this->db->join('services s','s.ser_id = ps.service_id','left');
        $this->db->where(array('p.flag' => 1, 'up.user_id' => $userId));
        $this->db->order_by("p.id", "desc");
        $query = $this->db->get();
//        meDebug($this->db->last_query());
        $returnArray = $query->result_array();
        /*if(!empty($query->result_array())) {
            $returnArray = $query->result_array();
            $userPackageArray = array();
            foreach ($returnArray as $key => $row)
            {
                $packageServicesArray = $this->getPackageServices($row['package_id'], 'grid');
                $returnArray[$key]['packageServices'] = $packageServicesArray;//: '' ;
/*
                foreach ($packageServicesArray as $key1 => $row1)
                {
                    $userPackageArray[$key]['']

                }*
            }
        }*/
//        meDebug($returnArray,1 );
        return $returnArray;
    }

    function getPinCodes($serviceId = 0) {
        if($serviceId) { // return based on service id
            $this->db->select("sp.*");
            $this->db->from('services s');
            $this->db->join('service_pincodes sp','sp.service_id = s.ser_id','left');
            $this->db->where(array('sp.service_id' => $serviceId));
            $query = $this->db->get();
            $retArray = array();
            if(!empty($query->result_array())) {
                foreach ($query->result_array() as $row)
                {
                    $retArray[] = $row['pincode'];
                }
                return $retArray;
            }
        }
        else { // return all
            $this->db->select("*");
            $this->db->from('services s');
            $this->db->join('service_pincodes sp','sp.id = s.ser_id','left');
            $query = $this->db->get();
            return $query->result_array();
        }
    }


    function getPackageServices($packageId = 0, $returnType = 'id') {
        if($packageId) { // return based on service id
            $this->db->select("*");
            $this->db->from('packages p');
            $this->db->join('package_services ps','ps.package_id = p.id','left');
            $this->db->join('services s','s.ser_id = ps.service_id','left');
            $this->db->where(array('ps.package_id' => $packageId));
            $query = $this->db->get();
            $retArray = array();
            if(!empty($query->result_array())) {
                foreach ($query->result_array() as $row)
                {
                    if($returnType == 'name') {
                        $retArray[] = $row['ser_name'];
                    } else if($returnType == 'grid') {
                        $retArray[] = $row;
                    } else {
                        $retArray[] = $row['service_id'];
                    }
                }
                if($returnType == 'grid') {
                    return $retArray;
                }
                else {
                    return  (!empty($retArray)) ? implode(",", $retArray) : '';
                }
            }
        }
        else { // return all
            $this->db->select("ps.*");
            $this->db->from('packages p');
            $this->db->join('package_services ps','ps.package_id = p.id','left');
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    /*
    Dashboard related functions.
    */
    function getDashboardCounts($cmpnyId) {
        $allData['enquiries'] = $this->getCounts('service_requests');
//        $allData['paymentCounts'] = $this->getCounts('');
        $allData['packages'] = $this->getCounts('packages', 1);
        $allData['services'] = $this->getCounts('services', 1);
        $allData['users'] = $this->getCounts('user_account_details', 1);
        return $allData;
    }
    function getCounts($tableName, $flag = 0) {
        if($flag) {
            $this->db->where(array('flag' => $flag));
        }
        return $this->db->count_all_results($tableName);
    }

    function getCmpnyEnquiriesCnt($cmpnyId) {
        $this->db->where(array('cmpny_id' => $cmpnyId));
        return $this->db->count_all_results('enquiry_details');
    }

    function getCmpnyVehiclesCnt($cmpnyId) {
        $this->db->where(array('vehicle_company_id' => $cmpnyId));
        return $this->db->count_all_results('vehicle_details');        
    }
    
    function getCmpnyDriversCnt($cmpnyId) {
        $this->db->where(array('cmpny_id' => $cmpnyId));
        return $this->db->count_all_results('driver_details');        
    }
    
    function getCmpnyTripsCnt($cmpnyId) {
        $this->db->select('e.id, t.id');
        $this->db->from("enquiry_details e");
        $this->db->join('trip_details t', "e.id = t.enquiry_id");
        $this->db->where(array('e.cmpny_id' => $cmpnyId));

        $count_reponse  = $this->db->count_all_results();
        return $count_reponse;
    }
    /*
    Dashboard related functions Ends here.
    */

    /*
    User account related functions.
    */
    function get_valid_user($email_or_mobile) {
        if(filter_var($email_or_mobile, FILTER_VALIDATE_EMAIL)) {
            $this->db->select('ua.level_id,ua.user_fullname,ua.user_id,ua.user_display_name,ua.nickname,ua.flag,ua.user_email,ua.user_mobile,l.level_name');
            $this->db->from('user_account_details ua');
            $this->db->join('user_levels l','ua.level_id = l.level_id','left');
            $this->db->where(array('ua.user_email' => $email_or_mobile,'ua.flag' => 1));
            $query = $this->db->get();
            return $query->row_array();
        }
        else {
            $this->db->select('ua.level_id,ua.user_fullname,ua.user_id,ua.user_display_name,ua.nickname,ua.flag,ua.user_email,ua.user_mobile,l.level_name');
            $this->db->from('user_account_details ua');
            $this->db->join('user_levels l','ua.level_id = l.level_id','left');
            $this->db->where(array('ua.user_mobile' => $email_or_mobile,'ua.flag' => 1));
            $this->db->where("ua.user_mobile !=", '');
            $query = $this->db->get();
            return $query->row_array();
        }
    }

    /*
    User account related functions Ends here.
    */

    /*
    Company related functions.
    */
    function getCmpnyDetails($cmpnyId) {
        $this->db->select('c.*');
        $this->db->from('company_profiles c');
        $this->db->where(array('c.id' => $cmpnyId,'c.flag' => 1));
        $query = $this->db->get();
        return $query->row_array();
    }

    function getOwnerCmpnyId($userId) {
        $this->db->select('c.id' );
        $this->db->from('user_account_details ua');
        $this->db->join('company_profiles c','c.owner_id = ua.user_id','left');
        $this->db->where(array('c.owner_id' => $userId,'c.flag' => 1,'ua.flag' => 1));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            return $query->row()->id;
        }
        else {
            return 0;
        }
    }


    function getCmpnyProfileDetails($cmpnyId) {
        //To set session variables.
        $this->db->select('c.cmpny_logo,c.cmpny_name' );
        $this->db->from('company_profiles c');
        $this->db->where(array('c.id' => $cmpnyId));
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function getUserProfileDetails($userId) {
        //To set session variables.
        $this->db->select('*' );
        $this->db->from('user_account_details u');
        $this->db->where(array('u.user_id' => $userId));
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function getOwnerCmpnyDetails($userId = 0) {
        if($userId) {
            //Edit 
            $this->db->select('c.*,ua.user_display_name owner_username,ua.user_fullname owner_name,ua.user_mobile owner_mobile,ua.user_email owner_email');
            $this->db->from('user_account_details ua');
            $this->db->join('company_profiles c','c.owner_id = ua.user_id','left');
            $this->db->where(array('c.owner_id' => $userId,'c.flag' => 1,'ua.flag' => 1));
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            //All for admin.
            $this->db->select('c.*,ua.user_display_name owner_username,ua.user_fullname owner_name,ua.user_mobile owner_mobile,ua.user_email owner_email');
            $this->db->from('user_account_details ua');
            $this->db->join('company_profiles c','c.owner_id = ua.user_id','left');
            $query = $this->db->get();
            return $query->result_array();

        }
    }

    /*
    Company related functions ends here.
    */

    /*
    Price Related functions.
    */
    function getPriceDetails($cmpnyId = 0, $priceId = 0) {
        // return $cmpnyId."DD".$priceId;
        if($cmpnyId && !$priceId) {
            //Index - Show company price list.
            $this->db->select("vmm.model_id,vmm.model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,p.*");
            $this->db->from('price_settings p');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = p.model_id','left');
            $this->db->where(array('p.cmpny_id' => $cmpnyId, 'p.flag !=' => 3));
            $query = $this->db->get();
            return $query->result_array();
        }
        elseif ($cmpnyId && $priceId) {            
            //edit - Show company price list.
            $this->db->select("vmm.model_id,vmm.model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,p.*");
            $this->db->from('price_settings p');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = p.model_id','left');
            $this->db->where(array('p.cmpny_id' => $cmpnyId, 'p.id' => $priceId, 'p.flag !=' => 3));
            $query = $this->db->get();
            return $query->row_array();
        }
        else {
            //admin - Show all companies price list.
            $this->db->select("vmm.model_id,vmm.model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,p.*");
            $this->db->from('price_settings p');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = p.model_id','left');
            $this->db->join('company_profiles c','c.id = p.cmpny_id','left');
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    function filterPriceDetails($filterArray) {
        $this->db->select("vmm.model_id,vmm.model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,p.*");
        $this->db->from('price_settings p');
        $this->db->join('vehicle_models_master vmm','vmm.model_id = p.model_id','left');
        $this->db->where($filterArray);
        $query = $this->db->get();
        // return $this->db->last_query();
        return $query->result_array();
    }

    /*
    Price Related functions ends here.
    */

    /*
    Enquiry related functions.
    */
    function getEnquiryDetails($cmpnyId = 0, $enquiryId = 0) {
        if($cmpnyId && !$enquiryId) {
            //Index - Show company price list.
            $this->db->select("e.*, e.id as enquiryId, cust.*,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
            $this->db->from('enquiry_details e');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = e.model_id','left');
            $this->db->order_by("e.id", "desc");
            $this->db->where(array('e.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            return $query->result_array();
        }
        else if(!$cmpnyId && $enquiryId) {
            // return $cmpnyId."DD".$enquiryId;
            // Views View.
            $this->db->select("e.*,cust.*,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
            $this->db->from('enquiry_details e');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = e.model_id','left');
            $this->db->order_by("e.id", "desc");
            $this->db->where(array('e.id' => $enquiryId));
            $query = $this->db->get();
            return $query->result_array();
        }
        elseif ($cmpnyId && $enquiryId) {  
            $this->db->select("e.*,cust.*,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
            $this->db->from('enquiry_details e');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = e.model_id','left');
            $this->db->order_by("e.id", "desc");
            $this->db->where(array('e.cmpny_id' => $cmpnyId, 'e.id' => $enquiryId));
            $query = $this->db->get();
            return $query->row_array();
        }
        else {
            //admin - Show all companies enquiry list.
            $this->db->select("e.*,cust.*,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
            $this->db->from('enquiry_details e');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = e.model_id','left');
            $this->db->order_by("e.id", "desc");
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    function filterEnquiryDetails($filterArray,$likeArray) {       
        //admin - Show all companies enquiry list.
        $this->db->select("e.*,cust.*,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
        $this->db->from('enquiry_details e');
        $this->db->join('customer_details cust','cust.id = e.customer_id','left');
        $this->db->join('vehicle_models_master vmm','vmm.model_id = e.model_id','left');
        // $this->db->order_by("e.id", "asc");
        $likeStr = '';
        if(is_array($likeArray) && !empty($likeArray)) {
            // $likeStr = "1 = 1";

            if(isset($likeArray['customerName']))
            $likeStr .= " (cust.fullname LIKE '%".$likeArray['customerName']."%')";
            if(isset($likeArray['customerMobile']))
            $likeStr .= "and (cust.mobile LIKE '%".$likeArray['customerMobile']."%')";
        }
        if($likeStr != '') {            
            $this->db->like($likeStr);
        }
        $this->db->where($filterArray);
        $query = $this->db->get();
        // return $this->db->last_query();
        $result = $query->result_array();
        if(!empty($result) && is_array($result)) {
            foreach ($result as $key => $value) {
                $result[$key]['facilities'] = $this->getFacilitiesDetails(1,$value['id']);
                $result[$key]['image_exist'] = $this->isImagesexist($value['id']);
            }   
        }
        return $result;
    }

    function getCompleteEnquiryDetails($enquiryId) {
        if($enquiryId) {
            $this->db->select("e.start_date,e.end_date,e.trip_from,e.trip_to,e.min_km,e.approx_km,e.cmpny_id as cmpny_id,cust.fullname,v.id as vehicle_id,v.vehicle_number,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,vmm.model_seating_capacity as seating_capacity,cust.id as cust_id,c.id as cmpny_id,c.cmpny_name,c.contact_person_name,c.contact_person_mobile,c.cmpny_mobile,c.cmpny_alt_mobile,epd.*");
            $this->db->from('enquiry_details e');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_models_master vmm','vmm.model_id = e.model_id','left');
            $this->db->join('vehicle_details v','v.id = e.vehicle_id','left');
            $this->db->join('enquiry_price_details epd','epd.enquiry_id = e.id','left');
            $this->db->join('company_profiles c','c.id = e.cmpny_id','left');
            $this->db->where(array('e.flag' => 1,'e.id' => $enquiryId));
            $query = $this->db->get();
            $result = $query->row_array();
            if(is_array($result) && !empty($result)) {
                $result['facilities'] = $this->getFacilitiesDetails(1,$result['vehicle_id']);
                $result['vehicleImages'] = $this->getvehicleImages(1,$result['vehicle_id']);
            }
            return $result;

        }
        else {
            return false;
        }
    }
    /*
    Enquiry related functions ends here.
    */

    /*
    vehicle and vehicle Models related functions.
    */

    function getallModelTypes($type = 0,$model_type = '') {
        if($type == 'model_name' && $model_type) { //Show all types and models list.
            // ("CONCAT((vmm.model_brand),(' '),(vmm.model_name) as candidate_full_name")
            $this->db->select("vmm.model_id,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
            $this->db->from('vehicle_models_master vmm');
            $this->db->where(array('vmm.flag' => 1, 'vmm.model_type' => $model_type));
            $query = $this->db->get();
            return $query->result_array();
        }
        else if($type == 'model_type') {
            //Group model types and get only names of the models types.
            $this->db->select('vmm.model_id, vmm.model_type, vmm.model_name');
            $this->db->from('vehicle_models_master vmm');
            $this->db->where(array('vmm.flag' => 1));
            // $this->db->group_by('vmm.model_type'); 
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            // Admin - show all.
            $this->db->select("vmm.model_id,vmm.model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name");
            $this->db->from('vehicle_models_master vmm');
            $this->db->where(array('vmm.flag' => 1));
            $query = $this->db->get();
            return $query->result_array();
        }

    }

    function getModeldetails($cmpny_id = 0 , $modelId = 0) {
        if($cmpny_id && !$modelId) {
            //Show all company prices.
            $this->db->select('vm.model_id,vm.model_brand,vm.model_name,vm.model_type,p.*');
            $this->db->from('price_settings p');
            $this->db->join('vehicle_models_master vm','vm.model_id = p.model_id','left');
            $this->db->where(array('p.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            return $query->row_array();
        }
        else if($cmpny_id && $modelId) {
            //Edit particular modelID.
        }
        else {
            // Admin - show all.
        }

    }


    function getVehicleDetails($cmpnyId = 0, $vehicleId = 0) {
        // return $cmpnyId."DD".$vehicleId;
        if ($cmpnyId && $vehicleId) {            
            //edit - Show company vechicle list.

            $this->db->select("v.id,vmm.model_id,UPPER(vmm.model_type) as model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,v.vehicle_number,v.flag");
            $this->db->from('vehicle_details v');
            $this->db->join('vehicle_models_master vmm','v.vehicle_model_id = vmm.model_id','left');
            $this->db->join('vehicle_images vi','v.id = vi.vehicle_id','left');
            $this->db->order_by("id", "desc");
            $this->db->where(array('v.vehicle_company_id' => $cmpnyId, 'v.id' => $vehicleId));
            $query = $this->db->get();
            $result = $query->row_array();
            if(!empty($result) && is_array($result)) {
                $result['facilities'] = $this->getFacilitiesDetails(1,$vehicleId);
                $result['image_exist'] = $this->isImagesexist($vehicleId);
            }
            return $result;

        }
        elseif($cmpnyId && !$vehicleId) {
            //Index - Show company vechicle list.
            // $this->db->select("v.*");
            $this->db->select("v.id,vmm.model_id,UPPER(vmm.model_type) as model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,v.vehicle_number,v.flag");
            $this->db->from('vehicle_details v');
            $this->db->join('vehicle_models_master vmm','v.vehicle_model_id = vmm.model_id');
            // $this->db->join('vehicle_images vi','v.id = vi.vehicle_id','right');
            $this->db->order_by("id", "desc");
            $this->db->where(array('v.vehicle_company_id' => $cmpnyId));
            $query = $this->db->get();
            $result = $query->result_array();
            if(!empty($result) && is_array($result)) {
                foreach ($result as $key => $value) {
                    $result[$key]['facilities'] = $this->getFacilitiesDetails(1,$value['id']);
                    $result[$key]['image_exist'] = $this->isImagesexist($value['id']);
                }   
            }
            return $result;
        }
        else {
            //admin - Show all companies vechicle list.

            $this->db->select("v.id,UPPER(vmm.model_type) as model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,v.vehicle_number,v.flag");
            $this->db->from('vehicle_details v');
            $this->db->join('vehicle_models_master vmm','v.vehicle_model_id = vmm.model_id','left');
            $this->db->join('vehicle_images vi','v.id = vi.vehicle_id','left');
            $this->db->order_by("id", "asc");
            // $this->db->where(array('v.vehicle_company_id' => $cmpnyId, 'v.id' => $vehicleId));
            $query = $this->db->get();
            $result = $query->result_array();
            if(!empty($result) && is_array($result)) {
                foreach ($result as $key => $value) {
                    $result[$key]['facilities'] = $this->getFacilitiesDetails(1,$value['id']);
                    $result[$key]['image_exist'] = $this->isImagesexist($value['id']);
                }   
            }
            return $result;
        }
    }

    function filterVehicleDetails($filterArray,$likeArray) {
        //admin - Show all companies vechicle list.
        $this->db->select("v.id,UPPER(vmm.model_type) as model_type,UPPER(CONCAT((vmm.model_brand),(' '),(vmm.model_name))) as model_name,v.vehicle_number,v.flag");
        $this->db->from('vehicle_details v');
        $this->db->join('vehicle_models_master vmm','v.vehicle_model_id = vmm.model_id','left');
        $this->db->join('vehicle_images vi','v.id = vi.vehicle_id','left');
        $this->db->order_by("id", "asc");
        $likeStr = '';
        if(is_array($likeArray) && !empty($likeArray) && isset($likeArray['vehicle_number'])) {
            $likeStr = "(v.vehicle_number LIKE '%".$likeArray['vehicle_number']."%')";
            $this->db->where($likeStr);
        }
        $this->db->where($filterArray);
        $query = $this->db->get();
        // return $this->db->last_query();
        $result = $query->result_array();
        if(!empty($result) && is_array($result)) {
            foreach ($result as $key => $value) {
                $result[$key]['facilities'] = $this->getFacilitiesDetails(1,$value['id']);
                $result[$key]['image_exist'] = $this->isImagesexist($value['id']);
            }   
        }
        return $result;
    }

    function getvehicleImages($flag,$vehicleId) {
        $this->db->select("vi.image_name");
        $this->db->from('vehicle_images vi');
        $this->db->order_by("id", "desc");
        $this->db->where(array('vi.vehicle_id' => $vehicleId,'vi.flag' => $flag));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;   
    }

    /*
    vehicle and vehicle Models related functions ends here.
    */

    /*
    Facility Details.
    */
    function getFacilitiesDetails($flag, $vehicleId = 0) {
        if($vehicleId) {
            $this->db->select("fm.id,fm.facility_name");
            $this->db->from('vehicle_faclities_slave fs');
            $this->db->join('vehicle_faclities_master fm','fm.id = fs.facility_id','left');
            $this->db->where(array('fs.flag' => $flag, 'fs.vehicle_id' => $vehicleId));
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            $this->db->select("fm.id,fm.facility_name");
            $this->db->from('vehicle_faclities_slave fs');
            $this->db->join('vehicle_faclities_master fm','fm.id = fs.facility_id','left');
            $this->db->where(array('fs.flag' => $flag));
            $query = $this->db->get();
            return $query->result_array();            
        }
    }

    function getAllFacilities($flag) {
        $this->db->select("f.*");
        $this->db->from('vehicle_faclities_master f');
        $this->db->where(array('f.flag' => $flag));
        $query = $this->db->get();
        return $query->result_array();
    }
    
    /*
    Facility Details Ends Here.
    */

    /*
    Driver related functions.
    */

    function getDriverMobileNumber($driverId) {
        $this->db->select("d.mobile,d.id");
        $this->db->from('driver_details d');
        $this->db->where(array('d.id' => $driverId));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    function getDriverMinimumDetails($cmpnyId = 0, $driverId = 0) {
        if($cmpnyId && !$driverId) {
            //Index - All the drivers related to this company.
            $this->db->select("d.fullname,d.id");
            $this->db->from('driver_details d');
            $this->db->where(array('d.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
        elseif ($cmpnyId && $driverId) {            
            //edit - Particular driver realted to particular company.
            $this->db->select("d.fullname,d.id");
            $this->db->from('driver_details d');
            $this->db->where(array('d.cmpny_id' => $cmpnyId, 'd.id' => $driverId));
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;

        }
        else {
            //admin - All Drivers.
            $this->db->select("d.fullname,d.id");
            $this->db->from('driver_details d');
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
        }
    }

    function getDriverDetails($cmpnyId = 0, $driverId = 0) {
        if($cmpnyId && !$driverId) {
            //Index - Show company vechicle list.
            $this->db->select("*");
            $this->db->from('driver_details d');
            $this->db->where(array('d.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            $result = $query->result_array();
            if(!empty($result) && is_array($result)) {
                foreach ($result as $key => $value) {
                    $result[$key]['langauages'] = $this->getLanguageDetails(1,$value['id']);
                    $result[$key]['places'] = $this->getPlaceDetails(1,$value['id']);
                }   
            }
            return $result;
        }
        elseif ($cmpnyId && $driverId) {            
            //edit - Show company vechicle list.
            $this->db->select("*");
            $this->db->from('driver_details d');
            $this->db->where(array('d.cmpny_id' => $cmpnyId, 'd.id' => $driverId));
            $query = $this->db->get();
            $result = $query->row_array();
            if(!empty($result) && is_array($result)) {
                $result['langauages'] = $this->getLanguageDetails(1,$driverId);
                $result['places'] = $this->getPlaceDetails(1,$driverId);
            }
            return $result;

        }
        else {
            //admin - Show all companies vechicle list.

            $this->db->select("*");
            $this->db->from('driver_details d');
            // $this->db->where(array('d.cmpny_id' => $cmpnyId, 'd.id' => $driverId));
            $query = $this->db->get();
            $result = $query->result_array();
            if(!empty($result) && is_array($result)) {
                foreach ($result as $key => $value) {
                    $result[$key]['langauages'] = $this->getLanguageDetails(1,$driverId);
                    $result[$key]['places'] = $this->getPlaceDetails(1,$driverId);
                }   
            }
            return $result;
        }
    }

    function getLanguageDetails($flag, $driverId = 0) {
        if($driverId) {
            $this->db->select("lm.id,lm.language_name");
            $this->db->from('languages_slave ls');
            $this->db->join('languages_master lm','lm.id = ls.language_id','left');
            $this->db->where(array('lm.flag' => $flag, 'ls.driver_id' => $driverId));
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            $this->db->select("lm.id,lm.language_name");
            $this->db->from('languages_slave ls');
            $this->db->join('languages_master lm','lm.id = ls.language_id','left');
            $this->db->where(array('lm.flag' => $flag));
            $query = $this->db->get();
            return $query->result_array();            
        }
    }

    function getPlaceDetails($flag, $driverId = 0) {
        if($driverId) {
            $this->db->select("pm.id,pm.place_name");
            $this->db->from('places_slave ps');
            $this->db->join('places_master pm','pm.id = ps.places_id','left');
            $this->db->where(array('pm.flag' => $flag, 'ps.driver_id' => $driverId));
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            $this->db->select("pm.id,pm.place_name");
            $this->db->from('places_slave ps');
            $this->db->join('places_master pm','pm.id = ps.places_id','left');
            $this->db->where(array('pm.flag' => $flag));
            $query = $this->db->get();
            return $query->result_array();            
        }
    }
    /*
    Driver related functions ends here.
    */


    /*
    Trip related functions starts here.
    */

    function getTripDetails($enquiryId = 0) {
        if(!$enquiryId) {
            //Index - Show company price list.
            $this->db->select("t.id as tripId,e.id as enquiryId,e.trip_from,e.trip_to,e.start_date,e.end_date,cust.fullname,cust.mobile,d.fullname as driverName,d.mobile as driverMobile,t.flag,t.payment_status_id,t.vehicle_type_id,t.vehicle_id,t.driver_id");
            $this->db->from('trip_details t');
            $this->db->join('enquiry_details e','t.enquiry_id = e.id','left');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_details v','v.id = t.vehicle_id','left');
            $this->db->join('driver_details d','d.id = t.driver_id','left');
            // $this->db->where(array('e.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            return $query->result_array();
        }
        else if($enquiryId) {
            // View and edits View.
            $this->db->select("t.id as tripId,e.id as enquiryId,e.trip_from,e.trip_to,e.start_date,e.end_date,cust.fullname,cust.mobile,d.fullname as driverName,d.mobile as driverMobile,t.flag,t.payment_status_id,t.vehicle_type_id,t.vehicle_id,t.driver_id");
            $this->db->from('trip_details t');
            $this->db->join('enquiry_details e','t.enquiry_id = e.id');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_details v','v.id = t.vehicle_id','left');
            $this->db->join('driver_details d','d.id = t.driver_id','left');
            $this->db->where(array('e.id' => $enquiryId));
            $query = $this->db->get();
            return $query->row_array();
        }/*
        else {
            //admin - Show all companies enquiry list.
            $this->db->select("t.id as tripId,e.id as enquiryId,e.trip_from,e.trip_to,e.start_date,e.end_date,cust.fullname,cust.mobile,d.fullname as driverName,d.mobile as driverMobile,t.flag,t.payment_status_id,t.vehicle_type_id,t.vehicle_id,t.driver_id");
            $this->db->from('trip_details t');
            $this->db->join('enquiry_details e','t.enquiry_id = e.id');
            $this->db->join('customer_details cust','cust.id = e.customer_id','left');
            $this->db->join('vehicle_details v','v.id = t.vehicle_id','left');
            $this->db->join('driver_details d','d.id = t.driver_id','left');
            $query = $this->db->get();
            return $query->result_array();
        }*/
    }
    /*
    Trip related functions ends here.
    */

    /*
    Other Misc functions.
    */
    function getAlllanguages($flag) {
        $this->db->select("l.*");
        $this->db->from('languages_master l');
        $this->db->where(array('l.flag' => $flag));
        $query = $this->db->get();
        return $query->result_array();
    }

    function getAllPlaces($flag) {
        $this->db->select("p.*");
        $this->db->from('places_master p');
        $this->db->where(array('p.flag' => $flag));
        $query = $this->db->get();
        return $query->result_array();
    }

    function getPriceIdBymodelId($modelId,$cmpnyId) {
        $this->db->select('p.id' );
        $this->db->from('price_settings p');
        $this->db->where(array('p.model_id' => $modelId,'p.cmpny_id' => $cmpnyId,'p.flag' => 1));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            return $query->row()->id;
        }
        else {
            return 0;
        }
    }

    function isImagesexist($vehicleId) {
        $query = $this->db->get_where('vehicle_images', array('vehicle_id' => $vehicleId));
        if($query->num_rows > 0){
            return "Yes";
        }
        else {
            return "No";
        }

    }

    function getCarPriceDetails($postArr) {
        $this->db->select("*");
        $this->db->from('price_settings');
        $this->db->where(array('model_id' => $postArr['vehicle_model'], 'cmpny_id' => $postArr['company_id']));
        $query = $this->db->get();
        if($query->num_rows > 0){
            return $query->row();
        }else {
            return array();
        }
    }

/*    public function getEnquiryDetails($enquiryId){
        $this->db->select('e.id, e.start_date, e.end_date, e.seaters, e.trip_from, e.trip_via, e.trip_to,  vmm.model_type, e.model_id, e.min_km, e.approx_km, 
        e.customer_id, c.fullname as customer_name, c.mobile as customer_mobile, c.alt_mobile as customer_alt_mobile, c.email as customer_email, 
        ep.per_km, ep.per_km_ac, ep.driver_allowance, ep.extra_per_km, ep.extra_per_km_ac, ep.driver_allowance_night, ep.advance_payment_type, ep.advance_payment');
        $this->db->from('enquiry_details e');
        $this->db->join('enquiry_price_details ep', 'ep.enquiry_id = e.id');
        $this->db->join('customer_details c', 'c.id = e.customer_id');
        $this->db->join('vehicle_models_master vmm', 'vmm.model_id = e.model_id');
        $this->db->where('e.flag', '1');
        $this->db->where('e.id', $enquiryId);
        $query = $this->db->get();
        if($query->num_rows > 0){
            $res = $query->result_array();
            return $res[0];
        }else {
            return array();
        }
    }*/

    public function getvalidTxnId($txn_id){
        $this->db->select("*", FALSE);
        $this->db->from('payment_details p');
        $this->db->where('p.txn_id',$txn_id); 
        $query = $this->db->get();
        return $query->result();
    }

    public function getpackageComments($packageId){
        $this->db->select("c.*, u.user_fullname");
        $this->db->from('user_comments c');
        $this->db->join('user_account_details u','u.user_id = c.user_id','left');
        $this->db->where('c.package_id',$packageId);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPaymentDetails($cmpnyId = 0, $paymentId = 0) {
        if($cmpnyId && !$paymentId) {
            //Index - Show company payments list.
            $this->db->select("p.id as paymentId,e.id as enquiryId, c.id as cmpnyId,cust.id as custId,p.txn_id as transactionId, cust.fullname as custName, cust.mobile as custMobile, e.created_date as enquiryDate, c.cmpny_name, p.amount ,p.created_date,p.flag");
            $this->db->from('payment_details p');
            $this->db->join('enquiry_details e','e.id = p.enquiry_id','left');
            $this->db->join('customer_details cust','cust.id = p.customer_id','left');
            $this->db->join('company_profiles c','c.id = p.cmpny_id','left');
            $this->db->where(array('p.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            return $query->result_array();
        }
        elseif ($cmpnyId && $paymentId) {            
            //view - Show company vechicle list.
            $this->db->select("p.id as paymentId,e.id as enquiryId, c.id as cmpnyId,cust.id as custId,p.txn_id as transactionId, cust.fullname as custName, cust.mobile as custMobile, e.created_date as enquiryDate, c.cmpny_name, p.amount ,p.created_date,p.flag");
            $this->db->from('payment_details p');
            $this->db->join('enquiry_details e','e.id = p.enquiry_id','left');
            $this->db->join('customer_details cust','cust.id = p.customer_id','left');
            $this->db->join('company_profiles c','c.id = p.cmpny_id','left');
            $this->db->where(array('p.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            return $query->result_array();
        }
        else {
            //admin - Show all companies vechicle list.
            $this->db->select("p.id as paymentId,e.id as enquiryId, c.id as cmpnyId,cust.id as custId,p.txn_id as transactionId, cust.fullname as custName, cust.mobile as custMobile, e.created_date as enquiryDate, c.cmpny_name, p.amount ,p.created_date,p.flag");
            $this->db->from('payment_details p');
            $this->db->join('enquiry_details e','e.id = p.enquiry_id','left');
            $this->db->join('customer_details cust','cust.id = p.customer_id','left');
            $this->db->join('company_profiles c','c.id = p.cmpny_id','left');
            // $this->db->where(array('p.cmpny_id' => $cmpnyId));
            $query = $this->db->get();
            return $query->result_array();
        }
    }
    function getCustomerMobile($enquiryId) {        
        $this->db->select("cust.*");
        $this->db->from('enquiry_details e');
        $this->db->join('customer_details cust','cust.id = e.customer_id');
        $this->db->where(array('e.id' => $enquiryId));
        $query = $this->db->get();
        return $query->row_array();
    }

    /*
    Other Misc functions Ends here.
    */

}
?>