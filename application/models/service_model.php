<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 06-02-2021
 * Time: 01:30
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class service_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function getAllServices(){
        $this->db->select('*');
        $this->db->from("services");
        $this->db->where(array( 'flag' => 1));
        $query = $this->db->get();
        return $query->result_array();
    }

    function getAllPackages(){
        $this->db->select('*');
        $this->db->from("packages");
        $query = $this->db->get();
        return $query->result();
    }

    function save_service($input_array) {
        $this->db->insert("services", $input_array);
        return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
    }

    function get_service_details($service_id){
        $this->db->select("*");
        $this->db->from("services ser");
        $this->db->where(array( 'ser.ser_id' => $service_id));
        $query = $this->db->get();
        return $query->result();
    }

    function update_service($input_array) {
        $this->db->where('ser_id',$input_array['ser_id']);
        $query = $this->db->update('services', $input_array);
        return ($query) ? true : false ;
    }

    function delete_service($service_id){
        $query =  $this->db->get_where("services",array('ser_id' => $service_id));
        if( $query->num_rows() > 0 )
        {
            $row = $query->row();
            $picture = $row->ser_image;
            unlink(realpath('assets/uploads/service/'.$picture));
            $this->db->delete("services", array('ser_id' => $service_id));
            return true;
        }
        return false;
    }
}
