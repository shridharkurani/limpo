<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class save_update_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function insert_update_service_pincode_details($pincodeArray, $serviceId = 0) {
        if(is_array($pincodeArray) && $serviceId) {
            foreach ($pincodeArray as $key => $value) {
                $pincodeInsertableArray[] = array(
                    'service_id' => $serviceId,
                    'pincode' => $value,
                );
            }
            $query = $this->db->get_where('service_pincodes', array('service_id' => $serviceId));
            if($query->num_rows > 0){
                $this->db->where('service_id',$serviceId); // I change id with book_id
                $this->db->delete('service_pincodes');
                $this->db->insert_batch('service_pincodes', $pincodeInsertableArray);
            }
            else {
                $this->db->insert_batch('service_pincodes', $pincodeInsertableArray);
            }
        }
    }

    function insert_update_package_service_details($packageServiceArray, $packageId = 0) {
        if(is_array($packageServiceArray) && $packageId) {
            $packageServiceInsertableArray = array();
            foreach ($packageServiceArray as $key => $value) {
                $packageServiceInsertableArray[] = array(
                    'package_id' => $packageId,
                    'service_id' => $value,
                );
            }
            $query = $this->db->get_where('package_services', array('package_id' => $packageId));
            if($query->num_rows > 0){
                $this->db->where('package_id',$packageId); // I change id with book_id
                $this->db->delete('package_services');
                $this->db->insert_batch('package_services', $packageServiceInsertableArray);
            }
            else {
                $this->db->insert_batch('package_services', $packageServiceInsertableArray);
            }
        }
    }

    /*
    * save user register details
    */
    function save_register_details($form_post_data_array) {
        $query = $this->db->get_where('user_account_details', array('user_email' => $form_post_data_array['user_email'],'flag' => 1));
        
         if($query->num_rows > 0){
          $user_query = $this->db->where('user_account_details', array('user_email' => $form_post_data_array['user_email']));
          $user_id = $user_query->row('user_id');
          $query = $this->db->update('user_account_details',$form_post_data_array);
          return $user_id;
        }
        else {
          $this->db->insert('user_account_details', $form_post_data_array);
          return $this->db->insert_id();
         }
    }

    // 1 - Activate, 2 - Block / Deactivate, 3 -Delete
    function updateFlag($id, $idName, $flag , $table) {
        $updateData = array(
            'flag' => $flag
        );
        $this->db->where($idName, $id);
        $this->db->update($table, $updateData);
        return true;
    }

    // 1 - Activate, 2 - Block / Deactivate, 3 -Delete
    function updateRequestFlag($id, $idName, $flag , $table) {
        $updateData = array(
            'flag' => $flag
        );
        $this->db->where($idName, $id);
        $this->db->update($table, $updateData);
        return true;
    }

    function insert_update_service_details($serviceDetails, $serviceId = 0) {
        if($serviceId) {
            $query = $this->db->where(array('ser_id' => $serviceId));
            $query = $this->db->update('services',$serviceDetails);
            return array("id" => $serviceId, "msg" => "Service Details Updated Successfully.");
        }
        else {
            $this->db->insert('services', $serviceDetails);
            return array("id" => $this->db->insert_id(), "msg" => "Service Details Saved Successfully.");

        }
    }


    function insert_update_discount_details($discountDetails, $discountId = 0) {
        if($discountId) {
            $query = $this->db->where(array('id' => $discountId));
            $query = $this->db->update('discounts',$discountDetails);
            return array("id" => $discountId, "msg" => "Discount Details Updated Successfully.");
        }
        else {
            $this->db->insert('discounts', $discountDetails);
            return array("id" => $this->db->insert_id(), "msg" => "Discount Details Saved Successfully.");

        }
    }

    function insert_update_service_request_details($serviceRequestDetails, $requestId = 0) {
        if($requestId) {/*
            $query = $this->db->where(array('ser_id' => $serviceId));
            $query = $this->db->update('service_requests',$serviceRequestDetails);
            return array("id" => $serviceId, "msg" => "Service Details Updated Successfully.");*/
        }
        else {
            $this->db->insert('service_requests', $serviceRequestDetails);
//            meDebug($this->db->last_query(),1);
            return array("id" => $this->db->insert_id(), "msg" => "Service request has been saved successfully.");

        }
    }

    function insert_update_package_details($packageDetails, $packageId = 0) {
        if($packageId) {
            $query = $this->db->where(array('id' => $packageId));
            $query = $this->db->update('packages',$packageDetails);
            return array("id" => $packageId, "msg" => "Package Details Updated Successfully.");
        }
        else {
            $this->db->insert('packages', $packageDetails);
            return array("id" => $this->db->insert_id(), "msg" => "Package Details Saved Successfully.");
        }
    }

    /*function insert_update_service_request_details($userId, $packageId, , $packageId = 0) {
        if($packageId) {
            $query = $this->db->where(array('id' => $packageId));
            $query = $this->db->update('packages',$packageDetails);
            return array("id" => $packageId, "msg" => "Package Details Updated Successfully.");
        }
        else {
            $this->db->insert('packages', $packageDetails);
            return array("id" => $this->db->insert_id(), "msg" => "Package Details Saved Successfully.");

        }
    }*/

    function insert_update_cmpny_details($cmpnyDetails, $cmpnyId = 0) {
      if($cmpnyId) {
          $query = $this->db->where(array('id' => $cmpnyId));
          $query = $this->db->update('company_profiles',$cmpnyDetails);
          return array("id" => $cmpnyId, "msg" => "Company Details Updated Successfully.");
      }
      else {
        $this->db->insert('company_profiles', $cmpnyDetails);
        return array("id" => $this->db->insert_id(), "msg" => "Company Details Saved Successfully.");

      }
    }

    function insert_update_price_details($priceArray, $priceId = 0) {
      // meDebug($priceId,true);
      if($priceId) {
        //Edit
          $query = $this->db->where(array('id' => $priceId));
          $query = $this->db->update('price_settings',$priceArray);
          return array("id" => $priceId, "msg" => "Price Details Updated Successfully.");
      }
      else {
        //Insert.
        $this->db->insert('price_settings', $priceArray);
        return array("id" => $this->db->insert_id(), "msg" => "Price Details Saved Successfully.");
      }
    }


    function insert_update_vehicle_details($vehicleArray, $uploadedImgsArray, $vehicleId = 0) {
      // meDebug($priceId,true);
      if($vehicleId) {
        //Edit
          $query = $this->db->where(array('id' => $vehicleId));
          $query = $this->db->update('vehicle_details',$vehicleArray);
          return array("id" => $vehicleId, "msg" => "Vehicle Details Updated Successfully.");
      }
      else {
        //Insert.
        $this->db->insert('vehicle_details', $vehicleArray);
        $insertedVehicleId = $this->db->insert_id();
        
        if(is_array($uploadedImgsArray) && !empty($uploadedImgsArray)) {
          $imgsArray = array();
          foreach ($uploadedImgsArray as $key => $img) {
            $imgsArray[] = array(
              'vehicle_id' => $insertedVehicleId,
              'image_name' => $img['file_name'],
              'file_type' => $img['file_type'],
              'file_size' => $img['file_size'],
              'image_width' => $img['image_width'],
              'image_height' => $img['image_height'],
              );
          }
          if(is_array($imgsArray) && !empty($imgsArray))
          $this->db->insert_batch('vehicle_images', $imgsArray);
        }
        return array("id" => $insertedVehicleId, "msg" => "Vehicle Details Saved Successfully.");
      }

    }

    function insert_update_driver_details($driverArray, $driverId = 0) {
      // meDebug($priceId,true);
      if($driverId) {
        //Edit
          $query = $this->db->where(array('id' => $driverId));
          $query = $this->db->update('driver_details',$driverArray);
          return array("id" => $driverId, "msg" => "Driver Details Updated Successfully.");
      }
      else {
        //Insert.
        $this->db->insert('driver_details', $driverArray);
        return array("id" => $this->db->insert_id(), "msg" => "Driver Details Saved Successfully.");
      }
    }

    function insert_update_vehicle_facility_details($faciltyArray, $vehicleId = 0) {
      if(is_array($faciltyArray) && $vehicleId) {
        foreach ($faciltyArray as $key => $value) {
          $faciltyInsertableArray[] = array(
                          'vehicle_id' => $vehicleId,
                          'facility_id' => $value,
                          );
        }
        $query = $this->db->get_where('vehicle_faclities_slave', array('vehicle_id' => $vehicleId));
        if($query->num_rows > 0){
          $this->db->where('vehicle_id',$vehicleId); // I change id with book_id
          $this->db->delete('vehicle_faclities_slave');
          $this->db->insert_batch('vehicle_faclities_slave', $faciltyInsertableArray);
        }
        else {
          $this->db->insert_batch('vehicle_faclities_slave', $faciltyInsertableArray);
        }
      }
    }

    function insert_update_driver_language_details($languageArray, $driverId = 0) {
      if(is_array($languageArray) && $driverId) {
        foreach ($languageArray as $key => $value) {
          $languageInsertableArray[] = array(
                          'language_id' => $value,
                          'driver_id' => $driverId,
                          'cmpny_id' => 1,
                          );
        }
        $query = $this->db->get_where('languages_slave', array('driver_id' => $driverId));
        if($query->num_rows > 0){
          $this->db->where('driver_id',$driverId); // I change id with book_id
          $this->db->delete('languages_slave');
          $this->db->insert_batch('languages_slave', $languageInsertableArray);
        }
        else {
          $this->db->insert_batch('languages_slave', $languageInsertableArray);
        }
      }
      else {

      }
    }
    
    function insert_update_driver_places_details($placesArray, $driverId = 0) {
      if(is_array($placesArray) && $driverId) {
        foreach ($placesArray as $key => $value) {
          $placesInsertableArray[] = array(
                          'places_id' => $value,
                          'driver_id' => $driverId,
                          'cmpny_id' => 1,
                          );
        }
        $query = $this->db->get_where('places_slave', array('driver_id' => $driverId));
        if($query->num_rows > 0){
          $this->db->where('driver_id',$driverId); // I change id with book_id
          $this->db->delete('places_slave');
          $this->db->insert_batch('places_slave', $placesInsertableArray);
        }
        else {
          $this->db->insert_batch('places_slave', $placesInsertableArray);
        }
      }
      else {

      }
    }

    function insert_update_customer_details($customerArray, $customer_id = 0) {
      // meDebug($priceId,true);
        if($customer_id) {
          //Edit
            $query = $this->db->where(array('id' => $customer_id));
            $query = $this->db->update('customer_details',$customerArray);
            return array("id" => $customer_id, "msg" => "Customer Details Updated Successfully.");
        }
        else {
          //Insert.
          $this->db->insert('customer_details', $customerArray);
          return array("id" => $this->db->insert_id(), "msg" => "Customer Details Saved Successfully.");
        }
      }

      function insert_update_enquiry_details($enquiryArray , $enquiryId = 0) {
      // meDebug($priceId,true);
        if($enquiryId) {
          //Edit
            $query = $this->db->where(array('id' => $enquiryId));
            $query = $this->db->update('enquiry_details',$enquiryArray);
            return array("id" => $enquiryId, "msg" => "Enquiry Details Updated Successfully.");
        }
        else {
          //Insert.
          $this->db->insert('enquiry_details', $enquiryArray);
          return array("id" => $this->db->insert_id(), "msg" => "Enquiry Details Saved Successfully.");
        }
      }

    function insert_update_enquiry_price($enquiryPriceArr, $enquiryId = 0)
    {
        if ($enquiryId) {
            //Edit
            $query = $this->db->where(array('enquiry_id' => $enquiryId));
            $query = $this->db->update('enquiry_price_details', $enquiryPriceArr);
            return array("id" => $enquiryId, "msg" => "Enquiry Details Updated Successfully.");
        } else {
            //Insert.
            $this->db->insert('enquiry_price_details', $enquiryPriceArr);
            return array("id" => $this->db->insert_id(), "msg" => "Enquiry Details Saved Successfully.");
        }
    }

    function save_cmpny_details($cmpnyDetails) {
      $this->db->insert('company_profiles', $cmpnyDetails);
      return array("id" => $user_id, "msg" => "Company Details Updated Successfully.");
    }

    function update_cmpny_details($cmpnyId,$cmpnyDetails) {
        $query = $this->db->get_where('company_profiles', array('id' => $cmpnyId));
        $query = $this->db->update('company_profiles',$cmpnyDetails);
        return array("id" => $user_id, "msg" => "Company Details Updated Successfully.");
    }

    function updatePriceFlag($priceId,$flag) {
      $updateData = array(
          'flag' => $flag
      );
      $this->db->where('id', $priceId);
      $this->db->update("price_settings", $updateData);
      return true;
    }

    function updateVehicleFlag($vehicleId,$flag) {
      $updateData = array(
          'flag' => $flag
      );
      $this->db->where('id', $vehicleId);
      $this->db->update("vehicle_details", $updateData);
      return true;      
    }

    function updateEnquiryFlag($enquiryId,$flag) {
        $updateData = array(
            'flag' => $flag
        );
        if($flag == 4) { //Completed
            $updateData['completed_datetime'] = date('Y-m-d H:i:s');
            if(!empty($enquiryId)) {
                $this->db->select('*');
                $this->db->from("service_requests");
                $this->db->where(array('id' => $enquiryId));
                $query = $this->db->get();
                $row = $query->row_array();
                if(!empty($row)) {
                    $this->db->set('remaining_count', 'remaining_count-1', FALSE);
                    $where = array('user_id' =>$row['user_id'],'package_id'=> $row['package_id']);
                    $this->db->where($where);
                    $this->db->update('user_packages');
//        meDebug($this->db->last_query(),1 );
                }

            }

        }
      $this->db->where('id', $enquiryId);
      $this->db->update("service_requests", $updateData);
      return true;      
    }

    function updateCmpnyFlag($cmpnyId,$flag) {
      $updateData = array(
          'flag' => $flag
      );
      $this->db->where('id', $cmpnyId);
      $this->db->update("company_profiles", $updateData);
      return true;
    }

    function updateDriverFlag($driver_id,$flag) {
      $updateData = array(
          'flag' => $flag
      );
      $this->db->where('id', $driver_id);
      $this->db->update("driver_details", $updateData);
      return true;
    }

    function save_vehicle_models($vehicle_model_array) {
      $this->db->insert('vehicle_models', $vehicle_model_array);
      return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
    }

    function save_user_packages($userPackageData) {
        $this->db->insert('user_packages', $userPackageData);
//        meDebug($this->db->last_query(),1 );
        return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
    }
    function save_user_details($user_array) {
        $query = $this->db->get_where('users', array('mobile' => $user_array['mobile']));
          if($query->num_rows >= 1){
            $user_query = $this->db->get_where('users', array('mobile' => $user_array['mobile']));
            $user_id = $user_query->row('user_id');
            return  $user_id;
        }
        else {
          $this->db->insert('users', $user_array);
          return $this->db->insert_id();
         }
    }
    
    function save_trip_details($trip_array) {
      $this->db->insert('trip_details', $trip_array);
      return array("id" => $this->db->insert_id(), "msg" => "Enquiry Details confirmed and sent message to driver and customer.");
    }
    
    function save_payment_details($payment_array) {
      $this->db->insert('payment_details', $payment_array);
      return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id(); 
    }

    function updatePaymentDetails($data,$id){
      $this->db->where('id',$id);
          $query = $this->db->update('payment_details', $data);
          return ($query) ? true : false ;  
    }

    function save_update_otp_details($data, $mobile) {
        $query = $this->db->get_where('user_account_details', array('user_mobile' => $mobile));

        if($query->num_rows >= 1) {
            $this->db->where('mobile',$mobile);
            $query = $this->db->update('otp_details', $data);
            return ($query) ? true : false ;
        }
        else {
            $this->db->insert('otp_details', $data);
            return $this->db->insert_id();
        }
    }

    function save_comment_details($comments_array) {
        $this->db->insert('user_comments', $comments_array);
        return ($this->db->affected_rows() != 1) ? false : $this->db->insert_id();
    }
}
    /*
    =========================================
    New Start ends here
    =========================================
    */
