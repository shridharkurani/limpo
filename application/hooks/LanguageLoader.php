<?php
    
    class LanguageLoader extends CI_Session
    {
        public function initialize() {
            $ci =& get_instance();
            $ci->load->helper('language');
            $lang   = $this->userdata('language_name');
            if($lang == "Spanish")
            {
                $ci->lang->load('spanish','spanish');
            }
            else{
                $ci->lang->load('fieldname','english');
            }
        }
    }
?>