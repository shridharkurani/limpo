<?php

/*
	@desc - Enquiry details.
	Author:SJKURANI.
	Date: 01-05-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Enquiries extends CI_Controller {

	private $cmpnyId;
	private $userId;
	
	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_update_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
    	$this->userId = $this->session->userdata('user_id'); 
	}
	function user() {
        is_authenticated_user(array('user'));
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Existing Requests");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Profile" => base_url()."dashboard/","Requests" => base_url().'enquiries/',"List" => base_url().'enquiries/'.'add'));
        $data = array();
        $data['enquiriesArray'] = $this->get_data_model->getEnquiryDetailsByUserId($this->userId);
//        meDebug($data,1);
        $this->layouts->view('enquiries/user_index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

	function index() {
        is_authenticated_user(array('admin'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Enquiries" => base_url().'enquiries/',"List" => base_url().'enquiries/'.'add'));
		$data = array();
		$cmpnyId = $this->session->userdata("cmpnyId");
		$data['enquiriesArray'] = $this->get_data_model->getEnquiryDetailsByUserId();
//		meDebug($data,1);
		$this->layouts->view('enquiries/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);

		// meDebug($data,true);
		/*Fix me */
	}
	
	function _add_edit_rules() {
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|trim|xss_clean');
		$this->form_validation->set_rules('end_date', 'End date', 'required|trim|xss_clean');
		$this->form_validation->set_rules('trip_from', 'From', 'required|trim|xss_clean');
		$this->form_validation->set_rules('trip_via', 'Via', 'trim|xss_clean');
		$this->form_validation->set_rules('trip_to', 'To', 'required|trim|xss_clean');
		$this->form_validation->set_rules('seaters', 'Seater', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('vehicle', 'Vehicle', 'trim|required|callback__verify_dropdown['.$this->input->post('vehicle').']');
		$this->form_validation->set_rules('vehicle_model', 'Vehicle Model', 'required|trim|callback__verify_dropdown['.$this->input->post('vehicle_model').']');


		$this->form_validation->set_rules('customer_name', 'Customer Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('customer_mobile', 'Customer Mobile', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('customer_alt_mobile', 'Customer Alternative Mobile', 'trim|xss_clean|integer');
		$this->form_validation->set_rules('customer_email', 'Customer Email', 'trim|xss_clean|valid_email');
	}

    public function add_edit($enquiryId = 0){
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Enquiry Details");
		$addOrEdit  = (!empty($enquiryId)) ? 'Edit' : 'Add' ;
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Enquiries" => base_url().'enquiries/', $addOrEdit => $addOrEdit));
		$data = array();

        $data['enquiryId'] = $enquiryId;
        $data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
        $data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId);
        // meDebug($data['vehicleDetails'],1);
        if($this->input->post('vehicle_type')) {
            $typeName = $this->input->post('vehicle_type');
            $data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);
        }
        if($enquiryId != 0){
            $data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_type');
            $data['enquiryDetails'] = $this->get_data_model->getEnquiryDetails($this->cmpnyId,$enquiryId);
			//echo'<pre/>';print_r($data);exit;
        }
        if(is_array($this->input->post()) && !empty($this->input->post())){
            {
                $model_id = $this->input->post('vehicle_model_id');

                $customer_id = $this->input->post('customer_id');
                $customerArray = array(
                    'fullname' => $this->input->post('customer_name'),
                    'mobile' => $this->input->post('customer_mobile'),
                        'alt_mobile' => !empty($this->input->post('customer_alt_mobile')) ? $this->input->post('customer_alt_mobile') : 0,
                    'email' => $this->input->post('customer_email'),
                    'created_date'	=> date('Y-m-d h:i:s'),
                    'created_ip' => $this->input->ip_address(),
                );
                $customerId = $this->save_update_model->insert_update_customer_details($customerArray, $customer_id);

                $priceId = $this->get_data_model->getPriceIdBymodelId($model_id, $this->cmpnyId);
                $priceArray = array(
                    'cmpny_id' => $this->cmpnyId,
                    'model_id' => $model_id,
                    'per_km' => $this->input->post('per_km'),
                    'per_km_ac' => $this->input->post('per_km_ac'),
                    'driver_allowance' => $this->input->post('driver_allowance'),
                    'min_km' => $this->input->post('min_km'),
                    'driver_allowance_night' => $this->input->post('extra_driver_allowance'),
                    'extra_per_km' => $this->input->post('extra_km'),
                    'extra_per_km_ac' => $this->input->post('extra_km_ac'),
                    'last_modified_by' => $this->userId,
                    'last_modified_ip' => $this->input->ip_address(),
                );

                if(!$enquiryId) {
                    //insert
                    $insertDetails = array(
                        'created_by'				=> $this->userId,
                        'created_date'				=> date('Y-m-d h:i:s'),
                        'created_ip'				=> $this->input->ip_address(),
                    );
                    $priceArray = array_merge($priceArray,$insertDetails);
                }
                $price = $this->save_update_model->insert_update_price_details($priceArray, $priceId);

                $start_date = new DateTime($_POST['start_date']);
                $start_date = $start_date->format('Y-m-d h:i:s');
                $end_date = new DateTime($_POST['end_date']);
                $end_date = $end_date->format('Y-m-d h:i:s');

                $enquiryArray = array(
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'trip_from' => $this->input->post('trip_from'),
                    'trip_via' => $this->input->post('trip_via'),
                    'trip_to' => $this->input->post('trip_to'),
                    'seaters' => $this->input->post('seaters'),
                    'min_km' => $this->input->post('min_km'),
                    'approx_km' => $this->input->post('approx_km'),
                    'vehicle_id' => $this->input->post('vehicle'),
                    'model_id' => $model_id,
                    'customer_id' => $customerId['id'],
                    'cmpny_id' => $this->cmpnyId,
                    'last_modified_by' => $this->userId,
                    'last_modified_ip' => $this->input->ip_address(),
                );
                $insertOrUpdate = 0;
                if(!$enquiryId) {
                    //insert
                    $insertOrUpdate = 1;
                    $insertDetails = array(
                        'created_by'				=> $this->userId,
                        'created_date'				=> date('Y-m-d h:i:s'),
                        'created_ip'				=> $this->input->ip_address(),
                    );
                    $enquiryArray = array_merge($enquiryArray,$insertDetails);
                }
                $returned_val = $this->save_update_model->insert_update_enquiry_details($enquiryArray, $enquiryId);
                $insertedId = $returned_val['id'];

                $paymentType = $this->input->post('advance_payment_type');
                if($paymentType == 1){
                    $advancePayment = $this->input->post('fixed_amount');
                }else if($paymentType == 2){
                    $advancePayment = $this->input->post('non_ac_percentage');
                }else if($paymentType == 3){
                    $advancePayment = $this->input->post('ac_percentage');
                }else{
                    $advancePayment = 0;
                }
                $enquiryPriceArr = array(
                    'enquiry_id' => $returned_val['id'],
                    'per_km' => $this->input->post('per_km'),
                    'per_km_ac' => $this->input->post('per_km_ac'),
                    'driver_allowance' => $this->input->post('driver_allowance'),
                    'extra_per_km' => $this->input->post('extra_per_km'),
                    'extra_per_km_ac' => $this->input->post('extra_per_km_ac'),
                    'driver_allowance_night' => $this->input->post('driver_allowance_night'),
                    'advance_payment' => $advancePayment,
                    'advance_payment_type' => $paymentType,
                    'flag' => 0,

                );
                $returned_val = $this->save_update_model->insert_update_enquiry_price($enquiryPriceArr, $enquiryId);
                // meDebug($returned_val,1);insertedId

                if($returned_val['id'] != 0) {
                	if($insertOrUpdate) {
                		$msg = "We have taken your enquiry for ".$enquiryArray['trip_from']." to ".$enquiryArray['trip_to'] ." on ".$start_date.". To know more and making payments click link ".base_url()."enquiries/view/".$insertedId;
						$response = sendCustomMsgs($msg,$customerArray['mobile']);
                	}
                    $this->session->set_flashdata('msg', $returned_val['msg']);
                }
                else {
                    $this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
                }
                redirect(base_url().'enquiries');

            }
        }else{
			$this->layouts->view('enquiries/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
    }

    public function getCarPriceDetails(){
	    $postArr = $_POST;
	    /*echo json_encode($_POST);
	    exit();*/
	    $priceDetails = $this->get_data_model->getCarPriceDetails($postArr);
	    if(empty($priceDetails)){
            $response['status'] = false;
        }else{
            $response['status'] = true;
        }
        $response['priceDetails'] = $priceDetails;
	    echo json_encode($response);
    }

	function _confirm_rules() {
		$this->form_validation->set_rules('vehicle', 'Vehicle', 'required|trim|callback__verify_dropdown['.$this->input->post('vehicle').']');
		$this->form_validation->set_rules('driverName', 'Driver', 'required|trim|callback__verify_dropdown['.$this->input->post('driverName').']');
		$this->form_validation->set_rules('paymentStatus', 'Payment Status', 'required|trim|callback__verify_dropdown['.$this->input->post('paymentStatus').']');
		$this->form_validation->set_rules('vehicleType', 'Vehicle Type', 'required|trim|callback__verify_dropdown['.$this->input->post('vehicleType').']');
	}

    public function confirm($enquiryId = 0) {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Enquiries List");
		$data = array();
    	$this->_confirm_rules();

    	if($enquiryId) {
        	$data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId);
			$data['completeEnquiryDetails'] = $this->get_data_model->getCompleteEnquiryDetails($enquiryId);
			$data['driver_details'] = $this->get_data_model->getDriverMinimumDetails($this->cmpnyId);
			$data['enquiryId'] = $enquiryId;
			$data['tripDetails'] = $this->get_data_model->getTripDetails($enquiryId);			  		
    	}

		if ($this->form_validation->run() == FALSE) {
			if(is_array($data['completeEnquiryDetails']) && !empty($data['completeEnquiryDetails'])) {
				$data['completeEnquiryDetails']['priceArray'] = priceCalculator($data['completeEnquiryDetails']);
				$this->layouts->view('enquiries/view_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, false);
			}
			else {
				show_404();
			}	    	
		}
		else
		{
			$tripArray = array(
						'enquiry_id' => $enquiryId,
						'vehicle_id' => $this->input->post('vehicle'),
						'driver_id' => $this->input->post('driverName'),
						'payment_status_id' => $this->input->post('paymentStatus'),
						'vehicle_type_id' => $this->input->post('vehicleType'),
						'confirmed_date_time' => date('Y-m-d h:i:s'),
						);
			$returned_val = $this->save_update_model->save_trip_details($tripArray);

			if($returned_val['id'] != 0) {
				$this->session->set_flashdata('msg', $returned_val['msg']);

				$driverId = $tripArray['driver_id'];

				$customerDetails = $this->get_data_model->getCustomerMobile($enquiryId);
				$driverDetails = $this->get_data_model->getDriverMobileNumber($driverId);
				$enquiryArray = $data['completeEnquiryDetails'];
				
				if(is_array($enquiryArray) && !empty($enquiryArray)) {
	                $start_date = new DateTime($enquiryArray['start_date']);
	                $start_date = $start_date->format('Y-m-d h:i A');
	                $end_date = new DateTime($enquiryArray['end_date']);
	                $end_date = $end_date->format('Y-m-d h:i A');

					$customerMobile = $customerDetails['mobile'];
					$driverMobile = $driverDetails['mobile'];

		    		$customerMsg = "Your enquiry for ".$enquiryArray['trip_from']." to ".$enquiryArray['trip_to'] ." on ".$start_date." is confirmed. for enquiry details click link ".base_url()."enquiries/view/".$enquiryId;

		    		$driverMsg = "Trip assigned to you on from ".$enquiryArray['trip_from']." to ".$enquiryArray['trip_to'] ." on ".$start_date." - ".$end_date;

					$customerMsgresponse = sendCustomMsgs($customerMsg,$customerMobile);
					$driverMsgresponse = sendCustomMsgs($driverMsg,$driverMobile);
				}
			}
			else {
				$this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
			}
			redirect(base_url().'enquiries');
	    }
    }

	function view($enquiryId = 0) {
		if($enquiryId) {
			$this->layouts->set_page_title("Enquiries List");
			$data = array();
			$data['completeEnquiryDetails'] = $this->get_data_model->getCompleteEnquiryDetails($enquiryId);
			$seoUrl = "Outstaion trip From ".$data['completeEnquiryDetails']['trip_from']." To ".$data['completeEnquiryDetails']['trip_to']." - ".$data['completeEnquiryDetails']['cmpny_name'];

			$this->layouts->set_title(SITE_NAME. " - " .$seoUrl);
			$this->layouts->set_description(SITE_NAME. " - " .$seoUrl);
			$data['enquiryId'] = $enquiryId;
			if(is_array($data['completeEnquiryDetails']) && !empty($data['completeEnquiryDetails'])) {
				$data['completeEnquiryDetails']['priceArray'] = priceCalculator($data['completeEnquiryDetails']);
				$this->layouts->view('enquiries/view_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, false);
			}
			else {
				show_404();
			}
		}
		else {
			redirect(base_url());
		}

	}

	function _verify_dropdown($field, $id) {
	    if (!$field) {
	         $this->form_validation->set_message('_verify_dropdown', 'Please select proper value for %s');
	         return FALSE;
	    }
	    else {
	    	return TRUE;
	    }
	}
    function actions($enquiryId,$flag) {
	    $userType = $this->session->userdata('user_type');
	    if($userType == 'admin') {
            $result = $this->save_update_model->updateEnquiryFlag($enquiryId,$flag);
            if($result) {
                $this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
                redirect(base_url().'enquiries');
            }
            else {
                $this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
                redirect(base_url().'enquiries');
            }
        }else {
            $this->session->set_flashdata('errormsg', lang('You are not allowed to perform that action.','ucword'));
            redirect(base_url().'dashboard');
        }
	}
	/*function lists() {
		//Admin.
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Enquiries" => base_url().'enquiries/',"List" => base_url().'enquiries/'.'add'));
		$data = array();
		$cmpnyId = 1;//$this->session->userdata("cmpnyId");
		$data['enquiriesArray'] = $this->get_data_model->getEnquiryDetails();
		// meDebug($data,true);
		Fix me 
		$this->layouts->view('enquiries/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function actions($enquiryId,$flag) {
		$result = $this->save_update_model->updateEnquiryFlag($enquiryId,$flag);
		if($result) {
			$this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
			redirect(base_url().'enquiries');
		}
		else {
			$this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
			redirect(base_url().'enquiries');
		}
	}*/

	/*function filter() {
    	// is_authenticated_user(array('super_admin'));

		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Filtered Enquiries List");

		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/", "Enquiries" => base_url()."enquiries", "Filtered List" => 'enquiries List'));
		$data = array();
		$posted_data = $this->input->post();
		$filterArray = array();
		$likeArray = array();
		if($this->cmpnyId) {
				$filterArray['e.cmpny_id'] = $this->cmpnyId;
		}

		if(isset($posted_data['customerName']) && !empty($posted_data['customerName'])) {
			$likeArray['customerName'] = $posted_data['customerName'];
			$data['postArray']['customerName'] = $likeArray['customerName'] ;
		}

		if(isset($posted_data['customerMobile']) && !empty($posted_data['customerMobile'])) {
			$likeArray['customerMobile'] = $posted_data['customerMobile'];
			$data['postArray']['customerMobile'] = $likeArray['customerMobile'] ;
		}

		if(isset($posted_data['customerName']) && !empty($posted_data['customerName'])) {
			$likeArray['customerName'] = $posted_data['customerName'];
			$data['postArray']['customerName'] = $likeArray['customerName'] ;
		}
		if(isset($posted_data['customerName']) && !empty($posted_data['customerName'])) {
			$likeArray['customerName'] = $posted_data['customerName'];
			$data['postArray']['customerName'] = $likeArray['customerName'] ;
		}
		
		if(isset($posted_data['status']) && !empty($posted_data['status'])) {
			$filterArray['v.flag'] = $posted_data['status'];
			$filterArray['status'] = $posted_data['status'];
		}
		$data['postArray']['filters'] = $filterArray;

		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		unset($filterArray['status']);
		$data['enquiriesArray'] = $this->get_data_model->filterEnquiryDetails($filterArray,$likeArray);
		// meDebug($data,true);
		// meDebug($data['vehicleDetails'],true);
		$this->layouts->view('enquiries/filter_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}*/
}

