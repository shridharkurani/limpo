<?php

/*
	@desc - User Registration.
	Author:sjkurani.
	Date: 01-05-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Prices extends CI_Controller {

	private $cmpnyId;
	private $userId;
	
	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_update_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
    	$this->userId = $this->session->userdata('user_id'); 
	}

	function index() {
    	is_authenticated_user(array('owner'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Prices for vehicle models");
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/", "Prices Settings" => base_url()."prices","List" => 'Prices List'));
		$data = array();

		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
    	// meDebug($this->cmpnyId,1);

		$data['cmpnyPriceDetails'] = $this->get_data_model->getPriceDetails($this->cmpnyId);

		$this->layouts->view('prices/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}
	
	function _add_edit_rules() {
		$this->form_validation->set_rules('vehicle_type', 'Vehicle Type', 'trim|required|callback__verify_dropdown['.$this->input->post('vehicle_type').']');
		$this->form_validation->set_rules('vehicle_model', 'Vehicle Model', 'required|trim|callback__verify_dropdown['.$this->input->post('vehicle_model').']');		
		$this->form_validation->set_rules('min_km', 'Min Km. Per day', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('price_per_km', 'Price Per Km', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('price_per_km_ac', 'Price Per Km. for A/C', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('driver_allowance', 'Driver Allowance', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('extra_driver_allowance', 'Driver allowance per night', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('extra_km', 'Price per extra Km', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('extra_km_ac', 'Price per extra Km. A/C', 'required|trim|xss_clean|integer');
	}

	function add_edit($priceId = 0) {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);

		$addOrEdit = ($priceId) ? "Edit" : "Add" ;
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/", "Prices Settings" => base_url()."prices", $addOrEdit => 'Prices List'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		if($this->input->post('vehicle_type')) {
			$typeName = $this->input->post('vehicle_type');
			$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);
		}
		if($priceId) {
			$data['priceId'] = $priceId;
			$data['priceDetails'] = $this->get_data_model->getPriceDetails($this->cmpnyId,$priceId);
			if(!empty($data['priceDetails'])) {
			$typeName = $data['priceDetails']['model_type'];
			$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);			
			}
			$this->layouts->set_page_title("Edit Price for models");

		}
		else {
			$this->layouts->set_page_title("Add Price for models");

		}

		$this->_add_edit_rules();
		if ($this->form_validation->run() == FALSE)
		{
			$this->layouts->view('prices/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else
		{
			// meDebug($this->input->post());
			$priceArray = array(
							'cmpny_id' => $this->cmpnyId,
							'model_id' => $this->input->post('vehicle_model'),
							'per_km' => $this->input->post('price_per_km'),
							'per_km_ac' => $this->input->post('price_per_km_ac'),
							'driver_allowance' => $this->input->post('driver_allowance'),
							'min_km' => $this->input->post('min_km'),
							'driver_allowance_night' => $this->input->post('extra_driver_allowance'),
							'extra_per_km' => $this->input->post('extra_km'),
							'extra_per_km_ac' => $this->input->post('extra_km_ac'),
							'last_modified_by'			=> $this->userId,
							'last_modified_ip'			=> $this->input->ip_address(),
						);
			if(!$priceId) {				
    			//insert
	    		$insertDetails = array(
					'created_by'				=> $this->userId,
					'created_date'				=> date('Y-m-d h:i:s'),
					'created_ip'				=> $this->input->ip_address(),
	    		);
	    		$priceArray = array_merge($priceArray,$insertDetails);
			}
			$returned_val = $this->save_update_model->insert_update_price_details($priceArray, $priceId);

			if($returned_val['id'] != 0) {
				$this->session->set_flashdata('msg', $returned_val['msg']);
			}
			else {
				$this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
			}
			redirect(base_url().'prices');

		}
	}
/*
	
	function edit($priceId) {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Add Prices");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Price Setting" => 'Price Setting'));
		$data = array();
		$data['priceDetails'] = $this->get_data_model->getPriceDetails($cmpnyId,$priceId);
		$this->layouts->view('prices/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}*/


	function lists() {
		//Admin.
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Prices of vehicle models");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Price Settings" => 'Price Settings'));
		$data = array();
		$cmpnyId = $this->session->userdata('cmpnyId');
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		$data['cmpnyPriceDetails'] = $this->get_data_model->getPriceDetails();
		// meDebug($data,true);
		$this->layouts->view('prices/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function actions($price_id,$flag) {
		$result = $this->save_update_model->updatePriceFlag($price_id,$flag);
		if($result) {
			$this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
			redirect(base_url().'prices');
		}
		else {
			$this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
			redirect(base_url().'prices');
		}
	}

	function filter() {
    	// is_authenticated_user(array('super_admin'));

		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Filtered List of Prices");
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/", "Prices Settings" => base_url()."prices","Filtered List" => 'Prices List'));
		$data = array();
		$posted_data = $this->input->post();
		$filterArray = array();
		if($this->cmpnyId) {
				$filterArray['cmpny_id'] = $this->cmpnyId;
		}
		if(isset($posted_data['vehicle_type']) && !empty($posted_data['vehicle_type'])) {
			$filterArray['vmm.model_type'] = $posted_data['vehicle_type'];
		}
		
		if(isset($posted_data['vehicle_model']) && !empty($posted_data['vehicle_model'])) {
			$filterArray['p.model_id'] = $posted_data['vehicle_model'];
		}
		
		if(isset($posted_data['status']) && !empty($posted_data['status'])) {
			$filterArray['p.flag'] = $posted_data['status'];
			$filterArray['status'] = $posted_data['status'];
		}
		$data['postArray']['filters'] = $filterArray;
		// $data['postedArray'] = $posted_data;

		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		unset($filterArray['status']);
		$data['cmpnyPriceDetails'] = $this->get_data_model->filterPriceDetails($filterArray);
/*		meDebug($filterArray);
		meDebug($data['cmpnyPriceDetails'],true);*/
		$this->layouts->view('prices/filter_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function _verify_dropdown($field, $id) {
	    if (!$field) {
	         $this->form_validation->set_message('_verify_dropdown', 'Please select proper value for %s');
	         return FALSE;
	    }
	    else {
	    	return TRUE;
	    }
	}
}

