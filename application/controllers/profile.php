<?php

/*
	@desc - User Registration.
	Author:sjkurani.
	Date: 01-05-2018
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller {

    private $cmpnyId;
    private $userId;

    public function __construct()
    {
        parent::__construct();
        LoadCssAndJs($this->layouts);
        $this->load->model('get_data_model');
        $this->load->model('save_update_model');
        $this->cmpnyId = $this->session->userdata('cmpnyId');
        $this->userId = $this->session->userdata('user_id');
    }

    function index() {
//        is_authenticated_user(array('admin','user'));
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("My Profile");
//        $this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","profile Profile" => 'profile Profile'));
        $data = array();
        /*if(!$this->cmpnyId) {
            redirect(base_url().'profile/add_edit');
            exit();
        }*/
        $data['userDetails'] = $this->get_data_model->getUserProfileDetails($this->userId);
        $this->layouts->view('profile/overview_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function _add_edit_rules() {
        $this->form_validation->set_rules('cmpny_name', ' profile Name', 'trim|required|xss_clean|min_length[3]');
        $this->form_validation->set_rules('cmpny_mobile', 'profile Mobile', 'trim|required|xss_clean|integer');
        $this->form_validation->set_rules('cmpny_alt_mobile', 'profile Alternative Mobile', 'trim|xss_clean|integer');
        $this->form_validation->set_rules('cmpny_email', ' profile Email', 'trim|xss_clean|valid_email');

        // $this->form_validation->set_rules('cmpny_logo', 'Password', 'trim|required|xss_clean|callback__verify_login');
        if(!empty($_FILES['cmpny_logo']['name'])) {
            $logo_name = $_FILES['cmpny_logo']['name'];
        }
        if($this->input->post('cmpny_logo')) {
            $logo_name = $this->input->post('cmpny_logo');
        }

        if(isset($logo_name)) {
            $this->form_validation->set_rules('cmpny_logo', 'profile Logo', 'callback__verify_uploading_file_and_upload');
        }

        $this->form_validation->set_rules('cmpny_address', 'profile Address ', 'trim|xss_clean');
        $this->form_validation->set_rules('contact_person_name', 'Contact Person Name', 'trim|xss_clean|min_length[3]');
        $this->form_validation->set_rules('contact_person_mobile', 'Contact Person Mobile', 'trim|xss_clean|integer');
    }

    function add_edit() {
        is_authenticated_user(array('admin','owner'));
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("profile Profile Details");
        $data = array();
        if($this->cmpnyId) {
            $add_edit = 'edit';
//            $data['packages'] = $this->get_data_model->getUserPacakges($this->userId);
            // meDebug($data,1);
        }
        else {
            $add_edit = "add";
        }
        $data['packages'] = $this->get_data_model->getAllpackages('grid');

        $this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","profile Profile" => base_url()."profile/", $add_edit => ucwords($add_edit)));
        $this->_add_edit_rules();
        if ($this->form_validation->run() == FALSE)
        {
            $this->layouts->view('profile/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
        else
        {
            //Save or update and redirect.
            $contact_owner = $this->input->post('is_contact_person_owner');
            $owner_id = $this->userId;

            if(isset($this->filedata['file_name'])) {
                $cmpny_logo = $this->filedata['file_name'];
            }
            else {
                $cmpny_logo = $this->input->post('saved_cmpny_logo');
            }

            $cmpnyDetails = array(
                'cmpny_name'				=> $this->input->post('cmpny_name'),
                'cmpny_mobile'				=> $this->input->post('cmpny_mobile'),
                'cmpny_alt_mobile'			=> $this->input->post('cmpny_alt_mobile'),
                'cmpny_email'				=> $this->input->post('cmpny_email'),
                'cmpny_logo' 				=> $cmpny_logo,
                'cmpny_address'				=> $this->input->post('cmpny_address'),
                'cmpny_desc'				=> $this->input->post('cmpny_desc'),
                'is_contact_person_owner'	=> $contact_owner,
                'contact_person_name'		=> $this->input->post('contact_person_name'),
                'contact_person_mobile'		=> $this->input->post('contact_person_mobile'),
                'last_modified_by'			=> $this->userId,
                'last_modified_ip'			=> $this->input->ip_address(),
            );
            if(!$this->cmpnyId) {
                //insert
                $insertDetails = array(
                    'owner_id'					=> $owner_id,
                    'created_by'				=> $owner_id,
                    'created_date'				=> date('Y-m-d h:i:s'),
                    'created_ip'				=> $this->input->ip_address(),
                );
                $cmpnyDetails = array_merge($cmpnyDetails,$insertDetails);
            }
            $returned_val = $this->save_update_model->insert_update_cmpny_details($cmpnyDetails, $this->cmpnyId);
            // meDebug($returned_val,1);
            if($returned_val['id'] != 0) {
                $sessiondata['cmpnyId'] = $returned_val['id'];
                $cmpnyProfileDeatails = $this->get_data_model->getCmpnyProfileDetails($this->cmpnyId);
                $cmpnyProfilePic = $cmpnyProfileDeatails['cmpny_logo'];
                if(!empty($cmpnyProfilePic)) {
                    $cmpnyProfilePicArr = explode(".", $cmpnyProfilePic);
                    $fullImgUrl = $cmpnyProfilePicArr[0]."_thumb.".$cmpnyProfilePicArr[1];
                }
                else {
                    $fullImgUrl = 0;
                }
                $sessiondata['session_cmpny_display_name'] = ucwords($cmpnyProfileDeatails['cmpny_name']);
                $sessiondata['session_profile_pic'] = $fullImgUrl;
                $sessiondata['cmpnyId'] = $returned_val['id'];
            }
            else {
                $sessiondata['cmpnyId'] = 0;
            }
            $this->session->set_userdata($sessiondata);
            $this->session->set_flashdata('msg', $returned_val['msg']);
            redirect(base_url().'profile');
        }
    }

    function lists() {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Exiting Companies");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i> Dashboard" => base_url()."dashboard/","Existing Companies" => "Existing Companies"));
        $data = array();
        $data['allCmpnyDetails'] = $this->get_data_model->getOwnerCmpnyDetails();
        // meDebug($data,true);
        $this->layouts->view('admin/companies_list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function actions($cmpny_id,$flag) {
        $result = $this->save_update_model->updateCmpnyFlag($this->cmpny_id,$flag);
        if($result) {
            $this->session->set_flashdata('msg', lang('Details Updated successfully','ucword')." ".$valid_fields['level_name']);
            redirect(base_url().'profile/lists');
        }
        else {
            $this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
            redirect(base_url().'profile/lists');
        }
    }

    function _verify_uploading_file_and_upload() {
        $config['upload_path'] = './assets/uploads/profile';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '10000';
        $config['max_width']  = '10240';
        $config['max_height']  = '7680';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('cmpny_logo'))
        {
            $this->form_validation->set_message('_verify_uploading_file_and_upload', $this->upload->display_errors());
            return false;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data('cmpny_logo'));
            $this->filedata = $data['upload_data'];
            $config['image_library'] = 'gd2';
            //Create thumbnail with resize.
            $config['source_image']	= './assets/uploads/profile/'.$data['upload_data']['file_name'];
            $config['new_image']	= './assets/uploads/profile/thumb/'.$data['upload_data']['file_name'];

            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']	= 352;
            $config['height']	= 150;

            $this->image_lib->initialize($config);

            $this->image_lib->resize();

            return true;
        }
    }
}

