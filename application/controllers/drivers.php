<?php

/*
	@desc - User Registration.
	Author:Sjkurani.
	Date: 01-05-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Drivers extends CI_Controller {
	
	private $cmpnyId;
	private $userId;

	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_update_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
    	$this->userId = $this->session->userdata('user_id'); 
	}

	function index() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Drivers");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Price Settings" => 'Price Settings'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		$data['driverDetails'] = $this->get_data_model->getDriverDetails($this->cmpnyId);
		// meDebug($data['driverDetails'],true);
		$this->layouts->view('drivers/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function _add_edit_rules() {		
		$this->form_validation->set_rules('driver_name', 'Driver Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('driver_mobile', 'Driver Mobile', 'required|trim|xss_clean|integer');
	}

	function add_edit($driverId = 0) {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Company Profile" => 'Company Profile'));
		$data = array();
		$data['languageArray'] = $this->get_data_model->getAlllanguages(1);
		$data['placesArray'] = $this->get_data_model->getAllPlaces(1);

		if($driverId) {
			$data['driverId'] = $driverId;
			$data['driverDetails'] = $this->get_data_model->getDriverDetails($this->cmpnyId,$driverId);
			// meDebug($data['driverDetails'],true);
			if(!empty($data['driverDetails']) && is_array($data['driverDetails']) && !empty($data['driverDetails']['langauages']) && is_array($data['driverDetails']['langauages'])) {
				$langauagesArraywithIds = array();
				foreach ($data['driverDetails']['langauages'] as $key => $value) {
					array_push($langauagesArraywithIds, $value['id']);
				}
				$data['langauagesArraywithIds'] = $langauagesArraywithIds;
			}

			if(!empty($data['driverDetails']) && is_array($data['driverDetails']) && !empty($data['driverDetails']['places']) && is_array($data['driverDetails']['places'])) {
				$placesArraywithIds = array();
				foreach ($data['driverDetails']['places'] as $key => $value) {
					array_push($placesArraywithIds, $value['id']);
				}
				$data['placesArraywithIds'] = $placesArraywithIds;
			// meDebug($data['languageArray'],true);

			}
			$this->layouts->set_page_title("Edit Driver Details");

		}
		else {
			$this->layouts->set_page_title("Add Driver Details");

		}

		$this->_add_edit_rules();
		if ($this->form_validation->run() == FALSE)
		{
			$this->layouts->view('drivers/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else
		{
			// meDebug($this->input->post());
			$user_id = $this->session->userdata('user_id');
			$languages = $this->input->post('language');
			$places = $this->input->post('place');
			$driverArray = array(
							'cmpny_id' => $this->cmpnyId,
							'fullname' => $this->input->post('driver_name'),
							'mobile' => $this->input->post('driver_mobile'),
							'last_modified_by'			=> $user_id,
							'last_modified_ip'			=> $this->input->ip_address(),
						);
			if(!$driverId) {				
    			//insert
	    		$insertDetails = array(
					'created_by'				=> $user_id,
					'created_date'				=> date('Y-m-d h:i:s'),
					'created_ip'				=> $this->input->ip_address(),
	    		);
	    		$driverArray = array_merge($driverArray,$insertDetails);
			}
			$returned_val = $this->save_update_model->insert_update_driver_details($driverArray, $driverId);

			if($returned_val['id'] != 0) {
				$this->session->set_flashdata('msg', $returned_val['msg']);
				$this->save_update_model->insert_update_driver_language_details($languages, $returned_val['id']);
				$this->save_update_model->insert_update_driver_places_details($places, $returned_val['id']);
			}
			else {
				$this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
			}
			redirect(base_url().'drivers');
		}
	}


	function lists() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Drivers");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Price Settings" => 'Price Settings'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		$data['driverDetails'] = $this->get_data_model->getdriverDetails($this->cmpnyId);
		$this->layouts->view('drivers/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function actions($driver_id,$flag) {
		$result = $this->save_update_model->updateDriverFlag($driver_id,$flag);
		if($result) {
			$this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
			redirect(base_url().'drivers');
		}
		else {
			$this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
			redirect(base_url().'drivers');
		}
	}

	function filter() {
		echo "string";
	}

	function _verify_dropdown($field, $id) {
	    if (!$field) {
	         $this->form_validation->set_message('_verify_dropdown', 'Please select proper value for %s');
	         return FALSE;
	    }
	    else {
	    	return TRUE;
	    }
	}
}

