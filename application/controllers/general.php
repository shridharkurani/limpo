<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class General extends CI_Controller {
	
	var $filedata = array();

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('save_update_model');
	    $this->load->model('get_data_model');
    	LoadCssAndJs($this->layouts);
	}

	function termsAndCondtions() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("Home" => base_url(),"Terms And Conditions" => 'Terms'));
	    $data = array();
		$this->layouts->view('general/termsAndCondtions_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);
	}

	function privacy() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("Home" => base_url(),"Privacy Policy" => 'Privacy Policy'));
	    $data = array();
		$this->layouts->view('general/privacy_policy_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);
	}

	function faqs() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("Home" => base_url(),"FAQs" => 'FAQs'));
	    $data = array();
		$this->layouts->view('general/faqs_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);
	}

	function aboutUs() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("Home" => base_url(),"About Us" => 'About Us'));
	    $data = array();
		$this->layouts->view('general/about_us_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);
	}

	function contactUs() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("Home" => base_url(),"Contact Us" => 'Contact Us'));
	    $data = array();
		$this->layouts->view('general/contact_us_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);	
	}

	function sitemap() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("Home" => base_url(),"Sitemap" => 'Sitemap'));
	    $data = array();
		$this->layouts->view('general/sitemap_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);		
	}
}