<?php

/*
	@desc - User Registration.
	Author:sjkurani
	Date: 27-04-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller {
	
	private $cmpnyId;
	private $userId;
	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
    	$this->userId = $this->session->userdata('user_id');
	}

	function index() {
//	    sendOTP(9036096754, "HI ");
    	is_authenticated_user(array('admin','user'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$data = array();
		$userType = $this->session->userdata('user_type');

        if($userType == 'admin') {
            $data['allCounts'] = $this->get_data_model->getDashboardCounts($this->cmpnyId );
            $data['enquiriesArray'] = $this->get_data_model->getEnquiryDetailsByUserId();
            $this->layouts->view('dashboard/admin_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
        else if($userType == 'user') {
            $data['allCounts'] = $this->get_data_model->getDashboardCounts($this->cmpnyId );

            $data['enquiriesArray'] = $this->get_data_model->getEnquiryDetailsByUserId($this->userId);
//            $data['enquiriesArray'] = $this->get_data_model->getEnquiryDetails($this->cmpnyId);
            $this->layouts->view('dashboard/user_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
    }

	function msg() {
		$msg = "Bangalore to mysore on 21-06-2018. View More details ".base_url();
		$response = sendCustomMsgs($msg,"7899452456");
		meDebug($response,true);
	}
	
	function sendMail() {
		$content = get_mail_content('google.com','registration');
		$response = send_trip_mail($content);
		meDebug($response);
	}
}

