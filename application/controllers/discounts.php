<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class discounts extends CI_Controller {

    private $cmpnyId;
    private $userId;

    public function __construct()
    {
        parent::__construct();
        LoadCssAndJs($this->layouts);
        $this->load->model('get_data_model');
        $this->load->model('save_update_model');
        $this->load->model('discount_model');
        $this->cmpnyId = $this->session->userdata('cmpnyId');
        $this->userId = $this->session->userdata('user_id');
    }

    function index() {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Existing discounts");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Discounts" => 'Discounts'));
        $data = array();
        $data['discounts'] = $data = $this->get_data_model->getAlldiscounts();
        $this->layouts->view('discounts/list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function actions($id,$flag) {
        $result = $this->save_update_model->updateFlag($id,'id', $flag, 'discounts');
        if($result) {
            $this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
            redirect(base_url().'discounts');
        }
        else {
            $this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
            redirect(base_url().'discounts');
        }
    }

    function add_edit($discountId = 0) {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Add New Discount");
        $addOrEdit = ($discountId) ? "Edit" : "Add" ;

        $this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","discounts" => base_url()."discounts/",$addOrEdit => 'discount'));
        $data = array();

        if($discountId) {
            $data['discountId'] = $discountId;
            $data['discountDetails'] = $this->get_data_model->getOne('discounts', $discountId, 'id');
            $this->layouts->set_page_title("Edit discount Details");
        }
        else {
            $this->layouts->set_page_title("Add discount Details");
        }
        $this->_add_edit_rules();
        if ($this->form_validation->run() == FALSE)
        {
            $this->layouts->view('discounts/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
        else
        {
            $discountArray = array(
                'discount_short_code' => $this->input->post('discount_short_code'),
                'discount_percentage' => $this->input->post('discount_percentage'),
            );

            if(!$discountId) {
                //insert
                $insertDetails = array(
                    'created_date'				=> date('Y-m-d h:i:s'),
                );
                $discountArray = array_merge($discountArray,$insertDetails);
            }

            $returned_val = $this->save_update_model->insert_update_discount_details($discountArray, $discountId);

            if($returned_val['id'] != 0) {
                $this->session->set_flashdata('msg', $returned_val['msg']);
            }
            else {
                $this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
            }
            redirect(base_url().'discounts');
        }
    }

    function _add_edit_rules() {
        $this->form_validation->set_rules('discount_short_code', 'Discount Short Code', 'required|trim|xss_clean|is_unique[discounts.discount_short_code]');
        $this->form_validation->set_rules('discount_percentage', 'Discount Percentage',  'required|integer|trim|xss_clean');
    }
}