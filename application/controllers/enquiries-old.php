<?php

/*
	@desc - User Registration.
	Author:SJKURANI.
	Date: 01-05-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Enquiries extends CI_Controller {

	private $cmpnyId;
	private $userId;
	
	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_update_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
    	$this->userId = $this->session->userdata('user_id'); 
	}

	function index() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Enquiries" => base_url().'enquiries/',"List" => base_url().'enquiries/'.'add'));
		$data = array();
		$cmpnyId = $this->session->userdata("cmpnyId");
		$data['enquiriesArray'] = $this->get_data_model->getEnquiryDetails($cmpnyId);
		if(is_array($data['enquiriesArray']) && !empty($data['enquiriesArray'])) {
			$this->layouts->view('enquiries/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else {
			$this->layouts->view('enquiries/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		// meDebug($data,true);
		/*Fix me */
	}
	
	function _add_edit_rules() {
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|trim|xss_clean');
		$this->form_validation->set_rules('end_date', 'End date', 'required|trim|xss_clean');
		$this->form_validation->set_rules('trip_from', 'From', 'required|trim|xss_clean');
		$this->form_validation->set_rules('trip_via', 'Via', 'trim|xss_clean');
		$this->form_validation->set_rules('trip_to', 'To', 'required|trim|xss_clean');
		$this->form_validation->set_rules('seaters', 'Seater', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('vehicle', 'Vehicle', 'trim|required|callback__verify_dropdown['.$this->input->post('vehicle').']');
		$this->form_validation->set_rules('vehicle_model', 'Vehicle Model', 'required|trim|callback__verify_dropdown['.$this->input->post('vehicle_model').']');


		$this->form_validation->set_rules('customer_name', 'Customer Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('customer_mobile', 'Customer Mobile', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('customer_alt_mobile', 'Customer Alternative Mobile', 'trim|xss_clean|integer');
		$this->form_validation->set_rules('customer_email', 'Customer Email', 'trim|xss_clean|valid_email');
	}

	function add_edit_old($enquiryId = 0) {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$addOrEdit = ($enquiryId) ? "Edit" : "Add" ;

		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/", "Enquiries" => base_url()."enquiries", $addOrEdit => 'enquiries List'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		if($this->input->post('vehicle_type')) {
			$typeName = $this->input->post('vehicle_type');
			$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);
		}
		/*if($enquiryId) {
			$cmpnyId = 01;
			$data['enquiryId'] = $enquiryId;
			$data['priceDetails'] = $this->get_data_model->getPriceDetails($cmpnyId,$priceId);
			if(!empty($data['priceDetails'])) {
			$typeName = $data['priceDetails']['model_type'];
			$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);			
			}
			$this->layouts->set_page_title("Edit Price for models");

		}
		else {
			$this->layouts->set_page_title("New Enquiry");

		}*/

		$this->_add_edit_rules();
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->layouts->view('enquiries/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else
		{
			// meDebug($this->input->post());
			$user_id = 1;//$this->session->userdata('user_id');
			$cmpnyId = 1;//$this->session->userdata('cmpnyId');
			$model_id = $this->input->post('vehicle_model');
			$customerArray = array(
							'fullname' => $this->input->post('customer_name'),
							'mobile' => $this->input->post('customer_mobile'),
							'alt_mobile' => $this->input->post('customer_alt_mobile'),
							'email' => $this->input->post(''),
							'created_date'	=> date('Y-m-d h:i:s'),
							'created_ip' => $this->input->ip_address(),
						);

			$customerId = $this->save_update_model->insert_update_customer_details($customerArray, $enquiryId);
			$priceId = $this->get_data_model->getPriceIdBymodelId($model_id,$cmpnyId);

			$priceArray = array(
							'cmpny_id' => 1,//$this->input->post(''),
							'model_id' => $this->input->post('vehicle_model'),
							'per_km' => $this->input->post('per_km'),
							'per_km_ac' => $this->input->post('per_km_ac'),
							'driver_allowance' => $this->input->post('driver_allowance'),
							'min_km' => $this->input->post('min_km'),
							'driver_allowance_night' => $this->input->post('extra_driver_allowance'),
							'extra_per_km' => $this->input->post('extra_km'),
							'extra_per_km_ac' => $this->input->post('extra_km_ac'),
							'last_modified_by'			=> $user_id,
							'last_modified_ip'			=> $this->input->ip_address(),
						);

			if(!$enquiryId) {				
    			//insert
	    		$insertDetails = array(
					'created_by'				=> $user_id,
					'created_date'				=> date('Y-m-d h:i:s'),
					'created_ip'				=> $this->input->ip_address(),
	    		);
	    		$priceArray = array_merge($priceArray,$insertDetails);
			}
			$customerId = $this->save_update_model->insert_update_price_details($priceArray, $priceId);

			$enquiryArray = array(
							'cmpny_id' => 1,//$this->input->post(''),
							'start_date' => date('Y-m-d h:i:s'),//$this->input->post('start_date'),
							'end_date' => date('Y-m-d h:i:s'),//$this->input->post('end_date'),
							'trip_from' => $this->input->post('trip_from'),
							'trip_via' => $this->input->post('trip_via'),
							'trip_to' => $this->input->post('trip_to'),
							'seaters' => $this->input->post('seaters'),
							'min_km' => $this->input->post('min_km'),
							'approx_km' => $this->input->post('approx_km'),
							// 'vehicle_type' => $this->input->post('vehicle_type'),
							'model_id' => $this->input->post('vehicle_model'),
							'customer_id' => $customerId['id'],
							'cmpny_id' => $cmpnyId,/*

							'per_km_ac' => $this->input->post('price_per_km_ac'),
							'driver_allowance' => $this->input->post('driver_allowance'),
							'driver_allowance_night' => $this->input->post('extra_driver_allowance'),
							'extra_per_km' => $this->input->post('extra_km'),
							'extra_per_km_ac' => $this->input->post('extra_km_ac'),
*/
							'last_modified_by'			=> $user_id,
							'last_modified_ip'			=> $this->input->ip_address(),
						);


			if(!$enquiryId) {				
    			//insert
	    		$insertDetails = array(
					'created_by'				=> $user_id,
					'created_date'				=> date('Y-m-d h:i:s'),
					'created_ip'				=> $this->input->ip_address(),
	    		);
	    		$enquiryArray = array_merge($enquiryArray,$insertDetails);
			}
			$returned_val = $this->save_update_model->insert_update_enquiry_details($enquiryArray, $enquiryId);

			if($returned_val['id'] != 0) {
				$this->session->set_flashdata('msg', $returned_val['msg']);
			}
			else {
				$this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
			}
			redirect(base_url().'enquiries');

		}
	}

	function lists() {
		//Admin.
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Enquiries");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Enquiries" => base_url().'enquiries/',"List" => base_url().'enquiries/'.'add'));
		$data = array();
		$cmpnyId = 1;//$this->session->userdata("cmpnyId");
		$data['enquiriesArray'] = $this->get_data_model->getEnquiryDetails();
		// meDebug($data,true);
		/*Fix me */
		$this->layouts->view('enquiries/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function actions($enquiryId,$flag) {
		$result = $this->save_update_model->updateEnquiryFlag($enquiryId,$flag);
		if($result) {
			$this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
			redirect(base_url().'enquiries');
		}
		else {
			$this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
			redirect(base_url().'enquiries');
		}
	}

	function view($enquiryId = 0) {
		if($enquiryId) {
			$this->layouts->set_title(SITE_NAME);
			$this->layouts->set_description(DEFAULT_SITE_DESC);
			$this->layouts->set_page_title("Enquiries List");
			$data = array();
			$data['completeEnquiryDetails'] = $this->get_data_model->getCompleteEnquiryDetails($enquiryId);
			$data['enquiryId'] = $enquiryId;
			if(is_array($data['completeEnquiryDetails']) && !empty($data['completeEnquiryDetails'])) {

				$data['completeEnquiryDetails']['priceArray'] = priceCalculator($data['completeEnquiryDetails']);
				$this->layouts->view('enquiries/view_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, false);
			}
			else {
				show_404();
			}

		}
		else {
			redirect(base_url());
		}

	}
	function filter() {
    	// is_authenticated_user(array('super_admin'));

		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Filtered Enquiries List");

		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/", "Enquiries" => base_url()."enquiries", "Filtered List" => 'enquiries List'));
		$data = array();
		$posted_data = $this->input->post();
		$filterArray = array();
		$likeArray = array();
		if($this->cmpnyId) {
				$filterArray['e.cmpny_id'] = $this->cmpnyId;
		}

		if(isset($posted_data['customerName']) && !empty($posted_data['customerName'])) {
			$likeArray['customerName'] = $posted_data['customerName'];
			$data['postArray']['customerName'] = $likeArray['customerName'] ;
		}

		if(isset($posted_data['customerMobile']) && !empty($posted_data['customerMobile'])) {
			$likeArray['customerMobile'] = $posted_data['customerMobile'];
			$data['postArray']['customerMobile'] = $likeArray['customerMobile'] ;
		}

		if(isset($posted_data['customerName']) && !empty($posted_data['customerName'])) {
			$likeArray['customerName'] = $posted_data['customerName'];
			$data['postArray']['customerName'] = $likeArray['customerName'] ;
		}
		if(isset($posted_data['customerName']) && !empty($posted_data['customerName'])) {
			$likeArray['customerName'] = $posted_data['customerName'];
			$data['postArray']['customerName'] = $likeArray['customerName'] ;
		}
		
		if(isset($posted_data['status']) && !empty($posted_data['status'])) {
			$filterArray['v.flag'] = $posted_data['status'];
			$filterArray['status'] = $posted_data['status'];
		}
		$data['postArray']['filters'] = $filterArray;

		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		unset($filterArray['status']);
		$data['enquiriesArray'] = $this->get_data_model->filterEnquiryDetails($filterArray,$likeArray);
		// meDebug($data,true);
		// meDebug($data['vehicleDetails'],true);
		$this->layouts->view('enquiries/filter_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

    public function add_edit($enquiryId = 0){
        $data['enquiryId'] = $enquiryId;
        $data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
        $data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId);
        // meDebug($data['vehicleDetails'],1);
        if($this->input->post('vehicle_type')) {
            $typeName = $this->input->post('vehicle_type');
            $data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);
        }
        if($enquiryId != 0){
            $data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_type');
            $data['enquiryDetails'] = $this->get_data_model->getEnquiryDet($enquiryId);
//            echo'<pre/>';print_r($data);exit;
        }

        if(isset($_POST) && !empty($_POST)){
            $this->_add_edit_rules();
            if ($this->form_validation->run() == FALSE){
                // $this->load->view('enquiries/add_edit_view', $data);

				$this->layouts->view('enquiries/add_edit_enquiries', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
            }else{
                $model_id = $this->input->post('vehicle_model');

                $customer_id = $this->input->post('customer_id');
                $customerArray = array(
                    'fullname' => $this->input->post('customer_name'),
                    'mobile' => $this->input->post('customer_mobile'),
                    'alt_mobile' => $this->input->post('customer_alt_mobile'),
                    'email' => $this->input->post('customer_email'),
                    'created_date'	=> date('Y-m-d h:i:s'),
                    'created_ip' => $this->input->ip_address(),
                );
                $customerId = $this->save_update_model->insert_update_customer_details($customerArray, $customer_id);

                $priceId = $this->get_data_model->getPriceIdBymodelId($model_id, $cmpnyId);
                $priceArray = array(
                    'cmpny_id' => $cmpnyId,
                    'model_id' => $this->input->post('vehicle_model'),
                    'per_km' => $this->input->post('per_km'),
                    'per_km_ac' => $this->input->post('per_km_ac'),
                    'driver_allowance' => $this->input->post('driver_allowance'),
                    'min_km' => $this->input->post('min_km'),
                    'driver_allowance_night' => $this->input->post('extra_driver_allowance'),
                    'extra_per_km' => $this->input->post('extra_km'),
                    'extra_per_km_ac' => $this->input->post('extra_km_ac'),
                    'last_modified_by' => $user_id,
                    'last_modified_ip' => $this->input->ip_address(),
                );

                if(!$enquiryId) {
                    //insert
                    $insertDetails = array(
                        'created_by'				=> $user_id,
                        'created_date'				=> date('Y-m-d h:i:s'),
                        'created_ip'				=> $this->input->ip_address(),
                    );
                    $priceArray = array_merge($priceArray,$insertDetails);
                }
                $price = $this->save_update_model->insert_update_price_details($priceArray, $priceId);

                $start_date = new DateTime($_POST['start_date']);
                $start_date = $start_date->format('Y-m-d h:i:s');
                $end_date = new DateTime($_POST['end_date']);
                $end_date = $end_date->format('Y-m-d h:i:s');

                $enquiryArray = array(
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'trip_from' => $this->input->post('trip_from'),
                    'trip_via' => $this->input->post('trip_via'),
                    'trip_to' => $this->input->post('trip_to'),
                    'seaters' => $this->input->post('seaters'),
                    'min_km' => $this->input->post('min_km'),
                    'approx_km' => $this->input->post('approx_km'),
                    'vehicle_id' => 1,//$this->input->post('vehicle_type'),
                    'model_id' => $this->input->post('vehicle_model'),
                    'customer_id' => $customerId['id'],
                    'cmpny_id' => $cmpnyId,
                    'last_modified_by' => $user_id,
                    'last_modified_ip' => $this->input->ip_address(),
                );
                if(!$enquiryId) {
                    //insert
                    $insertDetails = array(
                        'created_by'				=> $user_id,
                        'created_date'				=> date('Y-m-d h:i:s'),
                        'created_ip'				=> $this->input->ip_address(),
                    );
                    $enquiryArray = array_merge($enquiryArray,$insertDetails);
                }
                $returned_val = $this->save_update_model->insert_update_enquiry_details($enquiryArray, $enquiryId);

                $paymentType = $this->input->post('advance_payment_type');
                if($paymentType == 1){
                    $advancePayment = $this->input->post('fixed_amount');
                }else if($paymentType == 2){
                    $advancePayment = $this->input->post('non_ac_percentage');
                }else if($paymentType == 3){
                    $advancePayment = $this->input->post('ac_percentage');
                }else{
                    $advancePayment = 0;
                }
                $enquiryPriceArr = array(
                    'enquiry_id' => $returned_val['id'],
                    'per_km' => $this->input->post('per_km'),
                    'per_km_ac' => $this->input->post('per_km_ac'),
                    'driver_allowance' => $this->input->post('driver_allowance'),
                    'extra_per_km' => $this->input->post('extra_per_km'),
                    'extra_per_km_ac' => $this->input->post('extra_per_km_ac'),
                    'driver_allowance_night' => $this->input->post('driver_allowance_night'),
                    'advance_payment' => $advancePayment,
                    'advance_payment_type' => $paymentType,
                    'flag' => 0,

                );
                $returned_val = $this->save_update_model->insert_update_enquiry_price($enquiryPriceArr, $enquiryId);

                if($returned_val['id'] != 0) {
                    $this->session->set_flashdata('msg', $returned_val['msg']);
                }
                else {
                    $this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
                }
                redirect(base_url().'enquiries');

            }
        }else{
            $this->load->view('enquiries/add_edit_enquiries', $data);
        }
    }

    public function getCarPriceDetails(){
	    $postArr = $_POST;
	    /*echo json_encode($_POST);
	    exit();*/
	    $priceDetails = $this->get_data_model->getCarPriceDetails($postArr);
	    if(empty($priceDetails)){
            $response['status'] = false;
        }else{
            $response['status'] = true;
        }
        $response['priceDetails'] = $priceDetails;
	    echo json_encode($response);
    }
}

