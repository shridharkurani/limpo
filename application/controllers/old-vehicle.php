<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicle extends CI_Controller {
	
	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_data_model');
	}

	function index() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url()."dashboard/", 'Company' => 'Company'));
		$data = array();
		$data['is_company_added'] = $this->save_data_model->is_company_added('1');
		$this->layouts->view('vehicle/index', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function add_company(){
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url()."dashboard/", 'Company' => 'Company'));

		$data = array();
		$data['is_company_added'] = $this->save_data_model->is_company_added('1');

		$this->_add_company_rules();

		if ($this->form_validation->run() == FALSE) {
			
			$this->layouts->view('company/index', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);

		} else {
			$data = $this->input->post();
			$group_array = array('cmpny_name' => $data['company_name'],
								'cmpny_mobile' => $data['company_mobile'],
								'cmpny_alt_mobile' => $data['company_alternative_mobile'],
								'cmpny_email' => $data['company_email'],
								'cmpny_address' => $data['company_address'],
								'owner_id' => '1',//$data['user_id'],
								'created_by' => '1',//$data['responsible_person'],
								'created_date' => date('Y-m-d h:i:s'),
								'created_ip' => $this->input->ip_address(),
								'last_modified_by' => '1',
								'last_modified_ip' => $this->input->ip_address(),
								'last_modified_date' => date('Y-m-d h:i:s'),
								'flag' => '1'
								);

			$returned_group_id = $this->save_data_model->save_company_details($group_array);
			$this->session->set_flashdata('msg', "Company Added Successfully");
				redirect(base_url().'company','refresh');
		}
	}

	function add_contact(){
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url()."dashboard/", 'Company' => 'Company'));

		$data = array();
		$data['is_company_added'] = $this->save_data_model->is_company_added('1');
		if($data['is_company_added']) {

			$this->_add_contact_rules();

			if ($this->form_validation->run() == FALSE) {
				
				$this->layouts->view('company/index', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);

			} else {
				$data = $this->input->post();
				$group_array = array('cmpny_name' => $data['company_name'],
									'cmpny_mobile' => $data['company_mobile'],
									'cmpny_alt_mobile' => $data['company_alternative_mobile'],
									'cmpny_email' => $data['company_email'],
									'cmpny_address' => $data['cmpny_address'],
									'is_contact_person_owner' => $data['is_contact_person_owner'],
									'owner_id' => '1',//$data['user_id'],
									'contact_person_name' => $data['contact_person_name'],
									'contact_person_mobile' => $data['contact_person_mobile'],
									'created_by' => '1',//$data['responsible_person'],
									'created_date' => date('Y-m-d h:i:s'),
									'created_ip' => $this->input->ip_address(),
									'last_modified_by' => '1',
									'last_modified_ip' => $this->input->ip_address(),
									'last_modified_date' => date('Y-m-d h:i:s'),
									'flag' => '1'
									);

				$returned_group_id = $this->save_data_model->save_company_details($group_array);
				$this->session->set_flashdata('msg', "New Add Successfully");
				redirect(base_url().'company','refresh');
			}

		} else {
			$this->session->set_flashdata('msg', "Please add Company details first");
			redirect(base_url().'company','refresh');
		}
	}

	function _add_company_rules(){
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		$this->form_validation->set_rules('company_mobile', 'Company Mobile', 'trim|required');
		$this->form_validation->set_rules('company_email', 'Company Email', 'trim|email|required');
		$this->form_validation->set_rules('company_address', 'Company Address', 'trim|required');
	}

	function _add_contact_rules(){
		$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required');
		$this->form_validation->set_rules('owner_mobile', 'Owner Mobile', 'trim|required');
		$this->form_validation->set_rules('contact_person_name', 'Contact Person Name', 'trim|required');
		$this->form_validation->set_rules('contact_person_mobile', 'Contact Person Mobile', 'trim|required');
	}
}