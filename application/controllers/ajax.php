<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends CI_Controller {
	
	public function __construct()
	{
    parent::__construct();
	//libraries and helpers are loaded in autoload.php
    $this->load->model('get_data_model');
	$this->load->model('save_update_model');

	}

	function index() {
		redirect(base_url(),'refresh');
	}
	//New classes starts here.

    function otpDetail() {
        if(1 || $this->input->is_ajax_request()) {
            $mobileNumber = $this->input->post('mobileNumber');
            if(!empty($mobileNumber)) {
                $otp = mt_rand(100000, 999999);
                $otpDetailArray = array(
                    'otp' => $otp,
                    'mobile' => $mobileNumber,
                );
                $isOTPInserted = $this->save_update_model->save_update_otp_details($otpDetailArray, $mobileNumber);

                if($isOTPInserted === true) { // send otp message only if mobile number not registered with us.
                    echo json_encode(
                        array(
                            'status' => 2,
                        )
                    );
                }
                else if($isOTPInserted === false) {
                    echo json_encode(
                        array(
                            'status' => 3,
                        )
                    );
                }
                else { //inserted.
                    $message = "Use OTP ". $otp . " To register to LIMPO. Thank you";
//                    $isMessageSent = sendOTP($otp, $message);
                    echo json_encode(
                        array(
                            'status' => 1,
                        )
                    );
                }
            }
            else {
                echo json_encode(
                    array(
                        'status' => 0,
                    )
                );
            }
        }
        else {
            redirect(base_url(),"refresh");
        }
    }

    function get_coupon_code_validation(){
        if(1 || $this->input->is_ajax_request()){
            $couponCode= $this->input->get('coupon_code');
            $returned_array = array();
            if(!empty($couponCode))
            {
                $returned_array = $this->get_data_model->getOne("discounts",$couponCode, 'discount_short_code');
                if(!empty($returned_array)) {
                    $returned_array = array(
                        'status' => 1,
                        'percentage' =>$returned_array['discount_percentage'],
                        'id' => $returned_array['id'],
                    );
                }
            }
            echo json_encode($returned_array);
        }
        else {
            redirect(base_url(),"refresh");
        }
    }

	function get_vehicle_models() {
		// echo json_encode($this->input->post());
		if($this->input->is_ajax_request()){
			$type = $this->input->post('vehicle_type');
	      	$returned_array = $this->get_data_model->getallModelTypes("model_name",$type);
	      	echo json_encode($returned_array);
 		}
 		else {
 			redirect(base_url(),"refresh");
 		}
	}
}