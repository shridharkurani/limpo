<?php

/*
	@desc - User Registration.
	Author:sjkurani
	Date: 27-04-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	

class Trips extends CI_Controller {

	private $cmpnyId;
	private $userId;
	
	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_update_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
    	$this->userId = $this->session->userdata('user_id'); 
	}
	
	function index() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Trips");
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Trips" => base_url().'trips/',"List" => base_url().'Trips/'));
		$data = array();
		$cmpnyId = $this->session->userdata("cmpnyId");
		$data['tripArray'] = $this->get_data_model->getTripDetails($cmpnyId);
		if(is_array($data['tripArray']) && !empty($data['tripArray'])) {
			$this->layouts->view('trips/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else {
			$this->layouts->view('trips/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		// meDebug($data,true);
		/*Fix me */
	}
}