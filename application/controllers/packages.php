<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class packages extends CI_Controller {

    private $cmpnyId;
    private $userId;

    public function __construct()
    {
        parent::__construct();
        LoadCssAndJs($this->layouts);
        $this->load->model('get_data_model');
        $this->load->model('save_update_model');
        $this->load->model('package_model');
        $this->load->model('service_model');
        $this->cmpnyId = $this->session->userdata('cmpnyId');
        $this->userId = $this->session->userdata('user_id');
    }


    public function book($packageId)
    {
        is_authenticated_user(array('user'));

        $packageDetails = $this->get_data_model->getOne('packages',$packageId);
        $dataUser = $this->session->all_userdata();

        if (!empty($dataUser)) {
            $api = new Api(RAZOR_KEY, RAZOR_SECRET_KEY);

            if(!empty($returnVal) && !empty($dataUser)){
                /**
                 * You can calculate payment amount as per your logic
                 * Always set the amount from backend for security reasons
                 */
                $_SESSION['name'] = $dataUser['user_fullname'];
                $_SESSION['email'] = $dataUser['user_email'];
                $_SESSION['phone'] = $dataUser['user_mobile'];
                $_SESSION['package_name'] = $packageDetails['pkg_name'];
                $_SESSION['package_id'] = $packageDetails['id'];
                $_SESSION['payable_amount'] = $packageDetails['pkg_price'];

                $razorpayOrder = $api->order->create(array(
                    'receipt'         => rand(),
                    'amount'          => $_SESSION['payable_amount'] * 100, // 2000 rupees in paise
                    'currency'        => 'INR',
                    'payment_capture' => 1 // auto capture
                ));


                $amount = $razorpayOrder['amount'];

                $razorpayOrderId = $razorpayOrder['id'];

                $_SESSION['razorpay_order_id'] = $razorpayOrderId;

                $data = $this->prepareData($amount,$razorpayOrderId);

                $data['data'] = $data;
                $this->layouts->view('payments/rezorpay_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, false);

//                $this->load->view('payments/rezorpay_view',array('data' => $data));
            }
        }

    }

    function book1($packageId) {
        is_authenticated_user(array('user'));
        if($this->userId) {
            echo $this->userId;
            $packageDetails = $this->get_data_model->getOne('packages',$packageId);
            meDebug($packageDetails,1 );



        }
        else {
            $this->session->set_flashdata('errormsg', lang('Please login to book the packages','ucword'));
            redirect(base_url());
        }
    }

    function index() {
        is_authenticated_user(array('admin'));
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Existing packages");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","packages" => 'packages'));
        $data = array();
        $data['packages'] = $this->get_data_model->getAllpackages('name');

//        meDebug($data,1);
        $this->layouts->view('packages/list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function user() {
        is_authenticated_user(array('user'));
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Your Avilable Services ");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Profile" => base_url()."profile/","packages" => 'packages'));
        $data = array();
//        $data['remainingServiceCnt'] = 12;
        $data['packages'] = $this->get_data_model->getUserpackages($this->userId);

//        meDebug($data,1);
        $this->layouts->view('userPackages/list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function actions($id,$flag) {
        $result = $this->save_update_model->updateFlag($id,'id', $flag, 'packages');
        if($result) {
            $this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
            redirect(base_url().'packages');
        }
        else {
            $this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
            redirect(base_url().'packages');
        }
    }

    function request($userid, $packageId, $serviceId) {
        if($userid == $this->userId) {
//            meDebug(array($userId, $packageId, $flag),1);
            $userArray = array(
                'user_id' => $this->userId,
                'package_id' => $packageId,
                'service_id' => $serviceId,
            );
            $result = $this->save_update_model->insert_update_service_request_details($userArray);
            if($result) {
                $this->session->set_flashdata('msg', lang('Request has been placed, Check your requests tab for status.','ucword'));
                redirect(base_url().'enquiries/user');
            }
            else {
                $this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
                redirect(base_url().'packages/user');
            }
        } else {
            $this->session->set_flashdata('errormsg', lang('Not allowed to perform that action.','ucword'));
            redirect(base_url().'packages/user');
        }
    }

    function add_edit($packageId = 0) {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Add New Package");
        $addOrEdit = ($packageId) ? "Edit" : "Add" ;

        $this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Packages" => base_url()."packages/",$addOrEdit => 'Package'));
        $data = array();

        $data['services'] = $this->service_model->getAllServices();
        $data['servicesIdsArray'] = array();

        if($packageId) {
            $data['packageId'] = $packageId;
            $data['packageDetails'] = $this->get_data_model->getOne('packages', $packageId, 'id');
            $data['servicesIdsArray'] = (!empty($this->get_data_model->getPackageServices($packageId, 'id'))) ? $this->get_data_model->getPackageServices($packageId, 'id') : array();


            $this->layouts->set_page_title("Edit Package Details");
        }
        else {
            $this->layouts->set_page_title("Add Package Details");
        }
        $this->_add_edit_rules();
        if ($this->form_validation->run() == FALSE)
        {
            $this->layouts->view('packages/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
        else
        {
            $packageServicesArray = $this->input->post('package_services');
            $package_image = '';
            if(isset($this->filedata['file_name'])) {
                $package_image = $this->filedata['file_name'];
            }
            $packageArray = array(
                'pkg_name' => $this->input->post('package_name'),
                'pkg_price' => $this->input->post('package_price'),
                'pkg_image' => $package_image,
                'availble_counts' => $this->input->post('package_count'),
                'pkg_desc' => $this->input->post('package_desc'),
            );

            if(!$packageId) {
                //insert
                $insertDetails = array(
                    'created_date'				=> date('Y-m-d h:i:s'),
                );
                $packageArray = array_merge($packageArray,$insertDetails);
            }
//            meDebug($packageArray, 1);

            $returned_val = $this->save_update_model->insert_update_package_details($packageArray, $packageId);

            if($returned_val['id'] != 0) {
                $this->session->set_flashdata('msg', $returned_val['msg']);
                if(!empty($packageServicesArray)) {
                    $this->save_update_model->insert_update_package_service_details($packageServicesArray, $returned_val['id']);
                }
            }
            else {
                $this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
            }
            redirect(base_url().'packages');
        }
    }


    function all() {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Existing packages");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","packages" => 'packages'));
        $data = array();
        $data['packages'] = $this->get_data_model->getAllpackages('name');

        $this->layouts->view('packages/grid_view', array('navbar' => 'layouts/navbar'),$data,TRUE, true);
    }


    function view($packageId = 0) {
        if(!empty($packageId)) {
            $data = array();
            $data['packageId'] = $packageId;

            $packageDetails = $this->get_data_model->getPackageServices($packageId,'grid');
            if(!empty($packageDetails)) {
                $this->layouts->set_title(SITE_NAME);
                $this->layouts->set_description(DEFAULT_SITE_DESC);
                $this->layouts->set_page_title($packageDetails[0]['pkg_name']);
                $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","packages" => 'packages'));
                $data['package'] = $packageDetails[0];
                $data['paybleAmount'] = $data['package']['pkg_price'];
                $data['packageServices'] = $packageDetails;
                $data['packageComments'] = $this->get_data_model->getpackageComments($packageId);

//                meDebug($data['packageComments'],1 );
                $enteredCouponCode = ($this->input->post('coupon_code')) ? $this->input->post('coupon_code') : '';

                if(!empty($enteredCouponCode)) {
                    $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'trim|xss_clean|callback__verify_coupon_code');
                }
                else {
                    $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'trim|xss_clean');
                }
                if ($this->form_validation->run() == FALSE)
                {
                    $this->layouts->view('packages/view_view', array('navbar' => 'layouts/navbar'),$data,TRUE, TRUE);
                }
                else {
//                    meDebug($this->input->post(),1);
                    if(!empty($this->input->post('apply_coupon_code'))) {
                        $couponCode = $this->input->post('coupon_code');
                        $couponCodeDetails = $this->get_data_model->getOne("discounts",$couponCode, 'discount_short_code');
                        if(!empty($couponCodeDetails)) {
                            $data['packageId'] = $packageId;
                            $data['couponCodeId'] = $couponCodeDetails['id'];

                            $data['paybleAmount'] = $data['package']['pkg_price'] - (($data['package']['pkg_price'] * $couponCodeDetails['discount_percentage']) / 100 );
                            $this->layouts->view('packages/view_view', array('navbar' => 'layouts/navbar'),$data,TRUE, TRUE);
                        }
                    }
                    else if(!empty($this->input->post('pay')) && !empty($this->userId)) {
                        $this->session->set_flashdata('packageId', $this->input->post('packageId'));
                        $this->session->set_flashdata('couponCodeId', $this->input->post('couponCodeId'));
                        $this->session->set_flashdata('couponCode', $this->input->post('coupon_code'));
                        redirect(base_url().'payments/pay');
                    }
                    else if($this->userId && !empty($this->input->post('addComment')) && !empty($this->input->post('user_comment'))){
                        // save comment

                        $dataArray = array(
                            'package_id' => $packageId,
                            'comment' => $this->input->post('user_comment'),
                            'user_id' => $this->userId,
                        );
                        $this->save_update_model->save_comment_details($dataArray);
                        $this->session->set_flashdata('msg', "Comment added successfully.");
                        redirect(base_url().'packages/view/'.$packageId);
                    }
                    else if(empty($this->userId)){
                        $this->session->set_flashdata('errormsg', "You need to login to perform this action.");
                        redirect(base_url().'account/signin/');
                    }
                    else {
                        $this->session->set_flashdata('errormsg', "Not able to save / update details.");
                        redirect(base_url().'packages/view/'.$packageId);
                    }
                }
            }
            else {
                redirect(base_url());
            }
        }
        else {
            redirect(base_url());
        }
    }

    function _add_edit_rules() {
        $this->form_validation->set_rules('package_name', 'Package Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('package_count', 'Package Available Counts', 'required|integer|trim|xss_clean');
        $this->form_validation->set_rules('package_price', 'Package Price', 'required|integer|trim|xss_clean');
        $this->form_validation->set_rules('package_services', 'Select Services',  'required');
        $this->form_validation->set_rules('package_desc', 'Description', 'required|trim|xss_clean');

        if(!empty($_FILES['cmpny_logo']['name'])) {
            $logo_name = $_FILES['cmpny_logo']['name'];
        }
        if($this->input->post('cmpny_logo')) {
            $logo_name = $this->input->post('cmpny_logo');
        }

        if(isset($logo_name)) {
            $this->form_validation->set_rules('cmpny_logo', 'Company Logo', 'callback__verify_uploading_file_and_upload');
        }
    }

    function _verify_coupon_code() {
        $couponCode = $this->input->post('coupon_code');
        $couponCodeDetails = $this->get_data_model->getOne("discounts",$couponCode, 'discount_short_code');
//        meDebug($couponCodeDetails,1);
        if(!empty($couponCodeDetails)) {
            return true;
        }
        else {
            $this->form_validation->set_message('_verify_coupon_code', "Invalid coupon code");
            return false;
        }
    }


    function _verify_uploading_file_and_upload() {
        $config['upload_path'] = './assets/uploads/packages';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '10000';
        $config['max_width']  = '10240';
        $config['max_height']  = '7680';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('cmpny_logo'))
        {
            $this->form_validation->set_message('_verify_uploading_file_and_upload', $this->upload->display_errors());
            return false;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data('cmpny_logo'));
            $this->filedata = $data['upload_data'];
            $config['image_library'] = 'gd2';

            //Create thumbnail with resize.
            $config['source_image']	= './assets/uploads/packages/'.$data['upload_data']['file_name'];
            $config['new_image']	= './assets/uploads/packages/thumb/'.$data['upload_data']['file_name'];

            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']	= 400;
            $config['height']	= 180;

            $this->image_lib->initialize($config);

            $this->image_lib->resize();

            return true;
        }
    }
}