<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."libraries/razorpay/razorpay-php/Razorpay.php");

use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

class Payments extends CI_Controller {
    /**
     * This function loads the registration form
     */
    function __construct()
    {
        parent::__construct();
        LoadCssAndJs($this->layouts);
        $this->load->model('get_data_model');
        $this->load->model('save_update_model');
        /*
        $this->load->model('signup_model');
        $this->load->model('login_model');
        $this->load->model('common_model');*/
    }

    /**
     * This function creates order and loads the payment methods
     */
    public function pay()
    {
        is_authenticated_user(array('user'));

        $packageId = $this->session->flashdata('packageId');
        $couponCodeId = (!empty($this->session->flashdata('couponCodeId'))) ? $this->session->flashdata('couponCodeId') : 0;
        $couponCode = $this->session->flashdata('couponCode');
        $dataUser = $this->session->all_userdata();
        if (!empty($packageId) && !empty($dataUser)) {
            $api = new Api(RAZOR_KEY, RAZOR_SECRET_KEY);
            $packageDetails = $this->get_data_model->getOne('packages',$packageId);
            $paybleAmount = $packageDetails['pkg_price'];

            if(!empty($couponCodeId)) {
                $couponCodeDetails = $this->get_data_model->getOne("discounts",$couponCodeId);
                $paybleAmount = $packageDetails['pkg_price'] - (($packageDetails['pkg_price'] * $couponCodeDetails['discount_percentage']) / 100 );
            }

            if(!empty($packageDetails)){
                /**
                 * You can calculate payment amount as per your logic
                 * Always set the amount from backend for security reasons
                 */

                $userId = $this->session->userdata('user_id');

                $razorpayOrder = $api->order->create(array(
                    'receipt'         => rand(),
                    'amount'          => $paybleAmount * 100, // 2000 rupees in paise
                    'currency'        => 'INR',
                    'payment_capture' => 1 // auto capture
                ));


                $amount = $razorpayOrder['amount'];

                $razorpayOrderId = $razorpayOrder['id'];

                $sessiondata = array(
                    'name' =>  $dataUser['user_fullname'],
                    'email' =>  $dataUser['user_email'],
                    'phone' =>  $dataUser['user_mobile'],
                    'user_id' =>  $userId,
                    'package_name' =>  $packageDetails['pkg_name'],
                    'package_id' =>  $packageDetails['id'],
                    'coupon_code_id' =>  $couponCodeId,
                    'amount' =>  $paybleAmount,
                    'razorpay_order_id' => $razorpayOrderId,
                );
                $this->session->set_userdata($sessiondata);

                $data = $this->prepareData($amount,$razorpayOrderId);

                $data['data'] = $data;
                $this->layouts->view('payments/rezorpay_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, false);
            }
            else {
                $this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
                redirect(base_url().'packages/view/'.$packageId);
            }
        }
        else {
            $this->session->set_flashdata('errormsg', "Something went wrong please try later.");
            redirect(base_url().'packages/view/'.$packageId);
        }
    }

    /**
     * This function verifies the payment,after successful payment
     */
    public function verify()
    {
        $sessionData = $this->session->all_userdata();
        $success = true;
        $error = "payment_failed";
        if (empty($_POST['razorpay_payment_id']) === false) {
            $api = new Api(RAZOR_KEY, RAZOR_SECRET_KEY);
            try {
                $attributes = array(
                    'razorpay_order_id' => $this->session->userdata('razorpay_order_id'),
                    'razorpay_payment_id' => $_POST['razorpay_payment_id'],
                    'razorpay_signature' => $_POST['razorpay_signature']
                );
                $api->utility->verifyPaymentSignature($attributes);
            } catch(SignatureVerificationError $e) {
                $success = false;
                $error = 'Razorpay_Error : ' . $e->getMessage();
            }
        }
        if ($success === true) {
            /**
             * Call this function from where ever you want
             * to save save data before of after the payment
             */
            //Save payment details to data.

            $dataArray = array(
                'razorpay_order_id' => $sessionData['razorpay_order_id'],
                'razorpay_payment_id' => $_POST['razorpay_payment_id'],
                'razorpay_signature' => $_POST['razorpay_signature'],
                'user_id'=>$sessionData['user_id'],
                'package_id'=>$sessionData['package_id'],
                'coupon_code_id'=>$sessionData['coupon_code_id'],
                'created_date'=>date("Y-m-d h:i:s"),
                'amount'=> $sessionData['amount'],
                'flag'=> 1
            );
            $this->save_update_model->save_payment_details($dataArray);

            redirect(base_url().'payments/success');
        }
        else {
            redirect(base_url().'payments/paymentFailed');
        }
    }

    /**
     * This function preprares payment parameters
     * @param $amount
     * @param $razorpayOrderId
     * @return array
     */
    public function prepareData($amount,$razorpayOrderId)
    {
        $_SESSION = $this->session->all_userdata();
        $data = array(
            "key" => RAZOR_KEY,
            "amount" => $amount,
            "name" => 'LIMPO',
            "description" => $_SESSION['package_name'],
            "image" => asset_url()."imgs/logo.png",
            "prefill" => array(
                "name"  => $_SESSION['name'],
                "email"  => $_SESSION['email'],
                "contact" => $_SESSION['phone'],
            ),
            "notes"  => array(
                "address"  => $_SESSION['name'],
                "merchant_order_id" => rand(),
            ),
            "theme"  => array(
                "color"  => "#2D3E50"
            ),
            "order_id" => $razorpayOrderId,
        );
        return $data;
    }

    /**
     * This function saves your form data to session,
     * After successfull payment you can save it to database
     */
    public function setRegistrationData()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $contact = $this->input->post('contact');
        $amount = $_SESSION['payable_amount'];

        $registrationData = array(
            'order_id' => $_SESSION['razorpay_order_id'],
            'name' => $name,
            'email' => $email,
            'contact' => $contact,
            'amount' => $amount,
        );
        // save this to database

    }

    /**
     * This is a function called when payment successfull,
     * and shows the success message
     */
    public function success()
    {
        $sessionData = $this->session->all_userdata();
        $packageId = $sessionData['package_id'];

        $packageDetails = $this->get_data_model->getOne('packages',$packageId);
        $userArray = array(
            'user_id' => $sessionData['user_id'],
            'package_id' => $packageId,
            'service_count' => $packageDetails['availble_counts'],
            'remaining_count' => $packageDetails['availble_counts'],
        );

        $this->save_update_model->save_user_packages($userArray);
        $this->session->set_flashdata('msg', "Payment successful.");
        redirect(base_url().'packages/user');
    }
    /**
     * This is a function called when payment failed,
     * and shows the error message
     */
    public function paymentFailed()
    {
        $packageId = $this->session->userdata('packageId');
        $this->session->set_flashdata('errormsg', "Payment Failed, Please try again or contact us.");
        redirect(base_url().'packages/view/'.$packageId);
    }


}
