<?php

/*
	@desc - User Registration.
	Author:sjkurani
	Date: 27-04-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles extends CI_Controller {
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('save_update_model');
	    $this->load->model('get_data_model');
    	LoadCssAndJs($this->layouts);
	}

	function p($id = 0) {
    	is_authenticated_user(array('admin','owner'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Company Profile");
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Company Profile" => 'Company Profile'));
		$data = array();
//		meDebug($id,1);
		$data['cmpnyDetails'] = $this->get_data_model->getCmpnyDetails($this->cmpnyId);
		$this->layouts->view('company/overview_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}
}
