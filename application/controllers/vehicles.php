<?php

/*
	@desc - User Registration.
	Author:Sjkurani.
	Date: 01-05-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vehicles extends CI_Controller {

	private $cmpnyId;
	private $uploadedImgsArray;
	private $userId;

	public function __construct()
	{
	    parent::__construct();
    	LoadCssAndJs($this->layouts);
    	$this->load->model('get_data_model');
    	$this->load->model('save_update_model');
    	$this->cmpnyId = $this->session->userdata('cmpnyId');
			// meDebug($this->session->all_userdata(),1);
    	$this->userId = $this->session->userdata('user_id');    	
	}

	function index() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Vehicles List");
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Vehicles" => base_url()."vehicles/","List" => 'Vehicles'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		$data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId);
		// meDebug($data['vehicleDetails'],true);
		$this->layouts->view('vehicles/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function _add_edit_rules() {
		$this->form_validation->set_rules('vehicle_type', 'Vehicle Type', 'trim|required|callback__verify_dropdown['.$this->input->post('vehicle_type').']');
		$this->form_validation->set_rules('vehicle_model', 'Vehicle Model', 'required|trim|callback__verify_dropdown['.$this->input->post('vehicle_model').']');		
		$this->form_validation->set_rules('vehicle_number', 'Vehicle Number', 'required|trim|xss_clean');
		// meDebug($_FILES);
		if(!empty($_FILES['userfile']['name'][0])) {
			// echo "string";
			$vehicle_image = $_FILES['userfile']['name'];
		}
		if($this->input->post('userfile')) {
			// echo "string1";
			$vehicle_image = $this->input->post('userfile');
		}
		if(isset($vehicle_image) && !empty($vehicle_image)) {
		// meDebug($vehicle_image);

			$this->form_validation->set_rules('userfile', 'Vehicle Model Image', 'callback__verify_uploading_vehicle_image');
		}
	}

	function add_edit($vehicleId = 0) {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Add New Vehicle");
		$addOrEdit = ($vehicleId) ? "Edit" : "Add" ;
		
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Vehicles" => base_url()."vehicles/",$addOrEdit => 'Vehicles'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['facilityArray'] = $this->get_data_model->getAllFacilities(1);
		
		if($this->input->post('vehicle_type')) {
			$typeName = $this->input->post('vehicle_type');
			$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);
		}
		if($vehicleId) {
			$data['vehicleId'] = $vehicleId;
			$data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId,$vehicleId);
			// meDebug($data['vehicleDetails'],1);

			$data['imgsList'] = $this->get_data_model->getvehicleImages(1,$vehicleId);
			// meDebug($data['vehicleDetails'],true);

			if(!empty($data['vehicleDetails']) && is_array($data['vehicleDetails']) && !empty($data['vehicleDetails']['facilities']) && is_array($data['vehicleDetails']['facilities'])) {
				$faciltiesIdsArray = array();
				foreach ($data['vehicleDetails']['facilities'] as $key => $value) {
					array_push($faciltiesIdsArray, $value['id']);
				}
				$data['faciltiesIdsArray'] = $faciltiesIdsArray;
			}
			if(!empty($data['vehicleDetails'])) {
			$typeName = $data['vehicleDetails']['model_type'];
			$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);			
			}
			$this->layouts->set_page_title("Edit Vehicle Details");

		}
		else {
			$this->layouts->set_page_title("Add Vehicle Details");

		}

		$this->_add_edit_rules();
		if ($this->form_validation->run() == FALSE)
		{
			$this->layouts->view('vehicles/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else
		{
			// meDebug($this->input->post());
			$vehicle_facilities = $this->input->post('facility');
			$vehicleArray = array(
							'cmpny_id' => $this->cmpnyId,
							'vehicle_company_id' => $this->cmpnyId,
							'vehicle_model_id' => $this->input->post('vehicle_model'),
							'vehicle_number' => $this->input->post('vehicle_number'),
							// 'vehicle_facilities' => $vehicle_facilities,
							'last_modified_by'			=> $this->userId,
							'last_modified_ip'			=> $this->input->ip_address(),
						);
			if(!$vehicleId) {				
    			//insert
	    		$insertDetails = array(
					'created_by'				=> $this->userId,
					'created_date'				=> date('Y-m-d h:i:s'),
					'created_ip'				=> $this->input->ip_address(),
	    		);
	    		$vehicleArray = array_merge($vehicleArray,$insertDetails);
			}
			$returned_val = $this->save_update_model->insert_update_vehicle_details($vehicleArray,$this->uploadedImgsArray, $vehicleId);

			if($returned_val['id'] != 0) {
				$this->session->set_flashdata('msg', $returned_val['msg']);
				$returned_val = $this->save_update_model->insert_update_vehicle_facility_details($vehicle_facilities, $returned_val['id']);
			}
			else {
				$this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
			}
			redirect(base_url().'vehicles');
		}
	}

	function lists() {
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title("Existing Vehicles");
		$this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Price Settings" => 'Price Settings'));
		$data = array();
		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		$data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId);
		$this->layouts->view('vehicles/index_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function view($vehicleId) {
		if($vehicleId) {
			$this->layouts->set_title(SITE_NAME);
			$this->layouts->set_description(DEFAULT_SITE_DESC);

			$this->layouts->set_page_title("Vehicle Details");
			$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Vehicles" => base_url()."vehicles/","View" => 'Vehicles'));
			$data = array();
			$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
			$data['facilityArray'] = $this->get_data_model->getAllFacilities(1);

			$data['vehicleId'] = $vehicleId;
			$data['vehicleDetails'] = $this->get_data_model->getVehicleDetails($this->cmpnyId,$vehicleId);

			if(!empty($data['vehicleDetails']) && is_array($data['vehicleDetails']) && !empty($data['vehicleDetails']['facilities']) && is_array($data['vehicleDetails']['facilities'])) {
				$faciltiesIdsArray = array();
				foreach ($data['vehicleDetails']['facilities'] as $key => $value) {
					array_push($faciltiesIdsArray, $value['id']);
				}
				$data['faciltiesIdsArray'] = $faciltiesIdsArray;
			}
			if(!empty($data['vehicleDetails'])) {
				$typeName = '';//$data['vehicleDetails']['model_type'];
				$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name',$typeName);
				$data['imgsList'] = $this->get_data_model->getvehicleImages(1,$vehicleId);
			}
			// meDebug($data,true);

			$this->layouts->view('vehicles/view_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);			
		}
		else {

		}
	}

	function actions($vehicle_id,$flag) {
		$result = $this->save_update_model->updateVehicleFlag($vehicle_id,$flag);
		if($result) {
			$this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
			redirect(base_url().'vehicles');
		}
		else {
			$this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
			redirect(base_url().'vehicles');
		}
	}

	function filter() {
    	// is_authenticated_user(array('super_admin'));

		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);

		$this->layouts->set_page_title("Filtered Vehicles List");
		$this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Vehicles" => base_url()."vehicles/","Filtered List" => 'Vehicles'));
		$data = array();
		$posted_data = $this->input->post();
		$filterArray = array();
		$likeArray = array();
		if($this->cmpnyId) {
				$filterArray['vehicle_company_id'] = $this->cmpnyId;
		}
		if(isset($posted_data['vehicle_type']) && !empty($posted_data['vehicle_type'])) {
			$filterArray['vmm.model_type'] = $posted_data['vehicle_type'];
		}
		
		if(isset($posted_data['vehicle_model']) && !empty($posted_data['vehicle_model'])) {
			$filterArray['vehicle_model_id'] = $posted_data['vehicle_model'];
		}
		if(isset($posted_data['vehicle_number']) && !empty($posted_data['vehicle_number'])) {
			$likeArray['vehicle_number'] = $posted_data['vehicle_number'];
			$data['postArray']['vehicle_number'] = $likeArray['vehicle_number'] ;
		}
		if(isset($posted_data['status']) && !empty($posted_data['status'])) {
			$filterArray['v.flag'] = $posted_data['status'];
			$filterArray['status'] = $posted_data['status'];
		}
		$data['postArray']['filters'] = $filterArray;

		$data['modelTypeDetails'] = $this->get_data_model->getallModelTypes('model_type');
		$data['modelNameDetails'] = $this->get_data_model->getallModelTypes('model_name');
		unset($filterArray['status']);
		$data['vehicleDetails'] = $this->get_data_model->filterVehicleDetails($filterArray,$likeArray);
		// meDebug($data,true);
		// meDebug($data['vehicleDetails'],true);
		$this->layouts->view('vehicles/filter_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function _verify_dropdown($field, $id) {
	    if (!$field) {
	         $this->form_validation->set_message('_verify_dropdown', 'Please select proper value for %s');
	         return FALSE;
	    }
	    else {
	    	return TRUE;
	    }
	}

	function _verify_uploading_vehicle_image() {
		$name_array = array();
        $count = count($_FILES['userfile']['size']);
        foreach($_FILES as $key=>$value)
        for($s=0; $s<=$count-1; $s++) {
	        $_FILES['userfile']['name']=$value['name'][$s];
	        $_FILES['userfile']['type']    = $value['type'][$s];
	        $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
	        $_FILES['userfile']['error']       = $value['error'][$s];
	        $_FILES['userfile']['size']    = $value['size'][$s];

			$config['upload_path'] = FCPATH.'assets/uploads/vehicles/';
	        $config['allowed_types'] = 'gif|jpg|png';
	        $config['max_size']    = '10000';
	        $config['max_width']  = '102400';
	        $config['max_height']  = '76800';
			$config['encrypt_name'] = TRUE;

	        $this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload()) {
				$this->form_validation->set_message('_verify_uploading_vehicle_image', $this->upload->display_errors());
	        	// meDebug($this->upload->display_errors(),1);
	        	return false;
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$this->uploadedImgsArray[] = $data['upload_data'];
				$config['image_library'] = 'gd2';
				//Create thumbnail with resize.
				$config['source_image']	= './assets/uploads/vehicles/'.$data['upload_data']['file_name'];
				$config['new_image']	= './assets/uploads/vehicles/thumb/'.$data['upload_data']['file_name'];
		
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = TRUE;
				$config['width']	= 352;
				$config['height']	= 150;

		
				$this->image_lib->initialize($config);
		
				$this->image_lib->resize();
				// meDebug($config,1);
		
				// return true;
	        }
	    }
	    return true;
	}
}

