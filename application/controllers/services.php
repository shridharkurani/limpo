<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Services extends CI_Controller {

    private $cmpnyId;
    private $userId;

    public function __construct()
    {
        parent::__construct();
        LoadCssAndJs($this->layouts);
        $this->load->model('get_data_model');
        $this->load->model('save_update_model');
        $this->load->model('service_model');
        $this->cmpnyId = $this->session->userdata('cmpnyId');
        $this->userId = $this->session->userdata('user_id');
    }

    function index() {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Existing Services");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Services" => 'Services'));
        $data = array();
        $data['services'] = $data = $this->get_data_model->getAllServices();
        $this->layouts->view('services/list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function all($pincode = 0) {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Existing Services");
        $this->layouts->set_breadcrumb_array(array("<i class='fa fa-dashboard'> </i>  Dashboard" => base_url()."dashboard/","Services" => 'Services'));
        $data = array();
        $data['searchData']['pincode'] = $pincode = !empty($this->input->post('pincode')) ? $this->input->post('pincode') : '';
        $data['services'] = $this->get_data_model->getAllServicesByPincode($pincode);

        $this->layouts->view('services/grid_view', array('navbar' => 'layouts/navbar'),$data,TRUE, true);

//        meDebug($data,1);
//        $this->layouts->view('packages/list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
    }

    function actions($id,$flag) {
        $result = $this->save_update_model->updateFlag($id,'ser_id', $flag, 'services');
        if($result) {
            $this->session->set_flashdata('msg', lang('Details Updated Successfully','ucword'));
            redirect(base_url().'services');
        }
        else {
            $this->session->set_flashdata('errormsg', lang('something went wrong, please contact admin.','ucword'));
            redirect(base_url().'services');
        }
    }

    function add_edit($serviceId = 0) {
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $this->layouts->set_page_title("Add New Service");
        $addOrEdit = ($serviceId) ? "Edit" : "Add" ;

        $this->layouts->set_breadcrumb_array(array("Dashboard" => base_url()."dashboard/","Services" => base_url()."services/",$addOrEdit => 'Service'));
        $data = array();

        if($serviceId) {
            $data['serviceId'] = $serviceId;
            $data['serviceDetails'] = $this->get_data_model->getOne('services', $serviceId, 'ser_id');
            $data['serviceDetails']['service_pin'] = (!empty($this->get_data_model->getPinCodes($serviceId))) ? implode(",", $this->get_data_model->getPinCodes($serviceId)): '';

            $this->layouts->set_page_title("Edit Service Details");
        }
        else {
            $this->layouts->set_page_title("Add Service Details");
        }
        $this->_add_edit_rules();
        if ($this->form_validation->run() == FALSE)
        {
            $this->layouts->view('services/add_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
        }
        else
        {
            $servicePincode = $this->input->post('service_pin');

            if(isset($this->filedata['file_name'])) {
                $ser_image = $this->filedata['file_name'];
            }
            else {
                $ser_image = $this->input->post('saved_cmpny_logo');
            }
            $serviceArray = array(
                'ser_name' => $this->input->post('service_name'),
                'ser_image' => $this->input->post('vehicle_number'),
                'ser_description' => $this->input->post('service_desc'),
                'ser_image' => $ser_image,
            );

            if(!$serviceId) {
                //insert
                $insertDetails = array(
                    'created_date'				=> date('Y-m-d h:i:s'),
                );
                $serviceArray = array_merge($serviceArray,$insertDetails);
            }

            $returned_val = $this->save_update_model->insert_update_service_details($serviceArray, $serviceId);

            if($returned_val['id'] != 0) {
                $this->session->set_flashdata('msg', $returned_val['msg']);
                if(!empty($servicePincode)) {
                    $this->save_update_model->insert_update_service_pincode_details(explode(",", $servicePincode), $returned_val['id']);
                }
            }
            else {
                $this->session->set_flashdata('errormsg', "something went wrong please contact admin.");
            }
            redirect(base_url().'services');
        }
    }

    function _add_edit_rules() {
        $this->form_validation->set_rules('service_name', 'Service Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('service_pin', 'Available Pin Codes',  'required|trim|xss_clean');
        $this->form_validation->set_rules('service_desc', 'Description', 'required|trim|xss_clean');

        if(!empty($_FILES['cmpny_logo']['name'])) {
            $logo_name = $_FILES['cmpny_logo']['name'];
        }
        if($this->input->post('cmpny_logo')) {
            $logo_name = $this->input->post('cmpny_logo');
        }

        if(isset($logo_name)) {
            $this->form_validation->set_rules('cmpny_logo', 'Company Logo', 'callback__verify_uploading_file_and_upload');
        }
    }

    function _verify_uploading_file_and_upload() {
        $config['upload_path'] = './assets/uploads/services';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '10000';
        $config['max_width']  = '10240';
        $config['max_height']  = '7680';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('cmpny_logo'))
        {
            $this->form_validation->set_message('_verify_uploading_file_and_upload', $this->upload->display_errors());
            return false;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data('cmpny_logo'));
            $this->filedata = $data['upload_data'];
            $config['image_library'] = 'gd2';

            //Create thumbnail with resize.
            $config['source_image']	= './assets/uploads/services/'.$data['upload_data']['file_name'];
            $config['new_image']	= './assets/uploads/services/thumb/'.$data['upload_data']['file_name'];

            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']	= 400;
            $config['height']	= 180;

            $this->image_lib->initialize($config);

            $this->image_lib->resize();

            return true;
        }
    }

}