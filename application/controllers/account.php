<?php

/*
	@desc - User Registration.
	Author:sjkurani
	Date: 27-04-2018
*/

/*
	@Guideline - How to write controller?
		Set title, layout, page title and breadcrumb array.
		Set validation rules.
		Fetch needed data for views from modules.
		If false show the existing view with form repopulated.
		If true save values to database and  redirect to other pages.
*/


// Fix me.
/*
	register - Completed.
	Overview - Completed.
	Lists for admin - Not Verified.
	List Filter - Not Started.
	Actions - Not verified.
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('save_update_model');
	    $this->load->model('get_data_model');
    	LoadCssAndJs($this->layouts);
	}

	function index() {
		$this->signin();
	}

	function _register_rules(){
		$this->form_validation->set_rules('user_fullname', 'Full Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('user_email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[user_account_details.user_email]');		
		$this->form_validation->set_rules('user_mobile', 'Mobile Number', 'trim|required|integer|exact_length[10]|is_unique[user_account_details.user_mobile]|xss_clean');
		$this->form_validation->set_rules('user_pass', 'Password', 'trim|required|min_length[6]|xss_clean');
//        $this->form_validation->set_rules('user_otp', 'OTP', 'trim|required|xss_clean|callback__verify_otp');

    }

    function _verify_otp(){
        $mobileNumber = $this->input->post('user_mobile');
        $otp = $this->input->post('user_otp');
        $otp_fields = $this->get_data_model->getOne('otp_details', $mobileNumber , 'mobile');
        if(!empty($otp_fields) && $otp == $otp_fields['otp']) {
            return true;
        }
        else {
            $this->form_validation->set_message('_verify_otp', "Not a valid OTP. Please insert valid OTP.");
            return false;
        }

    }

	function register($otp = true,$registerArray = array()) {
        $userType = $this->session->userdata('user_type') ? $this->session->userdata('user_type') : '';
        if($userType) {
            redirect(base_url().'dashboard');
        }
		$data = array();
		$this->layouts->set_title();
		$this->layouts->set_description();
		$this->_register_rules();/*
		if(!empty($this->input->post('user_otp'))) {
        }
		else {
            $this->form_validation->set_rules('user_otp', 'OTP', 'trim|xss_clean');
        }*/

		if ($this->form_validation->run() == FALSE)
		{

            $this->layouts->view('account/register_view', array('navbar' => 'layouts/navbar'),$data,TRUE, TRUE);
		}
		else {
            $registerArray = array(
                'user_fullname'		=> $this->input->post('user_fullname'),
                'level_id'			=> 2,
                'user_display_name'	=> $this->input->post('user_fullname'),
                'user_mobile'		=> $this->input->post('user_mobile'),
                'user_email'		=> $this->input->post('user_email'),
                'nickname' 			=> $this->input->post('user_pass'), //$this->encrypt->encode($this->input->post('user_pass')),
                'created_by'		=> 1,
                'created_date' 		=> date('Y-m-d h:i:s'),
                'created_ip'		=> $this->input->ip_address(),
                'last_modified_by'	=> 1,
                'last_modified_ip'	=> $this->input->ip_address(),
            );
            $this->save_update_model->save_register_details($registerArray);

            $data['msg'] = $this->lang->line('username');
            $this->session->set_flashdata('msg', lang('Registration Successful, Please login with your entered credentials','ucword'));
            redirect(base_url().'account/signin');

		}
	}

	function _signin_rules()  {

		$this->form_validation->set_rules('email_mobile_username', ' Email / Username', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback__verify_login');
		
	}

	function signin() {
//        meDebug($this->session->all_userdata(),true);
        $userId = $this->session->userdata('user_type') ? $this->session->userdata('user_type') : '';
        if($userId) {
         redirect(base_url().'dashboard');
        }
        $this->layouts->set_title();
        $this->layouts->set_description();
        $this->_signin_rules();
        $data = array();
        if ($this->form_validation->run() == FALSE)
        {
            $this->layouts->view('account/signin_view', array('navbar' => 'layouts/navbar'),$data,TRUE, TRUE);
        }
        else
        {
            $valid_fields = $this->get_data_model->get_valid_user($this->input->post('email_mobile_username'));
            $userId = $valid_fields['user_id'];
            $cmpny_id = $this->get_data_model->getOwnerCmpnyId($userId);
            $sessiondata = array(
                'user_id' => (int)$valid_fields['user_id'],
                'user_fullname' => $valid_fields['user_fullname'],
                'user_email' =>  $valid_fields['user_email'],
                'user_mobile' =>  $valid_fields['user_mobile'],
                'user_display_name' =>  $valid_fields['user_display_name'],
                'user_type' => $valid_fields['level_name'],
                'logged_in' => TRUE
            );
            if($cmpny_id != 0) {
                $sessiondata['cmpnyId'] = $cmpny_id;

                $cmpnyProfileDeatails = $this->get_data_model->getCmpnyProfileDetails($cmpny_id);
                // meDebug($cmpnyProfileDeatails,true);
                $cmpnyProfilePic = $cmpnyProfileDeatails['cmpny_logo'];
                if(!empty($cmpnyProfilePic)) {
                    $cmpnyProfilePicArr = explode(".", $cmpnyProfilePic);
                    $fullImgUrl = $cmpnyProfilePicArr[0]."_thumb.".$cmpnyProfilePicArr[1];

                }
                else {
                    $fullImgUrl = 0;
                }
                $sessiondata['session_cmpny_display_name'] = ucwords($cmpnyProfileDeatails['cmpny_name']);
                $sessiondata['session_profile_pic'] = $fullImgUrl;
            }
            else {
                $sessiondata['cmpnyId'] = 0;
            }
            $this->session->set_userdata($sessiondata);
            $data['msg'] = $this->lang->line('username');
            $this->session->set_flashdata('msg', lang('login successful. you logged in as','ucword')." ".$valid_fields['level_name']);
            if($valid_fields['level_name'] == 'admin') {
                redirect(base_url().'dashboard');
            }
            else {
                redirect(base_url().'packages/all');
            }
            }
	}

	function _verify_login() {
		$signin_username = $this->input->post('email_mobile_username');
		$signin_password = $this->input->post('password');
		
		$valid_fields = $this->get_data_model->get_valid_user($signin_username);
		
		if(is_array($valid_fields) && !empty($valid_fields)) {

			if(empty($valid_fields['flag']) && empty($valid_fields['nickname'])) {
				$this->form_validation->set_message('_verify_login', "you have signedup using social paltforms, you may signin using social platform or reset password by clicking forgot password for your existing account.");
				return false;
			}
			else if($valid_fields['flag'] == 1) {
				$password = $valid_fields['nickname']; //$this->encrypt->decode($valid_fields['nickname']);

				if($password == $signin_password) {
					return true;
				}
				else {
					$this->form_validation->set_message('_verify_login', "The entered email and password does not match");
					return false;
				}
			}
			else {
				$this->form_validation->set_message('_verify_login', "We have sent activation link to provided mail id please activate and login");
				return false;
			}
		}
		else {	
			$this->form_validation->set_message('_verify_login', "The enter email / mobile not registed with us. please signup if you are new customer.");
			return false;
		}
	}

	function logout() {
		$sessiondata = array(
					    'user_id' => '',
					    'user_fullname' => '',
					    'user_email' => '',
					    'user_mobile' =>  '',
					    'user_name' =>  '',
					    'user_type' =>  '',
					    'language_name' =>  '',
					    'logged_in' =>  '',
					    'cmpnyId' => '',
				    	);
		
	    $this->session->unset_userdata($sessiondata);
	  	redirect('/', 'refresh');
	}

	
	function _forgot_rules()  {
		$this->form_validation->set_rules('email_mobile_username', ' Email or Mobile', 'trim|required|xss_clean');
	}

	function forgot() {
		//is_authenticated_user(array('buyer'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		//$this->layouts->set_page_title('User Add Basic Details');
		//$this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(),'Dashboard' => base_url()."dashboard/".$this->session->userdata('user_type'), 'Add' => 'Add'));

		$data = array();

		$data['user_type'] = '';
		
		$this->_forgot_rules();
		
		$data['username'] = $this->input->post('email_mobile_username');

		if ($this->form_validation->run() == FALSE) {

			$this->layouts->view('account/forgot_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, FALSE);

		} else {
			$data['user_type'] = $this->get_data_model->get_valid_user($data['username']);

			if(empty($data['user_type']))
			{
				$this->session->set_flashdata('msg', 'Email or mobile not exists');
				redirect(base_url().'account/forgot');

			} else {
				$password = $data['user_type']['nickname'] ;//$this->encrypt->decode($data['user_type']['nickname']);

				$sub = 'Forgot password for Eaccural.';
				$text = "<div> YOur password is ".$password.". Please login with your email and password. Thank you.</div>";
				$result = $this->curl->simple_post('https://api.sendgrid.com/api/mail.send.json', array('api_user'=>'sjkurani', 'api_key'=>'unix1234','to'=>$data['username'],'fromname'=>'Developer @ Eaccural','subject'=> $sub, 'html'=> $text, 'from'=>'developer@orgbitor.com','cc'=>'sjkurani@gmail.com'));

				$this->session->set_flashdata('msg', 'We have sent your password to your registered mail id.');
				redirect(base_url().'account/signin');
			}
		}
	}

	function  _verify_drop_down($val){

		if($val == '0') {
			$this->form_validation->set_message('_verify_drop_down', 'This field is required');
			return false;
		}
		else {
			return true;
		}
	}

	function myaccount() {
		
		is_authenticated_user(array('super_admin'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title(lang('my account','ucword'));
		// $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(),'Dashboard' => base_url()."dashboard/".$this->session->userdata('user_type'), 'Add' => 'Add'));
		
		$this->_myaccount_rules();

		$data['user_details'] = $this->get_data_model->get_myaccount_value($this->session->userdata('user_id')); 

		if ($this->form_validation->run() == FALSE) {
			$this->layouts->view('user/myaccount_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		}
		else {
			$data = $this->input->post();

			$account_array = array(
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'city' => $data['city'],
				'zip' => $data['zip'],
				'street' => $data['street'],
				'house' => $data['house'],
				'language_name' => $data['language'],
				'address' => $data['address_first'],
				'address2' => $data['address_second']);

			$this->save_update_model->admin_add_or_update($account_array,$this->session->userdata('user_id'));

			$account_modified_array = array(
				'last_modified_by' => $this->session->userdata('user_id'),
				'last_modified_date' => date('Y-m-d'),
				'last_modified_ip' => $this->input->ip_address());

			$this->save_update_model->admin_add_update_modified($account_modified_array,$this->session->userdata('user_id'));


			$this->session->set_flashdata('msg', "Account Update Successfully");
			redirect(base_url().'user/myaccount','refresh');
		}
	}

	function _check_dropdown_rules(){
		if($this->input->post('action_type')==0) {
			$this->form_validation->set_message('_check_dropdown_rules', "Please Select Correct");
			return false;
		}
		else {
			return true;
		}
	}

	function action(){
		
		// is_authenticated_user(array('super_admin'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title('Existing User List');
		$this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(),'Dashboard' => base_url()."dashboard/".$this->session->userdata('user_type'), 'Add' => 'Add'));

		$data = array();

		$data['user_management_list'] = $this->get_data_model->get_user_lists();
		
		$this->form_validation->set_rules('action_type', 'User Type', 'trim|xss_clean|custom__check_dropdown_rules');

		if ($this->form_validation->run() == FALSE) {
		
			$this->layouts->view('user/user_list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
		} else {

			$data = $this->input->post();
			$check_action = array();
			$check_action = $this->input->post('action_type');

			if($check_action==1){

				$user_list = $this->input->post('usercheck');
				if(!empty($user_list) && is_array($user_list)) {
					foreach ($user_list as $key => $value) {
						$user_list_array[] = array('user_id' => $value);

						$this->get_data_model->user_delete_all($user_list_array);

						$this->session->set_flashdata('msg', "Delete Successfully");
						redirect(base_url().'user','refresh');
					}
				}
			}
			if($check_action==2){

				$user_list = $this->input->post('usercheck');
				if(!empty($user_list) && is_array($user_list)) {
					foreach ($user_list as $key => $value) {
						$user_list_array[] = array('user_id' => $value);

						$this->get_data_model->user_activate_all($user_list_array);

						$this->session->set_flashdata('msg', "Delete Successfully");
						redirect(base_url().'user','refresh');
					}
				}
			}
			if($check_action==3){

				$user_list = $this->input->post('usercheck');
				if(!empty($user_list) && is_array($user_list)) {
					foreach ($user_list as $key => $value) {
						$user_list_array[] = array('user_id' => $value,
													'flag' => 2);

						$this->get_data_model->user_deactivate_all($user_list_array);

						$this->session->set_flashdata('msg', "Delete Successfully");
						redirect(base_url().'user','refresh');
					}
				}
			}
		}
	}

	function _edit_rules(){
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');

		if($this->input->post('user_type')==0){
    		$this->form_validation->set_rules('user_type', 'User Type', 'trim|xss_clean|required');
    	}

    	$this->form_validation->set_rules('city', 'City', 'trim|required');

    	$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required|min_length[6]|xss_clean');

    	$this->form_validation->set_rules('street_name', 'Street', 'trim|required');

    	$this->form_validation->set_rules('house_name', 'House', 'trim|required');

    	$this->form_validation->set_rules('address_first', 'Address 1', 'trim|required');
		
		
	}

	function edit($user_id = 0) {
		
		is_authenticated_user(array('super_admin'));
		exit();
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title(lang('edit user','ucword'));
		// $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(),'Dashboard' => base_url()."dashboard/".$this->session->userdata('user_type'), 'Add' => 'Add'));


		$data = array();
		$data['user_id'] = $user_id;

		if($user_id == 0) {
			// fix me check whether id exist in database 
			redirect(base_url().'user','refresh');
		} 

		$data['user_details'] = $this->get_data_model->get_user_details($user_id);
		$data['user_levels'] = $this->get_data_model->get_user_levels();
		
		$this->_edit_rules();

		if ($this->form_validation->run() == FALSE) {

			$this->layouts->view('user/user_edit_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);

		} else {
				$data = $this->input->post();
				$user_array = array('first_name' => $data['first_name'],
								'last_name' => $data['last_name'],
								'street' => $data['street_name'],
								'house' => $data['house_name'],
								'address' => $data['address_first'],
								'address2' => $data['address_second'],
								'city' => $data['city'],
								'zip' => $data['zipcode'],
								'language_name' => $data['user_lang']);

				$returned_id = $this->get_data_model->update_user_details($user_array,$this->input->post('userid'));

				$user_account_array = array(
									'level_id' => $data['user_type'],
									'created_date' => $data['user_datetime'],
									'last_modified_by' => 1, //$data['user_id']
									'last_modified_ip' => $this->input->ip_address());

				$this->get_data_model->update_user_account_details($user_account_array,$this->input->post('userid'));

				$this->session->set_flashdata('msg', "Account Update Successfully");
				redirect(base_url().'user','refresh');
		}
	}


	function change_password($userid = 0){
		
		is_authenticated_user(array('super_admin'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title(lang('change password','ucword'));
		// $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(),'Dashboard' => base_url()."dashboard/".$this->session->userdata('user_type'), 'Add' => 'Add'));

		$data = array();

		if($userid == 0) {
			// fix me check whether id exist in database 
			redirect(base_url().'user','refresh');
		}

		$data['user_id'] = $userid;
		$this->_change_password_rules();
		
	
		if ($this->form_validation->run() == FALSE) {

			$this->layouts->view('user/user_change_password_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);

		} else {
			$newpassword = $this->encrypt->encode($this->input->post('new_password'));
			$userid = $this->input->post('hidden_user_id');

			$this->get_data_model->change_account_password($userid,$newpassword);

			$this->session->set_flashdata('msg', "Change Password Done Successfully");
			redirect(base_url().'user','refresh');
		}
	}

	function _change_password_rules(){
		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean|callback__verify_old_password');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|callback__verify_confirm_password');
	}

	function _verify_old_password(){
		$userid = $this->input->post('hidden_user_id');
		$oldpassword = $this->input->post('old_password');
		$valid_password = $this->get_data_model->get_old_password($userid);

		
		if(is_array($valid_password) && !empty($valid_password)) {
			if(empty($valid_password[0]->nickname)) {
				$this->form_validation->set_message('_verify_old_password', "you have signedup using social paltforms, you may signin using social platform or reset password by clicking forgot password for your existing account.");
				return false;
			}
			else if($valid_password[0]->flag == 1) {
				$password = $this->encrypt->decode($valid_password[0]->nickname);
				if($password == $oldpassword) {
					return true;
				} else {
					$this->form_validation->set_message('_verify_old_password',"The entered old password does not match");
					return false;
				}	
			}
		}
	}

	function _verify_confirm_password(){
		$new_password = $this->input->post('new_password');
		$confirm_password = $this->input->post('confirm_password');

		if($new_password == $confirm_password){
			return true;
		} else {
			$this->form_validation->set_message('_verify_confirm_password',"New password and confirm password does not match");
			return false;
		}
	}

	function search(){
		$data = array();
		$posted_data = $this->input->post();
		$data['user_management_list'] = $this->get_data_model->get_userlist_data($posted_data);
		// debug_me($this->db->last_query());
		// exit();
		$this->layouts->view('user/user_list_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,true);
	}


	function show($user_id = 0) {
		
		is_authenticated_user(array('super_admin'));
		$this->layouts->set_title(SITE_NAME);
		$this->layouts->set_description(DEFAULT_SITE_DESC);
		$this->layouts->set_page_title(lang('show user','ucword'));
		// $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(),'Dashboard' => base_url()."dashboard/".$this->session->userdata('user_type'), 'Add' => 'Add'));


		$data = array();
		$data['user_id'] = $user_id;

		if($user_id == 0) {
			// fix me check whether id exist in database 
			redirect(base_url().'user','refresh');
		} 

		$data['user_details'] = $this->get_data_model->get_user_details($user_id);
		$data['user_levels'] = $this->get_data_model->get_user_levels();

		$this->layouts->view('user/user_show_view', array('navbar' => 'layouts/navbar','sidebar' => 'layouts/left'),$data,TRUE, TRUE);
	}

	function home(){
	    redirect(base_url().'home');
    }
}