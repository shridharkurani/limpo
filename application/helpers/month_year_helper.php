<?php

function formMonth(){
    $month = strtotime (date('F Y', mktime(0, 0, 0, date('m')-2, 1, date('Y')))); 
    $end = strtotime (date('F Y', mktime(0, 0, 0, date('m')+4, 1, date('Y')))); 
    while($month < $end){
        $selected = (date('F Y', $month)==date('F Y'))? ' selected' :'';
        echo '<option'.$selected.' value="'.date('m-Y', $month).'">'.date('F Y', $month).'</option>'."\n";
        $month = strtotime("+1 month", $month);
    }
}

function formsearchMonth($frommonth = 0){
    $month = strtotime (date('F Y', mktime(0, 0, 0, date('m')-11, 1, date('Y'))));
    $end = strtotime (date('F Y', mktime(0, 0, 0, date('m')+1, 1, date('Y')))); 
    while($month < $end){
        if($frommonth != 0) {
				 $selected = ($frommonth == date('m-Y',$month))? ' selected' :'';}
		else{
				 $selected = (date('m-Y', $month)==date('m-Y'))? ' selected' :'';
		}
        echo '<option'.$selected.' value="'.date('m-Y', $month).'">'.date('F Y', $month).'</option>'."\n";
        $month = strtotime("+1 month", $month);
    }
}

function tosearchMonth($tomonth){
    $month = strtotime (date('F Y', mktime(0, 0, 0, date('m')-11, 1, date('Y'))));
    $end = strtotime (date('F Y', mktime(0, 0, 0, date('m')+1, 1, date('Y')))); 
    while($month < $end){
        if($tomonth != 0) {
				 $selected = ($tomonth == date('m-Y',$month))? ' selected' :'';}
		else{
				 $selected = (date('m-Y', $month)==date('m-Y'))? ' selected' :'';
		}
        echo '<option'.$selected.' value="'.date('m-Y', $month).'">'.date('F Y', $month).'</option>'."\n";
        $month = strtotime("+1 month", $month);
    }
}