<?php
/*
	List of global functions.
*/
function LoadCssAndJs($layoutsObj) {
	$layoutsObj->add_css('assets/css/plugins/bootstrap/css/bootstrap.min.css')
				->add_css('http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic',FALSE)
				->add_css('http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800',FALSE)
				->add_css('/assets/font-awesome/css/font-awesome.min.css')
				->add_css('assets/css/style.css')
				->add_css('assets/css/plugins.css')
				->add_css('assets/css/demo.css')

				->add_js('assets/js/jquery.min.js')
				->add_js('assets/js/plugins/bootstrap/bootstrap.min.js')
				->add_js('assets/js/plugins/slimscroll/jquery.slimscroll.min.js')
				->add_js('assets/js/plugins/hisrc/hisrc.js')
				->add_js('assets/js/flex.js');
}

function asset_url(){
   return base_url().'assets/';
}

function is_authenticated_user($user_types_array) {
	$CI = & get_instance();
	$session_user_type = $CI->session->userdata('user_type');
	if(!in_array($session_user_type, $user_types_array)) {
		$CI->session->set_flashdata('errormsg', "You are not authorised to access that page.");
		redirect(base_url().'dashboard/'.$session_user_type);
	}
}

function check_user_authenticity($user_id) {
	$CI = & get_instance();  //get instance, access the CI superobject
	return (($CI->session->userdata('logged_in') == 1) && ($CI->session->userdata('user_id') == $user_id));
}
function send_grid_mails($user_array,$sub,$text) {
	$CI = & get_instance();  //get instance, access the CI superobject
	return $CI->curl->simple_post('https://api.sendgrid.com/api/mail.send.json', array('api_user'=>'sjkurani', 'api_key'=>'unix1234','to'=>$user_array['email'],'fromname'=>'Book4Us','subject'=> $sub, 'html'=> $text, 'from'=>'info@book4us.in','cc'=>'sjkurani@gmail.com'));
}
function get_mail_content($href,$type) {
	if($type == 'registration') {

		$img_name = asset_url()."local_image/logos/logo-blue.png";
		$mail_string = '<div 
				style = "
				color:#333;
				padding:2%;
				margin:2%;
				background: #f1f1f1;
				border-radius: 10px;
				"
				>
		    <center><img src='.$img_name.' ?></center>
		    <div>
		        <h1>Book4Us</h1>
		        <h3>The easy way to get away...</h3>
		    </div>
		    Hi,
		        Welcome to Orgbitor. Your account has been created successfully.
		        We are working towards creating platform for exhibition industry thanks for your interest.

		        <a target="_blank" href='.$href.'>Activate your Account</a>

		        <br>
		        Thanks & Regards, 
		        Orgbitor Team 

		    <div id="footeqr">
		    <legend>Follow us:</legend>
		    <ul style="display: flex;width:100%;list-style: none;">
		        <li style="width:25%;"><a href="https://www.facebook.com/book4ustravel/">Facebook</a></li>
		        <li style="width:25%;"><a href="https://www.google.co.in/search?q=book4us">Google Plus</a></li>
		        <li style="width:25%;"><a href="#">Twitter</a></li>
		        <li style="width:25%;"><a href="#">Instagram</a></li>
		    </ul>
		    </div>
		</div>';
		return $mail_string;
	}
}

function send_trip_mail($content) {
	$CI = & get_instance();  //get instance, access the CI superobject
	return $CI->curl->simple_post('https://api.sendgrid.com/api/mail.send.json', array('api_user'=>'sjkurani', 'api_key'=>'unix1234','to'=>'sjkurani@gmail.com','fromname'=>'Book4Us','subject'=> 'User trip Details', 'html'=> $content, 'from'=>'info@book4us.in','cc'=>'sjkurani@gmail.com'));
}

?>