<?php
/*
	Author - @sjkurani
	@desc - Loading global functions.
*/

/*
	Loading Css and JS with different functions in Layout Library.
*/
function LoadCssAndJs($layoutsObj) {
				//Basic Bootstrap style.
	$layoutsObj->add_css('assets/plugins/bootstrap/css/bootstrap.min.css')
				//Font style.
				->add_css('/assets/plugins/font-awesome/css/font-awesome.min.css')
				->add_css('/assets/plugins/font-awesome/css/font-awesome.min.css')
				//Datatable styles.
				->add_css('assets/plugins/DataTables/datatables.min.css')

				//other styles.
				->add_css('assets/css/style.css')
				->add_css('assets/css/styles.css')
				->add_css('assets/css/plugins/formvalidation.bootstrap/formValidation.css')
				->add_css('assets/plugins/bootstrap-timepicker/bootstrap-datetimepicker.min.css')

				//Basic Jquery.
				->add_js('assets/js/jquery.min.js')
				//Boostrap Jquery
				->add_js('assets/plugins/bootstrap/js/bootstrap.min.js')
				// Datatable Jquery
				->add_js('assets/plugins/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js')
				->add_js('assets/plugins/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.min.js')
				->add_js('assets/plugins/dropzone/js/dropzone.js')


				//Other Jquery
				->add_js('assets/js/my.js')
				->add_js('assets/js/plugins/bootstrap-datepicker/bootstrap-datetimepicker.min.js')
				->add_js('assets/js/plugins/formvalidation.bootstrap/formValidation.js')
				->add_js('assets/js/plugins/formvalidation.bootstrap/formvalidation.bootstrap.js')
				->add_js('assets/js/custom/enquiry.js')
				->add_js('assets/js/custom/imgGallery.js')
				->add_js('assets/js/flex.js')
				->add_js('assets/js/plugins/slimscroll/jquery.slimscroll.min.js')
				->add_js('assets/js/plugins/hisrc/hisrc.js')
				->add_js('assets/js/datatables_ajax.js');
}

function asset_url(){
   return base_url().'assets/';
}

function lang($line,$return_type = '', $id = '')
{
	if($return_type == 'caps') {
		return strtoupper($line);
	}
	else if($return_type == 'small') {
		return strtolower($line);
	}
	elseif ($return_type == 'ucword') {
		return ucwords($line);
	}
	else {
		return $line;			
	}
}
function priceCalculator($completeEnquiryArray) {
	if(is_array($completeEnquiryArray) && !empty($completeEnquiryArray)) {
		$priceArray['perKmAc'] = (isset($completeEnquiryArray['per_km_ac']) && !empty($completeEnquiryArray['per_km_ac'])) ? $completeEnquiryArray['per_km_ac'] : 0;
		$priceArray['perKmNonAc'] = (isset($completeEnquiryArray['per_km']) && !empty($completeEnquiryArray['per_km'])) ? $completeEnquiryArray['per_km'] : 0;
		$priceArray['minKmPerDay'] = (isset($completeEnquiryArray['min_km']) && !empty($completeEnquiryArray['min_km'])) ? $completeEnquiryArray['min_km'] : 0;
		$priceArray['distance'] = (isset($completeEnquiryArray['approx_km']) && !empty($completeEnquiryArray['approx_km'])) ? $completeEnquiryArray['approx_km'] : 0;
		$priceArray['driverAllowance'] = (isset($completeEnquiryArray['driver_allowance']) && !empty($completeEnquiryArray['driver_allowance'])) ? $completeEnquiryArray['driver_allowance'] : 0;
		$priceArray['driverAllowanceNight'] = (isset($completeEnquiryArray['driver_allowance_night']) && !empty($completeEnquiryArray['driver_allowance_night'])) ? $completeEnquiryArray['driver_allowance_night'] : 0;

		$startDateObj = new DateTime($completeEnquiryArray['start_date']);
		$endDateObj = new DateTime($completeEnquiryArray['end_date']);
		$numberOfDays = $endDateObj->diff($startDateObj)->format("%a");
		$numberOfNights = 0;
		$start_date = strtotime($completeEnquiryArray['start_date']);
		$end_date = strtotime($completeEnquiryArray['end_date']);

		if(date('H', $start_date) < DAY_START && date('i', $start_date) <= 59 && date('s', $start_date) <= 59 ) {
			$numberOfNights ++;
		}

		if(date('H', $end_date) >= DAY_END && date('i', $end_date) >= 0 && date('s', $end_date) >= 0 ) {
			$numberOfNights ++;
		}

		$totalDriverAllowance = $priceArray['nightTotalDriverAllowance'] = $priceArray['dayTotalDriverAllowance'] = 0;
		if($numberOfDays >= 1) {
			$priceArray['dayTotalDriverAllowance'] = $numberOfDays * $priceArray['driverAllowance'];
			$totalDriverAllowance += $priceArray['dayTotalDriverAllowance'] ;
		}
		if($numberOfNights >= 1) {
			$priceArray['nightTotalDriverAllowance'] = $numberOfNights * $priceArray['driverAllowanceNight'];
			$totalDriverAllowance += $priceArray['nightTotalDriverAllowance'] ;

		}
		$priceArray['totalDriverAllowance'] = $totalDriverAllowance;
		$priceArray['acBasePare'] = $priceArray['distance'] * $priceArray['perKmAc'];
		$priceArray['nonAcBasePare'] = $priceArray['distance'] * $priceArray['perKmNonAc'];

		$priceArray['totalAcCost'] = $priceArray['acBasePare'] + $totalDriverAllowance;
		$priceArray['totalNonAcCost'] = $priceArray['nonAcBasePare'] + $totalDriverAllowance;
		
		$priceArray['numberOfNights'] = $numberOfNights;
		$priceArray['numberOfDays'] = $numberOfDays;
 		return $priceArray;
	}
	else {
		return array();
	}
}

function sendOTP($mobileno, $message){

    $message = urlencode('Hi, this is a test message');
    $sender = 'SEDEMO';
    $apikey = '62659c5asfu4zvd7898g1kj013e77it8v';
    $baseurl = 'http://web.springedge.com/api/web/send?apikey='.$apikey;

    $url = $baseurl.'&sender='.$sender.'&to='.$mobileno.'&message='.$message;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);/*

    $message = urlencode($message);
    $sender = 'SEDEMO';
    $apikey = '62659c5asfu4zvd7898g1kj013e77it8v';
    $baseurl = 'http://web.springedge.com/api/web/send?apikey='.$apikey;

    $url = $baseurl.'&sender='.$sender.'&to='.$mobileno.'&message='.$message;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);*/
//    curl_close($ch);
meDebug("hi");
    meDebug($response,1 );
    // Use file get contents when CURL is not installed on server.
    if(!$response){
        $response = file_get_contents($url);
    }

}

function sendCustomMsgs($msg,$number) {
    /*Send SMS using PHP*/    
    
    //Your authentication key
    $authKey = "91546AOXsoNtT593eb4e5";
    
    //Multiple mobiles numbers separated by comma
    $mobileNumber = $number;
    
    //Sender ID,While using route4 sender id should be 6 characters long.
    $senderId = "MYUASD";
    
    //Your message to send, Add URL encoding here.
    $message = urlencode($msg);
    
    //Define route 
    $route = "transactional";
    //Prepare you post parameters
    $postData = array(
        'authkey' => $authKey,
        'mobiles' => $mobileNumber,
        'message' => $message,
        'sender' => $senderId,
        'route' => $route
    );
    
    //API URL
    $url="https://control.msg91.com/api/sendhttp.php";
    
    // init the resource
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
    ));
    

    //Ignore SSL certificate verification
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    
    //get response
    $output = curl_exec($ch);
    
    //Print error if any
    if(curl_errno($ch))
    {
        echo 'error:' . curl_error($ch);
    }
    
    curl_close($ch);

    return $output;
}

function meDebug($array,$type = false) {
	echo "<pre>";
	print_r($array);
	if($type) {
		exit();
	}
	echo "</pre>";
}

function is_authenticated_user($user_types_array) {

    $CI = & get_instance();
    $is_user_logged_in = $CI->session->userdata('logged_in');
	$session_user_type = $CI->session->userdata('user_type');
	if($is_user_logged_in) {
		if(!in_array($session_user_type, $user_types_array)) {
			$CI->session->set_flashdata('errormsg', "You are not authorised to access that page.");
			redirect(base_url().'dashboard');
		}
	}
	else {
        $CI->session->set_flashdata('errormsg', "Please login to perform the actions.");
		redirect(base_url().'account/');
	}
}

function send_grid_mails($user_array,$sub,$text) {
	$CI = & get_instance();  //get instance, access the CI superobject
	return $CI->curl->simple_post('https://api.sendgrid.com/api/mail.send.json', array('api_user'=>'sjkurani', 'api_key'=>'unix1234','to'=>$user_array['email'],'fromname'=>'Book4Us','subject'=> $sub, 'html'=> $text, 'from'=>'info@book4us.in','cc'=>'sjkurani@gmail.com'));
}
function get_mail_content($href,$type) {
	if($type == 'registration') {

		$img_name = asset_url()."local_image/logos/logo-blue.png";
		$mail_string = '<div 
				style = "
				color:#333;
				padding:2%;
				margin:2%;
				background: #f1f1f1;
				border-radius: 10px;
				"
				>
		    <center><img src='.$img_name.' ?></center>
		    <div>
		        <h1>Book4Us</h1>
		        <h3>The easy way to get away...</h3>
		    </div>
		    Hi,
		        Welcome to Orgbitor. Your account has been created successfully.
		        We are working towards creating platform for exhibition industry thanks for your interest.

		        <a target="_blank" href='.$href.'>Activate your Account</a>

		        <br>
		        Thanks & Regards, 
		        Orgbitor Team 

		    <div id="footeqr">
		    <legend>Follow us:</legend>
		    <ul style="display: flex;width:100%;list-style: none;">
		        <li style="width:25%;"><a href="https://www.facebook.com/book4ustravel/">Facebook</a></li>
		        <li style="width:25%;"><a href="https://www.google.co.in/search?q=book4us">Google Plus</a></li>
		        <li style="width:25%;"><a href="#">Twitter</a></li>
		        <li style="width:25%;"><a href="#">Instagram</a></li>
		    </ul>
		    </div>
		</div>';
		return $mail_string;
	}
}

function send_trip_mail($content) {
	$CI = & get_instance();  //get instance, access the CI superobject
	return $CI->curl->simple_post('https://api.sendgrid.com/api/mail.send.json', array('api_user'=>'sjkurani', 'api_key'=>'unix1234','to'=>'sjkurani@gmail.com','fromname'=>'Book4Us','subject'=> 'User trip Details', 'html'=> $content, 'from'=>'info@book4us.in','cc'=>'sjkurani@gmail.com'));
}

?>