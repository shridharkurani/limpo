<?php

	

	/**

	 * Form Label Name of Login Page

	 */

	$lang['analytics']          = 'analytics';

	$lang['username'] 			= "Username";

	$lang['password'] 			= "Password";

	$lang['keep'] 				= "Keep me logged in";

	$lang['login'] 				= "Login";

	$lang['inActive']  			= "You have not activated your account, please check email and activate your account or contact to administrator!!";

	$lang['isBlock']  			= "You account has been blocked for some reason, please contact to administrator!!";

	$lang['isDelete']  			= "You account has been removed for some reason, please contact to administrator!!";

	$lang['misMatch']  			= "Given login credentials are not matched, please use correct credentials!!";

	$lang['add']  				= "ADD";

	$lang['Add_Product']		= "Add New Product";

	$lang['Remove']				= "REMOVE";

	$lang['update'] 			= "UPDATE";

	$lang['cancel']   			= "CANCEL";

	$lang['action']   			= "Action";

	$lang['serial']   			= "Serial";

	$lang['pack']				= "PACK";

	$lang['ERR_PRI']			= "You don't have privileged to access that module, please contact super administrator !!";

	$lang['db_exception']		= "Some problem in database operation, please contact to super administrator !!";

	$lang['xml_download'] 		= "Download XML file";

  	$lang['re-upload']  		= "Re-Upload";

	$lang['quantity/article'] 	= 'Quantity/Article';
	
	$lang['user_delete_success'] 	= 'User(s) Deleted Successfully!';
	$lang['user_active_success'] 	= 'User(s) status change successfully!';

	

	

	 /**

	 * Common Label Name

	 */	 

	 $lang['addnew'] 		= "Add New";

	 $lang['add'] 			= "Add New";

	 $lang['modify']		= "Modify";

	 $lang['edit']			= "Modify";

	 $lang['delete']		= "Delete";

	 $lang['view']			= "View";

	 $lang['refresh']		= "Refresh";

	 $lang['back']			= "Back to List";

	 $lang['lists']			= "Back to List";

	 $lang['import_csv']	= "Import CSV";

	 $lang['importcsv']		= "Import CSV";

	 $lang['cancel_import']	= "Cancel Import";

	 $lang['import']		= "Import";

	 $lang['search']		= "Search";

	 $lang['csv_download']	= "Download CSV Header";

	 $lang['check_msg']		= "Please first select checkbox then try again !";

	 $lang['csv_check_msg']	= "Please select csv file path !!";

	 $lang['export']		= "Export";

	 $lang['invoice'] 		= "Invoice";
	  
	 $lang['notification'] 		= "Notification";
	 
	 $lang['instruction'] 		= "These Hospitals Report - Price is Not Set.";
	 

	/**

	 * Form Label Name of pricing Page

	 */ 

	$lang['repvsmod']					= "Report - Modality";

	$lang['price'] 						= "Price";
	
	$lang['price_list'] 				= "Price List";

	$lang['reportvsmodality_suc_add']  	= "Price has added successfully!!";

	$lang['reportvsmodality_err_add'] 	= "Price has not added, please try again!!";
	
	 $lang['price_suc_del']  			= "Price has deleted successfully!!";

	 $lang['price_err_del']  			= "Price has not deleted. To delete it, please try again!!";

	 $lang['vew_err_price']  			= "You have not privilege to update the information of this area!!";
	 
	  $lang['edit_price']				= "Edit Price Details";

	 $lang['priceinfo']      			= "Price Information";
	 
	 $lang['edt_suc_price']  			= "Price has updated successfully!!";

	 $lang['edt_err_price']  			= "Price has not updated successfully, please try again!!";

	/**

	 * Form Label Name of User Page

	 */

	$lang['User']		  = "User";	
	
	$lang['select_language'] = "Select Language";	

	$lang['Username']     = "User Name";

	$lang['usertype'] 	  = "User Type";

	$lang['country']  	  = "Country";

	$lang['parent']	  	  = "Depot Manager";

	$lang['firstname'] 	  = "First Name";

	$lang['middlename']   = "Middle Name";

	$lang['lastname'] 	  = "Last Name";

	$lang['street']	  	  = "Street Address";

	$lang['house'] 	  	  = "House Number";

	$lang['city'] 	  	  = "City";

	$lang['zipcode'] 	  = "Zip Code";

	$lang['email'] 	 	  = "Email ID";

	$lang['username']     = "Username";

	$lang['password']     = "Password";

	$lang['userimg']  	  = "User Image";

	$lang['userlist']  	  = "User List";

	$lang['adduser']  	  = "Add New User";

	$lang['view_user']	  = "View User Details";

	$lang['edit_user']	  = "Edit User Details";	

	$lang['userinfo']  	  = "User Information";

	$lang['user_suc_add'] = "User detail has been added successfully!!";

	$lang['user_err_add'] = "User detail has not added, please try again!!";

	$lang['user_suc_edt'] = "User detail has updated successfully!!";

	$lang['user_err_edt'] = "User detail has not updated, please try again!!";

	$lang['user_suc_del'] = "User detail has deleted successfully!!";

	$lang['user_err_del'] = "User detail has not deleted, please try again!!";

	$lang['user_err_viw'] = "You have not privilege to modified the information of this area!!";

	

	

	/**

	 * Form Label Name of Profile Page

	 */

	$lang['account']	      	= "My Account";

	$lang['old_password'] 		= "Old Password";

	$lang['new_password'] 		= "New Password";

	$lang['confirm_password'] 	= "Confirm Password";

	$lang['change_password']  	= "Change Password";	

	$lang['profile']	      	= "User Profile";

	$lang['edit_profile']	  	= "Edit Profile";

	$lang['view_profile']	  	= "View Profile details";

	$lang['view']	  			= "View Profile";

	

	 


	
	 /**

	 * Form Label Name of Hospital Page

	 */
		$lang['routercharge']		= "Router Charge";
		
		$lang['report_removal_condition']	= "Report Removal Condition";
	
	 $lang['routerstatus']		= "Router Status";
	 	
	 $lang['bill_status']		= "Bill Status";
	 $lang['hospital_code']		= "Hospital Code";	
	 $lang['no_of_router']     	= "No Of Router";
	 $lang['name']     			= "Name";
	 $lang['price_increment']   = "Price Increment";
	 $lang['responsible_sanitario']   = "Responsible Sanitario";
	 $lang['responsible_charges']   = "Responsible Charges";
	 $lang['confirm_date']     	= "Confirm Date";	
	 $lang['addhospital']		= "Add Hospital";

	 $lang['hospitalname']     	= "Name";
	 
	 $lang['hospitallist']     	= "HospitalList";

	 $lang['bill_status']     	= "Bill Status";
	 
	 $lang['address']     		= "Address";
	 
	 $lang['tax_status']     	= "Tax Status";
	 
	  $lang['condition']     	= "Condition";
	  
	 $lang['discount']     		= "Discount";
	 
	 $lang['minute']     		= "Minute";
	 
	 $lang['status']     		= "Status";
	 
	 $lang['phone_no2']     	= "Phone No2";
	 
	 $lang['contactperson2']    = "Contact Person";
	 
	 $lang['billing_currency']  = "Billing Currency";
	 
	 $lang['report_currency']   = "Report Currency";
	 
	 $lang['study_currency']    = "Study Currency";
	 
	$lang['sla-tatcondition']   = "SLA - TAT Condition";
	
	$lang['study_report_status']   = "Study & Report Status";
	
	$lang['emailid_1']    		= "Email Address1";
	 
	$lang['emailid_2']   		= "Email Address2";
	 
	 $lang['hospitallist']     	= "Hospital List";
	
	 $lang['view_hospital']		= "View Hospital Details";

	 $lang['edit_hospital']		= "Edit Hospital Details";

	 $lang['hospitalinfo']      = "Hospital Information";

	 $lang['hospital_suc_add']  = "Hospital has added successfully!!";

	 $lang['hospital_err_add']  = "This Hospital Or User Name has allready exits,Please try again!!";

	 $lang['edt_suc_hosp']  	= "Hospital has updated successfully!!";

	 $lang['edt_err_hosp']  	= "Hospital has not updated successfully, please try again!!";

	 $lang['hosp_suc_del']  	= "Hospital has deleted successfully!!";

	 $lang['hosp_err_del']  	= "Hospital has not deleted. To delete it, please try again!!";

	 $lang['vew_err_hosp']  	= "You have not privilege to update the information of this area!!";
	$lang['hospita_delete_success']="Selected hospital has deleted successfully!!";
	 $lang['hospita_active_success']="Selected  hospital has Active successfully!!";
	 $lang['hospita_inactive_success']="Selected hospital has In-active successfully!!";

	 /**

	 * Form Label Name of Report Page

	 */

	 $lang['reportcode']		= "Code";

	 $lang['name']     			= "Name";

	 $lang['addreport']			= "Add Report";

	 $lang['reportname']     	= "Name";
	 
	 $lang['points']     		= "Points";

	 $lang['into multiplying']	= "Multiplying Factor";

	 $lang['doctorcommission']  = "Doctor Commission";
	 
	 $lang['status']     		= "Status";
	 
	$lang['extracharge']		= "Eextra Charges";
	 
	 $lang['reportlist']     	= "Report List";
	
	 $lang['view_report']		= "View Report Details";

	 $lang['edit_report']		= "Edit Report Details";

	 $lang['reportinfo']      	= "Report Information";

	 $lang['report_suc_add']  	= "Report has added successfully!!";

	 $lang['report_err_add']  	= "Reports allready exist, Please try again!!";

	 $lang['edt_suc_report']  	= "Report has updated successfully!!";

	 $lang['edt_err_report']  	= "Report has not updated successfully, please try again!!";

	 $lang['report_suc_del'] 	= "Report has deleted successfully!!";

	 $lang['report_err_del']  	= "Report has not deleted. To delete it, please try again!!";

	 $lang['vew_err_report']  	= "You have not privilege to update the information of this area!!";

	 
 /** Form Label Name of Report Page

	 */

	 $lang['modalitycode']			= "Code";

	 $lang['name']     				= "Name";

	 $lang['addmodality']			= "Add Modality";

	 $lang['modalityname']     		= "Name";
	 
	 $lang['status']     			= "Status";
	 
	 $lang['modalitylist']     		= "Modality List";
	
	 $lang['view_modality']			= "View Modality Details";

	 $lang['edit_modality']			= "Edit Modality Details";

	 $lang['modalityinfo']      	= "Modality Information";

	 $lang['modality_suc_add']  	= "Modality has added successfully!!";

	 $lang['modality_err_add']  	= "Modality has not added successfully, Please try again!!";

	 $lang['edt_suc_modality']  	= "Modality has updated successfully!!";

	 $lang['edt_err_modality']  	= "Modality has not updated successfully, please try again!!";

	 $lang['modality_suc_del'] 	 	= "Modality has deleted successfully!!";

	 $lang['modality_err_del']  	= "Modality has not deleted. To delete it, please try again!!";

	 $lang['vew_err_modality']  	= "You have not privilege to update the information of this area!!";

	 
	 /** Form Label Name of ReportModality Page

	 */

	 $lang['modalitycode']				= "Code";

	 $lang['name']     					= "Name";

	 $lang['addreportmodality']			= "Add Report Modality";

	 $lang['modalityname']     			= "Modality Name";
	 
	  $lang['reportname']     			= "Report Name";
	 
	 $lang['addrmodility']   			= "SAVE";
	 
	 $lang['reportmodalitylist']     	= "ReportVsModality List";
	
	 $lang['reportvsmodalityinfo']		= "View ReportVsModality Details";

	 $lang['edit_ReportVsmodality']		= "Edit ReportVsModality Details";

	 $lang['modalityinfo']     			= "Modality Information";

	 $lang['reportmodality_suc_add']  	= "Report Vs Modality has added successfully!!";

	 $lang['reportmodality_err_add']  	= "Report Vs Modality has not added successfully, Please try again!!";

	 $lang['edt_suc_ReportModality']  	= "ReportModality has updated successfully!!";

	 $lang['edt_err_ReportModality']  	= "ReportModality has not updated successfully, please try again!!";

	 $lang['ReportModality_suc_del'] 	= "ReportModality has deleted successfully!!";

	 $lang['ReportModality_err_del']  	= "ReportModality has not deleted. To delete it, please try again!!";

	 $lang['vew_err_reportmodality']  	= "You have not privilege to update the information of this area!!";

	 
	 /** Form Label Name of ReportModalityCorrection Page

	 */

	 $lang['hospitalname']						= "Hospital Name";

	 $lang['correctname']     					= "Correction Name";

	 $lang['addrepmodcorrect']					= "Add Report Modality Correction";

	 $lang['modalityname']     					= "Modality Name";
	 
	  $lang['reportname']     					= "Report Name";
	  
	  $lang['report']     						= "Report";
	 
	 $lang['addrmodility']   					= "SAVE";
	 
	 $lang['reportmodalitycorrectlist']     	= "Report-Modality Correction List";
	
	 $lang['reportvsmodalitycorrectinfo']		= "View Report-Modality Correction Details";

	 $lang['edit_ReportVsmodalitycorrect']		= "Edit Report-Modality Correction Details";

	 $lang['modalityinfo']     					= "Modality Information";

	 $lang['reportmodalitycorr_suc_add']  		= "Report Vs Modality Correction has added successfully!!";

	 $lang['reportmodalitycorr_err_add']  		= "Report Vs Modality Correction has not added successfully, Please try again!!";

	 $lang['edt_suc_repvsmodcorrection']  		= "Report Vs Modality Correction has updated successfully!!";

	 $lang['edt_err_ReportModalitycorrect']  	= "Report Vs Modality Correction has not updated successfully, please try again!!";

	 $lang['ReportModalitycorrect_suc_del'] 	= "ReportModality Correction has deleted successfully!!";

	 $lang['ReportModalitycorrect_err_del']  	= "ReportModality Correction has not deleted. To delete it, please try again!!";

	 $lang['vew_err_reportmodalitycorrect']  	= "You have not privilege to update the information of this area!!";
	 
	 
	  /**

	 * Form Label Name of module page 

	 */

	 

	 $lang['add_suc_module']  = "Module has been added successfully!!";

	 $lang['add_err_module']  = "Details has not added successfully, Please try again!!";

	 $lang['edt_suc_module']  = "Details has updated successfully!!";

	 $lang['edt_err_module']  = "Details has not updated successfully, please try again!!";

	 $lang['dlt_suc_module']  = "Details has deleted successfully!!";

	 $lang['dlt_err_module']  = "Details has not deleted. To delete it, please try again!!";

	 $lang['vew_err_module']  = "You have not privilege to update the information of this module!!";

	 

	

	/**

	 * Form Label Name of Radio Logist Page

	 */

	 $lang['addradiologist']		= "Add Radio Logist";

	 $lang['radiologistname']     	= "Name";

	 $lang['type']					= "Type";
	 
	 $lang['payin']					= "Pay In";
	  
	 $lang['phone_no']     			= "Phone No";
	 
	  $lang['paycommission']		= "Pay Commission";
	 
	  $lang['doctortype']			= "Types of Doctor";
	  
	 $lang['paypoint']				= "Pay Point";
	  
	 $lang['extracharges']     		= "Extra Charges";
	 
	 $lang['status']     			= "Status";
	  
	 $lang['radiologistlist']     	= "RadioLogist List";
	
	 $lang['view_radiologist']		= "View RadioLogist Details";

	 $lang['edit_radio_logist']		= "Edit RadioLogist Details";

	 $lang['radiologistinfo']     	= "RadioLogist Information";

	 $lang['radiologist_suc_add']  	= "Radiologist has added successfully!!";

	 $lang['radiologist_err_add']  	= "Radiologist has not added successfully, Please try again!!";

	 $lang['edt_suc_radiologist']  	= "RadioLogist has updated successfully!!";

	 $lang['edt_err_radiologist']  	= "RadioLogist has not updated successfully, please try again!!";

	 $lang['radiologist_suc_del'] 	= "RadioLogist has deleted successfully!!";

	 $lang['radio_logist_err_del']  = "RadioLogist has not deleted. To delete it, please try again!!";

	 $lang['vew_err_radiologist']  	= "You have not privilege to update the information of this area!!";
	 
	$lang['radiologist_delete_success']="Selected RadioLogist has deleted successfully!!";
	
	 $lang['radiologist_active_success']="Selected  RadioLogist status successfully change!!";
	 $lang['fixed_monthly_charge']="Fixed Monthly Charge";

	 /**

	 * Form Label Name of Login Page

	 */
	 
	  $lang['edit_language']		= "Submit";
	  $lang['language_add'] 		= "Language has been Change successfully!!";
	  $lang['language_error_add'] 	= "Language has not change, please try again!!";
	  $lang['language_section']     ="Language Section";
	  
	  
	  	 /**

	 * Form Label Name of Currency setting

	 */
	 
	 $lang['currencycode'] = "Currency Code";
	 $lang['currencylist'] = "Currency List";
	 $lang['currencyname'] = "Currency Name";
	 $lang['currency_suc_add'] = "Currency has added successfully!!";
	 $lang['currency_err_add'] = "Currency has not added successfully, Please try again!!";
	 $lang['edt_suc_currency'] = "Currency has updated successfully!!";
	 $lang['edt_err_currency'] = "Currency has not updated successfully, please try again!!";
	 $lang['vew_err_currency'] = "You have not privilege to update the information of this area!!";
	 $lang['currency_suc_del'] = "Currency has deleted successfully!!";
	 $lang['currency_err_del'] = "Currency has not deleted. To delete it, please try again!!";
	 $lang['ERR_PRI'] = "Err Pri";
	 $lang['currencyinfo'] = "Currency Info";
	 $lang['addcurrency'] = "Add Currency";
	 $lang['edit_currency'] = "Edit Currency";
	 $lang['addcurrency'] = "Add Currency";
	 
	 
	 /**

	 * Form Label Name of General Configuration setting

	 */
	 $lang['taxrate'] = "Tax Rate";
	 
	 $lang['submit'] = " SUBMIT ";
	 $lang['price_status'] = "Price Status";
     $lang['returnValue'] = "Value Return";
     $lang['addgeneral_conf'] = "Add General Configuration";
	 $lang['GenConf_suc_add'] 	= "General Configuration has added successfully!!";
	 $lang['GenConf_err_add'] 	= "General Configuration has not added successfully, Please try again!!";
	 $lang['edt_suc_GenConf'] 	= "General Configuration has updated successfully!!";
	 $lang['edt_err_GenConf'] 	= "General Configuration has not updated successfully, please try again!!";
	 $lang['vew_err_GenConf'] 	= "You have not privilege to update the information of this area!!";

	  /**

	 * Form Label Name of Import Page

	 */
	  $lang['mandatoryreport']				= "These fields are mandatory in import Report (Order Time,Last Image Arr Time,Report Comp Time,Patient,MRN,Procedure,Report Status,Hospital,Modality,Final Rad,SLA,E2E TAT,Workflow ID)";
	  $lang['mandatorystudy']				= "These fields are mandatory in import study (Study ID,Patient,MRN,Description,Scan Time,Study Source Name,Acquire Start Time)";
	  $lang['studydetails'] 		= "Study Details";
	  $lang['reportdetails'] 		= "Report Details";
	  $lang['month'] 				= "Select Month";
	  $lang['upload_studies'] 		= "Import Studies";
	  $lang['upload_report'] 		= "Import Report";
	  $lang['import_file'] 			= "Import File";
	  $lang['import_suc_file'] 		= " Rows successfully import !! ";
	  $lang['import_err_file'] 		= " Studies report has  not been successfully import!! ";
	  $lang['import_suc_rfile']		= " Rows successfully import !! ";
	  $lang['import_err_rfile'] 	= " Row(s) Rejected!! ";
	  $lang['import_tfile'] 			= " Total Rows ";
	  $lang['import_errman_rfile'] 	= "Some fields are manadatory";
	  $lang['importreport'] 		= "Import Report";
	  $lang['Order_Time'] 			= "Order Time";
	  $lang['Last_Image_Arr_Time'] 	= "Last Image Arr Time";
	  $lang['Report_Comp_Time'] 	= "Report Comp Time";
	   $lang['patient'] 			= "Patient";
	  $lang['mrn'] 					= "MRN";
	  $lang['procedures'] 			= "Procedures";
	  $lang['reportstatus'] 		= "Reportstatus";
	  $lang['hospital'] 			= "Hospital";
	  $lang['modality'] 			= "Modality";
	  $lang['Final_Rad'] 			= "Final Rad";
	  $lang['SLA'] 					= "SLA";
	  $lang['E2E_TAT'] 				= "E2E TAT";
	  $lang['Workflow_ID'] 			= "Workflow ID";
	  $lang['DOB'] 					= "DOB";
	  $lang['Read_History'] 		= "Read History";
	  $lang['Ref_Physician'] 		= "Ref Physician";
	  $lang['MT'] 					= "MT";
	  $lang['Criticality'] 			= "Criticality";
	  $lang['Image_Count'] 			= "Image Count";
	  $lang['Client_Comm'] 			= "Client Comm";
	  $lang['Int_Comm'] 			= "Int Comm";
	  $lang['Requistion_Time'] 		= "Requistion Time";
	  $lang['Call_TAT'] 			= "Call TAT";
	  $lang['Follow_up_Comments'] 	= "Follow up Comments";
	  $lang['Rad_Notes'] 			= "Rad Notes";
	  $lang['Groups'] 			    = "Group";
	  $lang['Discussion_Details'] 	= "Discussion Details";
	  $lang['Age'] 					= "Age";
	  $lang['Gender'] 				= "Gender";
	  $lang['First_Image_Arr_Time'] = "First Image Arr Time";
	  $lang['DOE'] 					= "DOE";
	  $lang['Rpt_Only?'] 			= "Rpt Only?";
	  $lang['Acc_#'] 				= "Acc #";
	  $lang['Tot_Reads'] 			= "Tot Reads";
	  $lang['Rpt_Type'] 			= "Rpt Type";
	  $lang['Ord_Created_by'] 		= "Ord Created by";
	  $lang['Ord_Last_Mod_By'] 		= "Ord Last Mod By";
	  $lang['P2P'] 					= "P2P";
	  $lang['Assigners'] 			= "Assigners";
	  $lang['Report_Text'] 			= "Report Text";
	  $lang['Rad_1_Name'] 			= "Rad 1 Name";
	  $lang['Rad_1_Assigned_time'] 	= "Rad 1 Assigned time";
	  $lang['Rad_1_Picked_time'] 	= "Rad 1 Picked time";
	  $lang['Rad_1_Proof_Req_time'] = "Rad 1 Proof Req time";
	  $lang['Rad_1_Completed_time'] = "Rad 1 Completed time";
	  $lang['Rad_2_Name'] 			= "Rad 2 Name";
	   $lang['Rad_2_Assigned_time'] = "Rad 2 Assigned time";
	  $lang['Rad_2_Picked_time'] 	= "Rad 2 Picked time";
	  $lang['Rad_2_Proof_Req_time'] = "Rad 2 Proof Req time";
	  $lang['Rad_2_Completed_time'] = "Rad 2 Completed time";
	  $lang['Rad_3_Name'] 			= "Rad 3 Name";
	  $lang['Rad_3_Assigned_time'] 	= "Rad 3 Assigned time";
	  $lang['Rad_3_Picked_time'] 	= "Rad 3 Picked time";
	  $lang['Rad_3_Proof_Req_time'] = "Rad 3 Proof Req time";
	  $lang['Rad_3_Completed_time'] = "Rad 3 Completed time";
	  $lang['MT_1_Name'] 			= "MT 1 Name";
	  $lang['MT_1_Assigned_time'] 	= "MT 1 Assigned time";
	  $lang['MT_1_Picked_time'] 	= "MT 1 Picked time";
	  $lang['MT_1_Completed_time'] 	= "MT 1 Completed time";
	  $lang['Add#1_Rad_1_Name'] 	= "Add#1 Rad 1 Name";
	  $lang['Add#1_Rad_1_Completed_time'] 	= "Add#1 Rad 1 Completed time";
	  $lang['Add#1_Text'] 			= "Add#1 Text";
	  $lang['Add#2_Rad_1_Name'] 	= "Add#2 Rad 1 Name";
	  $lang['Add#2_Rad_1_Completed_time'] 	= "Add#2 Rad 1 Completed time";
	  $lang['Add#2_Text'] 			= "Add#2 Text";
	  $lang['Patient_Location'] 	= "Patient Location";
	  $lang['Insurance_Type'] 		= "Insurance Type";
	  $lang['Clinical_History'] 	= "Clinical History";
	  $lang['Surgical_History'] 	= "Surgical History";
	  $lang['Lab_Results'] 			= "Lab Results";
	  $lang['DEO_Comments'] 		= "DEO Comments";
	  $lang['Clinical_Diagnosis'] 	= "Clinical Diagnosis";
	  $lang['Contraste'] 			= "Contraste";	  
	  $lang['description'] 			= "Description";
	  $lang['file_path'] 			= "File Path";
	  $lang['import'] 				= "Import";
	  $lang['Destination_AET'] 		= "Destination AET";
	  $lang['Destination_IP'] 		= "Destination IP";
	  $lang['DOB'] 					= "DOB";
	  $lang['Acc'] 					= "Acc #";
	  $lang['RSP'] 					= "RSP";
	  $lang['Institution_Name'] 	= "Institution Name";
	  $lang['Prior'] 				= "Prior?";
	  $lang['Reconciled'] 			= "Reconciled?";
	  $lang['Modality'] 			= "Modality";
	  $lang['Image_Count'] 			= "Image Count";
	  $lang['Referring_Physician'] 	= "Referring Physician";
	  $lang['Source_AE_Title'] 		= "Source AE Title";
	  $lang['Source_IP'] 			= "Source IP";
	  $lang['PACS_Instance'] 	    = "PACS Instance";
	  $lang['Acquire_Start_Time'] 	= "Acquire Start Time";
	  $lang['Acquire_Complete_Time'] = "Acquire Complete Time";
	  $lang['Study_UID'] 			= "Study UID";
	  $lang['Gender'] 				= "Gender";
	  $lang['Study_ID'] 			= "Study ID";
	  $lang['Scan_Time'] 			= "Scan Time";
	 $lang['Study_Source_Name']      = "Study Source Name";
	 $lang['import_err_mfile']      = "Manadatory Field Misssing!!";
	 $lang['studyprice_list']      = "Study Price List";
	 $lang['reportyprice_list']    = "Report Price List";
	 $lang['studyforinvoice']      = "Study For Invoice";
	 $lang['reportforinvoice']     = "Report For Invoice";
	 $lang['study_arrival_time']    = "Study Arrival Time";
	 $lang['radiologist']      		= "radiologist";
	 $lang['corrected_report']      = "Corrected report";
	 $lang['diff_between_report_name'] = "Diff. Report";
	 $lang['report_date']      		= "Report Date";
	 $lang['report_time']      		= "Report time";
	 $lang['id']      				= "ID";
	 $lang['tat']      				= "TAT";
	 $lang['rt_Time']      			= "rt Time";
	   $lang['updatefinalize']      	= "Update For Finalize";
	  $lang['report_suc_final']  	= "Finalize Report has been successfully updated !!"; 
	 $lang['report_err_final']  	= "Finalize Report has not been successfully updated!!";
	 $lang['check_msg']  			="Please first select checkbox then try again !";
	  $lang['checkhosp_msg']  		="Please first select any hospital to generate invoice !";
	  $lang['studydetails']         = "Studies Details";
	  $lang['reportdetails']        = "Report Details";
	  $lang['study_suc_final']  	= "Finalize Study Report has been updated successfully!!"; 
	 $lang['study_err_final']  		= "Finalize Study Report has not been updated successfully!!";
	   $lang['Storage_study']       = "Study";
	  $lang['Reports_storage']     = "Report";
	  $lang['Month']            	= "Month";
	  $lang['from_month']          = "From Month";
	  $lang['to_month']            = "To Month";
	  
	  $lang['Storage_study_charge']    	= "Study Charge";
	  $lang['Reports_storage_charge']  	= "Reports Charge";
	  $lang['total']            		= "Total";
	  $lang['invoice_date']            	= "Invoice Date";
	   $lang['studyinvoice']            = "Study Invoice";
	  $lang['reportinvoice']            = "Report Invoice";
	  $lang['invoice_suc_add'] = "Invoice has been created successfully!!";
	$lang['invoice_err_add'] = "Invoice Value has not added successfully, Please try again!!";
	 $lang['invoicestudy_suc_add'] = "Invoice Study Value has added successfully!!";
	$lang['invoicestudy_err_add'] = "Invoice Study Value has not added successfully, Please try again!!";
	 $lang['filename']            = "Invoice File Name"; 
	 $lang['date']            = "Date";
	$lang['time']            = "Time";
	$lang['patient_count']   = "Count Of Patient";
	 $lang['reported_hospital']  = "Reported Hospital";
	  $lang['record_delete']  		= "Selected Record successfully deleted!!"; 
	 $lang['recordfilter_delete']  		= "Filtered Record successfully deleted!!"; 
	 
	   /**

	 * Form Label Name of Hospital Group Page

	 */
	 
	 $lang['groupname'] 					= "Group Name";
	 $lang['hospitalgrouplist']     		= "Hospital Group List";
	 $lang['group_email'] 					= "Group Email";	
	 $lang['view_hospital_group']			= "View Hospital Group Details";
	 $lang['addhospitalgroup']				= "Add Hospital Group";
	 $lang['edit_hospital_group']			= "Edit Hospital Group Details";
	 $lang['hospgroup_suc_add'] 			= "Hospital Group has added successfully!!";
	 $lang['hospgroup_err_add'] 			= "Hospital Group has not added successfully, Please try again!!";
	 $lang['edt_suc_hospgroup'] 			= "Hospital Group has updated successfully!!";
	 $lang['edt_err_hospgroup'] 			= "Hospital Group has not updated successfully, please try again!!";
	 $lang['vew_err_hospgroup'] 			= "You have not privilege to update the information of this area!!";
	 $lang['hospgroup_suc_del'] 			= "Hospital Group  has deleted successfully!!";
	 $lang['hospgroup_err_del']  			= "Hospital Group  has not deleted. To delete it, please try again!!";

	  /**

	 * Form Label Name of Currency Value setting

	 */
	 $lang['currencyvalue']     	= "Currency Value";
	 $lang['1usd']           		= "1 USD";
     $lang['returnValue'] 			= "Value Return";
     $lang['addcurrencyvalue'] 		= "Add Currency Value";
	 $lang['currencyval_suc_add'] 	= "Currency Value has added successfully!!";
	 $lang['currencyval_err_add'] 	= "Currency Value has not added successfully, Please try again!!";
	 $lang['edt_suc_currencyval'] 	= "Currency Value has updated successfully!!";
	 $lang['edt_err_currencyval'] 	= "Currency Value has not updated successfully, please try again!!";
	 $lang['vew_err_currencyval'] 	= "You have not privilege to update the information of this area!!";
	 $lang['Standar_Price'] 	= "Standar Price";
	 $lang['report_standar_price'] 	= "Report Standar Price";
	 $lang['new_hospital_radio_expo'] 	= "New Hospital & Radiologist";
	 
	 /**
	 * Form Label Name of currenct month in spanish

	 */
	 $lang['Jan'] = "enero";
	 $lang['Feb']	= "febrero";
	 $lang['Mar']	= "marzo";
	 $lang['Apr'] = "abril";
	 $lang['May'] = "Mayo";
	 $lang['Jun'] = "junio";
	 $lang['Jul'] = "julio";
	 $lang['Aug'] = "agosto";
	 $lang['sep'] = "septiembre";
	 $lang['Oct'] = "octubre";
	 $lang['Nov'] = "noviembre";
	 $lang['Dec'] = "diciembre";

/* email template*/
	 $lang['dynamicvalue']     = "Dynamic dropdown";
	 $lang['subject']           = "Subject";
     $lang['description'] 		= "Description";
	  $lang['templatetype'] 		= "Template Type";	
     $lang['emailtemplatelist'] 		= "Email Template";
	 $lang['email_suc_add'] 		= "Email Template has added successfully!!";
	 $lang['email_err_add'] = "Email Template has not added successfully, Please try again!!";
	 $lang['edt_suc_email'] = "Email Template has updated successfully!!";
	 $lang['edt_err_email'] = "Email Template has not updated successfully, please try again!!";
	 $lang['vew_err_email'] = "You have not privilege to update the information of this area!!";
	 $lang['port'] = "Port";
	 $lang['email_type'] = "Email Type";
	 $lang['emailconf_suc_add'] = "Email Configration has added successfully!!";
	 $lang['emailconf_err_add'] = "Email Configration has not added successfully, Please try again!!";
	 $lang['edt_suc_emailconf'] = "Email Configration has updated successfully!!";
	 $lang['edt_err_emailconf'] = "Email Configration has not updated successfully, please try again!!";
	 $lang['vew_err_emailconf'] = "You have not privilege to update the information of this area!!";
	 
	
	 $lang['report_detailpage'] = "Column add in Invoice report detail page"; 
	 $lang['study_detailpage'] = "Column add in Invoice Study detail page"; 
	 
	  //doctor_Comission
   $lang['doctor_Comission']          = "Doctor Comission";
  $lang['Report_Status']          = "Report Status"; 
     $lang['Unit_Point']    = "Unit Point";
   $lang['multiply_factor']    = "Multiply Factor";
  $lang['Total_Points']     = "Total Points";
  $lang['Total_Comission']     = "Total Comission";
  $lang['Total_Extracharge']     = "Total Extracharge";
  $lang['Total_Pay']     = "Total Pay";
	 
  $lang['alias']     = "Name Alias";
   $lang['check_invoice']    = "Check Invoice";
   
   //dashboard page
   $lang['Summary']          = "Summary";
  $lang['Total_No_of_Hospital']          = "Total No of Hospital";
     $lang['New_Hospitals']      = "New Hospitals";
  $lang['No_of_Reports']      = "No. of Reports";  
     $lang['No_of_Studies(with Test)']    = "No of Studies(with Test)";
  $lang['No_of_Study(without Test)']     = "No of Study(without Test)";
  $lang['Total_Income_from_Reported_Hospital_Group']     = "Total Income from Reported Hospital Group";
  $lang['Total_Income_From_Study_Group']     = "Total Income From Study Group";
  $lang['Total_Payment_to_doctors']     = "Total Payment to doctors";
  $lang['Total_Report']     = "Total Report";
  $lang['Total_Study']        = "Total Study";
  $lang['New_Radiologist']        = "New Radiologist";
   $lang['demo']        = "Demo";
     $lang['report_detail']        = "Report Status Not In Our List";
	 $lang['Total_Pay_Point']        = "Total Pay Point";
	 
	 $lang['invoice_email_log']     ="Invoice Email Log";
	$lang['timestamp']       ="Timestamp";
	$lang['created_invoice']       = "Created Invoice"; 
	$lang['Exceptional_Report']      = "Exceptional Report"; 
	$lang['Exceptional_Studies']      = "Exceptional Studies"; 
	$lang['hospital_detail']       = "Hospital_detail"; 
	$lang['router_detail']       = "Router Detail";
	$lang['router_price']        = "Router Price";
	$lang['router_tax']        = "Router Tax";
	$lang['router_total']        = "Router Total";
	$lang['no_of_router']        = "No Of Router";
	$lang['responsible_sanitario_detail']    ="Responsible Sanitario Detail";
	$lang['responsible_sanitario_price']    ="Responsible Sanitario Price";
	$lang['responsible_sanitario_tax']    ="Responsible Sanitario Tax";
	$lang['responsible_sanitario_total']    ="Responsible Sanitario Total";
	$lang['grand_total']        ="Grand Total";
	$lang['sla_vs_tat_detail']      ="SLA Vs TAT Detail";
	$lang['TOTAL_REPORTES']       = "TOTAL REPORTES"; 
	$lang['Total_report_price']      = "Total report price"; 
	$lang['TOTAL_SlaTat']        = "TOTAL SlaTat"; 
	$lang['Total_SlaTat_Price']      = "Total SlaTat Price"; 
	$lang['Total_Studies']       = " Total Studies";
	$lang['Study_Unit_Price']       = "Study Unit Price";
	$lang['Study_Total_Price']      = "Study Total Price";
	$lang['email_config']        = "Email Config";
	$lang['host']        = "Host";
	$lang['ftppath']        = "Directory Path";








	/* start here new lang */

	$lang['not']				= 'not';
	$lang['hospitals']			= 'hospitals';
	$lang['new']				= 'new';
	$lang['statastics']			= 'statastics';
	$lang['dashboard']			= 'dashboard';
	$lang['admin']				= 'admin';
	$lang['super']			    = 'super';
	$lang['in']				= 'in';
	$lang['our']			= 'our';
	$lang['list']			= 'list';
	$lang['total']			= 'total';
	$lang['total no of hospital']	= 'total no of hospital';
	$lang['new hospitals']		= 'new hospitals';
	$lang['number of reports']	= 'no. of reports';
	$lang['number of studies (with test)']	= 'no. of studies (with test)';
	$lang['number of studies (without test)']	=	'no. of studies (without test)';
	$lang['total income from reported hospital group']	=	'total income from reported hospital group';
	$lang['total income from study group']	=	'total income from study group';
	$lang['total payment to doctors']	=	'total payment to doctors';
	$lang['import']		=	'import';
	$lang['import reports']		=	'import reports';
	$lang['import studies']		=	'import studies';
	$lang['show import errors']		=	'show import errors';
	$lang['final report']		=	'final report';
	$lang['report details']		=	'report details';
	$lang['study detail']		=	'study detail';
	$lang['exceptional report']		=	'exceptional report';
	$lang['exceptional study']		=	'exceptional study';
	$lang['SLA Vs TAT']			=	'SLA Vs TAT';
	$lang['invoice']			=	'invoice';
	$lang['generate invoice']	=	'generate invoice';
	$lang['generate report invoice']	= 'generate report invoice';
	$lang['generate study invoice']	=	'generate study invoice';
	$lang['created invoice']	=	'created invoice';
	$lang['doctor payment']		=	'doctor payment';
	$lang['invoice email log']	=	'invoice email log';
	$lang['hospital management']	=	'hospital management';
	$lang['hospital groups']	=	'hospital groups';
	$lang['add new hospital group']		=	'add new hospital group';
	$lang['list of hospitals']		=	'list of hospitals';
	$lang['add new hospital']		=	'add new hospital';
	$lang['master settings']		=	'master settings';
	$lang['modality']		=	'modality';
	$lang['price settings']			=	'price settings';
	$lang['general configuration']	=	'general configuration';
	$lang['reports'] = 'reports';
	$lang['radioLogist']	=	'radioLogist';
	$lang['report vs modality correction']		=	'report vs modality correction';
	$lang['email template']		=	'email template';
	$lang['user management']	=	'user management';
	$lang['list of users']		=	'list of users';
	$lang['add new user']		=	'add new user';
	$lang['system settings']	=	'system settings';
	$lang['modules']			=	'modules';
	$lang['user roles']			=	'user roles';
	$lang['user permissions']	=	'user permissions';
	$lang['currency setting']	=	'currency setting';
	$lang['login successful. you logged in as'] = 'login successful. you logged in as';
	$lang['These fields are mandatory in import Report (Order Time,Last Image Arr Time,Report Comp Time,Patient,MRN,Procedure,Report Status,Hospital,Modality,Final Rad,SLA,E2E TAT,Workflow ID)']		=	'These fields are mandatory in import Report (Order Time,Last Image Arr Time,Report Comp Time,Patient,MRN,Procedure,Report Status,Hospital,Modality,Final Rad,SLA,E2E TAT,Workflow ID)';

	$lang['show error']		=	'show error';
	$lang['import using FTP']	=	'import using FTP';
	$lang['select month']	=	'select month';
	$lang['import using excel file']	=	'import using excel file';
	$lang['export to excel']	=	'export to excel';
	
	$lang['select file']	=	'select file';
	$lang['filters']	=	'filters';
	$lang['report status']	=	'report status';
	$lang['select']	=	'select';
	$lang['final rad']	=	'final rad';
	$lang['from month']		=	'from month';
	$lang['to month']		=	'to month';
	$lang['check invoice']		=	'check invoice';
	$lang['action']		=	'action';
	$lang['ascending']		=	'ascending';
	$lang['descending']		=	'descending';
	$lang['filter and search']		=	'filter and search';
	$lang['date']	=	'date';
	$lang['order time']		=	'order time';
	$lang['last image arr time']	=	'last image arr time';
	$lang['report comp time']		=	'report comp time';
	$lang['patient']		=	'patient';
	$lang['MRN']		=	'MRN';
	$lang['procedures']		=	'procedures';
	$lang['SLA']		=	'SLA';
	$lang['E2E TAT']		=	'E2E TAT';
	$lang['workflow ID']		=	'workflow ID';
	$lang['month']		=	'month';
	$lang['hospital group list']	=	'hospital group list';
	$lang['add new group']		=	'add new group';
	$lang['refresh']	=	'refresh';
	$lang['group name']		=	'group name';
	$lang['select group name']		=	'select group name';
	$lang['hospital']	=	'hospital';
	$lang['select hospital']	=	'select hospital';
	$lang['status']		=	'status';
	$lang['select action']		=	'select action';
	$lang['active']		=	'active';
	$lang['in-Active']		=	'in-Active';
	$lang['serial']		=	'serial';
	$lang['hospital name']		=	'hospital name';
	$lang['group email']		=	'group email';
	$lang['edit']		=	'edit';
	$lang['delete']		=	'delete';
	$lang['activate']	=	'activate';
	$lang['deactivate']	=	'deactivate';
	$lang['add hospital group']		=	'add hospital group';
	$lang['hospital list']		=	'hospital list';
	$lang['responsible person']		=	'responsible person';
	$lang['select responsible person']		=	'select responsible person';
	$lang['submit']		=	'submit';
	$lang['edit hospital group']	=	'edit hospital group';
	$lang['are you sure you want to delete this?'] = 'are you sure you want to delete this?';
	$lang['confirm']	=	'confirm';
	$lang['close']	=	'close';
	$lang['bill status']	=	'bill status';
	$lang['select bill status']		=	'select bill status';
	$lang['demo']		=	'demo';
	$lang['half monthly']		=	'half monthly';
	$lang['monthly']	=	'monthly';
	$lang['Study & Report Status']		=	'Study & Report Status';
	$lang['select study & report status']		=	'select study & report status';
	$lang['study only']		=	'study only';
	$lang['study & on request update']		=	'study & on request update';
	$lang['report only']	=	'report only';
	$lang['study with report']		=	'study with report';
	$lang['separate study & report']		=	'separate study & report';
	$lang['active selected']	=	'active selected';
	$lang['in-Active selected']		=	'in-Active selected';
	$lang['delete selected']		=	'delete selected';
	$lang['email'] 	=	'email';
	$lang['phone number']	=	'phone no.';
	$lang['address']	=	'address';
	$lang['tax status']		=	'tax status';
	$lang['study - report status']	=	'study - report status';
	$lang['report currency']	=	'report currency';
	$lang['study currency']		=	'study currency';
	$lang['applied']		=	'applied';
	$lang['not applied']	=	'not applied';
	$lang['show']		=	'show';
	$lang['modality list']		=	'modality list';
	$lang['add new modality']	=	'add new modality';
	$lang['modality name']		=	'modality name';
	$lang['add modality']		=	'add modality';
	$lang['select status']		=	'select status';
	$lang['price list']			=	'price list';
	$lang['standard price']		=	'standard price';
	$lang['study price list']	=	'study price list';
	$lang['price']				=	'price';
	$lang['edit price list']	=	'edit price list';
	$lang['show price list']	=	'show price list';
	$lang['clear']				=	'clear';
	$lang['general configuration']	=	'general configuration';
	$lang['tax rate']	=	'tax rate';
	$lang['invoice date']	=	'invoice date';
	$lang['price status']	=	'price status';
	$lang['in']		=	'in';
	$lang['fix amount']	=	'fix amount';
	$lang['price increment']	=	'price increment';
	$lang['column add in invoice report detail page']	=	'column add in invoice report detail page';
	$lang['column add in invoice study detail page']	=	'column add in invoice study detail page';
	$lang['Study id']	=	'Study id';
	$lang['description']	=	'description';
	$lang['scan time']	=	'scan time';
	$lang['study source name']	=	'study source name';
	$lang['report list']	=	'report list';
	$lang['add new report']	=	'add new report';
	$lang['report name']	=	'report name';
	$lang['name']	=	'name';
	$lang['doctor commission']	=	'doctor commission';
	$lang['points']		=	'points';
	$lang['multiplying factor']		=	'multiplying factor';
	$lang['extra charges']	=	'extra charges';
	$lang['add report']		=	'add report';
	$lang['in peso']	=	'in peso';
	$lang['in dollar']	=	'in dollar';
	$lang['show report']	=	'show report';
	$lang['edit report']	=	'edit report';
	$lang['select modality']	=	'select modality';
	$lang['existing radio logist list']		=	'existing radio logist list';
	$lang['add new radiologist']	=	'add new radiologist';
	$lang['type']	=	'type';
	$lang['name alias']	=	'name alias';
	$lang['email id']	=	'email id';
	$lang['doctor type']	=	'doctor type';
	$lang['pay in']	=	'pay in';
	$lang['pay commission']	=	'pay commission';
	$lang['pay point']	=	'pay point';
	$lang['fixed monthly charge']	=	'fixed monthly charge';
	$lang['add radiologist']	=	'add radiologist';
	$lang['select type']	=	'select type';
	$lang['doctor']	=	'doctor';
	$lang['types of doctor']	=	'types of doctor';
	$lang['select type of doctor']	=	'select type of doctor';
	$lang['local']	=	'local';
	$lang['international']	=	'international';
	$lang['select pay in']	=	'select pay in';
	$lang['peso']	=	'peso';
	$lang['dollar']	=	'dollar';
	$lang['yes']	=	'yes';
	$lang['no']		=	'no';
	$lang['edit radiologist']	=	'edit radiologist';
	$lang['show radiologist']	=	'show radiologist';
	$lang['report modality correction list']	=	'report modality correction list';
	$lang['add new report modality correction']	=	'add new report modality correction';
	$lang['select report']	=	'select report';
	$lang['correction name']	=	'correction name';
	$lang['add report modality correction']	=	'add report modality correction';
	$lang['general']	=	'general';
	$lang['other']		=	'other';
	$lang['next']		=	'next';
	$lang['show report modality correction']	=	'show report modality correction';
	$lang['edit report modality correction']	=	'edit report modality correction';
	$lang['email template list']	=	'email template list';
	$lang['add new email template']	=	'add new email template';
	$lang['template type']	=	'template type';
	$lang['subject']	=	'subject';
	$lang['description']	=	'description';
	$lang['add email template']	=	'add email template';
	$lang['dynamic dropdown']	=	'dynamic dropdown';
	$lang['select template type']	=	'select template type';
	$lang['invoice']	=	'invoice';
	$lang['edit email template']	=	'edit email template';
	$lang['existing user list']		=	'existing user list';
	$lang['username']	=	'username';
	$lang['user type']	=	'user type';
	$lang['created']	=	'created';
	$lang['change password']	=	'change password';
	$lang['add user']	=	'add user';
	$lang['first name']	=	'first name';
	$lang['last name']	=	'last name';
	$lang['select user type']	=	'select user type';
	$lang['city']	=	'city';
	$lang['zipcode']	=	'zipcode';
	$lang['street']		=	'street';
	$lang['house']		=	'house';
	$lang['address 1']	=	'address 1';
	$lang['address 2']	=	'address 2';
	$lang['language']	=	'language';
	$lang['english']	=	'english';
	$lang['spanish']	=	'spanish';
	$lang['basic details']	=	'basic details';
	$lang['password']	=	'password';
	$lang['select language']	=	'select language';
	$lang['show user']	=	'show user';
	$lang['edit user']	=	'edit user';
	$lang['old password']	=	'old password';
	$lang['new password']	=	'new password';
	$lang['confirm password']	=	'confirm password';
	$lang['import studies']		=	'import studies';
	$lang['search']		=	'search';
	$lang['acquire start time']	=	'acquire start time';
	$lang['these fields are mandatory in import study (study id, patient, MRN, description, scan time, study source name, acquire start time)']		=	'these fields are mandatory in import study (study id, patient, MRN, description, scan time, study source name, acquire start time)';

	$lang['import errors']		=	'import errors';
	$lang['error name']			=	'error name';
	$lang['error file']			=	'error file';
	$lang['error type']		=	'error type';
	$lang['final reports details - ready for invoice']	=	'final reports details - ready for invoice';

	$lang['all']	=	'all';
	$lang['uninvoiced']	=	'uninvoiced';
	$lang['diff. report']	=	'diff. report';
	$lang['same report name']	=	'same report name';
	$lang['diff report name']	=	'diff report name';
	$lang['reported hospitals']	=	'reported hospitals';
	$lang['export']	=	'export';
	$lang['remove from invoice']	=	'remove from invoice';
	$lang['remove selected record']	=	'remove selected record';
	$lang['id']	=	'id';
	$lang['study arrival time']	=	'study arrival time';
	$lang['corrected report']	=	'corrected report';
	$lang['final study details - ready for invoice']	=	'final study details - ready for invoice';
	$lang['acquire complete time']	=	'acquire complete time';
	$lang['final reports details - ready for invoice']	=	'final reports details - ready for invoice';
	$lang['new hospital and radiologist']	=	'new hospital and radiologist';
	$lang['report date']	=	'report date';
	$lang['report time']	=	'report time';
	$lang['rt time']	=	'rt time';
	$lang['count of patient']	=	'count of patient';
	$lang['time']	=	'time';
	$lang['generate report invoices']	=	'generate report invoices';
	$lang['study invoice']	=	'study invoice';
	$lang['study and report status']	=	'study and report status';
	$lang['select create invoice']	=	'select create invoice';
	$lang['create real invoice']	=	'create real invoice';
	$lang['report']	=	'report';
	$lang['study']	=	'study';
	$lang['reports charge']	=	'reports charge';
	$lang['study charge']	=	'study charge';
	$lang['generate report invoices']	=	'generate report invoices';
	$lang['doctor payment']	=	'doctor payment';
	$lang['select doctor commission']	=	'select doctor commission';
	$lang['create doctor commission']	=	'create doctor commission';
	$lang['Number of Reports']	=	'No. of Reports';
	$lang['total points']	=	'total points';
	$lang['total commission']	=	'total commission';
	$lang['total extra charge']	=	'total extra charge';
	$lang['total pay']	=	'total pay';
	$lang['invoice email logs']	=	'invoice email logs';
	$lang['invoice month']	=	'invoice month';
	$lang['user details']	=	'user details';
	$lang['settings']	=	'settings';
	$lang['hospital code']	=	'hospital code';
	$lang['hospital phone']	=	'hospital phone';
	$lang['hospital address']	=	'hospital address';
	$lang['user']	=	'user';
	$lang['role']	=	'role';
	$lang['add new']	=	'add new';
	$lang['select user']	=	'select user';
	$lang['select role']	=	'select role';
	$lang['hospital bill status']	=	'hospital bill status';
	$lang['report removal condition']	=	'report removal condition';
	$lang['router status']	=	'router status';
	$lang['router charged']	=	'router charged';
	$lang['number of router']	=	'no. of router';
	$lang['price increment']	=	'price increment';
	$lang['confirm date']	=	'confirm date';
	$lang['responsible sanitario']	=	'responsible sanitario';
	$lang['responsible charges']	=	'responsible charges';
	$lang['condition']	=	'condition';
	$lang['discount']	=	'discount';
	$lang['minute']		=	'minute';
	$lang['select tax status']	=	'select tax status';
	$lang['select study currency']	=	'select study currency';
	$lang['select condition']	=	'select condition';
	$lang['my account']	=	'my account';
	$lang['account details']	=	'account details';
	$lang['update my profile']	=	'update my profile';
	$lang['report price list']	=	'report price list';
	$lang['select report currency']	=	'select report currency';
	$lang['currency']	=	'currency';
	$lang['edit study price']	=	'edit study price';
	$lang['price (in peso)']	=	'price (in peso)';
	$lang['price (in dollar)']	=	'price (in dollar)';

?>