<?php
class Layouts {
//CI  instance 
	private $CI;
//Layout title
	private $layout_title = null;
	private $page_title = null;
//Layout title
	private $layout_description = null;
//hold includes like css and js
	private $cssincludes = array();
	private $jsincludes = array();

	public function __construct() {
        $this->CI =& get_instance();
	}
	
	public function set_title($title = '') {
		if(!empty($title)) {
			$this->layout_title = $title;
		}
		else {
			$this->layout_title = SITE_NAME;
		}
	}
	public function set_description($desc = '') {
		if(!empty($desc)) {
			$this->layout_description = $desc;
		}
		else {
			$this->layout_title = DEFAULT_SITE_DESC;
		}
	}

	public function set_page_title($str) {
		$this->page_title = ucwords($str);
	}

	public function set_breadcrumb_array($b_array) {
		$this->breadcrumb_array = $b_array;
	}

	public function print_breadcrumb() {
		if(isset($this->breadcrumb_array) && !empty($this->breadcrumb_array) && is_array($this->breadcrumb_array)) {
			echo '<ul class="breadcrumb custom-breadcrumb">';
			$count = count($this->breadcrumb_array);
			foreach ($this->breadcrumb_array as $key => $value) {
			if (--$count <= 0) {
			 echo "<li class='active'>".$key."</li>";
			   break;
			}
			echo '<li><a href='.$value.'>'.$key.'</a></li>';
			}
			echo "</ul>";
		}
	}
	
	public function print_msg_div($this_session) {
		if($this_session->flashdata('msg')){ 
	        echo ' <div class="alert alert-success row error_msgs"><b>
	        <a href="#" class="close" data-dismiss="alert">&times;</a>';
		    echo $this_session->flashdata('msg');
		    echo '</b></div>';
		}
		if($this_session->flashdata('errormsg')){ 
	        echo ' <div class="alert alert-danger row error_msgs"><b>
	        <a href="#" class="close" data-dismiss="alert">&times;</a>';
		    echo $this_session->flashdata('errormsg'); 
		    echo '</b></div>';
		}

		if(isset($err_msg)){
	        echo ' <div class="alert alert-danger row error_msgs"><b>
	        <a href="#" class="close" data-dismiss="alert">&times;</a>';
		    echo $err_msg; 
		    echo '</b></div>';
		}

		if (isset($msg)) {
		    echo ' <div class="alert alert-success row"><b>
		    <a href="#" class="close" data-dismiss="alert">&times;</a>';
		    echo $msg;
		    echo '</b></div>';
		}

		/* if (validation_errors()){
		    echo '<div class="alert alert-danger row error_msgs"><b>
			<a href="#" class="close" data-dismiss="alert">&times;</a>';
		    echo validation_errors();
		    echo '</b></div>';                                 
		}*/
	}

	public function add_css($path, $prepend_base_url = true)
	{
		if($prepend_base_url) {
			$this->CI->load->helper('url'); //loads helper 
			$this->cssincludes[] =  base_url(). $path;
		}
		else {
			$this->cssincludes[] = $path;
		}
		return $this;
	}

	public function add_js($path, $prepend_base_url = true)
	{
		if($prepend_base_url) {
			$this->CI->load->helper('url'); //loads helper 
			$this->jsincludes[] =  base_url(). $path;
		}
		else {
			$this->jsincludes[] = $path;
		}
		return $this;
	}
	public function print_css() {
		$final_css_includes = "";
		foreach ($this->cssincludes as $include) {
			if(preg_match('/js$/', $include)) {
				$final_css_includes .= '<script src="'.$include.'"></script>';
			}
			else if(preg_match('/css$/', $include)) {
				$final_css_includes .= '<link href="'.$include.'" rel="stylesheet"/>';
			}
			else {
				$final_css_includes .= '<link href="'.$include.'" rel="stylesheet"/>';
			}
		}
			return $final_css_includes;
	}
	public function print_js() {
		$final_js_includes = "";
		foreach ($this->jsincludes as $include) {
			if(preg_match('/js$/', $include)) {
				$final_js_includes .= '<script src="'.$include.'"></script>';
			}
			else if(preg_match('/css$/', $include)) {
				$final_js_includes .= '<link href="'.$include.'" rel="stylesheet"/>';
			}
			else {
				$final_js_includes .= '<link href="'.$include.'" rel="stylesheet"/>';
			}
		}
			return $final_js_includes;
	}

	public function view($view_name, $layouts = array(), $params = array(), $default = TRUE,$layouts_status = TRUE) {
		if($default) {

			$header_params['layout_title'] = $this->layout_title;
			$header_params['page_title'] = $this->page_title;
			$header_params['layout_description'] = $this->layout_description;
			
			if(isset($this->breadcrumb_array)) {
				$header_params['breadcrumb_array'] = $this->breadcrumb_array;				
			}

			$this->CI->load->view('layouts/header',$header_params);

			//$this->CI->load->view('layouts/navbar',$header_params,$params);


			//load navbar and sidebars within the view.
			if($layouts_status == TRUE && is_array($layouts) && count($layouts) >= 1) {
				foreach ($layouts as $layout_key => $layout) {
					$this->CI->load->view($layout, $params);
				}
			}
			$this->CI->load->view($view_name, $params);
			
			$this->CI->load->view('layouts/footer');

		}
		else {
		//render viewer
		$this->CI->load->view($view_name, $params);
		}
	}
}
?>